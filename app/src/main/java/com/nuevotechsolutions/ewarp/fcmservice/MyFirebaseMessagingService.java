package com.nuevotechsolutions.ewarp.fcmservice;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.room.Room;

import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.activities.ChecklistHomeActivity;
import com.nuevotechsolutions.ewarp.activities.LoginActivity;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private static final String CHANNEL_ID = "MyFirebaseMessagingService";
    private final int notifId = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
    String title;
    String message;
    CheckListDatabase db;
    List<UserDetails> userDetails = new ArrayList<>();
    Intent intent;
    private RemoteViews mRemoteViews;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("newToken", s);
        getSharedPreferences("_", MODE_PRIVATE).edit().putString("fb", s).apply();

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        super.onMessageReceived(remoteMessage);
      Log.e("data", String.valueOf(remoteMessage.getData()));
        sendNotification(remoteMessage, notifId);
    }

    public static String getToken(Context context) {
        return context.getSharedPreferences("_", MODE_PRIVATE).getString("fb", "empty");
    }

    private void sendNotification(RemoteMessage remoteMessage, int notficationId) {
        db = Room.databaseBuilder(MyFirebaseMessagingService.this, CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = db.productDao().getUserDetail();
        if (userDetails != null && !userDetails.isEmpty()) {
            intent = new Intent(this, ChecklistHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }else{
            intent = new Intent(this, LoginActivity.class);
        }
            PendingIntent pendingIntent = PendingIntent.getActivity(this, notficationId /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            String channelId = getString(R.string.default_notification_channel_id);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        File imageFile = new File(remoteMessage.getData().get("image"));
//       Bitmap notificationImg = StringToBitMap(imageUri);
        Log.e("test",""+imageFile);
        String filePath = imageFile.toString();
        URL url = null;
        Bitmap bigPicture = null;
        try {
            url = new URL(filePath);
            bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            Log.e("testimage", String.valueOf(bigPicture));

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this, channelId)
                            .setSmallIcon(getSmallNoifIcon())
                            .setLargeIcon(bigPicture)
                            .setColor(ContextCompat.getColor(this, R.color.colorAccent))
                            .setContentTitle(remoteMessage.getData().get("title"))
                            .setContentText(remoteMessage.getData().get("body"))
                            .setStyle(new NotificationCompat.BigPictureStyle()
                                    .bigPicture(bigPicture))
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setContentIntent(pendingIntent);

            NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
            bigText.bigText(remoteMessage.getData().get("body"));
            bigText.setBigContentTitle(remoteMessage.getData().get("title"));
//        bigText.setSummaryText(remoteMessage.getData().get("body"));
            notificationBuilder.setStyle(bigText);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addNextIntent(intent);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId,
                        remoteMessage.getData().get("title"),
                        NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(channel);
            }

            notificationManager.notify(notficationId /* ID of notification */, notificationBuilder.build());

        }

        private int getSmallNoifIcon () {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                return R.mipmap.ic_app_icon_foreground_new;
            } else {
                return R.mipmap.ic_app_icon_foreground_new;
            }
        }


    }


