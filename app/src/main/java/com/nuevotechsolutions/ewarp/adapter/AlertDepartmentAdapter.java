package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.DepartmentList;
import com.nuevotechsolutions.ewarp.model.NoticeDataList;

import java.util.List;

public class AlertDepartmentAdapter extends RecyclerView.Adapter<AlertDepartmentAdapter.AlertDeparmentViewHolder> {

    private Context context;
    private List<DepartmentList> departmentLists;
    public int lastSelectedPosition = -1;



    public AlertDepartmentAdapter(Context context, List<DepartmentList> departmentLists) {
        this.context = context;
        this.departmentLists = departmentLists;

    }

    @NonNull
    @Override
    public AlertDeparmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view=inflater.inflate(R.layout.alert_department_item,parent,false);
        return new AlertDeparmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlertDeparmentViewHolder holder, int position) {
        DepartmentList departmentList=departmentLists.get(position);
        holder.radioButton.setChecked(position == lastSelectedPosition);

        if (departmentList.getDepartment_nm()!=null){
            holder.radioButton.setText(departmentList.getDepartment_nm());
        }

    }

    @Override
    public int getItemCount() {
        return departmentLists.size();
    }

    public class AlertDeparmentViewHolder extends RecyclerView.ViewHolder {

        RadioButton radioButton;

        public AlertDeparmentViewHolder(@NonNull View itemView) {
            super(itemView);
            radioButton=itemView.findViewById(R.id.alertDepartmentName);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                     notifyDataSetChanged();

                }
            };
            radioButton.setOnClickListener(clickListener);

        }

    }
    public String bind() {
        if (lastSelectedPosition==-1)
            return null;
        else
            return departmentLists.get(lastSelectedPosition).getDepartment_nm();
    }
}
