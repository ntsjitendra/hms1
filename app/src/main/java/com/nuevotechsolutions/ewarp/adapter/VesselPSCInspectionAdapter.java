package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselMasterWorkId;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import java.util.List;


public class VesselPSCInspectionAdapter extends RecyclerView.Adapter<VesselPSCInspectionAdapter.VesselPSCInspectionViewHolder> {

    Context context;
    List<VesselMasterWorkId> list;
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(VesselMasterWorkId item,int position);
    }

    public VesselPSCInspectionAdapter(Context context, List<VesselMasterWorkId> list,OnItemClickListener listener){
        this.context=context;
        this.list=list;
        this.listener=listener;
    }


    @NonNull
    @Override
    public VesselPSCInspectionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        View view=inflater.inflate(R.layout.vessel_master_checklist_item,viewGroup,false);
        return new VesselPSCInspectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VesselPSCInspectionViewHolder vesselPSCInspectionViewHolder, int i) {
        vesselPSCInspectionViewHolder.bind(list.get(i), listener);
        int j=i;
        VesselMasterWorkId stringList=list.get(i);
        vesselPSCInspectionViewHolder.VesselWorkIdLabelTextView.setTextColor(ContextCompat.getColor(context, R.color.QRCodeBlackColor));
//        vesselPSCInspectionViewHolder.VesselChecklistNameTextView.setText("S.No: "+ ++j);
        vesselPSCInspectionViewHolder.VesselWorkIdLabelTextView.setText(++j+".Ref.No: "+ stringList.getCreated_by() +"/"+ stringList
                .getCrnt_date().substring(0, 4) + "/" + stringList.getWrk_id());
//        vesselPSCInspectionViewHolder.VesselCreatedByTextView.setText("Created By: "+ stringList.getCreated_by().toLowerCase());
        vesselPSCInspectionViewHolder.VesselCreatedByTextView.setText("Created By: "+ stringList.getCreated_by());
        vesselPSCInspectionViewHolder.VesselCreationDateTextView.setText("Created On: " +
                new UtilClassFuntions().dateConversion(stringList.getCrnt_date()));
        if (stringList.getLast_modify_dt()!=null){
        vesselPSCInspectionViewHolder.VesselLastModifiedDateTextView.setText("Last Modified: " +
                new UtilClassFuntions().dateConversion(stringList.getLast_modify_dt()));
        }
//        vesselPSCInspectionViewHolder.VesselCommentTextView.setText("Comment: "+ stringList.getComment().toLowerCase());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VesselPSCInspectionViewHolder extends RecyclerView.ViewHolder{

        TextView VesselChecklistNameTextView,VesselWorkIdLabelTextView,VesselCreatedByTextView,
                VesselCreationDateTextView,VesselLastModifiedDateTextView,VesselCommentTextView;

        public VesselPSCInspectionViewHolder(@NonNull View itemView) {
            super(itemView);
            VesselChecklistNameTextView=itemView.findViewById(R.id.VesselChecklistNameTextView);
            VesselWorkIdLabelTextView=itemView.findViewById(R.id.VesselWorkIdLabelTextView);
            VesselCreatedByTextView=itemView.findViewById(R.id.VesselCreatedByTextView);
            VesselCreationDateTextView=itemView.findViewById(R.id.VesselCreationDateTextView);
            VesselLastModifiedDateTextView=itemView.findViewById(R.id.VesselLastModifiedDateTextView);
            VesselCommentTextView=itemView.findViewById(R.id.VesselCommentTextView);
        }

        public void bind(final VesselMasterWorkId list, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(list,getAdapterPosition());
                }
            });
        }

    }

}
