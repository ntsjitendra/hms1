package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.interfaces.AudioRemoveInterface;
import com.nuevotechsolutions.ewarp.model.VoiceRecordData;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Manish on 10/8/2017.
 */

public class RecordingListAdapter extends RecyclerView.Adapter<RecordingListAdapter.ViewHolder> {

    private ArrayList<VoiceRecordData> recordingArrayList;
    private Context context;
    private MediaPlayer mPlayer;
    private boolean isPlaying = false;
    private int last_index = -1;
    AudioRemoveInterface audioRemoveInterface;
        ArrayList<String> arrayList;
    private int pos;


    public RecordingListAdapter(Context context, ArrayList<VoiceRecordData> recordingArrayList, ArrayList<String> addressList, AudioRemoveInterface audioRemoveInterface1) {
        this.context = context;
        this.recordingArrayList = recordingArrayList;
        this.audioRemoveInterface = audioRemoveInterface1;
        this.arrayList = addressList;
        Log.e("recordsize", String.valueOf(recordingArrayList.size()));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_voice_record_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        pos = position;
        setUpData(holder, pos);
    }

    @Override
    public int getItemCount() {
        return recordingArrayList.size();
    }


    private void setUpData(ViewHolder holder, int position) {

        VoiceRecordData recording = recordingArrayList.get(position);
        holder.textViewName.setText(recording.getFileName());
        if (recording.isDownloaded() == false) {
            new DownloadFileFromURL().execute();
//            try {
//                URL u = new URL(arrayList.get(position));
//                InputStream is = u.openStream();
//
//                DataInputStream dis = new DataInputStream(is);
//
//                byte[] buffer = new byte[1024];
//                int length;
//
//                FileOutputStream fos = new FileOutputStream(new File(context.getExternalFilesDir(Environment.DIRECTORY_MUSIC) + "/EWARPRecoder/" + recording.getFileName()));
//                while ((length = dis.read(buffer)) > 0) {
//                    fos.write(buffer, 0, length);
//                }
//
//            } catch (MalformedURLException mue) {
//                Log.e("SYNC getUpdate", "malformed url error", mue);
//            } catch (IOException ioe) {
//                Log.e("SYNC getUpdate", "io error", ioe);
//            } catch (SecurityException se) {
//                Log.e("SYNC getUpdate", "security error", se);
//            }

        }

        if (recording.isPlaying()) {
            holder.imageViewPlay.setImageResource(R.drawable.ic_pause);
            TransitionManager.beginDelayedTransition((ViewGroup) holder.itemView);
            holder.seekBar.setVisibility(View.VISIBLE);
            holder.seekUpdation(holder);
        } else {
            if (recording.isDownloaded() == false) {
                holder.imageViewPlay.setImageResource(R.drawable.ic_download);
            } else {
                holder.imageViewPlay.setImageResource(R.drawable.ic_play);
            }
            TransitionManager.beginDelayedTransition((ViewGroup) holder.itemView);
            holder.seekBar.setVisibility(View.GONE);
        }

        holder.manageSeekBar(holder);

    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... f_url) {

            VoiceRecordData recording = recordingArrayList.get(pos);

            try {
                URL u = new URL(arrayList.get(pos));

                InputStream is = u.openStream();

                DataInputStream dis = new DataInputStream(is);

                byte[] buffer = new byte[1024];
                int length;

                FileOutputStream fos = new FileOutputStream(new File(context.getExternalFilesDir(Environment.DIRECTORY_MUSIC) + "/EWARPRecoder/" + recording.getFileName()));
                while ((length = dis.read(buffer)) > 0) {
                    fos.write(buffer, 0, length);
                }

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded

        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewPlay;
        SeekBar seekBar;
        TextView textViewName;
        ViewHolder holder;
        private String recordingUri;
        private ImageView removeAudio;
        private int lastProgress = 0;
        private Handler mHandler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                seekUpdation(holder);
            }
        };

        public ViewHolder(View itemView) {
            super(itemView);

            imageViewPlay = itemView.findViewById(R.id.imageViewPlay);
            seekBar = itemView.findViewById(R.id.seekBar);
            textViewName = itemView.findViewById(R.id.textViewRecordingname);
            removeAudio = itemView.findViewById(R.id.removeAudio);

            imageViewPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    VoiceRecordData recording = recordingArrayList.get(position);

                    recordingUri = recording.getUri();
                    Log.e("abc", recording.getUri());
                    if (isPlaying) {
                        stopPlaying();
                        if (position == last_index) {
                            recording.setPlaying(false);
                            stopPlaying();
                            notifyItemChanged(position);
                        } else {
                            markAllPaused();
                            recording.setPlaying(true);
                            notifyItemChanged(position);
                            startPlaying(recording, position);
                            last_index = position;
                        }

                    } else {
                        startPlaying(recording, position);
                        recording.setPlaying(true);
                        seekBar.setMax(mPlayer.getDuration());
                        Log.d("isPlayin", "False");
                        notifyItemChanged(position);
                        last_index = position;
                    }

                }

            });
            removeAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isPlaying){
                        stopPlaying();
                    }
                    int position = getAdapterPosition();
                    audioRemoveInterface.onRemoveAudioClickListener(position);
                }
            });
        }

        public void manageSeekBar(ViewHolder holder) {
            holder.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (mPlayer != null && fromUser) {
                        mPlayer.seekTo(progress);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }

        private void markAllPaused() {
            for (int i = 0; i < recordingArrayList.size(); i++) {
                recordingArrayList.get(i).setPlaying(false);
                recordingArrayList.set(i, recordingArrayList.get(i));
            }
            notifyDataSetChanged();
        }

        private void seekUpdation(ViewHolder holder) {
            this.holder = holder;
            if (mPlayer != null) {
                int mCurrentPosition = mPlayer.getCurrentPosition();
                holder.seekBar.setMax(mPlayer.getDuration());
                holder.seekBar.setProgress(mCurrentPosition);
                lastProgress = mCurrentPosition;
            }
            mHandler.postDelayed(runnable, 100);
        }

        private void stopPlaying() {
            try {
                mPlayer.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mPlayer = null;
            isPlaying = false;
        }

        private void startPlaying(final VoiceRecordData audio, final int position) {
            mPlayer = new MediaPlayer();
            try {
                mPlayer.setDataSource(recordingUri);
                mPlayer.prepare();
                mPlayer.start();
            } catch (IOException e) {
                Log.e("LOG_TAG", "prepare() failed");
            }
            //showing the pause button
            seekBar.setMax(mPlayer.getDuration());
            isPlaying = true;

            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    audio.setPlaying(false);
                    notifyItemChanged(position);
                }
            });
        }

    }
}