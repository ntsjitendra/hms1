package com.nuevotechsolutions.ewarp.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.controller.ImageData;
import com.nuevotechsolutions.ewarp.controller.ImageEventHandler;
import com.nuevotechsolutions.ewarp.controller.ImageHandler;
import com.nuevotechsolutions.ewarp.interfaces.ImageInterface;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;


public class ImageListDataAdapter extends RecyclerView.Adapter<ImageListDataAdapter.SingleItemRowHolder> {

    private Context mContext;
    private String sectionName;
    List<String> itemsList;
    Bitmap bitmap, bitmapUndo;
    int position;
    List<String> locationList;
    List<String> timeStampList;
    List<String> photoTextValueList;
    ImageButton submitButton;
    private String PhotoTextValue;
    private ImageInterface imageInterface;
    private static final int MAX_WIDTH = 1000;
    private static final int MAX_HEIGHT = 130;

    int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));

    //    public ImageListDataAdapter(Context context,List<String> itemsList, String sectionName,String imagedata) {
//        this.itemsList = itemsList;
//        this.mContext = context;
//        this.sectionName = sectionName;
//        this.imagedata=imagedata;
//
//
//    }
    public ImageListDataAdapter(Context context, List<String> itemsList, String sectionName, int position, List<String> locationList, List<String> timeStampList, List<String> photoTextValueList, ImageButton submitButton, ImageInterface imageInterface) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.sectionName = sectionName;
        this.position = position;
        this.locationList = locationList;
        this.timeStampList = timeStampList;
        this.photoTextValueList = photoTextValueList;
        this.submitButton = submitButton;
        this.imageInterface = imageInterface;

    }

    public ImageListDataAdapter(Context context, List<String> itemsList, String sectionName, int position, List<String> locationList, List<String> timeStampList, List<String> photoTextValueList, ImageButton submitButton) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.sectionName = sectionName;
        this.position = position;
        this.locationList = locationList;
        this.timeStampList = timeStampList;
        this.photoTextValueList = photoTextValueList;
        this.submitButton = submitButton;

    }

    public ImageListDataAdapter(Context context, List<String> itemsList, String sectionName, int position, List<String> locationList, List<String> timeStampList, ImageButton submitButton) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.sectionName = sectionName;
        this.position = position;
        this.locationList = locationList;
        this.timeStampList = timeStampList;
        this.submitButton = submitButton;

    }


    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_single_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        String singleItem = itemsList.get(i);
        Log.e("a", singleItem);
        String singleLocation = "";
        String singleTimeStamp = "";
        String singlePhotoTextValue = "";
        if (locationList.size() > 0) {
            singleLocation = locationList.get(i);
            singleTimeStamp = timeStampList.get(i);
            singlePhotoTextValue = photoTextValueList.get(i);
            Log.e("singlePhotoTextValue", singlePhotoTextValue);

            if (singleLocation == null) {
                singleLocation = "Not available";
            }


        }

        if (singleItem.contains("http")) {
            /*Glide.with(mContext)
                    .load(singleItem)
                    .centerCrop()
                    .fitCenter()
                    .error(R.drawable.attach)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(holder.itemImage);*/

            Picasso.with(mContext)
                    .load(singleItem)
                    .centerCrop()
                    .fit()
                    .error(R.drawable.attach)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.itemImage);
        } else {
            /*Glide.with(mContext)
                    .load(singleItem)
                    .centerCrop()
                    .fitCenter()
                    .error(R.drawable.attach)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(holder.itemImage);*/

            Picasso.with(mContext)
                    .load(new File(singleItem))
                    .centerCrop()
                    .fit()
                    .error(R.drawable.attach)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.itemImage);
        }


        holder.tvTitle.setText(singleItem);
        holder.tvTitleSectionName.setText(singleTimeStamp + "; " + singleLocation);
        holder.tvPhotoTextValue.setText(singlePhotoTextValue);
        holder.tvPhotoTextValuePosition.setText(String.valueOf(i));

        /*Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/

        holder.cardImageLI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null == holder.itemImage.getDrawable()) {
                    Log.e("imageView Is null", "Null");
                } else {
                    imageViewDialog(mContext, i, holder.itemImage, holder.tvPhotoTextValue, holder);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }


    private void imageViewDialog(Context context, int i, ImageView imageView, TextView tvPhotoTextValue, SingleItemRowHolder holder) {
        if (null == imageView.getDrawable()) {
            Log.e("imageView", "imageView is null");
        } else {
            bitmapUndo = ((BitmapDrawable) imageView.getDrawable().getCurrent()).getBitmap();
//                BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
//                bitmapUndo = drawable.getBitmap();
        }

        bitmap = new ImageHandler().imgView(imageView, bitmap);
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialog.setContentView(R.layout.image_view_layout);
        final ImageView imageView57 = dialog.findViewById(R.id.imageView57);
        ImageView closeButton = dialog.findViewById(R.id.closeButton);
        ImageView deleteButton = dialog.findViewById(R.id.deleteButton);
        ImageView editButton = dialog.findViewById(R.id.editButton);
        ImageView saveButton = dialog.findViewById(R.id.saveButton);
        ImageView undoButton = dialog.findViewById(R.id.undoButton);
        RelativeLayout edit = dialog.findViewById(R.id.edit);
        RelativeLayout save = dialog.findViewById(R.id.save);
        RelativeLayout undo = dialog.findViewById(R.id.undo);
        RelativeLayout editText = dialog.findViewById(R.id.editText);
        ImageView editTextButton = dialog.findViewById(R.id.editTextButton);
        EditText photoText = dialog.findViewById(R.id.photoText);

        imageView57.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 750, 800, false));

        if (tvPhotoTextValue != null) {
            String text = tvPhotoTextValue.getText().toString();
            if (!text.equals("")) {
                photoText.setText(text);
                photoText.setVisibility(View.VISIBLE);
                if (submitButton.isEnabled()) {
                    photoText.setEnabled(true);
                } else {
                    photoText.setEnabled(false);
                }
            }
        }

        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(imageView57);
        pAttacher.update();
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (submitButton.isEnabled()) {
                    edit.setBackgroundColor(context.getResources().getColor(R.color.colorPrimarySelect));
                    imageView57.setOnTouchListener(new ImageEventHandler(imageView57, imageView));
                    undoButton.setEnabled(true);
                    saveButton.setEnabled(true);
                    deleteButton.setEnabled(true);
                }
            }
        });
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (submitButton.isEnabled()) {
                    if (tvPhotoTextValue != null) {
                        String text = tvPhotoTextValue.getText().toString();
                        if (!text.equals("")) {
                            photoText.setText(text);
                        }
                    }
                    photoText.setVisibility(View.VISIBLE);
                    undoButton.setEnabled(false);
                    saveButton.setEnabled(false);
                    edit.setEnabled(false);

                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (submitButton.isEnabled()) {
                    String[] temp;
                    String in1 = (String) holder.tvTitle.getText();
                    temp = in1.split("/");
                    in1 = temp[temp.length - 1];
                    if (!photoText.equals("")) {
                        PhotoTextValue = photoText.getText().toString();
                        Log.e("PhotoTextValue", PhotoTextValue);
                        Log.e("PhotoName", in1);
                        Log.e("PhotoTextPosition", String.valueOf(position));
                        tvPhotoTextValue.setText(PhotoTextValue);
                        int k = Integer.parseInt(holder.tvPhotoTextValuePosition.getText().toString());
                        imageInterface.onPhotoTextValue(position, PhotoTextValue, in1, context, k);
                    }

                    new ImageData(imageView57, in1, imageView).saveFile();
                    saveButton.setEnabled(false);
                    undoButton.setEnabled(false);
                    editText.setEnabled(false);
                    BitmapDrawable drawable = (BitmapDrawable) imageView57.getDrawable();
                    bitmapUndo = drawable.getBitmap();
                    imageView.setImageBitmap(bitmapUndo);
                    dialog.dismiss();

                }
            }
        });

        undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (submitButton.isEnabled()) {
                    //imageView57.setImageBitmap(bitmapUndo);
                    holder.itemImage.setImageBitmap(bitmapUndo);
                    imageView57.setImageBitmap(Bitmap.createScaledBitmap(bitmapUndo, 750, 800, false));
//                        saveButton.setEnabled(false);
//                        undoButton.setEnabled(false);
                }
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (submitButton.isEnabled()) {
                    Toast.makeText(context, "" + i, Toast.LENGTH_SHORT).show();
                        itemsList.remove(i);
                        ImageListDataAdapter.this.notifyDataSetChanged();
//                            imageView57.setImageBitmap(null);
//                            itemImage.setImageBitmap(null);
                    dialog.dismiss();
                } else {
                    Toast.makeText(context, "This point is locked. you can't delete this photo", Toast.LENGTH_LONG).show();
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle, tvTitleSectionName, tvPhotoTextValue, tvPhotoTextValuePosition;
        protected ImageView itemImage;
        protected CardView cardImageLI;

        public SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.tvTitleSectionName = (TextView) view.findViewById(R.id.tvTitleSectionName);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
            this.tvPhotoTextValue = (TextView) view.findViewById(R.id.tvPhotoTextValue);
            this.tvPhotoTextValuePosition = (TextView) view.findViewById(R.id.tvPhotoTextValuePosition);
            this.cardImageLI = view.findViewById(R.id.cardImageLI);

        }

    }

}