package com.nuevotechsolutions.ewarp.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ImageModel;
import com.nuevotechsolutions.ewarp.model.NoticeDataList;

import java.util.ArrayList;
import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;


public class SliderImageAdapter extends PagerAdapter {


    private List<NoticeDataList> noticeList;
    private LayoutInflater inflater;
    private Context context;


    public SliderImageAdapter(Context context, List<NoticeDataList> imageModelArrayList) {
        this.context = context;
        this.noticeList = imageModelArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return noticeList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.item_slider, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);
        NoticeDataList noticeDataList=noticeList.get(position);
        Glide.with(context)
                .load(noticeDataList.getFile_path())
                .into(imageView);

//        imageView.setImageResource(imageModelArrayList.get(position).getImage_drawable());

        view.addView(imageLayout, 0);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.pop_up_window_image);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                // set the custom dialog components - text, image and button
                ImageView imageView1 = dialog.findViewById(R.id.imagevewPopup);
                ImageView popupclose = dialog.findViewById(R.id.closePopup);
                Glide.with(context)
                        .load(noticeDataList.getFile_path())
                        .override(300, 200)
                        .into(imageView1);
//                imageView1.setImageResource(Integer.parseInt(imageModelArrayList.get(position).getFile_path()));
                PhotoViewAttacher pAttacher;
                pAttacher = new PhotoViewAttacher(imageView1);
                pAttacher.update();
                popupclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}

