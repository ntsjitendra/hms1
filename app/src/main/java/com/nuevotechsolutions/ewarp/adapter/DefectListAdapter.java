package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.DefectListModel.DefectListDataList;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import java.util.List;


public class DefectListAdapter extends RecyclerView.Adapter<DefectListAdapter.DefectListViewHolder> {

    Context context;
    List<DefectListDataList> list;
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(DefectListDataList item,int position);
    }

    public DefectListAdapter(Context context, List<DefectListDataList> list, OnItemClickListener listener){
        this.context=context;
        this.list=list;
        this.listener=listener;
    }

    @NonNull
    @Override
    public DefectListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        View view=inflater.inflate(R.layout.defectlist_item,viewGroup,false);
        return new DefectListViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull DefectListViewHolder DefectlistStatusView, int i) {
        DefectlistStatusView.bind(list.get(i), listener);
        int j=i;
        DefectListDataList stringList=list.get(i);
        DefectlistStatusView.DefectlistWorkIdLabelTextView.setTextColor(ContextCompat.getColor(context, R.color.QRCodeBlackColor));
        DefectlistStatusView.DefectlistWorkIdLabelTextView.setText(++j+".Ref.No: "+ stringList.getCreated_by() +"/"+ stringList
                .getAdded_date().substring(0, 4) + "/" + stringList.getWrk_id());
        DefectlistStatusView.DefectlistCreationDateTextView.setText("Created On: "+ stringList.getAdded_date().toLowerCase());
        DefectlistStatusView.DefectlistDepartmentTextView.setText("Department: " +
                new UtilClassFuntions().dateConversion(stringList.getAdded_date()));
//        DefectlistStatusView.DefectlistDeficiencyTextView.setText("Deficiency: " +
//                new UtilClassFuntions().dateConversion(stringList.getModified_dt()));
//        DefectlistStatusView.DefectlistPriorityView.setText("Priority: "+ stringList.getComment().toLowerCase());
//        DefectlistStatusView.aDefectlistTargetDateView.setText("Target Date: "+ stringList.getComment().toLowerCase());
//        DefectlistStatusView.DefectlistStatusView.setText("Status: "+ stringList.getComment().toLowerCase());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DefectListViewHolder extends RecyclerView.ViewHolder {

        TextView DefectlistNameTextView,DefectlistWorkIdLabelTextView,DefectlistCreationDateTextView,
                DefectlistDepartmentTextView,DefectlistDeficiencyTextView,DefectlistPriorityView,
                DefectlistTargetDateView,DefectlistStatusView;


        public DefectListViewHolder(@NonNull View itemView) {
            super(itemView);
            DefectlistNameTextView=itemView.findViewById(R.id.DefectlistNameTextView);
            DefectlistWorkIdLabelTextView=itemView.findViewById(R.id.DefectlistWorkIdLabelTextView);
            DefectlistCreationDateTextView=itemView.findViewById(R.id.DefectlistCreationDateTextView);
            DefectlistDepartmentTextView=itemView.findViewById(R.id.DefectlistDepartmentTextView);
            DefectlistDeficiencyTextView=itemView.findViewById(R.id.DefectlistDeficiencyTextView);
            DefectlistPriorityView=itemView.findViewById(R.id.DefectlistPriorityView);
            DefectlistTargetDateView=itemView.findViewById(R.id.DefectlistTargetDateView);
            DefectlistStatusView=itemView.findViewById(R.id.DefectlistStatusView);

        }

        public void bind(final DefectListDataList list, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(list,getAdapterPosition());
                }
            });
        }

    }

}
