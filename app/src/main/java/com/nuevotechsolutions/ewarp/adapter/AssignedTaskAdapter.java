package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import java.util.List;

public class AssignedTaskAdapter extends RecyclerView.Adapter<AssignedTaskAdapter.AssignedViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(MasterWorkId item,int position);
    }


    Context context;
    List<MasterWorkId> list;
    private OnItemClickListener listener;

    public AssignedTaskAdapter(Context context, List<MasterWorkId> list,OnItemClickListener listener) {

        this.context=context;
        this.list=list;
        this.listener=listener;
    }

    @NonNull
    @Override
    public AssignedViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        View view=inflater.inflate(R.layout.completed_checklist_item,viewGroup,false);
        return new AssignedTaskAdapter.AssignedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AssignedViewHolder assignedViewHolder, int i) {
        assignedViewHolder.bind(list.get(i), listener);
        int j=i;
        MasterWorkId stringList=list.get(i);
        String assignType=stringList.getAssign_type();
        if (assignType!=null){
            assignType="A";
        }else {
            assignType="M";
        }
        assignedViewHolder.unReadStatus.setVisibility(View.GONE);
        assignedViewHolder.checklistNameTextView.setTextColor(ContextCompat.getColor(context, R.color.QRCodeBlackColor));
        assignedViewHolder.checklistNameTextView.setText(++j+". "+ stringList.getList());
        assignedViewHolder.workIdLabelTextView.setText("REF.NO: "+ stringList.getShort_nm()+ "/" + stringList
                .getWrk_id_crtn_dt().substring(0, 4) + "/" + stringList.getWrk_id()+"/"+assignType);
        assignedViewHolder.creationDateTextView.setText("Assigned: " +
                new UtilClassFuntions().dateConversion(stringList.getWrk_id_crtn_dt()));
        assignedViewHolder.cancellationDateTextView.setText("Target Date : " +
                new UtilClassFuntions().dateConversion(stringList.getTarget_complition_date()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class AssignedViewHolder extends RecyclerView.ViewHolder {

      TextView checklistNameTextView,workIdLabelTextView,creationDateTextView,cancellationDateTextView;
      ImageView unReadStatus;

      public AssignedViewHolder(@NonNull View itemView) {
          super(itemView);

          checklistNameTextView=itemView.findViewById(R.id.checklistNameTextView);
          workIdLabelTextView=itemView.findViewById(R.id.workIdLabelTextView);
          creationDateTextView=itemView.findViewById(R.id.creationDateTextView);
          cancellationDateTextView=itemView.findViewById(R.id.completedDateTextView);
          unReadStatus=itemView.findViewById(R.id.unReadStatus);

      }
        public void bind(final MasterWorkId list, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(list,getAdapterPosition());
                }
            });
        }
  }

}
