package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;

import java.util.List;



    public class TeamMemberListAdapter extends RecyclerView.Adapter<TeamMemberListAdapter.TeamListViewHolder> {
        Context context;
        private static List<TeamSelectionList> MemberNamelist1;
        public TeamMemberListAdapter(Context context, List<TeamSelectionList> MemberNamelist1){

            this.context=context;
            this.MemberNamelist1=MemberNamelist1;
            Log.e("member", "test"+String.valueOf(MemberNamelist1.size()));
            Log.e("member1", "test"+String.valueOf(this.MemberNamelist1.size()));


        }
        @NonNull
        @Override
        public TeamListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
            View view=inflater.inflate(R.layout.team_member_list,viewGroup,false);
            return new TeamListViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull TeamListViewHolder teamListViewHolder, int i) {
           TeamSelectionList teamSelection = MemberNamelist1.get(i);
           int j=1+i;
           teamListViewHolder.memberName.setText(j+". "+teamSelection.getFirst_name()+" ("+teamSelection.getRank()+")");
           Log.e("name:",teamSelection.getFirst_name());


        }

        @Override
        public int getItemCount() {
            return MemberNamelist1.size();
        }

        public class TeamListViewHolder extends RecyclerView.ViewHolder {

            TextView memberName;

            public TeamListViewHolder(@NonNull View itemView) {
                super(itemView);
                memberName = itemView.findViewById(R.id.tvTeamMember);
            }


        }
    }