package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.ManualsModel.MainList;

import java.util.List;


public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.MainListtViewHolder> {

    Context context;
    List<MainList> list;
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(MainList item,int position);
    }

    public MainListAdapter(Context context, List<MainList> list, OnItemClickListener listener){
        this.context=context;
        this.list=list;
        this.listener=listener;
    }

    @NonNull
    @Override
    public MainListtViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        View view=inflater.inflate(R.layout.manuals_main_list_item,viewGroup,false);
        return new MainListtViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainListtViewHolder mainListtViewHolder, int i) {

        mainListtViewHolder.bind(list.get(i), listener);
        int j=i;
        MainList stringList=list.get(i);
        mainListtViewHolder.keyMainListTextView.setText(stringList.getMain_list_key());
        mainListtViewHolder.flagMainListTextView.setText(stringList.getFlag());
        mainListtViewHolder.MainListNameTextView.setText(stringList.getMain_list_name());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MainListtViewHolder extends RecyclerView.ViewHolder{

        TextView flagMainListTextView,keyMainListTextView,MainListNameTextView;

        public MainListtViewHolder(@NonNull View itemView) {
            super(itemView);
            flagMainListTextView=itemView.findViewById(R.id.flagMainListTextView);
            keyMainListTextView=itemView.findViewById(R.id.keyMainListTextView);
            MainListNameTextView=itemView.findViewById(R.id.MainListNameTextView);
        }

        public void bind(final MainList list, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(list,getAdapterPosition());
                }
            });
        }

    }

}
