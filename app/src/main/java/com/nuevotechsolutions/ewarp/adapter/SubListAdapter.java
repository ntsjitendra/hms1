package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.ManualsModel.SubList;

import java.util.List;


public class SubListAdapter extends RecyclerView.Adapter<SubListAdapter.SubListtViewHolder> {

    Context context;
    List<SubList> list;
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(SubList item, int position);
    }

    public SubListAdapter(Context context, List<SubList> list, OnItemClickListener listener){
        this.context=context;
        this.list=list;
        this.listener=listener;
    }

    @NonNull
    @Override
    public SubListtViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        View view=inflater.inflate(R.layout.manuals_sub_list_item,viewGroup,false);
        return new SubListtViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubListtViewHolder subListtViewHolder, int i) {

        subListtViewHolder.bind(list.get(i), listener);
        int j=i;
        SubList stringList=list.get(i);
        subListtViewHolder.keyMainListTextView.setText(stringList.getMain_list_key());
        subListtViewHolder.SubListNameTextView.setText(stringList.getSub_list_name());
        subListtViewHolder.SubListUrlTextView.setText(stringList.getUrl());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SubListtViewHolder extends RecyclerView.ViewHolder{

        TextView keyMainListTextView,SubListNameTextView,SubListUrlTextView;

        public SubListtViewHolder(@NonNull View itemView) {
            super(itemView);
            keyMainListTextView=itemView.findViewById(R.id.keySubListTextView);
            SubListNameTextView=itemView.findViewById(R.id.SubListNameTextView);
            SubListUrlTextView=itemView.findViewById(R.id.SubListUrlTextView);
        }

        public void bind(final SubList list, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(list,getAdapterPosition());
                }
            });
        }
    }

}
