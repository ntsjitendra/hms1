package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.utils.DescriptionDataCalculation;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import java.util.List;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


public class CompletedChecklistAdapter extends RecyclerView.Adapter<CompletedChecklistAdapter.CompletedChecklistViewHolder> {

    public MasterWorkId masterWorkIdone;
    Context context;
    List<MasterWorkId> list;
    List<EnginDescriptionData> descriptionData;
    private OnItemClickListener listener;

    public CompletedChecklistAdapter(Context context, List<MasterWorkId> list, List<EnginDescriptionData> descriptionData, OnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.descriptionData = descriptionData;
        this.listener = listener;
    }

    public void updateList(List<MasterWorkId> list, List<EnginDescriptionData> descriptionData){
        this.list = list;
        this.descriptionData = descriptionData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CompletedChecklistViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.completed_checklist_item, viewGroup, false);
        return new CompletedChecklistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompletedChecklistViewHolder completedChecklistViewHolder, int i) {

        completedChecklistViewHolder.bind(list.get(i), listener);
        int j = i;
        MasterWorkId stringList = list.get(i);
        String assignType = stringList.getAssign_type();
        if (assignType != null) {
            assignType = "A";
        } else {
            assignType = "M";
        }

        if (descriptionData != null) {
            Set hs = new DescriptionDataCalculation().descriptionDataFilter(descriptionData, stringList.getUid(), stringList.getWrk_id());
            completedChecklistViewHolder.descriptionTextView.setText(hs.toString().replace("[", " ").replace(",", "").replace("]", ""));
        }
        if (stringList.getSeen() == false) {
            completedChecklistViewHolder.unReadStatus.setVisibility(View.VISIBLE);
        } else {
            completedChecklistViewHolder.unReadStatus.setVisibility(View.GONE);
        }

        if (list.get(i).getWrk_status().equals(2)) {
            completedChecklistViewHolder.checklistNameTextView.setTextColor(ContextCompat.getColor(context, R.color.darkColorRed));
            completedChecklistViewHolder.checklistNameTextView.setText(++j + ". " + stringList.getList());
            completedChecklistViewHolder.workIdLabelTextView.setText(" REF.NO: " + stringList.getShort_nm() + "/" + stringList
                    .getWrk_id_crtn_dt().substring(0, 4) + "/" + stringList.getWrk_id() + "/" + assignType);
            completedChecklistViewHolder.creationDateTextView.setText(" Created: " +
                    new UtilClassFuntions().dateConversion(stringList.getWrk_id_crtn_dt()));
            completedChecklistViewHolder.completedDateTextView.setText(" Cancelled: " +
                    new UtilClassFuntions().dateConversion(stringList.getModified_dt()));

        } else if (list.get(i).getWrk_status().equals(1)) {
            completedChecklistViewHolder.checklistNameTextView.setTextColor(ContextCompat.getColor(context, R.color.dark_green));
            completedChecklistViewHolder.checklistNameTextView.setText(++j + ". " + stringList.getList());
            completedChecklistViewHolder.workIdLabelTextView.setText(" REF.NO: " + stringList.getShort_nm() + "/" + stringList
                    .getWrk_id_crtn_dt().substring(0, 4) + "/" + stringList.getWrk_id() + "/" + assignType);
            completedChecklistViewHolder.creationDateTextView.setText(" Created: " +
                    new UtilClassFuntions().dateConversion(stringList.getWrk_id_crtn_dt()));
            completedChecklistViewHolder.completedDateTextView.setText(" Completed: " +
                    new UtilClassFuntions().dateConversion(stringList.getModified_dt()));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void removeItem(int position) {
        list.remove(position);

        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(MasterWorkId item, int position) {
        list.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    public MasterWorkId getData(int position) {
        MasterWorkId masterWorkId = new MasterWorkId();
        masterWorkId = list.get(position);
        return masterWorkId;
    }

    public interface OnItemClickListener {
        void onItemClick(MasterWorkId item, int position);
    }

    public class CompletedChecklistViewHolder extends RecyclerView.ViewHolder {

        public RelativeLayout viewForeGroud;
        TextView checklistNameTextView, workIdLabelTextView, creationDateTextView,
                completedDateTextView, descriptionTextView;
        ImageView unReadStatus;

        public CompletedChecklistViewHolder(@NonNull View itemView) {
            super(itemView);
            checklistNameTextView = itemView.findViewById(R.id.checklistNameTextView);
            workIdLabelTextView = itemView.findViewById(R.id.workIdLabelTextView);
            creationDateTextView = itemView.findViewById(R.id.creationDateTextView);
            completedDateTextView = itemView.findViewById(R.id.completedDateTextView);
            descriptionTextView = itemView.findViewById(R.id.descriptionTextView);
            unReadStatus = itemView.findViewById(R.id.unReadStatus);
//            viewbackground=itemView.findViewById(R.id.view_background);
            viewForeGroud = itemView.findViewById(R.id.viewForeGroud);

        }

        public void bind(final MasterWorkId list, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(list, getAdapterPosition());
                }
            });
        }

    }

}
