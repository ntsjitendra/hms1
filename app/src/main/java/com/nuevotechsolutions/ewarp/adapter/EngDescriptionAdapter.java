package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.interfaces.TeamSelectionInterFace;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;

import java.util.List;


public class EngDescriptionAdapter extends RecyclerView.Adapter<EngDescriptionAdapter.EngDescriptionViewHolder>{

    private Context context;
    private List<AuxEngLabel> list;
    private List<String> discLabel;
    private List<String> discValue;
    private TeamSelectionInterFace teamSelectionInterFace;
    String value;

    public EngDescriptionAdapter(List<AuxEngLabel> list, Context context, TeamSelectionInterFace teamSelectionInterFace)
    {
        this.context=context;
        this.list=list;
        this.teamSelectionInterFace=teamSelectionInterFace;
    }

    @NonNull
    @Override
    public EngDescriptionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        View view=inflater.inflate(R.layout.description_item,viewGroup,false);
        return new EngDescriptionAdapter.EngDescriptionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EngDescriptionViewHolder engDescriptionViewHolder, int i) {
        AuxEngLabel stringList = list.get(i);
        Integer post=list.size();

        engDescriptionViewHolder.textInputLayout.setHint(stringList.getEng_label().toUpperCase());


        engDescriptionViewHolder.editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


                EditText editText = engDescriptionViewHolder.editText;
                TextView textInputLayout=engDescriptionViewHolder.textInputLayout;
                teamSelectionInterFace.onDescriptionButtonClickListener(i, editText,textInputLayout);
                Log.e("hasFocus", s.toString());
            }
        });



        /*for(i=0;i<=list.size();i++) {
            discLabel.add(engDescriptionViewHolder.textView.getText().toString());
            discValue.add(engDescriptionViewHolder.editText.getText().toString());
        }*/

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class EngDescriptionViewHolder extends RecyclerView.ViewHolder{

       TextView textView;
       EditText editText;
        TextView textInputLayout;

       public EngDescriptionViewHolder(@NonNull View itemView) {
           super(itemView);

           textInputLayout=itemView.findViewById(R.id.textInputLayout);
           editText=itemView.findViewById(R.id.inputeLayout);

       }
   }

}
