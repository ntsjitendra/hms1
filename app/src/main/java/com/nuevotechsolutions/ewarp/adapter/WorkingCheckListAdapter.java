package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.utils.DescriptionDataCalculation;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import java.util.List;
import java.util.Set;


public class WorkingCheckListAdapter extends RecyclerView.Adapter<WorkingCheckListAdapter.WorkingChecklistViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(MasterWorkId item,int position);
    }

    Context context;
    List<MasterWorkId> list;
    List<EnginDescriptionData> descriptionData;
    private final OnItemClickListener listener;

    public WorkingCheckListAdapter(Context context, List<MasterWorkId> list, List<EnginDescriptionData> descriptionData, OnItemClickListener listener) {

        this.context=context;
        this.list=list;
        this.descriptionData=descriptionData;
        this.listener=listener;
    }

    @NonNull
    @Override
    public WorkingChecklistViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        View view=inflater.inflate(R.layout.completed_checklist_item,viewGroup,false);
        return new WorkingChecklistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WorkingChecklistViewHolder workingChecklistViewHolder, int i) {

        workingChecklistViewHolder.bind(list.get(i), listener);
        int j=i;
        MasterWorkId stringList=list.get(i);
        String assignType=stringList.getAssign_type();
        if (assignType!=null){
            assignType="A";
        }else {
            assignType="M";
        }
        if (descriptionData!=null){
            Set hs =new DescriptionDataCalculation().descriptionDataFilter(descriptionData,stringList.getUid(),stringList.getWrk_id());
            workingChecklistViewHolder.descriptionTextView.setText(hs.toString().replace("["," ").replace(",","").replace("]",""));
        }
        if(stringList.getSeen()==false){
            workingChecklistViewHolder.unReadStatus.setVisibility(View.VISIBLE);
        }else{
            workingChecklistViewHolder.unReadStatus.setVisibility(View.GONE);
        }

        workingChecklistViewHolder.checklistNameTextView.setTextColor(ContextCompat.getColor(context, R.color.QRCodeBlackColor));
        workingChecklistViewHolder.checklistNameTextView.setText(++j+". "+ stringList.getList());
        workingChecklistViewHolder.workIdLabelTextView.setText(" REF.NO: "+ stringList.getShort_nm()+ "/" + stringList
                .getWrk_id_crtn_dt().substring(0, 4) + "/" + stringList.getWrk_id()+"/"+assignType);
        workingChecklistViewHolder.creationDateTextView.setText(" Created: " +
                new UtilClassFuntions().dateConversion(stringList.getWrk_id_crtn_dt()));
            workingChecklistViewHolder.cancellationDateTextView.setText(" Last Updated: " +
                    new UtilClassFuntions().dateConversion(stringList.getModified_dt()));


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class WorkingChecklistViewHolder extends RecyclerView.ViewHolder {

        TextView checklistNameTextView,workIdLabelTextView,creationDateTextView,
                cancellationDateTextView,descriptionTextView;
        ImageView unReadStatus;

        public WorkingChecklistViewHolder(@NonNull View itemView) {
            super(itemView);
            checklistNameTextView=itemView.findViewById(R.id.checklistNameTextView);
            workIdLabelTextView=itemView.findViewById(R.id.workIdLabelTextView);
            creationDateTextView=itemView.findViewById(R.id.creationDateTextView);
            cancellationDateTextView=itemView.findViewById(R.id.completedDateTextView);
            descriptionTextView=itemView.findViewById(R.id.descriptionTextView);
            unReadStatus=itemView.findViewById(R.id.unReadStatus);
        }


        public void bind(final MasterWorkId list, final OnItemClickListener listener) {
        itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(list,getAdapterPosition());
                }
            });
        }
    }
}
