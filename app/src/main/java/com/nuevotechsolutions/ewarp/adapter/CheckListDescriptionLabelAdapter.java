package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;

import java.util.List;

public class CheckListDescriptionLabelAdapter extends RecyclerView.Adapter<CheckListDescriptionLabelAdapter.DescriptionLabelViewHolder> {

    Context context;
    List<String>labelName;
    List<String>labelValue;
    List<EnginDescriptionData> eDD;
    public CheckListDescriptionLabelAdapter(Context context, List<String> labelName, List<String> labelValue,List<EnginDescriptionData> eDD){

       this.context=context;
       this.labelName=labelName;
       this.labelValue=labelValue;
       this.eDD=eDD;
    }

    @NonNull
    @Override
    public DescriptionLabelViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        View view=inflater.inflate(R.layout.description_label_item,viewGroup,false);
        return new DescriptionLabelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DescriptionLabelViewHolder descriptionLabelViewHolder, int i) {

        if (eDD!=null && eDD.size()>0){
            descriptionLabelViewHolder.labelNameEditText.setText(eDD.get(i).getDecription());
            descriptionLabelViewHolder.labelValueEditText.setText(eDD.get(i).getDec_answer());

        }else {
            descriptionLabelViewHolder.labelNameEditText.setText(labelName.get(i).toUpperCase());
            String value = labelValue.get(i).toUpperCase();
            if (value.contains("[") || value.contains("]")) {
                if (value.contains("[")) {
                    value = value.replace("[", "");
                }
                if (value.contains("]")) {
                    value = value.replace("]", "");
                }
            }
            descriptionLabelViewHolder.labelValueEditText.setText(value);
        }
    }

    @Override
    public int getItemCount() {
        return labelName.size();
    }

    public class DescriptionLabelViewHolder extends RecyclerView.ViewHolder{

        TextView labelNameEditText,labelValueEditText;

        public DescriptionLabelViewHolder(@NonNull View itemView) {
            super(itemView);
            labelNameEditText= itemView.findViewById(R.id.textView4);
            labelValueEditText=itemView.findViewById(R.id.textView2);
        }


    }

}
