package com.nuevotechsolutions.ewarp.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.activities.RecordingListActivity;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.utils.Logger;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CompletedChecklistDetailsAdapter extends RecyclerView.Adapter<CompletedChecklistDetailsAdapter.CompletedChecklistDetailsViewHolder> {

    static List<ChecklistRecordDataList> checklistRecordDataLists = new ArrayList<>();
    private Context context;
    private List<Component> componentList;
    private RecycleCheckpointAdapter.OnItemClicked onClick;
    private Map<Integer, List<String>> imageMap;
    private Map<Integer, List<String>> locationMap;
    private Map<Integer, List<String>> timeStampMap;
    private Map<Integer, List<String>> photoTextValueMap;
    private Dialog mDialog;


    String tempTitleOfSection = "";

    String image;
    String[] itemsList;
    String[] location, time, phototextValu;

    String rb1, rb2, rb3, uid, rbData;
    String[] rbTemp;
    int radioFlag = 0;
    private String alertDepartment = null;

    ArrayList<String> audioList;
    ArrayList<String> audioListAddress;
    Map<Integer, ArrayList<String>> audioFileMap = new HashMap<>();
    private static final int AUDIO_LIST_UPSATE = 121;

    public CompletedChecklistDetailsAdapter(Context context,
                                            List<Component> componentList,
                                            List<ChecklistRecordDataList> checklistRecordDataListsm1,
                                            Map<Integer, List<String>> imageMap,
                                            Map<Integer, List<String>> locationMap,
                                            Map<Integer, List<String>> timeStampMap,
                                            Map<Integer, List<String>> photoTextValueMap) {
        this.context = context;
        this.componentList = componentList;
        this.checklistRecordDataLists = checklistRecordDataListsm1;
        this.imageMap = imageMap;
        this.locationMap = locationMap;
        this.timeStampMap = timeStampMap;
        this.photoTextValueMap = photoTextValueMap;
//        imagesList = new ArrayList<>();

        if (componentList != null && componentList.size() > 0) {
            uid = componentList.get(0).getUid();
            rbData = componentList.get(0).getOption_btn_type();
            rbTemp = rbData.split(",", 3);
            rb1 = rbTemp[0];
            rb2 = rbTemp[1];
            rb3 = rbTemp[2];
        }

        setHasStableIds(true);
        Log.e("componentList", String.valueOf(componentList.size()));
        Log.e("imageMap", String.valueOf(imageMap.size()));
        Log.e("Constructor", String.valueOf(checklistRecordDataLists.size()));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public CompletedChecklistDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.completed_checklist_details_item, viewGroup, false);
        return new CompletedChecklistDetailsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompletedChecklistDetailsViewHolder completedChecklistDetailsViewHolder, int i) {
        Component component = componentList.get(i);
        List<String> imagesList = imageMap.get(i);
        List<String> locationList = locationMap.get(i);
        List<String> timeStampList = timeStampMap.get(i);
        List<String> photoTextValueList = photoTextValueMap.get(i);

        if (radioFlag == i) {
            if (component.getSuggested_ans() != null && component.getSuggested_ans().equalsIgnoreCase(rb2)) {
                completedChecklistDetailsViewHolder.radioButtonNo.setChecked(true);
                radioFlag++;
            } else if (component.getSuggested_ans() != null && component.getSuggested_ans().equalsIgnoreCase(rb3)) {
                completedChecklistDetailsViewHolder.radioButtonNa.setChecked(true);
                radioFlag++;
            } else if (component.getSuggested_ans() != null && component.getSuggested_ans().equalsIgnoreCase(rb1)) {
                completedChecklistDetailsViewHolder.radioButtonYes.setChecked(true);
                radioFlag++;
            } else {
                radioFlag++;
            }
        }

        if (component.getRb_points_description() != null) {
            completedChecklistDetailsViewHolder.radioButtonYes.setText(rb1);
            completedChecklistDetailsViewHolder.radioButtonNo.setText(rb2);
            completedChecklistDetailsViewHolder.radioButtonNa.setText(rb3);
        }
        if (component.getYesnona().equalsIgnoreCase("false") && component.getAdditional().equalsIgnoreCase("table")) {
            completedChecklistDetailsViewHolder.rlTable.setVisibility(View.VISIBLE);
            completedChecklistDetailsViewHolder.llview.setVisibility(View.VISIBLE);

        }else if(component.getYesnona().equalsIgnoreCase("false") && component.getAdditional().equalsIgnoreCase("textBox")){
            completedChecklistDetailsViewHolder.llview.setVisibility(View.VISIBLE);
            completedChecklistDetailsViewHolder.rlTable.setVisibility(View.GONE);
        } else {
            completedChecklistDetailsViewHolder.llview.setVisibility(View.VISIBLE);
            completedChecklistDetailsViewHolder.rlTable.setVisibility(View.GONE);
        }
        ImageListDataAdapter itemListDataAdapter = null;

        if (component.getCuserpoint().equalsIgnoreCase("false")) {
            completedChecklistDetailsViewHolder.textViewQuestion.setTextColor(ContextCompat.getColor(context, R.color.darkGray));
            completedChecklistDetailsViewHolder.textViewQuestion.setText(component.getRb_points_description());
            completedChecklistDetailsViewHolder.textViewQuestion.setEnabled(false);
            completedChecklistDetailsViewHolder.radioButtonYes.setEnabled(false);
            completedChecklistDetailsViewHolder.radioButtonNo.setEnabled(false);
            completedChecklistDetailsViewHolder.radioButtonNa.setEnabled(false);
        } else {
            completedChecklistDetailsViewHolder.textViewQuestion.setText(component.getRb_points_description());
            completedChecklistDetailsViewHolder.textViewQuestion.setEnabled(false);
            completedChecklistDetailsViewHolder.radioButtonYes.setEnabled(false);
            completedChecklistDetailsViewHolder.radioButtonNo.setEnabled(false);
            completedChecklistDetailsViewHolder.radioButtonNa.setEnabled(false);
        }
        if (component.getCuserpoint().equalsIgnoreCase("false")) {
            completedChecklistDetailsViewHolder.edit_cmntBtn.setEnabled(false);

        } else {
            completedChecklistDetailsViewHolder.edit_cmntBtn.setEnabled(false);
        }
        if (component.getCuserpoint().equalsIgnoreCase("false")) {
            completedChecklistDetailsViewHolder.submitButton1.setEnabled(false);
        } else {
            completedChecklistDetailsViewHolder.submitButton1.setEnabled(false);
        }
        if (component.getCuserpoint().equalsIgnoreCase("false")) {
            completedChecklistDetailsViewHolder.switch_alert.setEnabled(false);
        } else {
            completedChecklistDetailsViewHolder.switch_alert.setEnabled(false);
        }

//        checkpointViewHolder.linearLayoutTimeStamp.setVisibility(View.GONE);
        Log.e("visible1", "visible1");


//        if (component.getComment() != null) {
//            checkpointViewHolder.editText_comments.setText("");
//        }
//
//        if (component.getAlert() != null) {
//            checkpointViewHolder.editText_Alert.setText("");
//        }

        if (component.getActive_time() != null && component.getCrnt_dt_tm() != null) {
            completedChecklistDetailsViewHolder.Text_submit.setVisibility(View.VISIBLE);
            completedChecklistDetailsViewHolder.Text_submit.setText("Active Time" + component.getActive_time() + "and Submit Time" + component.getCrnt_dt_tm());
        }

        if (component.getTitle_of_section() != null && !component.getTitle_of_section().equalsIgnoreCase("false")) {
//            checkpointViewHolder.textViewTitle.setVisibility(View.VISIBLE);
//            completedChecklistDetailsViewHolder.textViewTitle.setText(component.getTitle_of_section());
            if (tempTitleOfSection.equals("") && i == 0) {
                tempTitleOfSection = component.getTitle_of_section();
                completedChecklistDetailsViewHolder.textViewTitle.setVisibility(View.VISIBLE);
                completedChecklistDetailsViewHolder.textViewTitle.setText(component.getTitle_of_section());
            } else {
                if (tempTitleOfSection.equals(component.getTitle_of_section()) && i > 0) {
                    completedChecklistDetailsViewHolder.textViewTitle.setVisibility(View.GONE);
                } else {
                    completedChecklistDetailsViewHolder.textViewTitle.setText(component.getTitle_of_section());
                    tempTitleOfSection = component.getTitle_of_section();
                }
            }
        } else {
            completedChecklistDetailsViewHolder.textViewTitle.setVisibility(View.GONE);
        }

        if (component.getPhoto() != null && component.getPhoto().equalsIgnoreCase("false")) {
            completedChecklistDetailsViewHolder.photoButton1.setVisibility(View.GONE);
            completedChecklistDetailsViewHolder.linearLayoutImageViews.setVisibility(View.GONE);
            completedChecklistDetailsViewHolder.image_recyclerView.setVisibility(View.VISIBLE);
            itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, photoTextValueList, completedChecklistDetailsViewHolder.submitButton1);
            completedChecklistDetailsViewHolder.image_recyclerView.setHasFixedSize(true);
            completedChecklistDetailsViewHolder.image_recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            completedChecklistDetailsViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);
        } else {
            if (component.getCuserpoint().equalsIgnoreCase("false")) {
                completedChecklistDetailsViewHolder.photoButton1.setVisibility(View.VISIBLE);
                completedChecklistDetailsViewHolder.photoButton1.setEnabled(false);
                completedChecklistDetailsViewHolder.linearLayoutImageViews.setVisibility(View.GONE);

                completedChecklistDetailsViewHolder.image_recyclerView.setVisibility(View.GONE);
                itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, photoTextValueList, completedChecklistDetailsViewHolder.submitButton1);
                completedChecklistDetailsViewHolder.image_recyclerView.setHasFixedSize(true);
                completedChecklistDetailsViewHolder.image_recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                completedChecklistDetailsViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);
            } else {
                completedChecklistDetailsViewHolder.photoButton1.setVisibility(View.VISIBLE);
                completedChecklistDetailsViewHolder.photoButton1.setEnabled(true);
                completedChecklistDetailsViewHolder.linearLayoutImageViews.setVisibility(View.GONE);
                completedChecklistDetailsViewHolder.image_recyclerView.setVisibility(View.VISIBLE);


                itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, photoTextValueList, completedChecklistDetailsViewHolder.submitButton1);
                completedChecklistDetailsViewHolder.image_recyclerView.setHasFixedSize(true);
                completedChecklistDetailsViewHolder.image_recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                completedChecklistDetailsViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);
            }

        }

        if (component.getAlert() != null && component.getAlert().equalsIgnoreCase("false")) {
            completedChecklistDetailsViewHolder.switch_alert.setVisibility(View.GONE);
        } else {

            if (component.getCuserpoint().equalsIgnoreCase("false")) {
                completedChecklistDetailsViewHolder.switch_alert.setVisibility(View.VISIBLE);
                completedChecklistDetailsViewHolder.switch_alert.setEnabled(false);
            } else {
                completedChecklistDetailsViewHolder.switch_alert.setVisibility(View.VISIBLE);
                completedChecklistDetailsViewHolder.switch_alert.setEnabled(false);

            }

        }

        if (component.getGuidance() != null && !component.getGuidance().equalsIgnoreCase("false")) {
            if (component.getCuserpoint().equalsIgnoreCase("false")) {
                completedChecklistDetailsViewHolder.buttonGuidance.setVisibility(View.VISIBLE);
                completedChecklistDetailsViewHolder.buttonGuidance.setEnabled(false);
            } else {
                completedChecklistDetailsViewHolder.buttonGuidance.setVisibility(View.VISIBLE);
                completedChecklistDetailsViewHolder.buttonGuidance.setEnabled(true);
            }
        } else {
            completedChecklistDetailsViewHolder.buttonGuidance.setVisibility(View.GONE);

        }

        if (component.getVoice_record() != null && component.getVoice_record().equalsIgnoreCase("false")) {
            completedChecklistDetailsViewHolder.llaudio.setVisibility(View.GONE);
            completedChecklistDetailsViewHolder.llAudiolist.setVisibility(View.GONE);
        } else {
            if (component.getCuserpoint().equalsIgnoreCase("false")) {
                completedChecklistDetailsViewHolder.llaudio.setVisibility(View.VISIBLE);
                completedChecklistDetailsViewHolder.llAudiolist.setVisibility(View.VISIBLE);
                completedChecklistDetailsViewHolder.llaudio.setEnabled(false);
                completedChecklistDetailsViewHolder.llAudiolist.setEnabled(true);
            } else {
                completedChecklistDetailsViewHolder.llaudio.setVisibility(View.VISIBLE);
                completedChecklistDetailsViewHolder.llAudiolist.setVisibility(View.VISIBLE);
                completedChecklistDetailsViewHolder.llaudio.setEnabled(false);
                completedChecklistDetailsViewHolder.llAudiolist.setEnabled(true);
            }

        }


        Log.e("checkSizebefore", String.valueOf(checklistRecordDataLists.size()));

        if (!checklistRecordDataLists.isEmpty()) {
            Log.e("checkSize", String.valueOf(checklistRecordDataLists.size()));
            for (int j = 0; j < checklistRecordDataLists.size(); j++) {
                Log.e("checkList:", checklistRecordDataLists.get(j).getAns());
                if (checklistRecordDataLists.get(j).getChecklist_points().equals(component.getPoints())) {

                    if (checklistRecordDataLists.get(j).getAns().equals(rb1)) {
                        completedChecklistDetailsViewHolder.radioButtonYes.setChecked(true);
                    } else if (checklistRecordDataLists.get(j).getAns().equals(rb2)) {
                        completedChecklistDetailsViewHolder.radioButtonNo.setChecked(true);
                    } else if (checklistRecordDataLists.get(j).getAns().equals(rb3)) {
                        completedChecklistDetailsViewHolder.radioButtonNa.setChecked(true);
                    }
                    completedChecklistDetailsViewHolder.radioButtonYes.setEnabled(false);
                    completedChecklistDetailsViewHolder.radioButtonNo.setEnabled(false);
                    completedChecklistDetailsViewHolder.radioButtonNa.setEnabled(false);
                    completedChecklistDetailsViewHolder.edit_cmntBtn.setEnabled(false);
                    completedChecklistDetailsViewHolder.submitButton1.setEnabled(false);
                    completedChecklistDetailsViewHolder.photoButton1.setEnabled(false);
                    completedChecklistDetailsViewHolder.switch_alert.setEnabled(false);
                    completedChecklistDetailsViewHolder.linearLayoutTimeStamp.setVisibility(View.VISIBLE);

                    if (checklistRecordDataLists.get(j).getAudio_data() != null) {
                        try {
                            JSONArray jsonArray = new JSONArray(checklistRecordDataLists.get(j).getAudio_data());
                            String count = String.valueOf(jsonArray.length());
                            Log.e("size1", String.valueOf(count));
                            completedChecklistDetailsViewHolder.tvAudioCount.setText(count);
                            completedChecklistDetailsViewHolder.tvAudioCount.setVisibility(View.VISIBLE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    if (checklistRecordDataLists.get(j).getComment() != null) {
                        completedChecklistDetailsViewHolder.Text_comments.setVisibility(View.VISIBLE);
                        completedChecklistDetailsViewHolder.Text_comments.setText("Comment:" + checklistRecordDataLists.get(j).getComment());
                    }
                    if (checklistRecordDataLists.get(j).getSeverity() != null) {
                        completedChecklistDetailsViewHolder.Text_Alert.setVisibility(View.VISIBLE);
                        alertDepartment = checklistRecordDataLists.get(j).getDepartment();
                        if (alertDepartment == null) {
                            alertDepartment = "Not available";
                        }

                        String textAlertTitle = "<b>" + "Severity:" + "</b>" + checklistRecordDataLists.get(j).getSeverity()
                                + "<b>" + ", Department:" + "</b>" + alertDepartment
                                + "<b>" + ",\nAlert Comment:" + "</b>" + checklistRecordDataLists.get(j).getAlrt_cmnt();
                        completedChecklistDetailsViewHolder.Text_Alert.setText(Html.fromHtml(textAlertTitle));
                    }
                    String lctn = checklistRecordDataLists.get(j).getCrnt_lctn();
                    if (lctn != null) {
                        String[] temt = lctn.split("_", 2);
                        lctn = temt[0];
                    } else {
                        lctn = "Not available";
                    }

                    String crntTime = "";
                    if (checklistRecordDataLists.get(j).getCrnt_dt_tm() != null) {
                        crntTime = checklistRecordDataLists.get(j).getCrnt_dt_tm();
                    } else {
                        crntTime = checklistRecordDataLists.get(j).getGnrtn_date();
                    }
                    String textSubmitTitle = "<b>" + "Time:" + "</b>" + crntTime + "<b>" + ",\nLocation:" + "</b>" + lctn + "<b>" + ",\nSubmited By:" + "</b>" +
                            checklistRecordDataLists.get(j).getDone_by();
                    completedChecklistDetailsViewHolder.Text_submit.setText(Html.fromHtml(textSubmitTitle));

                    if (checklistRecordDataLists.get(j).getImage_data() != null /*&& checklistRecordDataLists.get(j).getImage_data().contains("http")*/) {
                        completedChecklistDetailsViewHolder.image_recyclerView.setVisibility(View.VISIBLE);

                        image = checklistRecordDataLists.get(j).getImage_data();
                        Log.e("image_data", image);
                        String[] images = StringUtils.substringsBetween(image, "\"", "\"");
                        if (images != null) {
                            if (imagesList == null) {
                                imagesList = new ArrayList<>();
                            }
                            imagesList.clear();
                            for (String temp : images) {
                                if (temp.contains("https")) {
                                    imagesList.add(temp);

                                    JSONArray imgArray = null;

                                    try {
                                        imgArray = new JSONArray(image);
                                        for (int r = 0; r < imgArray.length(); r++) {
                                            JSONObject imgObj = imgArray.getJSONObject(r);
                                            locationList.add(imgObj.getString("imagelocation"));
                                            timeStampList.add(imgObj.getString("imagetimedate"));
                                            photoTextValueList.add(imgObj.getString("photoTextValue"));
                                        }
                                        location = new String[locationList.size()];
                                        time = new String[timeStampList.size()];
                                        phototextValu = new String[photoTextValueList.size()];
                                        location = locationList.toArray(location);
                                        time = timeStampList.toArray(time);
                                        phototextValu = photoTextValueList.toArray(phototextValu);
                                        completedChecklistDetailsViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } else {

                                    if (temp.contains("storage")) {
                                        imagesList.add(temp.replace("\\", ""));


                                        String t = "";
                                        t = temp.substring(temp.length() - 21);
                                        try {
                                            JSONObject a = new JSONObject(checklistRecordDataLists.get(j).getImageJson());
                                            JSONArray b = new JSONArray();
                                            for (int k = 0; k < a.length(); k++) {
                                                JSONObject c = new JSONObject();
                                                JSONObject d = new JSONObject();
                                                d = a.getJSONObject(t);

                                                c.put("imagepath", temp.replace("\\", ""));
                                                c.put("imagelocation", d.getString("location"));
                                                c.put("imagename", t);
                                                c.put("photoTextValue", d.getString("photoTextValue"));
                                                c.put("imagetimedate", d.getString("timeStamp"));
                                                b.put(c);

                                            }
                                            image = b.toString();

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }

                                }
                            }
                            Log.e("images", String.valueOf(imagesList));
                        } else {

                        }
                        itemsList = new String[imagesList.size()];
                        itemsList = imagesList.toArray(itemsList);

//                        ********************************
                        itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, photoTextValueList, completedChecklistDetailsViewHolder.submitButton1);
                        completedChecklistDetailsViewHolder.image_recyclerView.setHasFixedSize(true);
                        completedChecklistDetailsViewHolder.image_recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        completedChecklistDetailsViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);


                    } else {
                        completedChecklistDetailsViewHolder.image_recyclerView.setVisibility(View.VISIBLE);

                    }


                }
            }
        }


        completedChecklistDetailsViewHolder.llAudiolist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAudioList(i, component.getPoints());
            }
        });

        completedChecklistDetailsViewHolder.readview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowAnswerListDialog(i, completedChecklistDetailsViewHolder, component.getPoints());
            }
        });
    }

    private void showAudioList(int position, String CPoints) {
        Log.e("positioRec", String.valueOf(position));
        for (int i = 0; i < componentList.size(); i++) {
            Component c = componentList.get(i);
            for (int j = 0; j < checklistRecordDataLists.size(); j++) {
                if (checklistRecordDataLists.get(j).getChecklist_points().equals(CPoints)) {
//                        audioList.clear();
                    String audio = checklistRecordDataLists.get(j).getAudio_data();
                    try {
                        if (audio != null) {
                            audioList = new ArrayList<>();
                            audioListAddress = new ArrayList<>();
                            File root = new File(context.getExternalFilesDir(Environment.DIRECTORY_MUSIC), "/EWARPRecoder");
                            if (!root.exists()) {
                                root.mkdirs();
                            }
                            String mPath = root.getAbsolutePath();
                            JSONArray audioArray = new JSONArray(audio);
                            for (int k = 0; k < audioArray.length(); k++) {
                                JSONObject audioObj = new JSONObject();
                                audioObj = audioArray.getJSONObject(k);
//                                Log.e("jj", audioObj.getString("audionm"));
//                                Log.e("jjk", mPath + File.separator + audioObj.getString("audionm"));
//                                Log.e("jjkk", audioObj.getString("audio"));

                                if (!audioList.contains(mPath + File.separator + audioObj.getString("audionm"))) {
                                    audioList.add(mPath + File.separator + audioObj.getString("audionm"));
                                }
                                if (!audioListAddress.contains(audioObj.getString("audio"))) {
                                    audioListAddress.add(audioObj.getString("audio"));
                                }

                            }
                            audioFileMap.put(position, audioList);
//                            audioFileMapAddress.put(position, audioListAddress);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        if (audioFileMap.get(position) != null && audioFileMap.get(position).size() > 0) {
            Intent intent = new Intent(context, RecordingListActivity.class);
//            intent.putExtra("filename", fileName);
//            intent.putExtra("filepath", mFilePath);
            intent.putExtra("position", position);
            intent.putStringArrayListExtra("fileNameList", audioFileMap.get(position));
//            intent.putStringArrayListExtra("addressList", audioFileMapAddress.get(position));
            Log.e("size", String.valueOf(audioFileMap.get(position).size()));
            ((Activity) context).startActivityForResult(intent, AUDIO_LIST_UPSATE);
        } else {
            Toast.makeText(context, "No recording available!", Toast.LENGTH_SHORT).show();
        }
    }

    public void ShowAnswerListDialog(int i, final CompletedChecklistDetailsViewHolder completedChecklistDetailsViewHolder, String points) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        AlertDialog alertDialog = builder.create();

//        mDialog = new Dialog(context);

//        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dView = LayoutInflater.from(context).inflate(R.layout.dialog_table_answer, null, false);

//        alertDialog.setContentView(R.layout.dialog_table_answer);

        RecyclerView recyclerView = dView.findViewById(R.id.table_view_recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        Component component = componentList.get(i);
        String json = component.getColumn_name();

        Log.e("json", json);

        String answers = null;
        for (int c = 0; c < componentList.size(); c++) {
            for (int ch = 0; ch < checklistRecordDataLists.size(); ch++) {
                if (componentList.get(i).getPoints().equals(checklistRecordDataLists.get(ch).getChecklist_points())
                        && componentList.get(i).getUid().equals(String.valueOf(checklistRecordDataLists.get(ch).getUid()))) {
                    answers = checklistRecordDataLists.get(ch).getClm_value();
                    break;
                }
            }
        }

        try {

            if (answers != null) {
//                Logger.printLog("ANswers list", "::  " + answers);

                JSONArray quesJsonArray = new JSONArray(json);
                JSONArray jsonArray1 = new JSONArray(answers);

                Logger.printLog("ANswers list", "::  " + jsonArray1);

                recyclerView.setAdapter(new QuestionAnswerAdapter(quesJsonArray, jsonArray1, context));

                alertDialog.setView(dView);
                alertDialog.show();

            } else {
                Toast.makeText(context, "No answer found!", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            Logger.printLog("QUES JSON EXC", "::  " + e.getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return componentList.size();
    }

    class CompletedChecklistDetailsViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewQuestion, textViewcrntLocation1;
        TextView Text_comments, Text_Alert, Text_submit;
        RadioButton radioButtonYes, radioButtonNo, radioButtonNa;
        RadioGroup radioGroup_ans;
        ImageButton edit_cmntBtn;
        ImageButton photoButton1;/*switch1,*/
        ImageButton submitButton1;
        ImageButton buttonGuidance;
        Switch switch_alert;
        LinearLayout llaudio, llAudiolist;
        ImageView imageView1, imageView2, imageView3, imageView4;
        RecyclerView image_recyclerView;
        TextView tvAudioCount, tableview, readview;
        RelativeLayout rlTable;

        //*************************************************************
        RadioButton rbLeast, rbHigh, rbVeryHigh, departmentA, departmentB, departmentC, departmentD;
        EditText alrtCmnt, departmentCmnt;
        String dialogAlrt, dialogAlrtCmnt, dialogDepartment, dialogDepartmentCmnt;
        Button alrtSubmit, alrtCancel;
        Dialog dialog;
        LinearLayout linearLayoutImageViews, linearLayoutImageViews1;
        LinearLayout linearLayoutTimeStamp, lrrootview, llview;
        RelativeLayout constraintLayoutOfCheckpoints;

        public CompletedChecklistDetailsViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.textViewTitleCCD);
            buttonGuidance = itemView.findViewById(R.id.buttonGuidanceCCD);

            textViewQuestion = itemView.findViewById(R.id.textView_questionCCD);

            Text_comments = itemView.findViewById(R.id.editText_commentsCCD);
            Text_Alert = itemView.findViewById(R.id.editText_AlertCCD);
            Text_submit = itemView.findViewById(R.id.editText_submitCCD);

            radioGroup_ans = itemView.findViewById(R.id.radioGroup_ansCCD);
            radioButtonYes = itemView.findViewById(R.id.rb1YesCCD);
            radioButtonNo = itemView.findViewById(R.id.rb1NoCCD);
            radioButtonNa = itemView.findViewById(R.id.rb1NaCCD);

            edit_cmntBtn = itemView.findViewById(R.id.edit_cmntCCD);

            photoButton1 = itemView.findViewById(R.id.photoButton1CCD);

            switch_alert = itemView.findViewById(R.id.switch_alertCCD);

            submitButton1 = itemView.findViewById(R.id.submitButton1CCD);

            imageView1 = itemView.findViewById(R.id.imageView1CCD);
            imageView2 = itemView.findViewById(R.id.imageView2CCD);
            imageView3 = itemView.findViewById(R.id.imageView3CCD);
            imageView4 = itemView.findViewById(R.id.imageView4CCD);
//            linearLayoutImageViews = itemView.findViewById(R.id.linearLayoutImageViews);

            linearLayoutImageViews = itemView.findViewById(R.id.linearLayoutImageViewsCCD);
            image_recyclerView = itemView.findViewById(R.id.image_recyclerViewCCD);
            linearLayoutTimeStamp = itemView.findViewById(R.id.linearLayoutTimeStampCCD);
            constraintLayoutOfCheckpoints = itemView.findViewById(R.id.constraintLayoutOfCheckpointsCCD);
            lrrootview = itemView.findViewById(R.id.lrrootviewCCD);
            llaudio = itemView.findViewById(R.id.llaudio);
            llAudiolist = itemView.findViewById(R.id.llAudioList);
            tvAudioCount = itemView.findViewById(R.id.tvAudioCount);
            llview = itemView.findViewById(R.id.llview);
            rlTable = itemView.findViewById(R.id.rlTableComp);
            readview = itemView.findViewById(R.id.ReadviewComp);
            llview = itemView.findViewById(R.id.llviewComp);

            //******************************************************************
//            dialog = new Dialog(context);
//            dialog.setContentView(R.layout.alert_layout);
//            rbLeast = dialog.findViewById(R.id.rbLeast);
//            rbHigh = dialog.findViewById(R.id.rbHigh);
//            rbVeryHigh = dialog.findViewById(R.id.rbVeryHigh);
//            alrtCmnt = dialog.findViewById(R.id.alertComment);
//            departmentA = dialog.findViewById(R.id.departmentA);
//            departmentB = dialog.findViewById(R.id.departmentB);
//            departmentC = dialog.findViewById(R.id.departmentC);
//            departmentD = dialog.findViewById(R.id.departmentD);
//            departmentCmnt = dialog.findViewById(R.id.departmentComment);
//            alrtSubmit = dialog.findViewById(R.id.alertSubmit);
//            alrtCancel = dialog.findViewById(R.id.alertCancel);

        }
    }

}
