package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.interfaces.TableFormSubmItInterface;
import com.nuevotechsolutions.ewarp.utils.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class QuestionAnswerAdapter extends RecyclerView.Adapter<QuestionAnswerAdapter.TableViewHolder> {
    private Context context;
    private JSONArray questionList;
    JSONArray answerJsonArray1;
    //private Map<Integer, List<String>> answerJsonArray;
    //private Map<Integer, List<String>> submittedBy;
    TableFormSubmItInterface tableFormSubmItInterface;

//    public QuestionAnswerAdapter(JSONArray questionList,
//                                 Map<Integer, List<String>> answerJsonArray,
//                                 Map<Integer, List<String>> submittedBy, Context context) {
//        this.context = context;
//        this.questionList = questionList;
//        this.answerJsonArray = answerJsonArray;
//        this.submittedBy = submittedBy;
//
//    }

    public QuestionAnswerAdapter(JSONArray questionList, JSONArray answerJsonArray1, Context context) {
        this.context = context;
        this.questionList = questionList;
        this.answerJsonArray1 = answerJsonArray1;
        //this.submittedBy = submittedBy;

    }

    @NonNull
    @Override
    public TableViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.table_answer_adapter, parent, false);
        return new QuestionAnswerAdapter.TableViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TableViewHolder holder, int position) {
        String component = null;
        try {
//            component = questionList.getString(position);

//            holder.tvquestion.setText("Ques " + String.valueOf(position+1) + ":- " + component);

//            String[] answers = new String[answerJsonArray.get(position).length - 1];
            List<String> answers = new ArrayList<>();
            List<String> submits = new ArrayList<>();

            JSONObject jo = answerJsonArray1.getJSONObject(position);

            for (int i = 0; i < questionList.length(); i++) {
                String key = String.valueOf(i + 1);
                answers.add("Ans " + key + ":- " + jo.getString(key));
            }

            holder.tvSubmittedBy.setText(String.format("Submitted By : %s", jo.getString("name")));
            holder.tvCommentTable.setText(String.format("Comment :-\n\n%s", jo.getString("p_comment")));
            Glide.with(context)
                    .load(jo.getString("p_image"))
                    .into(holder.imgTable);

//            Logger.printLog("Single Ques Ans", "::  " + answerJsonArray.get(position).size());

//            for (int i = 0; i < answerJsonArray.get(position).size(); i++) {
////               answers[0] = answerJsonArray.get(position)[i] + "\n\nSubmitted by : " + submittedBy.get(position)[i];
//                answers.add("Ans " + String.valueOf(i + 1) + ":- " + answerJsonArray.get(position).get(i));
//                submits.add("Submitted By : " + submittedBy.get(position).get(i));
//            }

            Logger.printLog("Single Ques Ans 1", "::  " + answers);

            AnswerAdapter answerAdapter = new AnswerAdapter(context, R.layout.table_single_answer, answers, questionList);
            holder.listAnswers.setAdapter(answerAdapter);

            setListViewHeightBasedOnItems(holder.listAnswers);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
//        return questionList.length();
        return answerJsonArray1.length();

    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                float px = 500 * (listView.getResources().getDisplayMetrics().density);
                item.measure(View.MeasureSpec.makeMeasureSpec((int) px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);
            // Get padding
            int totalPadding = listView.getPaddingTop() + listView.getPaddingBottom();

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight + totalPadding;
            listView.setLayoutParams(params);
            listView.requestLayout();
            //setDynamicHeight(listView);
            return true;

        } else {
            return false;
        }

    }

//    public void setListViewHeightBasedOnChildren(ListView listView) {
//        ListAdapter listAdapter = listView.getAdapter();
//        if (listAdapter == null) {
//            // pre-condition
//            return;
//        }
//
//        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
//        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
//        for (int i = 0; i < listAdapter.getCount(); i++) {
//            View listItem = listAdapter.getView(i, null, listView);
//
//            if(listItem != null){
//                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
//                listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
//                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
//                totalHeight += listItem.getMeasuredHeight();
//
//            }
//        }
//
//        ViewGroup.LayoutParams params = listView.getLayoutParams();
//        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
//        listView.setLayoutParams(params);
//        listView.requestLayout();
//    }

    public class AnswerAdapter extends ArrayAdapter<String> {

        Context context;
        List<String> answerList;
        JSONArray questionList;
        //List<String> submittedList;

//        public AnswerAdapter(Context context, int resource, List<String> answerList, List<String> submittedList) {
//            super(context, resource, answerList);
//            this.answerList = answerList;
//            this.submittedList = submittedList;
//            this.context = context;
//
//        }

        public AnswerAdapter(Context context, int resource, List<String> answers, JSONArray questionList) {
            super(context, resource, answers);
            this.answerList = answers;
            this.questionList = questionList;
            this.context = context;
        }

        @Override
        public int getCount() {
            return answerList.size();
        }

        @Nullable
        @Override
        public String getItem(int position) {
            return answerList.get(position);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = LayoutInflater.from(context).inflate(R.layout.table_single_answer, parent, false);

            TextView txtQuestion = view.findViewById(R.id.txtQuestion);
            TextView txtAnswer = view.findViewById(R.id.txtAnswer);
            try {

                txtQuestion.setText("Ques " + String.valueOf(position + 1) + ":- " + questionList.getString(position));
                txtAnswer.setText(answerList.get(position));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return view;
        }
    }

    public class TableViewHolder extends RecyclerView.ViewHolder {

        //        TextView tvquestion;
        TextView tvSubmittedBy;
        TextView tvCommentTable;
        ImageView imgTable;
        ListView listAnswers;

        public TableViewHolder(@NonNull View itemView) {
            super(itemView);

            listAnswers = itemView.findViewById(R.id.listAnswers);
//            tvquestion = itemView.findViewById(R.id.tvquestion);
            tvSubmittedBy = itemView.findViewById(R.id.txtSubmit);
            imgTable = itemView.findViewById(R.id.imgTable);
            tvCommentTable = itemView.findViewById(R.id.tvCommentTable);

        }
    }
}
