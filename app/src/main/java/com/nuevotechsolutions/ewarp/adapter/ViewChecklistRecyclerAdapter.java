package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ViewChecklistRecyclerAdapter extends RecyclerView.Adapter<ViewChecklistRecyclerAdapter.ViewChecklistRecyclerViewHolder> {

    Context context;
    JSONArray arrChecklist;
    String rb1, rb2, rb3, rbData;
    String[] rbTemp;
    String tempTitle;
    private List<Component> componentList;

    public ViewChecklistRecyclerAdapter(Context context, JSONArray arrChecklist, List<Component> componentList) {
        this.context = context;
        this.arrChecklist = arrChecklist;
        this.componentList = componentList;
        if (arrChecklist != null && arrChecklist.length() > 0) {
            try {
                rbData = arrChecklist.getJSONObject(0).getString("option_btn_type");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            rbTemp = rbData.split(",", 3);
            rb1 = rbTemp[0];
            rb2 = rbTemp[1];
            rb3 = rbTemp[2];
        }
        setHasStableIds(true);

    }

    @NonNull
    @Override
    public ViewChecklistRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_room_item, parent, false);
        return new ViewChecklistRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewChecklistRecyclerViewHolder holder, int position) {

        JSONObject component = new JSONObject();
        if (position==0){
            tempTitle="";
        }
//        Component component1 = componentList.get(position);
//        Log.e("compo+", String.valueOf(component1));

        try {
            component = arrChecklist.getJSONObject(position);
            if (component!=null && component.optString("yesnona").equals("false")) {
                holder.rlTable.setVisibility(View.VISIBLE);
                holder.llview.setVisibility(View.VISIBLE);

            } else {
                holder.llview.setVisibility(View.VISIBLE);
                holder.rlTable.setVisibility(View.GONE);
            }
            Log.e("com+", String.valueOf(component));
            if (component!=null && !component.optString("title_of_section").equals("false")){
                if (tempTitle.equals("")){
                    holder.textViewTitle.setText(component.optString("title_of_section"));
                    tempTitle = component.optString("title_of_section");
                }else if (tempTitle.equals(component.optString("title_of_section"))){
                    holder.textViewTitle.setVisibility(View.GONE);
                }else if (!tempTitle.equals(component.optString("title_of_section"))){
                    holder.textViewTitle.setText(component.optString("title_of_section"));
                    tempTitle = component.optString("title_of_section");
                }
            }else {
                holder.textViewTitle.setVisibility(View.GONE);
            }

            if (component!=null && !component.optString("rb_points_description").equals("")){
                holder.textView_question.setText(component.optString("rb_points_description"));
                holder.rb1Yes.setText(rb1);
                holder.rb1No.setText(rb2);
                holder.rb1Na.setText(rb3);
            }else {
                holder.textView_question.setVisibility(View.GONE);
                holder.rb1Yes.setVisibility(View.GONE);
                holder.rb1No.setVisibility(View.GONE);
                holder.rb1Na.setVisibility(View.GONE);
            }

            if (component!=null && !component.optString("photo").equals("false")){
                holder.photoButton1.setVisibility(View.VISIBLE);
//                holder.cameraTitleTextView.setVisibility(View.VISIBLE);
            }else{
                holder.photoButton1.setVisibility(View.GONE);
//                holder.cameraTitleTextView.setVisibility(View.GONE);
            }

            if (component!=null && !component.optString("comment").equals("false")){
                holder.edit_cmnt.setVisibility(View.VISIBLE);
//                holder.commentTitleTextView.setVisibility(View.VISIBLE);
            }else{
                holder.edit_cmnt.setVisibility(View.GONE);
//                holder.commentTitleTextView.setVisibility(View.GONE);
            }

            if (component!=null && !component.optString("alert").equals("false")){
                holder.switch_alert.setVisibility(View.VISIBLE);
                holder.alertTitleTextView.setVisibility(View.VISIBLE);
            }else{
                holder.switch_alert.setVisibility(View.GONE);
                holder.alertTitleTextView.setVisibility(View.GONE);
            }

            if (component!=null && !component.optString("voice_record").equals("false")){
                holder.llaudio.setVisibility(View.VISIBLE);
                holder.llAudioList.setVisibility(View.VISIBLE);
            }else{
                holder.llaudio.setVisibility(View.GONE);
                holder.llAudioList.setVisibility(View.GONE);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return arrChecklist.length();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class ViewChecklistRecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle,textView_question;
        RadioButton rb1Yes,rb1No,rb1Na;
        ImageButton photoButton1,edit_cmnt,submitButton1;
        Switch switch_alert;
        TextView cameraTitleTextView,commentTitleTextView,alertTitleTextView,submitTitleTextView,tableview, readview;
        LinearLayout llaudio,llAudioList,llview;
        RelativeLayout rlTable;

        public ViewChecklistRecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textView_question = itemView.findViewById(R.id.textView_question);

            rb1Yes = itemView.findViewById(R.id.rb1Yes);
            rb1No = itemView.findViewById(R.id.rb1No);
            rb1Na = itemView.findViewById(R.id.rb1Na);

            photoButton1 = itemView.findViewById(R.id.photoButton1);
            edit_cmnt = itemView.findViewById(R.id.edit_cmnt);
            switch_alert = itemView.findViewById(R.id.switch_alert);
            submitButton1 = itemView.findViewById(R.id.submitButton1);

            cameraTitleTextView = itemView.findViewById(R.id.cameraTitleTextView);
            commentTitleTextView = itemView.findViewById(R.id.commentTitleTextView);
            alertTitleTextView = itemView.findViewById(R.id.alertTitleTextView);
            submitTitleTextView = itemView.findViewById(R.id.submitTitleTextView);

            llaudio = itemView.findViewById(R.id.llaudio);
            llAudioList = itemView.findViewById(R.id.llAudioList);
            llview = itemView.findViewById(R.id.llview);
            rlTable = itemView.findViewById(R.id.rlTable);
            readview = itemView.findViewById(R.id.Readview);

        }
    }

}
