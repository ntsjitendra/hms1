package com.nuevotechsolutions.ewarp.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.activities.IsolationActivity;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.TeamSelectionInterFace;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserPointAccessData;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.LogoutClass;
import com.nuevotechsolutions.ewarp.utils.MultipartUtility;
import com.nuevotechsolutions.ewarp.utils.SharedPrefancesClearData;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.content.Context.MODE_PRIVATE;


@SuppressWarnings("All")
public class CheckListAdapter extends RecyclerView.Adapter<CheckListAdapter.CheckListViewHolder> {

    private List<CheckList> checkLists;
    private List<AuxEngDetails> auxEngDetails;
    private Context context;
    CheckListDatabase db;
    List<TeamSelectionList> teamSelectionLists;
    List<AuxEngLabel> auxEngLabelList;

    private int tempCount = 0;


    AlertDialog.Builder alertDialog;
    List<String> auxillaryEngNo1, auxillaryEngMantType, unitNo1;
    List<String> teamList;
    Map<String, String> firstTeamList = new HashMap<>();
    Map<String, String> secondTeamlist = new HashMap<>();
    Map<Integer, String> valueDetails = new HashMap<>();
    Map<Integer, String> lableDetails = new HashMap<>();
    List<String> description = new ArrayList<>();
    List<UserDetails> userDetails = new ArrayList<>();

    JSONObject checklistdata1 = new JSONObject();
    JSONObject teammember1 = new JSONObject();
    JSONObject valueObject = new JSONObject();
    private TeamSelectionList teamSelectionListClass;
    private TeamSelectionAdapter teamSelectionAdapter;
    RecyclerView recyclerView;
    List<TeamSelectionList> list = new ArrayList<>();
    private int Totalteamlist;
    private List<String> ranklist1;
    private List<String> rankPresent1;
    private Map<String, String> selectionRank = new HashMap<>();
    Integer tempId;

    String titleRefNo;

    public CheckListAdapter(Context context, List<CheckList> checkLists/*, List<AuxEngDetails> auxEngDetails*/) {
        this.checkLists = checkLists;
//        this.auxEngDetails = auxEngDetails;
        this.context = context;
    }

    @NonNull
    @Override
    public CheckListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.checklist_items, viewGroup, false);
        CheckListViewHolder checkListViewHolder = new CheckListViewHolder(view);
        return checkListViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CheckListViewHolder checkListViewHolder, int i) {

        if (checkLists.size() > 0) {
            CheckList checkList = checkLists.get(i);
            int j = i;
            checkListViewHolder.textViewItemName.setText(++j + ". " + checkList.getList().toUpperCase());

            List<CheckList> list;
            List<MasterWorkId> masterList;

            db = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class,
                    ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
            list = db.productDao().getAllCheckList();
            masterList = db.productDao().getMasterWorkIdData();
            userDetails = db.productDao().getUserDetail();
//            for(int l=0;l<masterList.size();l++){
//                if (masterList.get(l).getGrtd_by().equals(userDetails.get(0).getUser_id()) && masterList.get(i).getWrk_status().equals(0)){
//                    tempCount++;
//                }
//            }

            checkListViewHolder.textViewItemName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (tempCount<=10){
//                        teamSelectionDialog(list.get(i).getUid(), checkListViewHolder.textViewItemName.getText().toString());
//                    }else {
//                        Toast.makeText(context, "Can not create more then number of 10 checklist!"+tempCount, Toast.LENGTH_SHORT).show();
//                    }
                    if (new NetworkDetector().isNetworkReachable(context) == true) {
                        if (checkLists.get(i).getSingleuser().equals("false")) {
                            teamSelectionDialog(checkLists.get(i).getUid(), checkListViewHolder.textViewItemName.getText().toString());
                        } else {
                            List<TeamSelectionList> totalTeamList = new ArrayList<>();
                            totalTeamList = db.productDao().getTeamSelectionData();
                            List<TeamSelectionList> singlerankMultiUser = new ArrayList<>();
                            for (int i = 0; i < totalTeamList.size(); i++) {
                                if (totalTeamList.get(i).getRank().equalsIgnoreCase(userDetails.get(0).getRank())) {
                                    singlerankMultiUser.add(totalTeamList.get(i));
                                }
                            }
                            if (singlerankMultiUser.size() > 1) {
                                teamSelectionDialog(checkLists.get(i).getUid(), checkListViewHolder.textViewItemName.getText().toString());

                            } else {
                                try {
                                    String uid = checkLists.get(i).getUid();
                                    Toast.makeText(context, "Single user checklist!", Toast.LENGTH_SHORT).show();
                                    List<AuxEngLabel> AuxEnglist1 = new ArrayList<>();
                                    auxEngLabelList = db.productDao().getAllAuxEngLebels();

                                    for (AuxEngLabel auxEngLabel : auxEngLabelList) {
                                        if (auxEngLabel.getUid().equals(uid)) {
                                            AuxEngLabel auxEngLabel1 = new AuxEngLabel();
                                            auxEngLabel1.setEng_label(auxEngLabel.getEng_label());
                                            AuxEnglist1.add(auxEngLabel1);
                                        }
                                    }
                                    JSONArray listArray = new JSONArray();
                                    JSONObject c_userPoint = new JSONObject();
                                    c_userPoint.put("user_id", userDetails.get(0).getUser_id());
                                    c_userPoint.put("uid", uid);
                                    c_userPoint.put("rank_id", userDetails.get(0).getRank_id());
                                    c_userPoint.put("name", userDetails.get(0).getFirst_name());
                                    c_userPoint.put("rank", userDetails.get(0).getRank());
                                    listArray.put(c_userPoint);
                                    teammember1.put("teamdtls", listArray);

                                    if (AuxEnglist1.size() == 0) {
                                        String strDate = new UtilClassFuntions().currentDateTime();

                                        checklistdata1.put("uid", uid);
                                        checklistdata1.put("grtd_by", userDetails.get(0).getUser_id());
                                        checklistdata1.put("crtntime", strDate);
                                        checklistdata1.put("user_id", userDetails.get(0).getUser_id());
                                        checklistdata1.put("wrk_status", 0);
                                        checklistdata1.put("dbnm", userDetails.get(0).getDbnm());
                                        checklistdata1.put("accessToken", userDetails.get(0).getAccessToken());

                                        String dataKey = "checklistdata";
                                        String dataKey1 = "teammember";

//                                    Log.e("employeid", list.get(0).getEmployee_id());
                                        Log.e("id", uid);

                                        MultipartUtility multipart = new MultipartUtility(ApiUrlClass.create, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember1);

                                        Log.e("a", String.valueOf(checklistdata1));
                                        Log.e("b", String.valueOf(teammember1));
                                        Log.e("c", dataKey);
                                        Log.e("d", dataKey1);

                                        List<String> response = multipart.finish();
                                        System.out.println("SERVER REPLIED:");
                                        String wrk_id = "";
                                        for (String line : response) {
                                            System.out.println(line);
                                            Log.e("CheckList:res:wrk_id", line);
                                            JSONObject jsonObject = new JSONObject(line);
                                            Integer res = jsonObject.getInt("response");

                                            if (res.equals(200)) {
                                                wrk_id = jsonObject.getString("wrk_id");
                                                Log.d("wrkId:", wrk_id);
                                                if (!wrk_id.equals("")) {

                                                    //******************************************
                                                    ObjectMapper objectMapper = new ObjectMapper();
                                                    List<UserPointAccessData> userPointAccessDataList = new ArrayList<>();
                                                    JSONArray userAccessPoint = jsonObject.getJSONArray("userpoint");


                                                    if (userAccessPoint != null && userAccessPoint.length() >= 0) {
                                                        for (int i = 0; i < userAccessPoint.length(); i++) {
                                                            JSONObject jsonObject1 = (JSONObject) userAccessPoint.get(i);
                                                            UserPointAccessData userPointAccessData1 = objectMapper.readValue
                                                                    (jsonObject1.toString(), UserPointAccessData.class);
                                                            userPointAccessDataList.add(userPointAccessData1);

                                                        }
                                                    }

//
                                                    Log.e("ddata", "saved...");
                                                    //*********************************************************
                                                    MasterWorkId masterWorkId = new MasterWorkId();
                                                    masterWorkId.setWrk_id(Integer.valueOf(wrk_id));
                                                    masterWorkId.setGrtd_by(userDetails.get(0).getUser_id());
                                                    masterWorkId.setWrk_id_crtn_dt(strDate);
                                                    masterWorkId.setUid(Integer.valueOf(uid));
                                                    masterWorkId.setModified_dt(strDate);
                                                    masterWorkId.setWrk_status(0);
                                                    masterWorkId.setDelete_id(0);
                                                    masterWorkId.setTeam_list(String.valueOf(teammember1.getJSONArray("teamdtls")));
                                                    Log.e("tl...", String.valueOf(teammember1.getJSONArray("teamdtls")));


                                                    List<CheckList> checkLists = new ArrayList<>();
                                                    checkLists = db.productDao().getAllCheckList();
                                                    for (int i = 0; i < checkLists.size(); i++) {
                                                        if (checkLists.get(i).getUid().equals(uid)) {
                                                            titleRefNo = db.productDao().getAllCheckList().get(i).getShort_nm()
                                                                    + "/" + strDate.substring(0, 4) + "/" + wrk_id + "/" + "M";
                                                            masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                                            masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                                            masterWorkId.setChecklist_type(db.productDao().getAllCheckList().get(i).getChecklist_type());
                                                        }
                                                    }

                                                    Thread threadSetSubmit = new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                                        }
                                                    });
                                                    threadSetSubmit.start();

                                                }
                                            } else if (res.equals(10003)) {
                                                new LogoutClass().logoutMethod(context);
                                            }
                                        }

                                        List<CheckList> lists = new ArrayList<>();
                                        String title = "";
                                        String checklist_type = "";
                                        lists = db.productDao().getAllCheckList();
                                        for (int i = 0; i < lists.size(); i++) {
                                            if (lists.get(i).getUid().equals(uid)) {
                                                title = lists.get(i).getList().toUpperCase();
                                                checklist_type = lists.get(i).getChecklist_type();
                                            }
                                        }
                                        String engNo = "", engType = "", unitNo = "";
                                        navigateToIsolationActivity(uid, engNo, engType, unitNo, title, wrk_id, checklist_type);

                                    } else {
                                        List<CheckList> lists = new ArrayList<>();
                                        String title = "";
                                        String checklist_type = "";
                                        lists = db.productDao().getAllCheckList();
                                        for (int i = 0; i < lists.size(); i++) {
                                            if (lists.get(i).getUid().equals(uid)) {
                                                title = lists.get(i).getList().toUpperCase();
                                                checklist_type = lists.get(i).getChecklist_type();
                                            }
                                        }
                                        descriptionDialog(uid, title);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                    } else {
                        Toast.makeText(context, "No internet connection!", Toast.LENGTH_SHORT).show();
                        tempId = genrateTempWrkId();
                        if (checkLists.get(i).getSingleuser().equals("false")) {
                            offlineTeamSelectionDialog(checkLists.get(i).getUid(), checkListViewHolder.textViewItemName.getText().toString());
                        } else {
                            List<TeamSelectionList> totalTeamList = new ArrayList<>();
                            totalTeamList = db.productDao().getTeamSelectionData();
                            List<TeamSelectionList> singlerankMultiUser = new ArrayList<>();
                            for (int j = 0; j < totalTeamList.size(); j++) {
                                if (totalTeamList.get(j).getUid().equals(Integer.parseInt(checkLists.get(i).getUid()))) {
                                    singlerankMultiUser.add(totalTeamList.get(j));
                                }
                            }
                            if (singlerankMultiUser.size() > 1) {
                                Toast.makeText(context, "Single user checklist!", Toast.LENGTH_SHORT).show();
                                offlineTeamSelectionDialog(checkLists.get(i).getUid(), checkListViewHolder.textViewItemName.getText().toString());

                            } else {
                                try {
                                    String uid = checkLists.get(i).getUid();
                                    Toast.makeText(context, "Single user checklist!", Toast.LENGTH_SHORT).show();
                                    List<AuxEngLabel> AuxEnglist1 = new ArrayList<>();
                                    auxEngLabelList = db.productDao().getAllAuxEngLebels();

                                    for (AuxEngLabel auxEngLabel : auxEngLabelList) {
                                        if (auxEngLabel.getUid().equals(uid)) {
                                            AuxEngLabel auxEngLabel1 = new AuxEngLabel();
                                            auxEngLabel1.setEng_label(auxEngLabel.getEng_label());
                                            AuxEnglist1.add(auxEngLabel1);
                                        }
                                    }
                                    JSONArray listArray = new JSONArray();
                                    JSONObject c_userPoint = new JSONObject();
                                    c_userPoint.put("user_id", userDetails.get(0).getUser_id());
                                    c_userPoint.put("uid", uid);
                                    c_userPoint.put("rank_id", userDetails.get(0).getRank_id());
                                    c_userPoint.put("name", userDetails.get(0).getFirst_name());
                                    c_userPoint.put("rank", userDetails.get(0).getRank());
                                    listArray.put(c_userPoint);
                                    teammember1.put("teamdtls", listArray);

                                    if (AuxEnglist1.size() == 0) {
                                        String strDate = new UtilClassFuntions().currentDateTime();

                                        checklistdata1.put("uid", uid);
                                        checklistdata1.put("grtd_by", userDetails.get(0).getUser_id());
                                        checklistdata1.put("crtntime", strDate);
                                        checklistdata1.put("user_id", userDetails.get(0).getUser_id());
                                        checklistdata1.put("wrk_status", 0);
                                        checklistdata1.put("dbnm", userDetails.get(0).getDbnm());
                                        checklistdata1.put("accessToken", userDetails.get(0).getAccessToken());

//
                                        Log.e("ddata", "saved..." + genrateTempWrkId());
                                        //*********************************************************
                                        MasterWorkId masterWorkId = new MasterWorkId();
                                        masterWorkId.setWrk_id(tempId);
                                        masterWorkId.setGrtd_by(userDetails.get(0).getUser_id());
                                        masterWorkId.setWrk_id_crtn_dt(strDate);
                                        masterWorkId.setUid(Integer.valueOf(uid));
                                        masterWorkId.setModified_dt(strDate);
                                        masterWorkId.setWrk_status(0);
                                        masterWorkId.setDelete_id(0);
                                        masterWorkId.setTemp_Wrk_id(tempId);
                                        masterWorkId.setTeam_list(String.valueOf(teammember1.getJSONArray("teamdtls")));
                                        Log.e("tl...", String.valueOf(teammember1.getJSONArray("teamdtls")));


                                        List<CheckList> checkLists = new ArrayList<>();
                                        checkLists = db.productDao().getAllCheckList();
                                        for (int i = 0; i < checkLists.size(); i++) {
                                            if (checkLists.get(i).getUid().equals(uid)) {
                                                titleRefNo = db.productDao().getAllCheckList().get(i).getShort_nm()
                                                        + "/" + strDate.substring(0, 4) + "/" + tempId + "/" + "M";
                                                masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                                masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                                masterWorkId.setChecklist_type(db.productDao().getAllCheckList().get(i).getChecklist_type());
                                            }
                                        }

                                        Thread threadSetSubmit = new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                            }
                                        });
                                        threadSetSubmit.start();

                                        List<CheckList> lists = new ArrayList<>();
                                        String title = "";
                                        String checklist_type = "";
                                        lists = db.productDao().getAllCheckList();
                                        for (int i = 0; i < lists.size(); i++) {
                                            if (lists.get(i).getUid().equals(uid)) {
                                                title = lists.get(i).getList().toUpperCase();
                                                checklist_type = lists.get(i).getChecklist_type();
                                            }
                                        }
                                        String engNo = "", engType = "", unitNo = "";
                                        navigateToIsolationActivity(uid, engNo, engType, unitNo, title, String.valueOf(tempId), checklist_type);

                                    } else {
                                        List<CheckList> lists = new ArrayList<>();
                                        String title = "";
                                        String checklist_type = "";
                                        lists = db.productDao().getAllCheckList();
                                        for (int i = 0; i < lists.size(); i++) {
                                            if (lists.get(i).getUid().equals(uid)) {
                                                title = lists.get(i).getList().toUpperCase();
                                                checklist_type = lists.get(i).getChecklist_type();
                                            }
                                        }
                                        offlineDescriptionDialog(uid, title);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                        //////////////////////////////////////
                    }

                    //alertDialog(checkList.getUid(), checkListViewHolder.textViewItemName.getText().toString());
                    //navigateToIsolationActivity(checkList.getUid());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return checkLists.size();
    }


    class CheckListViewHolder extends RecyclerView.ViewHolder {
        TextView textViewItemName;

        public CheckListViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewItemName = itemView.findViewById(R.id.textViewItemName);
        }
    }

    private void navigateToIsolationActivity(String uid, String EngNo, String EngType, String unitNo, String title, String wrk_id, String checklist_type) {
        Intent intent = new Intent(context, IsolationActivity.class);

        intent.putExtra("uid", uid);
        /*intent.putExtra("EngNo", EngNo);
        intent.putExtra("EngType", EngType);
        intent.putExtra("unitNo", unitNo);*/
        intent.putExtra("title", titleRefNo);
        intent.putExtra("wrk_id", wrk_id);
        intent.putExtra("checklist_type", checklist_type);

        new SharedPrefancesClearData().ClearDescriptionDataKey(context);

        SharedPreferences.Editor editor = context.getSharedPreferences("descriptionKey", MODE_PRIVATE).edit();

        Iterator<Map.Entry<Integer, String>> it = lableDetails.entrySet().iterator();
        Iterator<Map.Entry<Integer, String>> it1 = valueDetails.entrySet().iterator();
//        EnginDescriptionData enginDescriptionData=new EnginDescriptionData();
        while (it.hasNext() && it1.hasNext()) {
            Map.Entry<Integer, String> pair = it.next();
            Map.Entry<Integer, String> pair1 = it1.next();
            editor.putString(String.valueOf(pair.getValue()), pair1.getValue());
            description.add(pair1.getValue());
//            enginDescriptionData.setDec_answer(pair1.getValue());
        }
        SharedPreferences.Editor editorDes = context.getSharedPreferences("EngDescription", MODE_PRIVATE).edit();
        editorDes.putString(wrk_id, String.valueOf(description));
        Log.e("DescriptionHashMap...", String.valueOf(description));
        lableDetails.clear();
        valueDetails.clear();

//        Thread threadSetSubmit = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                db.productDao().setSingleEnginData(enginDescriptionData);
//            }
//        });
//        threadSetSubmit.start();

        // editor.putString("wrk_id", wrk_id);
        editorDes.apply();
        editor.apply();

        context.startActivity(intent);
    }

    //****************************************

    /*private void alertDialog(String uid, String title)
    {
        final Dialog dialog1 = new Dialog(context);
        dialog1.setContentView(R.layout.room_no_layout);
        final Spinner auxillaryEngNoSpinner = dialog1.findViewById(R.id.auxillaryEngSpinner);
        final Spinner auxillaryEngMaintTypeSpinner = dialog1.findViewById(R.id.auxillaryEngMaintTypeSpinner);
        final Spinner UnitNoSpinner = dialog1.findViewById(R.id.UnitNoSpinner);
        Button submitButton = dialog1.findViewById(R.id.auxillaryEngSubmit);

        auxillaryEngNo1 = new ArrayList<String>();
        auxillaryEngMantType = new ArrayList<String>();
        unitNo1 = new ArrayList<String>();

        if(auxEngDetails!=null){
            for (AuxEngDetails auxEngDetails:auxEngDetails) {

                if(auxEngDetails.getUid().equalsIgnoreCase(uid)){
                    auxillaryEngNo1.add(String.valueOf(auxEngDetails.getAuxillary_eng_number()));
                    auxillaryEngMantType.add(auxEngDetails.getAuxillary_eng_type());
                    unitNo1.add(String.valueOf(auxEngDetails.getUnit_no()));
                }
            }
        }else {
            auxillaryEngNo1.add("");
            auxillaryEngMantType.add("");
            unitNo1.add("");
        }

        ArrayAdapter auxEngNoAda = new ArrayAdapter(context, android.R.layout.simple_list_item_1, auxillaryEngNo1);
        ArrayAdapter auxEngMantTypeAda = new ArrayAdapter(context, android.R.layout.simple_list_item_1, auxillaryEngMantType);
        ArrayAdapter unitNoAda = new ArrayAdapter(context, android.R.layout.simple_list_item_1, unitNo1);

        auxillaryEngNoSpinner.setAdapter(auxEngNoAda);
        auxillaryEngMaintTypeSpinner.setAdapter(auxEngMantTypeAda);
        UnitNoSpinner.setAdapter(unitNoAda);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToIsolationActivity(uid,auxillaryEngNoSpinner.getSelectedItem().toString(),
                        auxillaryEngMaintTypeSpinner.getSelectedItem().toString(),
                        UnitNoSpinner.getSelectedItem().toString(),
                        title);



                dialog1.cancel();
            }
        });

        dialog1.show();
    }*/


    //*******************************************
    public void teamSelectionDialog(String uid, String title) {
        alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Alert Dialog");
        alertDialog.setIcon(R.drawable.ewarpletest);
        alertDialog.setMessage("Are you sure? This will begin a new task.");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                final Dialog dialog1 = new Dialog(context);
                dialog1.setCancelable(false);
                dialog1.setContentView(R.layout.team_selection_layout);
                LinearLayout rl = (LinearLayout) dialog1.findViewById(R.id.rl);
                Button submitButton = dialog1.findViewById(R.id.teamSelectionSubmitBtn);
                Button cancelButton = dialog1.findViewById(R.id.teamSelectionCancelBtn);
                EditText teamSearchView = dialog1.findViewById(R.id.teamSearchView);
                ImageView imageinfo = dialog1.findViewById(R.id.imageinfo);
                CheckBox checkedAllTeam = dialog1.findViewById(R.id.checkedAllTeam);
                recyclerView = dialog1.findViewById(R.id.recyclerViewTeamSelection);

                recyclerView.setLayoutManager(new LinearLayoutManager(context));
                db = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class,
                        ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

                teamSelectionLists = db.productDao().getTeamSelectionData();
                Log.e("test", String.valueOf(teamSelectionLists));
                for (TeamSelectionList teamSelectionList : teamSelectionLists) {
                    if (teamSelectionList.getUid().equals(Integer.parseInt(uid))) {
                        TeamSelectionList teamSelectionList1 = new TeamSelectionList();
                        teamSelectionList1.setRank(String.valueOf(teamSelectionList.getRank()));
                        teamSelectionList1.setUser_id(String.valueOf(teamSelectionList.getUser_id()));
                        teamSelectionList1.setFirst_name(teamSelectionList.getFirst_name() + " " + teamSelectionList.getLast_name());
                        list.add(teamSelectionList1);
                    }
                }

                teamSelectionAdapter = new TeamSelectionAdapter(list, context, new TeamSelectionInterFace() {
                    @Override
                    public void onTeamSelectionButtonClickListener(int i, TextView engineerCtgryTextView, TextView engineerNameTextView, TextView emplId) {
                        teamDetails(i, engineerCtgryTextView, engineerNameTextView, emplId);
                    }

                    @Override
                    public void onDescriptionButtonClickListener(int post, EditText editText, TextView textInputLayout) {

                    }
                });

                recyclerView.setAdapter(teamSelectionAdapter);
//                notifyDataSetChanged();

                teamSearchView.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        filter(charSequence.toString());
//                        teamSelectionAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                checkedAllTeam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (checkedAllTeam.isChecked()) {
                            teamSelectionAdapter.selectAll();
                        } else {
                            teamSelectionAdapter.unselectall();

                        }
                    }
                });

                imageinfo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

                        // Inflate the custom layout/view
                        View customView = inflater.inflate(R.layout.checklistinfolpop_up, null);
                        PopupWindow mPopupWindow = new PopupWindow(
                                customView,
                                RecyclerView.LayoutParams.WRAP_CONTENT,
                                RecyclerView.LayoutParams.WRAP_CONTENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if (Build.VERSION.SDK_INT >= 21) {
                            mPopupWindow.setElevation(5.0f);
                        }
                        TextView tvcatlist = (TextView) customView.findViewById(R.id.tvcatlist);
                        List<String> ranklist = new ArrayList<>();
                        ranklist1 = new ArrayList<>();
                        for (int i = 0; i < list.size(); i++) {
                            ranklist.add(list.get(i).getRank());
                            Log.e("listt", list.get(i).getRank());
                        }
                        for (int k = 0; k < ranklist.size(); k++) {
//                            int m = 0;
                            if (!ranklist1.contains(ranklist.get(k))) {
                                ranklist1.add(ranklist.get(k));
//                                m++;
                                Log.e("adddd", ranklist.get(k));
                                StringBuilder stringBuilder = new StringBuilder();
                                for (String teamlist : ranklist1) {
                                    stringBuilder.append("* " + teamlist + "\n");
                                }
                                tvcatlist.setText(stringBuilder.toString());
                            }

                        }


                        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.ib_close);

                        // Set a click listener for the popup window close button
                        closeButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Dismiss the popup window
                                mPopupWindow.dismiss();
                            }
                        });
                        mPopupWindow.showAtLocation(rl, Gravity.TOP, 0, 100);
                    }
                });
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        List<String> rankpresentList = new ArrayList<>();
                        rankPresent1 = new ArrayList<>();
                        for (int i = 0; i < list.size(); i++) {
                            rankpresentList.add(list.get(i).getRank());
                            Log.e("listt", list.get(i).getRank());
                        }
                        for (int k = 0; k < rankpresentList.size(); k++) {
                            int m = 0;
                            if (!rankPresent1.contains(rankpresentList.get(k))) {
                                rankPresent1.add(rankpresentList.get(k));
                                selectionRank.put(String.valueOf(k), rankpresentList.get(k));
                                Log.e("rankpresent", rankpresentList.get(k));

                            }
                        }
                        List<UserDetails> userDetails = new ArrayList<>();
                        db = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                        userDetails = db.productDao().getUserDetail();
                        Set<String> firstlist = new HashSet<String>(firstTeamList.values());
                        Set<String> secondlist = new HashSet<String>(selectionRank.values());

                        firstlist.add(userDetails.get(0).getRank());
                        if (!rankPresent1.contains(userDetails.get(0).getRank())) {
                            secondlist.add(userDetails.get(0).getRank());
                        }

                        Log.e("firstlist", String.valueOf(firstTeamList.values()));
                        Log.e("secondlist", String.valueOf(selectionRank.values()));
//                       StringBuilder leftRankList = new StringBuilder();
                        boolean listt = firstlist.equals(secondlist);
//                        for(int i = 0;i<secondlist.size();i++){
//                            Iterator itr = firstlist.iterator();
//                            if(!secondlist.equals(itr.hasNext())){
//                              leftRankList.append(itr.next());
//                          }
//                        }

                        if (listt == true) {
                            submitButton.setEnabled(false);
                            try {
                                JSONArray listArray = new JSONArray();
                                List<TeamSelectionList> list = new ArrayList<>();
                                List<TeamSelectionList> list1 = new ArrayList<>();

                                list = db.productDao().getTeamSelectionData();


                                for (int i = 0; i < list.size(); i++) {
                                    if (list.get(i).getUid().equals(Integer.parseInt(uid))) {
                                        list1.add(list.get(i));
                                    }
                                }

                                //Add teamlead in user access point list.
                                int c_user = 0;
                                JSONObject c_userPoint = new JSONObject();
                                c_userPoint.put("user_id", userDetails.get(0).getUser_id());
                                c_userPoint.put("uid", uid);
                                c_userPoint.put("rank_id", userDetails.get(0).getRank_id());
                                c_userPoint.put("name", userDetails.get(0).getFirst_name());
                                c_userPoint.put("rank", userDetails.get(0).getRank());
                                if (c_user == 0) {
                                    listArray.put(c_userPoint);
                                    c_user = 1;
                                }//End
                                for (Map.Entry<String, String> entry : firstTeamList.entrySet()) {
                                    entry.getKey();
                                    entry.getValue();
                                    JSONObject valueObject1 = new JSONObject();
                                    valueObject1.put("user_id", entry.getKey());
                                    valueObject1.put("uid", uid);
                                    for (int i = 0; i < list.size(); i++) {
                                        if (entry.getKey().equals(list.get(i).getUser_id())) {
                                            valueObject1.put("rank_id", list.get(i).getRank_id());
                                            valueObject1.put("name", list.get(i).getFirst_name());
                                            valueObject1.put("rank", list.get(i).getRank());
                                        }
                                    }
                                    listArray.put(valueObject1);
                                    Log.e("array...", String.valueOf(listArray));

                                }
                                teammember1.put("teamdtls", listArray);


                                Iterator<Map.Entry<String, String>> it3 = secondTeamlist.entrySet().iterator();
                                while (it3.hasNext()) {
                                    Map.Entry<String, String> pair = it3.next();
                                    System.out.println(pair.getKey() + " = " + pair.getValue());
                                }

                                firstTeamList.clear();
                                secondTeamlist.clear();
                                List<AuxEngLabel> AuxEnglist1 = new ArrayList<>();
                                auxEngLabelList = db.productDao().getAllAuxEngLebels();

                                for (AuxEngLabel auxEngLabel : auxEngLabelList) {
                                    if (auxEngLabel.getUid().equals(uid)) {
                                        AuxEngLabel auxEngLabel1 = new AuxEngLabel();
                                        auxEngLabel1.setEng_label(auxEngLabel.getEng_label());
                                        AuxEnglist1.add(auxEngLabel1);
                                    }
                                }
                                if (AuxEnglist1.size() == 0) {
                                    String strDate = new UtilClassFuntions().currentDateTime();

                                    checklistdata1.put("uid", uid);
                                    checklistdata1.put("grtd_by", userDetails.get(0).getUser_id());
                                    checklistdata1.put("crtntime", strDate);
                                    checklistdata1.put("user_id", userDetails.get(0).getUser_id());
                                    checklistdata1.put("wrk_status", 0);
                                    checklistdata1.put("dbnm", userDetails.get(0).getDbnm());
                                    checklistdata1.put("accessToken", userDetails.get(0).getAccessToken());

                                    String dataKey = "checklistdata";
                                    String dataKey1 = "teammember";

//                                    Log.e("employeid", list.get(0).getEmployee_id());
                                    Log.e("id", uid);

                                    MultipartUtility multipart = new MultipartUtility(ApiUrlClass.create, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember1);

                                    Log.e("a", String.valueOf(checklistdata1));
                                    Log.e("b", String.valueOf(teammember1));
                                    Log.e("c", dataKey);
                                    Log.e("d", dataKey1);

                                    List<String> response = multipart.finish();
                                    System.out.println("SERVER REPLIED:");
                                    String wrk_id = "";
                                    for (String line : response) {
                                        System.out.println(line);
                                        Log.e("CheckList:res:wrk_id", line);
                                        JSONObject jsonObject = new JSONObject(line);
                                        Integer res = jsonObject.getInt("response");

                                        if (res.equals(200)) {
                                            wrk_id = jsonObject.getString("wrk_id");
                                            Log.d("wrkId:", wrk_id);
                                            if (!wrk_id.equals("")) {

                                                //******************************************
                                                ObjectMapper objectMapper = new ObjectMapper();
                                                List<UserPointAccessData> userPointAccessDataList = new ArrayList<>();
                                                JSONArray userAccessPoint = jsonObject.getJSONArray("userpoint");


                                                if (userAccessPoint != null && userAccessPoint.length() >= 0) {
                                                    for (int i = 0; i < userAccessPoint.length(); i++) {
                                                        JSONObject jsonObject1 = (JSONObject) userAccessPoint.get(i);
                                                        UserPointAccessData userPointAccessData1 = objectMapper.readValue
                                                                (jsonObject1.toString(), UserPointAccessData.class);
                                                        userPointAccessDataList.add(userPointAccessData1);

                                                    }
                                                }

//
                                                Log.e("ddata", "saved...");
                                                //*********************************************************
                                                MasterWorkId masterWorkId = new MasterWorkId();
                                                masterWorkId.setWrk_id(Integer.valueOf(wrk_id));
                                                masterWorkId.setGrtd_by(list.get(0).getUser_id());
                                                masterWorkId.setWrk_id_crtn_dt(strDate);
                                                masterWorkId.setUid(Integer.valueOf(uid));
                                                masterWorkId.setModified_dt(strDate);
                                                masterWorkId.setWrk_status(0);
                                                masterWorkId.setDelete_id(0);
                                                masterWorkId.setTeam_list(String.valueOf(teammember1.getJSONArray("teamdtls")));
                                                Log.e("tl...", String.valueOf(teammember1.getJSONArray("teamdtls")));


                                                List<CheckList> checkLists = new ArrayList<>();
                                                checkLists = db.productDao().getAllCheckList();
                                                for (int i = 0; i < checkLists.size(); i++) {
                                                    if (checkLists.get(i).getUid().equals(uid)) {
                                                        titleRefNo = db.productDao().getAllCheckList().get(i).getShort_nm()
                                                                + "/" + strDate.substring(0, 4) + "/" + wrk_id + "/" + "M";
                                                        masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                                        masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                                        masterWorkId.setChecklist_type(db.productDao().getAllCheckList().get(i).getChecklist_type());
                                                    }
                                                }

                                                Thread threadSetSubmit = new Thread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                                    }
                                                });
                                                threadSetSubmit.start();

                                            }
                                        } else if (res.equals(10003)) {
                                            new LogoutClass().logoutMethod(context);
                                        }
                                    }

                                    List<CheckList> lists = new ArrayList<>();
                                    String title = "";
                                    String checklist_type = "";
                                    lists = db.productDao().getAllCheckList();
                                    for (int i = 0; i < lists.size(); i++) {
                                        if (lists.get(i).getUid().equals(uid)) {
                                            title = lists.get(i).getList().toUpperCase();
                                            checklist_type = lists.get(i).getChecklist_type();
                                        }
                                    }
                                    String engNo = "", engType = "", unitNo = "";
                                    navigateToIsolationActivity(uid, engNo, engType, unitNo, title, wrk_id, checklist_type);
                                } else {
                                    descriptionDialog(uid, title);
                                }

                                // navigateToIsolationActivity(uid,engNo,engType,unitNo,title);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (JsonParseException e) {
                                e.printStackTrace();
                            } catch (JsonMappingException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                            dialog1.cancel();
                            list.clear();
                            firstTeamList.clear();
                            secondTeamlist.clear();
                        } else {
                            Toast.makeText(context, "Task cannot begin! Please select at least one user from each rank.", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        list.clear();
                        firstTeamList.clear();
                        secondTeamlist.clear();
                    }
                });

                dialog1.show();
                Window window = dialog1.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(context, "No", Toast.LENGTH_SHORT).show();
            }
        });
        alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alertDialog.show();

    }


    private void offlineDescriptionDialog(String uid, String title) {

        Dialog dialog2 = new Dialog(context);
        dialog2.setCancelable(false);
        dialog2.setContentView(R.layout.description_dialog);
        Button submitButton1 = dialog2.findViewById(R.id.descriptionSubmitBtn1);
        Button cancelButton1 = dialog2.findViewById(R.id.descriptionCancelBtn1);
        ProgressBar prDescription = dialog2.findViewById(R.id.prDescription);

        RecyclerView descriptionRecyclerView = dialog2.findViewById(R.id.description_recyclerView);

        descriptionRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        List<AuxEngLabel> list1 = new ArrayList<>();


        db = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        auxEngLabelList = db.productDao().getAllAuxEngLebels();

        for (AuxEngLabel auxEngLabel : auxEngLabelList) {
            if (auxEngLabel.getUid().equals(uid)) {
                AuxEngLabel auxEngLabel1 = new AuxEngLabel();
                auxEngLabel1.setEng_label(auxEngLabel.getEng_label());
                list1.add(auxEngLabel1);
            }
        }

        descriptionRecyclerView.setAdapter(new EngDescriptionAdapter(list1, context, new TeamSelectionInterFace() {
            @Override
            public void onTeamSelectionButtonClickListener(int i, TextView engineerCtgryTextView, TextView engineerNameTextView, TextView emplId) {

            }

            @Override
            public void onDescriptionButtonClickListener(int post, EditText editText, TextView textInputLayout) {
                description(post, editText, textInputLayout);
            }
        }));

        submitButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<EnginDescriptionData> eDD = new ArrayList<>();

                if (valueDetails.size() == list1.size()) {
                    prDescription.setVisibility(View.VISIBLE);
                    submitButton1.setEnabled(false);

                    try {
                        JSONArray jsonArray = new JSONArray();
                        Iterator<Map.Entry<Integer, String>> it = valueDetails.entrySet().iterator();
                        Iterator<Map.Entry<Integer, String>> it1 = lableDetails.entrySet().iterator();
                        while (it.hasNext() && it1.hasNext()) {
                            Map.Entry<Integer, String> label = it1.next();
                            Map.Entry<Integer, String> value = it.next();

                            System.out.println(value.getKey() + " = " + value.getValue());


                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(label.getValue(), value.getValue());
                            jsonArray.put(jsonObject);

                            Log.e("checklistdata", String.valueOf(checklistdata1));


                            EnginDescriptionData eDD1 = new EnginDescriptionData();
                            JSONObject obj=new JSONObject();
                            obj=jsonArray.getJSONObject(0);
                            eDD1.setUser_id(userDetails.get(0).getUser_id());
                            eDD1.setUid(Integer.valueOf(uid));
                            eDD1.setDecription(label.getValue());
                            eDD1.setDec_answer(value.getValue());
                            eDD1.setComp_name(userDetails.get(0).getComp_name());
                            eDD.add(eDD1);

                        }
                        checklistdata1.put("description", jsonArray);
                        List<UserDetails> list = new ArrayList<>();
                        db = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                        list = db.productDao().getUserDetail();

                        Date date = new Date();
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String strDate = formatter.format(date);


                        checklistdata1.put("uid", uid);
                        checklistdata1.put("rank_id", list.get(0).getRank_id());
                        checklistdata1.put("grtd_by", list.get(0).getEmployee_id());
                        checklistdata1.put("cmp_name", list.get(0).getComp_name());
                        checklistdata1.put("crtntime", strDate);
                        checklistdata1.put("device_id", uid);
                        checklistdata1.put("user_id", list.get(0).getUser_id());
                        checklistdata1.put("wrk_status", 0);
                        checklistdata1.put("dbnm", list.get(0).getDbnm());
                        checklistdata1.put("accessToken", list.get(0).getAccessToken());

                        String dataKey = "checklistdata";
                        String dataKey1 = "teammember";

//                        Log.e("employeid", list.get(0).getEmployee_id());
//                    Log.e("rankid", list.get(0).getRank_id());
                        Log.e("id", uid);

//                        MultipartUtility multipart = new MultipartUtility(ApiUrlClass.create, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember1);

                        Log.e("a", String.valueOf(checklistdata1));
                        Log.e("b", String.valueOf(teammember1));
                        Log.e("c", dataKey);
                        Log.e("d", dataKey1);

                        MasterWorkId masterWorkId = new MasterWorkId();
                        masterWorkId.setWrk_id(tempId);
                        masterWorkId.setGrtd_by(list.get(0).getUser_id());
                        masterWorkId.setWrk_id_crtn_dt(strDate);
                        masterWorkId.setUid(Integer.valueOf(uid));
                        masterWorkId.setModified_dt(strDate);
                        masterWorkId.setWrk_status(0);
                        masterWorkId.setDelete_id(0);
                        masterWorkId.setTemp_Wrk_id(tempId);
                        masterWorkId.setDescription(jsonArray.toString());
                        masterWorkId.setTeam_list(String.valueOf(teammember1.getJSONArray("teamdtls")));
                        Log.e("tl1...", String.valueOf(teammember1.getJSONArray("teamdtls")));

                        List<CheckList> checkLists = new ArrayList<>();
                        checkLists = db.productDao().getAllCheckList();
                        for (int i = 0; i < checkLists.size(); i++) {
                            if (checkLists.get(i).getUid().equals(uid)) {
                                titleRefNo = db.productDao().getAllCheckList().get(i).getShort_nm()
                                        + "/" + strDate.substring(0, 4) + "/" + tempId + "/" + "M";
                                masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                masterWorkId.setChecklist_type(db.productDao().getAllCheckList().get(i).getChecklist_type());
                            }
                        }



                        for (int i=0; i<eDD.size(); i++){
                            eDD.get(i).setWrk_id(tempId);
                        }

                        Thread threadDescriptionData=new Thread(new Runnable() {
                            @Override
                            public void run() {
                                db.productDao().setDescriptioToTable(eDD);

                            }
                        });
                        threadDescriptionData.start();



                        Thread threadSetSubmit = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                            }
                        });
                        threadSetSubmit.start();

                        List<CheckList> lists = new ArrayList<>();
                        String title = "";
                        String checklist_type = "";
                        lists = db.productDao().getAllCheckList();
                        for (int i = 0; i < lists.size(); i++) {
                            if (lists.get(i).getUid().equals(uid)) {
                                title = lists.get(i).getList().toUpperCase();
                                checklist_type = lists.get(i).getChecklist_type();
                            }
                        }
                        String engNo = "", engType = "", unitNo = "";
                        navigateToIsolationActivity(uid, engNo, engType, unitNo, title, String.valueOf(tempId), checklist_type);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    dialog2.dismiss();
                } else {
                    Toast.makeText(context, "Description is mendatory !", Toast.LENGTH_SHORT).show();
                }
            }
        });


        cancelButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });
        dialog2.show();
        Window window = dialog2.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


    }

    private void description(int post, EditText editText, TextView textInputLayout) {
        if (valueDetails.containsKey(post)) {
            valueDetails.remove(post);
            lableDetails.remove(post);

            valueDetails.put(post, editText.getText().toString().toUpperCase());
            lableDetails.put(post, textInputLayout.getHint().toString().toUpperCase());
            Log.e("rm", String.valueOf(post));

            Log.e("put", String.valueOf(valueDetails));
            Log.e("put", String.valueOf(lableDetails));
        } else {
            valueDetails.put(post, editText.getText().toString().toUpperCase());
            lableDetails.put(post, textInputLayout.getHint().toString().toUpperCase());
            Log.e("detailsList", String.valueOf(valueDetails));
            Log.e("label", String.valueOf(lableDetails));
        }
    }

    private void teamDetails(int i, TextView engineerCtgryTextView, TextView engineerNameTextView, TextView emplId) {
        String getemplId = emplId.getText().toString();
        Log.e("idjk", "j" + getemplId);
        if (firstTeamList.containsKey(getemplId) && secondTeamlist.containsKey(getemplId)) {
            firstTeamList.remove(getemplId);
            secondTeamlist.remove(getemplId);

            Log.e("firstRemove", String.valueOf(firstTeamList) + emplId);
            Log.e("secondRemove", String.valueOf(secondTeamlist) + emplId);

        } else {

            firstTeamList.put(getemplId, engineerCtgryTextView.getText().toString());
            secondTeamlist.put(getemplId, engineerNameTextView.getText().toString());
//            selectionRank.put(getemplId, engineerCtgryTextView.getText().toString());
            Log.e("firstAll", String.valueOf(firstTeamList) + emplId);
            Log.e("secondAll", String.valueOf(secondTeamlist) + emplId);
        }

    }

    void filter(String text) {
        List<TeamSelectionList> temp = new ArrayList();
        temp.clear();
        if (text.length() == 0) {
            temp.addAll(list);
            Log.e("hdsfkuvsk", String.valueOf(list));
        } else {
            for (TeamSelectionList d : list) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.getRank().toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d);
                    Log.e("newlist", String.valueOf(temp));
                }
                if (d.getFirst_name().toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d);
                }
            }

        }
        teamSelectionAdapter.updateList(temp);
    }

    public Integer genrateTempWrkId() {
        String s= String.valueOf(System.currentTimeMillis());
        s=s.substring(3,s.length()-1);
        Integer tempWrk_id = Integer.valueOf(s);
        tempWrk_id=tempWrk_id*-1;
        return tempWrk_id;
    }
    // offline teamselection dialog
    public void offlineTeamSelectionDialog(String uid, String title) {
        alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Alert Dialog");
        alertDialog.setIcon(R.drawable.ewarpletest);
        alertDialog.setMessage("Are you sure? This will begin a new task.");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                final Dialog dialog1 = new Dialog(context);
                dialog1.setCancelable(false);
                dialog1.setContentView(R.layout.team_selection_layout);
                LinearLayout rl = (LinearLayout) dialog1.findViewById(R.id.rl);
                Button submitButton = dialog1.findViewById(R.id.teamSelectionSubmitBtn);
                Button cancelButton = dialog1.findViewById(R.id.teamSelectionCancelBtn);
                EditText teamSearchView = dialog1.findViewById(R.id.teamSearchView);
                ImageView imageinfo = dialog1.findViewById(R.id.imageinfo);
                CheckBox checkedAllTeam = dialog1.findViewById(R.id.checkedAllTeam);
                recyclerView = dialog1.findViewById(R.id.recyclerViewTeamSelection);

                recyclerView.setLayoutManager(new LinearLayoutManager(context));
                db = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class,
                        ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

                teamSelectionLists = db.productDao().getTeamSelectionData();
                Log.e("test", String.valueOf(teamSelectionLists));
                for (TeamSelectionList teamSelectionList : teamSelectionLists) {
                    if (teamSelectionList.getUid().equals(Integer.parseInt(uid))) {
                        TeamSelectionList teamSelectionList1 = new TeamSelectionList();
                        teamSelectionList1.setRank(String.valueOf(teamSelectionList.getRank()));
                        teamSelectionList1.setUser_id(String.valueOf(teamSelectionList.getUser_id()));
                        teamSelectionList1.setFirst_name(teamSelectionList.getFirst_name() + " " + teamSelectionList.getLast_name());
                        list.add(teamSelectionList1);
                    }
                }

                teamSelectionAdapter = new TeamSelectionAdapter(list, context, new TeamSelectionInterFace() {
                    @Override
                    public void onTeamSelectionButtonClickListener(int i, TextView engineerCtgryTextView, TextView engineerNameTextView, TextView emplId) {
                        teamDetails(i, engineerCtgryTextView, engineerNameTextView, emplId);
                    }

                    @Override
                    public void onDescriptionButtonClickListener(int post, EditText editText, TextView textInputLayout) {

                    }
                });

                recyclerView.setAdapter(teamSelectionAdapter);
//                notifyDataSetChanged();

                teamSearchView.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        filter(charSequence.toString());
//                        teamSelectionAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                checkedAllTeam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (checkedAllTeam.isChecked()) {
                            teamSelectionAdapter.selectAll();
                        } else {
                            teamSelectionAdapter.unselectall();

                        }
                    }
                });

                imageinfo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

                        // Inflate the custom layout/view
                        View customView = inflater.inflate(R.layout.checklistinfolpop_up, null);
                        PopupWindow mPopupWindow = new PopupWindow(
                                customView,
                                RecyclerView.LayoutParams.WRAP_CONTENT,
                                RecyclerView.LayoutParams.WRAP_CONTENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if (Build.VERSION.SDK_INT >= 21) {
                            mPopupWindow.setElevation(5.0f);
                        }
                        TextView tvcatlist = (TextView) customView.findViewById(R.id.tvcatlist);
                        List<String> ranklist = new ArrayList<>();
                        ranklist1 = new ArrayList<>();
                        for (int i = 0; i < list.size(); i++) {
                            ranklist.add(list.get(i).getRank());
                            Log.e("listt", list.get(i).getRank());
                        }
                        for (int k = 0; k < ranklist.size(); k++) {
//                            int m = 0;
                            if (!ranklist1.contains(ranklist.get(k))) {
                                ranklist1.add(ranklist.get(k));
//                                m++;
                                Log.e("adddd", ranklist.get(k));
                                StringBuilder stringBuilder = new StringBuilder();
                                for (String teamlist : ranklist1) {
                                    stringBuilder.append("* " + teamlist + "\n");
                                }
                                tvcatlist.setText(stringBuilder.toString());
                            }

                        }


                        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.ib_close);

                        // Set a click listener for the popup window close button
                        closeButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Dismiss the popup window
                                mPopupWindow.dismiss();
                            }
                        });
                        mPopupWindow.showAtLocation(rl, Gravity.TOP, 0, 100);
                    }
                });
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        List<String> rankpresentList = new ArrayList<>();
                        rankPresent1 = new ArrayList<>();
                        for (int i = 0; i < list.size(); i++) {
                            rankpresentList.add(list.get(i).getRank());
                            Log.e("listt", list.get(i).getRank());
                        }
                        for (int k = 0; k < rankpresentList.size(); k++) {
                            int m = 0;
                            if (!rankPresent1.contains(rankpresentList.get(k))) {
                                rankPresent1.add(rankpresentList.get(k));
                                selectionRank.put(String.valueOf(k), rankpresentList.get(k));
                                Log.e("rankpresent", rankpresentList.get(k));

                            }
                        }
                        List<UserDetails> userDetails = new ArrayList<>();
                        db = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                        userDetails = db.productDao().getUserDetail();
                        Set<String> firstlist = new HashSet<String>(firstTeamList.values());
                        Set<String> secondlist = new HashSet<String>(selectionRank.values());

                        firstlist.add(userDetails.get(0).getRank());
                        if (!rankPresent1.contains(userDetails.get(0).getRank())) {
                            secondlist.add(userDetails.get(0).getRank());
                        }

                        Log.e("firstlist", String.valueOf(firstTeamList.values()));
                        Log.e("secondlist", String.valueOf(selectionRank.values()));
//                       StringBuilder leftRankList = new StringBuilder();
                        boolean listt = firstlist.equals(secondlist);
//                        for(int i = 0;i<secondlist.size();i++){
//                            Iterator itr = firstlist.iterator();
//                            if(!secondlist.equals(itr.hasNext())){
//                              leftRankList.append(itr.next());
//                          }
//                        }

                        if (listt == true) {
                            submitButton.setEnabled(false);
                            try {
                                JSONArray listArray = new JSONArray();
                                List<TeamSelectionList> list = new ArrayList<>();
                                List<TeamSelectionList> list1 = new ArrayList<>();

                                list = db.productDao().getTeamSelectionData();


                                for (int i = 0; i < list.size(); i++) {
                                    if (list.get(i).getUid().equals(Integer.parseInt(uid))) {
                                        list1.add(list.get(i));
                                    }
                                }

                                //Add teamlead in user access point list.
                                int c_user = 0;
                                JSONObject c_userPoint = new JSONObject();
                                c_userPoint.put("user_id", userDetails.get(0).getUser_id());
                                c_userPoint.put("uid", uid);
                                c_userPoint.put("rank_id", userDetails.get(0).getRank_id());
                                c_userPoint.put("name", userDetails.get(0).getFirst_name());
                                c_userPoint.put("rank", userDetails.get(0).getRank());
                                if (c_user == 0) {
                                    listArray.put(c_userPoint);
                                    c_user = 1;
                                }//End
                                for (Map.Entry<String, String> entry : firstTeamList.entrySet()) {
                                    entry.getKey();
                                    entry.getValue();
                                    JSONObject valueObject1 = new JSONObject();
                                    valueObject1.put("user_id", entry.getKey());
                                    valueObject1.put("uid", uid);
                                    for (int i = 0; i < list.size(); i++) {
                                        if (entry.getKey().equals(list.get(i).getUser_id())) {
                                            valueObject1.put("rank_id", list.get(i).getRank_id());
                                            valueObject1.put("name", list.get(i).getFirst_name());
                                            valueObject1.put("rank", list.get(i).getRank());
                                        }
                                    }
                                    listArray.put(valueObject1);
                                    Log.e("array...", String.valueOf(listArray));

                                }
                                teammember1.put("teamdtls", listArray);


                                Iterator<Map.Entry<String, String>> it3 = secondTeamlist.entrySet().iterator();
                                while (it3.hasNext()) {
                                    Map.Entry<String, String> pair = it3.next();
                                    System.out.println(pair.getKey() + " = " + pair.getValue());
                                }

                                firstTeamList.clear();
                                secondTeamlist.clear();
                                List<AuxEngLabel> AuxEnglist1 = new ArrayList<>();
                                auxEngLabelList = db.productDao().getAllAuxEngLebels();

                                for (AuxEngLabel auxEngLabel : auxEngLabelList) {
                                    if (auxEngLabel.getUid().equals(uid)) {
                                        AuxEngLabel auxEngLabel1 = new AuxEngLabel();
                                        auxEngLabel1.setEng_label(auxEngLabel.getEng_label());
                                        AuxEnglist1.add(auxEngLabel1);
                                    }
                                }
                                if (AuxEnglist1.size() == 0) {
                                    String strDate = new UtilClassFuntions().currentDateTime();

                                    checklistdata1.put("uid", uid);
                                    checklistdata1.put("grtd_by", userDetails.get(0).getUser_id());
                                    checklistdata1.put("crtntime", strDate);
                                    checklistdata1.put("user_id", userDetails.get(0).getUser_id());
                                    checklistdata1.put("wrk_status", 0);
                                    checklistdata1.put("dbnm", userDetails.get(0).getDbnm());
                                    checklistdata1.put("accessToken", userDetails.get(0).getAccessToken());

                                    String dataKey = "checklistdata";
                                    String dataKey1 = "teammember";

//                                    Log.e("employeid", list.get(0).getEmployee_id());
                                    Log.e("id", uid);

//                                    MultipartUtility multipart = new MultipartUtility(ApiUrlClass.create, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember1);

                                    Log.e("a", String.valueOf(checklistdata1));
                                    Log.e("b", String.valueOf(teammember1));
                                    Log.e("c", dataKey);
                                    Log.e("d", dataKey1);

                                    MasterWorkId masterWorkId = new MasterWorkId();
                                    masterWorkId.setWrk_id(tempId);
                                    masterWorkId.setGrtd_by(list.get(0).getUser_id());
                                    masterWorkId.setWrk_id_crtn_dt(strDate);
                                    masterWorkId.setUid(Integer.valueOf(uid));
                                    masterWorkId.setModified_dt(strDate);
                                    masterWorkId.setWrk_status(0);
                                    masterWorkId.setDelete_id(0);
                                    masterWorkId.setTemp_Wrk_id(tempId);
                                    masterWorkId.setTeam_list(String.valueOf(teammember1.getJSONArray("teamdtls")));
                                    Log.e("tl...", String.valueOf(teammember1.getJSONArray("teamdtls")));


                                    List<CheckList> checkLists = new ArrayList<>();
                                    checkLists = db.productDao().getAllCheckList();
                                    for (int i = 0; i < checkLists.size(); i++) {
                                        if (checkLists.get(i).getUid().equals(uid)) {
                                            titleRefNo = db.productDao().getAllCheckList().get(i).getShort_nm()
                                                    + "/" + strDate.substring(0, 4) + "/" + tempId + "/" + "M";
                                            masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                            masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                            masterWorkId.setChecklist_type(db.productDao().getAllCheckList().get(i).getChecklist_type());
                                        }
                                    }

                                    Thread threadSetSubmit = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                        }
                                    });
                                    threadSetSubmit.start();

                                    List<CheckList> lists = new ArrayList<>();
                                    String title = "";
                                    String checklist_type = "";
                                    lists = db.productDao().getAllCheckList();
                                    for (int i = 0; i < lists.size(); i++) {
                                        if (lists.get(i).getUid().equals(uid)) {
                                            title = lists.get(i).getList().toUpperCase();
                                            checklist_type = lists.get(i).getChecklist_type();
                                        }
                                    }
                                    String engNo = "", engType = "", unitNo = "";
                                    navigateToIsolationActivity(uid, engNo, engType, unitNo, title, String.valueOf(tempId), checklist_type);
                                } else {
                                    offlineDescriptionDialog(uid, title);
                                }

                                // navigateToIsolationActivity(uid,engNo,engType,unitNo,title);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            dialog1.cancel();
                            list.clear();
                            firstTeamList.clear();
                            secondTeamlist.clear();
                        } else {
                            Toast.makeText(context, "Task cannot begin! Please select at least one user from each rank.", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        list.clear();
                        firstTeamList.clear();
                        secondTeamlist.clear();
                    }
                });

                dialog1.show();
                Window window = dialog1.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(context, "No", Toast.LENGTH_SHORT).show();
            }
        });
        alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alertDialog.show();
}

    private void descriptionDialog(String uid, String title) {

        Dialog dialog2 = new Dialog(context);
        dialog2.setCancelable(false);
        dialog2.setContentView(R.layout.description_dialog);
        Button submitButton1 = dialog2.findViewById(R.id.descriptionSubmitBtn1);
        Button cancelButton1 = dialog2.findViewById(R.id.descriptionCancelBtn1);
        ProgressBar prDescription = dialog2.findViewById(R.id.prDescription);

        RecyclerView descriptionRecyclerView = dialog2.findViewById(R.id.description_recyclerView);

        descriptionRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        List<AuxEngLabel> list1 = new ArrayList<>();


        db = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        auxEngLabelList = db.productDao().getAllAuxEngLebels();

        for (AuxEngLabel auxEngLabel : auxEngLabelList) {
            if (auxEngLabel.getUid().equals(uid)) {
                AuxEngLabel auxEngLabel1 = new AuxEngLabel();
                auxEngLabel1.setEng_label(auxEngLabel.getEng_label());
                list1.add(auxEngLabel1);
            }
        }

        descriptionRecyclerView.setAdapter(new EngDescriptionAdapter(list1, context, new TeamSelectionInterFace() {
            @Override
            public void onTeamSelectionButtonClickListener(int i, TextView engineerCtgryTextView, TextView engineerNameTextView, TextView emplId) {

            }

            @Override
            public void onDescriptionButtonClickListener(int post, EditText editText, TextView textInputLayout) {
                description(post, editText, textInputLayout);
            }
        }));

        submitButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<EnginDescriptionData> eDD = new ArrayList<>();

                if (valueDetails.size() == list1.size()) {
                    prDescription.setVisibility(View.VISIBLE);
                    submitButton1.setEnabled(false);

                    try {
                        JSONArray jsonArray = new JSONArray();
                        Iterator<Map.Entry<Integer, String>> it = valueDetails.entrySet().iterator();
                        Iterator<Map.Entry<Integer, String>> it1 = lableDetails.entrySet().iterator();
                        while (it.hasNext() && it1.hasNext()) {
                            Map.Entry<Integer, String> label = it1.next();
                            Map.Entry<Integer, String> value = it.next();

                            System.out.println(value.getKey() + " = " + value.getValue());


                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(label.getValue(), value.getValue());
                            jsonArray.put(jsonObject);

                            Log.e("checklistdata", String.valueOf(checklistdata1));


                            EnginDescriptionData eDD1 = new EnginDescriptionData();
                            JSONObject obj=new JSONObject();
                            obj=jsonArray.getJSONObject(0);
                            eDD1.setUser_id(userDetails.get(0).getUser_id());
                            eDD1.setUid(Integer.valueOf(uid));
                            eDD1.setDecription(label.getValue());
                            eDD1.setDec_answer(value.getValue());
                            eDD1.setComp_name(userDetails.get(0).getComp_name());
                            eDD.add(eDD1);


                        }

                        checklistdata1.put("description", jsonArray);
                        List<UserDetails> list = new ArrayList<>();
                        db = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                        list = db.productDao().getUserDetail();

                        Date date = new Date();
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String strDate = formatter.format(date);


                        checklistdata1.put("uid", uid);
                        checklistdata1.put("rank_id", list.get(0).getRank_id());
                        checklistdata1.put("grtd_by", list.get(0).getEmployee_id());
                        checklistdata1.put("cmp_name", list.get(0).getComp_name());
                        checklistdata1.put("crtntime", strDate);
                        checklistdata1.put("device_id", uid);
                        checklistdata1.put("user_id", list.get(0).getUser_id());
                        checklistdata1.put("wrk_status", 0);
                        checklistdata1.put("dbnm", list.get(0).getDbnm());
                        checklistdata1.put("accessToken", list.get(0).getAccessToken());

                        String dataKey = "checklistdata";
                        String dataKey1 = "teammember";

//                        Log.e("employeid", list.get(0).getEmployee_id());
//                    Log.e("rankid", list.get(0).getRank_id());
                        Log.e("id", uid);

                        MultipartUtility multipart = new MultipartUtility(ApiUrlClass.create, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember1);

                        Log.e("a", String.valueOf(checklistdata1));
                        Log.e("b", String.valueOf(teammember1));
                        Log.e("c", dataKey);
                        Log.e("d", dataKey1);

                        List<String> response = multipart.finish();
                        System.out.println("SERVER REPLIED:");
                        String wrk_id = "";
                        for (String line : response) {
                            System.out.println(line);
                            Log.e("CheckList:res:wrk_id", line);
                            JSONObject jsonObject = new JSONObject(line);
                            Integer res = jsonObject.getInt("response");

                            if (res.equals(200)) {
                                wrk_id = jsonObject.getString("wrk_id");
                                Log.d("wrkId:", wrk_id);
                                if (!wrk_id.equals("")) {

                                    //******************************************
                                    ObjectMapper objectMapper = new ObjectMapper();
                                    List<UserPointAccessData> userPointAccessDataList = new ArrayList<>();
                                    JSONArray userAccessPoint = jsonObject.getJSONArray("userpoint");


                                    if (userAccessPoint != null && userAccessPoint.length() >= 0) {
                                        for (int i = 0; i < userAccessPoint.length(); i++) {
                                            JSONObject jsonObject1 = (JSONObject) userAccessPoint.get(i);
                                            UserPointAccessData userPointAccessData1 = objectMapper.readValue
                                                    (jsonObject1.toString(), UserPointAccessData.class);
                                            userPointAccessDataList.add(userPointAccessData1);

                                        }
                                    }

//                            Thread threadDeleteAllTables=new Thread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    db.productDao().updateToTable(userPointAccessDataList);
//
//                                }
//                            });
//                            threadDeleteAllTables.start();
                                    Log.e("ddata", "saved...");
                                    //*********************************************************

                                    MasterWorkId masterWorkId = new MasterWorkId();
                                    masterWorkId.setWrk_id(Integer.valueOf(wrk_id));
                                    masterWorkId.setGrtd_by(list.get(0).getUser_id());
                                    masterWorkId.setWrk_id_crtn_dt(strDate);
                                    masterWorkId.setUid(Integer.valueOf(uid));
                                    masterWorkId.setModified_dt(strDate);
                                    masterWorkId.setWrk_status(0);
                                    masterWorkId.setDelete_id(0);
                                    masterWorkId.setTeam_list(String.valueOf(teammember1.getJSONArray("teamdtls")));
                                    Log.e("tl1...", String.valueOf(teammember1.getJSONArray("teamdtls")));

                                    List<CheckList> checkLists = new ArrayList<>();
                                    checkLists = db.productDao().getAllCheckList();
                                    for (int i = 0; i < checkLists.size(); i++) {
                                        if (checkLists.get(i).getUid().equals(uid)) {
                                            titleRefNo = db.productDao().getAllCheckList().get(i).getShort_nm()
                                                    + "/" + strDate.substring(0, 4) + "/" + wrk_id + "/" + "M";
                                            masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                            masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                            masterWorkId.setChecklist_type(db.productDao().getAllCheckList().get(i).getChecklist_type());
                                        }
                                    }

                                    for (int i=0; i<eDD.size(); i++){
                                        eDD.get(i).setWrk_id(Integer.valueOf(wrk_id));
                                    }

                                    Thread threadDescriptionData=new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    db.productDao().setDescriptioToTable(eDD);

                                }
                            });
                            threadDescriptionData.start();


//                            **************************************


//                            List<EnginDescriptionData> enginDD=new ArrayList<>();
//                            while (it.hasNext() && it1.hasNext()) {
//                                Map.Entry<Integer, String> label = it1.next();
//                                Map.Entry<Integer, String> value = it.next();
//
//                                EnginDescriptionData enginDescriptionData = new EnginDescriptionData();
//                                enginDescriptionData.setUid(Integer.parseInt(uid));
//                                enginDescriptionData.setEntry_time(strDate);
//                                enginDescriptionData.setWrk_id(Integer.parseInt(wrk_id));
//                                enginDescriptionData.setDecription(label.getValue());
//                                enginDescriptionData.setDec_answer(value.getValue());
//                                enginDescriptionData.setComp_name(list.get(0).getComp_name());
//                                enginDescriptionData.setEmp_id(list.get(0).getEmployee_id());
//                                enginDD.add(enginDescriptionData);
//                            }
//
//                            Thread threadDescriptionData=new Thread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    db.productDao().setEnginDescriptionData(enginDD);
//
//                                }
//                            });
//                            threadDescriptionData.start();
//                            List<EnginDescriptionData> edd=new ArrayList<>();
//                            edd=db.productDao().getEnginDescriptionData();
//
//                            for (int i=0;i<edd.size();i++){
//                                Log.e("print", String.valueOf(edd.get(i).getWrk_id()));
//                            }
//

//                            **************************************


                                    Thread threadSetSubmit = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));
                                        }
                                    });
                                    threadSetSubmit.start();

                                }
                            } else if (res.equals(10003)) {
                                new LogoutClass().logoutMethod(context);
                            }

                        }

                        List<CheckList> lists = new ArrayList<>();
                        String title = "";
                        String checklist_type = "";
                        lists = db.productDao().getAllCheckList();
                        for (int i = 0; i < lists.size(); i++) {
                            if (lists.get(i).getUid().equals(uid)) {
                                title = lists.get(i).getList().toUpperCase();
                                checklist_type = lists.get(i).getChecklist_type();
                            }
                        }
                        String engNo = "", engType = "", unitNo = "";
                        navigateToIsolationActivity(uid, engNo, engType, unitNo, title, wrk_id, checklist_type);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    dialog2.dismiss();
                } else {
                    Toast.makeText(context, "Description is mendatory !", Toast.LENGTH_SHORT).show();
                }
            }
        });


        cancelButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });
        dialog2.show();
        Window window = dialog2.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


    }

}