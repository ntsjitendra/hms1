package com.nuevotechsolutions.ewarp.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.interfaces.VesselImageInterface;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselComponent;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class VesselShipInspectionChecklistAdapter extends RecyclerView.Adapter
        <VesselShipInspectionChecklistAdapter.VesselShipInspectionChecklistViewHolder> {

    static List<VesselChecklistRecordDataList> vesselChecklistRecordDataLists = new ArrayList<>();
    //    ImageListDataAdapter itemListDataAdapter;
    String[] itemsList;
    String[] location, time;
    //    List<String> imagesList;
    VesselPscInspectionChecklistAdapter.VesselPscInspectionChecklistViewHolder vesselPscInspectionChecklistViewHolder;
    private Context context;
    private List<VesselComponent> vesselComponentList;
    private VesselImageInterface vesselImageInterface;
    private VesselPscInspectionChecklistAdapter.OnItemClicked onClick;
    private Map<Integer, List<String>> imageMap;
    private Map<Integer, List<String>> locationMap;
    private Map<Integer, List<String>> timeStampMap;
    private static String checklist_type;
    private static int lastDataSize=0;
    private int j=0;
    String tempTitleOfSection = "";

    /*ImageListDataAdapter itemListDataAdapter;*/
    private static String points;
    String image;

    public VesselShipInspectionChecklistAdapter(String points) {
        this.points = points;
        Log.e("ss", points);
    }
    public VesselShipInspectionChecklistAdapter(Context context,
                                               List<VesselComponent> vesselComponentList,
                                               List<VesselChecklistRecordDataList> vesselChecklistRecordDataLists,
                                               Map<Integer, List<String>> imageMap,
                                               Map<Integer, List<String>> locationMap,
                                               Map<Integer, List<String>> timeStampMap,
                                               String checklist_type,
                                               VesselImageInterface vesselImageInterface) {
        this.context = context;
        this.vesselComponentList = vesselComponentList;
        this.vesselChecklistRecordDataLists = vesselChecklistRecordDataLists;
        this.imageMap = imageMap;
        this.locationMap = locationMap;
        this.timeStampMap = timeStampMap;
        this.checklist_type = checklist_type;
        this.vesselImageInterface = vesselImageInterface;
        setHasStableIds(true);
        Log.e("componentList", String.valueOf(vesselComponentList.size()));
        Log.e("imageMap", String.valueOf(imageMap.size()));
        Log.e("Constructor", String.valueOf(vesselChecklistRecordDataLists.size()));
    }


    @NonNull
    @Override
    public VesselShipInspectionChecklistViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.vessel_ship_inspection_item, viewGroup, false);
        return new VesselShipInspectionChecklistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VesselShipInspectionChecklistViewHolder vesselShipInspectionChecklistViewHolder, int i) {
        int listSize=0;
        if (lastDataSize==0){
            listSize=vesselChecklistRecordDataLists.size();
        }else {
            listSize=lastDataSize;
            Log.e("listSize2", String.valueOf(listSize));
        }
        Log.e("VesselistSize", String.valueOf(listSize));
        String grades[]={"1","2","3","4","5","6","7","8","9","10"};

        ArrayAdapter<String> gradeArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, grades);
        vesselShipInspectionChecklistViewHolder.gradeSpinner.setAdapter(gradeArrayAdapter);
        vesselShipInspectionChecklistViewHolder.gradeSpinner.setSelection(8);

        vesselShipInspectionChecklistViewHolder.gradeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0 ||i == 1 || i == 2 ||i == 3){
                    vesselShipInspectionChecklistViewHolder.gradeSpinner.getSelectedView().setBackgroundColor(Color.RED);
                }else if(i == 4 ||i == 5 ){
                    vesselShipInspectionChecklistViewHolder.gradeSpinner.getSelectedView().setBackgroundColor(view.getResources().getColor(R.color.dark_yellow));
                }
                else if(i == 6 ||i == 7 ){
                    vesselShipInspectionChecklistViewHolder.gradeSpinner.getSelectedView().setBackgroundColor(view.getResources().getColor(R.color.blue));
                }
                else if(i == 8 ||i == 9 ){
                    vesselShipInspectionChecklistViewHolder.gradeSpinner.getSelectedView().setBackgroundColor(view.getResources().getColor(R.color.color_green));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        VesselComponent vesselComponent = vesselComponentList.get(i);
        List<String> imagesList = imageMap.get(i);
        List<String> locationList = locationMap.get(i);
        List<String> timeStampList = timeStampMap.get(i);

        ImageListDataAdapter itemListDataAdapter = null;

        if (tempTitleOfSection.equals("")) {
            tempTitleOfSection = vesselComponent.getTitle_of_section();
            vesselShipInspectionChecklistViewHolder.textViewTitleShip.setText(vesselComponent.getTitle_of_section());
        }

        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setVisibility(View.VISIBLE);
        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setHasFixedSize(true);
        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setAdapter(itemListDataAdapter);
        vesselShipInspectionChecklistViewHolder.photoButton1.setVisibility(View.VISIBLE);

        Log.e("checkSizebefore", String.valueOf(vesselChecklistRecordDataLists.size()));
        if (tempTitleOfSection.equals(vesselComponent.getTitle_of_section())) {
            if (i != 0)
                vesselShipInspectionChecklistViewHolder.textViewTitleShip.setVisibility(View.GONE);
        }else{
            vesselShipInspectionChecklistViewHolder.textViewTitleShip.setText(vesselComponent.getTitle_of_section());
            tempTitleOfSection = vesselComponent.getTitle_of_section();
        }
        vesselShipInspectionChecklistViewHolder.textView_questionPSC.setText(vesselComponent.getRb_points_description());

        itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, vesselShipInspectionChecklistViewHolder.submitButton1);
        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setHasFixedSize(true);
        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setAdapter(itemListDataAdapter);

       vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setVisibility(View.VISIBLE);
        itemListDataAdapter.notifyDataSetChanged();


        if (!vesselChecklistRecordDataLists.isEmpty()) {
            Log.e("checkSize", String.valueOf(vesselChecklistRecordDataLists.size()));
            for (int j = 0; j < vesselChecklistRecordDataLists.size(); j++) {
                Log.e("checkList:", vesselChecklistRecordDataLists.get(j).getAns());
                if (vesselChecklistRecordDataLists.get(j).getChecklist_points().equals(vesselComponent.getPoints())) {

                     vesselShipInspectionChecklistViewHolder.edit_cmntBtn.setEnabled(false);
                    vesselShipInspectionChecklistViewHolder.submitButton1.setEnabled(false);
                    vesselShipInspectionChecklistViewHolder.photoButton1.setEnabled(false);
                    vesselShipInspectionChecklistViewHolder.linearLayoutTimeStamp.setVisibility(View.VISIBLE);
                    Log.e("visible", "visible");
                    if (vesselChecklistRecordDataLists.get(j).getComment()!=null){
                        vesselShipInspectionChecklistViewHolder.Text_comments.setVisibility(View.VISIBLE);
                        String commentTitle="<b>"+"Comment:"+"</b>" + vesselChecklistRecordDataLists.get(j).getComment();
                        vesselShipInspectionChecklistViewHolder.Text_comments.setText(Html.fromHtml(commentTitle));
                    }

                    String lctn = vesselChecklistRecordDataLists.get(j).getCrnt_lctn();
                    if (lctn != null) {
                        String[] temt = lctn.split("_", 2);
                        lctn = temt[0];
                    }
                    String textSubmitTitle="<b>"+"Time:"+"</b>" + vesselChecklistRecordDataLists.get(j).getGnrtn_date() + "<b>"+"\nLocation:"+"</b>" + lctn;
                    vesselShipInspectionChecklistViewHolder.Text_submit.setText(Html.fromHtml(textSubmitTitle));

                    if (vesselChecklistRecordDataLists.get(j).getImage_data() != null && vesselChecklistRecordDataLists.get(j).getImage_data().contains("http")) {
                        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setVisibility(View.VISIBLE);

                        image = vesselChecklistRecordDataLists.get(j).getImage_data();
                        Log.e("image_data", image);
                        String[] images = StringUtils.substringsBetween(image, "\"", "\"");
                        if (images != null) {
                            imagesList.clear();
                            for (String temp : images) {
                                if (temp.contains("https")) {
                                    imagesList.add(temp);
                                }
                            }
                            Log.e("images", String.valueOf(imagesList));
                        } else {

                        }
                        itemsList = new String[imagesList.size()];
                        itemsList = imagesList.toArray(itemsList);

//                        ********************************

                        JSONArray imgArray = null;

                        try {
                            imgArray = new JSONArray(image);
                            for (int r = 0; r < imgArray.length(); r++) {
                                JSONObject imgObj = imgArray.getJSONObject(r);
                                locationList.add(imgObj.getString("imagelocation"));
                                timeStampList.add(imgObj.getString("imagetimedate"));
                            }
                            location = new String[locationList.size()];
                            time = new String[timeStampList.size()];
                            location = locationList.toArray(location);
                            time = timeStampList.toArray(time);
                            itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, vesselShipInspectionChecklistViewHolder.submitButton1);
                            vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setHasFixedSize(true);
                            vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                            vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setAdapter(itemListDataAdapter);
//                            itemListDataAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


//                        ********************************

                        itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, vesselShipInspectionChecklistViewHolder.submitButton1);
                        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setHasFixedSize(true);
                        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setAdapter(itemListDataAdapter);
                        itemListDataAdapter.notifyDataSetChanged();



                    } else {

                        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setVisibility(View.VISIBLE);

                    }


                }
            }
        }

        ImageListDataAdapter finalItemListDataAdapter = itemListDataAdapter;
        vesselShipInspectionChecklistViewHolder.photoButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView[] imageViews = {};
                vesselImageInterface.onPhotoButtonClickListenerAdapter(i, vesselComponent.getPoints(), imageViews, finalItemListDataAdapter, imagesList, locationList, timeStampList,"yes");
            }
        });

        vesselShipInspectionChecklistViewHolder.image_recyclerViewship.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onItemClick(i);
            }
        });

        vesselShipInspectionChecklistViewHolder.submitButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton[] radioButtons = {};
                ImageButton editTextComment = vesselShipInspectionChecklistViewHolder.edit_cmntBtn;
                ImageButton button = vesselShipInspectionChecklistViewHolder.submitButton1;
                ImageButton photoButton = vesselShipInspectionChecklistViewHolder.photoButton1;
                Spinner spinner = vesselShipInspectionChecklistViewHolder.gradeSpinner;
                Switch aSwitch=new Switch(context);
                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String strDate = formatter.format(date);

                points = vesselComponent.getPoints();

                if (vesselComponentList.size()>i){
                    j=i;
                    j++;
                }

                vesselImageInterface.onSubmitButtonClickListener(i, points, radioButtons, editTextComment, aSwitch, button,
                        vesselShipInspectionChecklistViewHolder.textView_questionPSC,
                        vesselShipInspectionChecklistViewHolder.Text_submit, strDate,
                        vesselShipInspectionChecklistViewHolder.linearLayoutTimeStamp,
                        vesselShipInspectionChecklistViewHolder.submitButton1, photoButton,
                        finalItemListDataAdapter, imagesList,locationList,timeStampList,spinner);

            }
        });


        vesselShipInspectionChecklistViewHolder.edit_cmntBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.comment_layout);
                EditText editText = dialog.findViewById(R.id.editText);
                Button submitButton = dialog.findViewById(R.id.button);
                Button cancelButton = dialog.findViewById(R.id.buttonCancel);
                dialog.setCancelable(false);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            String commenttitle="<b>"+"Comment:"+"</b>" + editText.getText().toString();
                            vesselShipInspectionChecklistViewHolder.Text_comments.setVisibility(View.VISIBLE);
                            vesselShipInspectionChecklistViewHolder.Text_comments.setText(Html.fromHtml(commenttitle ));

                        vesselImageInterface.onCommentButtonClickListener(i, vesselComponent.getPoints(), context, editText,
                                vesselShipInspectionChecklistViewHolder.linearLayoutTimeStamp);

                        hideKeyboardFrom(context, v);
                        dialog.dismiss();

                    }
                });
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeyboardFrom(context, v);
                        dialog.dismiss();
                    }
                });
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        });

    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public int getItemCount() {
        return vesselComponentList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    public void setImageMap(Map<Integer, List<String>> imageMap,
                            Map<Integer, List<String>> locationMap,
                            Map<Integer, List<String>> timeStampMap) {
        this.imageMap = imageMap;
        this.locationMap = locationMap;
        this.timeStampMap = timeStampMap;
        Log.e("imageMap", String.valueOf(imageMap.size()));
        Log.e("locatinMap", String.valueOf(locationMap.size()));
        Log.e("timeStamp", String.valueOf(timeStampMap.size()));
    }
    public void setLastData(int lastDataSize) {
        this.lastDataSize = lastDataSize;
        Log.e("timeStamp", String.valueOf(timeStampMap.size()));
    }

    public interface OnItemClicked {
        void onItemClick(int position);
    }

    public class VesselShipInspectionChecklistViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTitleShip, textView_questionPSC;
        TextView Text_comments, Text_submit;
        Spinner spinnerGrade;
        ImageButton edit_cmntBtn;
        ImageButton photoButton1;
        ImageButton submitButton1;
        RecyclerView image_recyclerViewship;
        LinearLayout linearLayoutImageViews;
        LinearLayout linearLayoutTimeStamp, PSCRootView;
        RelativeLayout relativeLayoutOfCheckpoints;
        Spinner gradeSpinner;

        public VesselShipInspectionChecklistViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitleShip = itemView.findViewById(R.id.textViewTitleShip);
            textView_questionPSC = itemView.findViewById(R.id.textView_questionShip);

            Text_comments = itemView.findViewById(R.id.editText_commentsShip);
            Text_submit = itemView.findViewById(R.id.editText_submitShip);

            spinnerGrade=itemView.findViewById(R.id.spinner_grade_ship);
            edit_cmntBtn = itemView.findViewById(R.id.edit_cmntShip);
            photoButton1 = itemView.findViewById(R.id.photoButton1Ship);
            submitButton1 = itemView.findViewById(R.id.submitButton1Ship);
            linearLayoutImageViews = itemView.findViewById(R.id.linearLayoutImageViews);
            image_recyclerViewship = itemView.findViewById(R.id.image_recyclerViewShip);
            linearLayoutTimeStamp = itemView.findViewById(R.id.linearLayoutTimeStampShip);
            relativeLayoutOfCheckpoints = itemView.findViewById(R.id.relativeLayoutOfCheckpoints);
            PSCRootView = itemView.findViewById(R.id.ShipRootView);
            gradeSpinner=itemView.findViewById(R.id.spinner_grade_ship);


        }
    }

}
