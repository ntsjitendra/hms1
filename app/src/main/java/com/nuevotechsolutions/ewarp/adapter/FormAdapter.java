package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.interfaces.TableFormSubmItInterface;

import org.json.JSONArray;
import org.json.JSONException;

public class FormAdapter extends RecyclerView.Adapter<FormAdapter.TableViewHolder> {
    private Context context;
    private JSONArray componentList;
    TableFormSubmItInterface tableFormSubmItInterface;

    public FormAdapter(JSONArray componentList, Context context, TableFormSubmItInterface tableFormSubmItInterface) {
        this.context = context;
        this.componentList = componentList;
        this.tableFormSubmItInterface = tableFormSubmItInterface;

    }

    @NonNull
    @Override
    public TableViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.form_adapter, parent, false);
        return new FormAdapter.TableViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TableViewHolder holder, int position) {
        String component = null;
        try {
            component = componentList.getString(position);
            Integer post = componentList.length();
            holder.tvquestion.setText(component);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        holder.etinputeLayout.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                EditText editText = holder.etinputeLayout;
                TextView tvquestion = holder.tvquestion;
                tableFormSubmItInterface.onFormSubmitonButtonClickListener(position, editText, tvquestion);
            }
        });

    }


    @Override
    public int getItemCount() {
        return componentList.length();

    }

    public class TableViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        EditText etinputeLayout;
        TextView tvquestion;

        public TableViewHolder(@NonNull View itemView) {
            super(itemView);

            tvquestion = itemView.findViewById(R.id.tvquestion);
            etinputeLayout = itemView.findViewById(R.id.etinputeLayout);

        }
    }
}
