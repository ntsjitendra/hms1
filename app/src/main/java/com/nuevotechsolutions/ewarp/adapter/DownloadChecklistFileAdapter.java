package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DownloadChecklistFileAdapter extends RecyclerView.Adapter<DownloadChecklistFileAdapter.DownloadChecklistViewHolder> {

    JSONArray arrList;
    Context context;
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(JSONArray arrList,int position);
    }

    public DownloadChecklistFileAdapter(Context context, JSONArray arrList, OnItemClickListener listener) {
        this.context = context;
        this.arrList = arrList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DownloadChecklistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.checklist_items, parent, false);
        DownloadChecklistFileAdapter.DownloadChecklistViewHolder downloadChecklistViewHolder = new DownloadChecklistViewHolder(view);
        return downloadChecklistViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DownloadChecklistViewHolder holder, int position) {

        holder.bind(arrList, listener);
        if (arrList.length()>0){
            try {
                JSONObject obj=arrList.getJSONObject(position);
                int j = position;
                holder.textViewItemName.setText(++j + ". " + obj.getString("list").toUpperCase());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public int getItemCount() {
        return arrList.length();
    }

    public class DownloadChecklistViewHolder extends RecyclerView.ViewHolder {

        TextView textViewItemName;

        public DownloadChecklistViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewItemName = itemView.findViewById(R.id.textViewItemName);
        }

        public void bind(final JSONArray arrList, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(arrList,getAdapterPosition());
                }
            });
        }

    }

}
