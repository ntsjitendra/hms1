package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselMasterWorkId;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import java.util.List;



public class VesselShipInspectionAdapter extends RecyclerView.Adapter<VesselShipInspectionAdapter.VesselShipInspectionViewHolder> {

    Context context;
    List<VesselMasterWorkId> list;
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(VesselMasterWorkId item,int position);
    }

    public VesselShipInspectionAdapter(Context context, List<VesselMasterWorkId> list, OnItemClickListener listener){
        this.context=context;
        this.list=list;
        this.listener=listener;
    }

    @NonNull
    @Override
    public VesselShipInspectionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        View view=inflater.inflate(R.layout.vessel_master_checklist_item,viewGroup,false);
        return new VesselShipInspectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VesselShipInspectionViewHolder vesselShipInspectionViewHolder, int i) {
        vesselShipInspectionViewHolder.bind(list.get(i), listener);
        int j=i;
        VesselMasterWorkId stringList=list.get(i);
        Log.e("list", String.valueOf(stringList));


        vesselShipInspectionViewHolder.VesselWorkIdLabelTextView.setText(++j+".Ref.No: "+ stringList.getCreated_by() +"/"+ stringList
                .getCrnt_date().substring(0, 4) + "/" + stringList.getWrk_id());
//        vesselShipInspectionViewHolder.VesselCreatedByTextView.setText("Created By: "+ stringList.getCreated_by().toLowerCase());
        vesselShipInspectionViewHolder.VesselCreatedByTextView.setText("Created By: "+ stringList.getCreated_by());
        vesselShipInspectionViewHolder.VesselCreationDateTextView.setText("Created On: " +
                new UtilClassFuntions().dateConversion(stringList.getCrnt_date()));
        vesselShipInspectionViewHolder.VesselLastModifiedDateTextView.setText("Last Modified: " +
                new UtilClassFuntions().dateConversion(stringList.getLast_modify_dt()));
//        vesselShipInspectionViewHolder.VesselCommentTextView.setText("Comment: "+ stringList.getComment().toLowerCase());

        if(stringList.getWrk_status()!=null && stringList.getWrk_status().equals(0)) {
            vesselShipInspectionViewHolder.VesselWorkIdLabelTextView.setTextColor(ContextCompat.getColor(context, R.color.QRCodeBlackColor));
        }
        if(stringList.getWrk_status()!=null && stringList.getWrk_status().equals(1)) {
            vesselShipInspectionViewHolder.VesselWorkIdLabelTextView.setTextColor(ContextCompat.getColor(context,R.color.darkColorRed));
        }

        if(stringList.getWrk_status()!=null && stringList.getWrk_status().equals(2)){
            vesselShipInspectionViewHolder.VesselWorkIdLabelTextView.setTextColor(ContextCompat.getColor(context,R.color.dark_green));

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VesselShipInspectionViewHolder extends RecyclerView.ViewHolder {

        TextView VesselChecklistNameTextView,VesselWorkIdLabelTextView,VesselCreatedByTextView,
                VesselCreationDateTextView,VesselLastModifiedDateTextView,VesselCommentTextView;


        public VesselShipInspectionViewHolder(@NonNull View itemView) {
            super(itemView);
            VesselChecklistNameTextView=itemView.findViewById(R.id.VesselChecklistNameTextView);
            VesselWorkIdLabelTextView=itemView.findViewById(R.id.VesselWorkIdLabelTextView);
            VesselCreatedByTextView=itemView.findViewById(R.id.VesselCreatedByTextView);
            VesselCreationDateTextView=itemView.findViewById(R.id.VesselCreationDateTextView);
            VesselLastModifiedDateTextView=itemView.findViewById(R.id.VesselLastModifiedDateTextView);
            VesselCommentTextView=itemView.findViewById(R.id.VesselCommentTextView);

        }

        public void bind(final VesselMasterWorkId list, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(list,getAdapterPosition());
                }
            });
        }

    }

}
