package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.NoticeDataList;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.NotificationViewHoder> {

    public interface OnItemClickListener {
        void onItemClick(NoticeDataList item,int position);
    }

    private Context context;
    private List<NoticeDataList> noticeList;
    private final OnItemClickListener listener;

    public NoticeAdapter(Context context, List<NoticeDataList> noticeList, OnItemClickListener listener) {
        this.context = context;
        this.noticeList = noticeList;
        this.listener=listener;

    }


    @NonNull
    @Override
    public NotificationViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view=inflater.inflate(R.layout.adapter_notice_list,parent,false);
        return new NotificationViewHoder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHoder holder, int position) {
        holder.bind(noticeList.get(position), listener);
        NoticeDataList noticeDataList=noticeList.get(position);
        if(noticeDataList.getFile_path()!=null){
            Picasso.with(context)
                    .load(noticeDataList.getFile_path())
                    .centerCrop()
                    .fit()
                    .error(R.drawable.attach)
                    .into(holder.imgNotification);
        }
        if (noticeDataList.getTitle()!=null){
            holder.tvNotifiTitle.setText(noticeDataList.getTitle());
        }
        if (noticeDataList.getAbout_notice()!=null){
            holder.tvNotifiDescription.setText(noticeDataList.getAbout_notice());
        }
    }

    @Override
    public int getItemCount() {
        return noticeList.size();
    }

    public class NotificationViewHoder extends RecyclerView.ViewHolder {
       private CardView noticeCardViewItem;
       private ImageView imgNotification;
       private TextView tvNotifiTitle;
       private TextView tvNotifiDescription;
        public NotificationViewHoder(@NonNull View itemView) {
            super(itemView);
            noticeCardViewItem=itemView.findViewById(R.id.noticeCardViewItem);
            imgNotification=itemView.findViewById(R.id.imgNotification);
            tvNotifiTitle=itemView.findViewById(R.id.tvNotifiTitle);
            tvNotifiDescription=itemView.findViewById(R.id.tvNotifiDescription);
        }
        public void bind(final NoticeDataList list, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(list,getAdapterPosition());
                }
            });
        }
    }
}
