package com.nuevotechsolutions.ewarp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nuevotechsolutions.ewarp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomCameraAdapter extends RecyclerView.Adapter<CustomCameraAdapter.CustomCameraViewHolder> {
    private Context context;
   List<String> imgList;
    private AdapterCallback adapterCallback;


    public CustomCameraAdapter(Context context, List<String> imgList) {
        this.context = context;
        this.imgList = imgList;
        adapterCallback = ((AdapterCallback) context);


        Log.e("image",imgList.get(0)+imgList.size());
    }



    @Override
    public CustomCameraViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.camera_reclr_item, parent, false);
        CustomCameraViewHolder customCameraViewHolder = new CustomCameraViewHolder(view);
        return customCameraViewHolder;
    }

    @Override
    public void onBindViewHolder(CustomCameraViewHolder holder, int position) {
        String imageName = imgList.get(position);
        Log.e("image1",imageName);
        Glide.with(context)
                .load(imageName)
                .centerCrop().fitCenter()
                .error(R.drawable.attach)
                .into(holder.cameraImageview);
        holder.cameraImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    adapterCallback.onMethodCallback();
                } catch (ClassCastException e) {
                    // do something
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return imgList.size();
    }


    public class CustomCameraViewHolder extends RecyclerView.ViewHolder {
       protected ImageView cameraImageview;

        public CustomCameraViewHolder(@NonNull View itemView) {
            super(itemView);
            cameraImageview = itemView.findViewById(R.id.imgCameraPic);

        }
    }
    public static interface AdapterCallback {
        void onMethodCallback();
    }
}
