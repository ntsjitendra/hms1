package com.nuevotechsolutions.ewarp.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.ImageInterface;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.DepartmentList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.Logger;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@SuppressWarnings("all")
public class RecycleCheckpointAdapter extends RecyclerView.Adapter<RecycleCheckpointAdapter.CheckpointViewHolder> {
    public int SELECT_IMAGE = 101;
    static List<ChecklistRecordDataList> checklistRecordDataLists = new ArrayList<>();
    //    ImageListDataAdapter itemListDataAdapter;
    String[] itemsList;
    String[] location, time, phototextValu;
    //    List<String> imagesList;
    CheckpointViewHolder checkpointViewHolder;
    private Context context;
    private List<Component> componentList;
    private ImageInterface imageInterface;
    private OnItemClicked onClick;
    private Map<Integer, List<String>> imageMap;
    private Map<Integer, List<String>> locationMap;
    private Map<Integer, List<String>> timeStampMap;
    private Map<Integer, List<String>> photoTextValueMap;
    private Map<Integer, ArrayList<String>> audioFileMap;
    private static String checklist_type;
    private static int lastDataSize = 0;
    private int j = 0;
    String tempTitleOfSection = "";
    public String[] temp;
    int tempPosition = 0;
    Spinner spinner;
    int radioFlag = 0;
    private CheckListDatabase checkListDatabase;
    List<CheckList> checkLists;
    String rb1, rb2, rb3, uid, rbData;
    String[] rbTemp;
    private String alertDepartment = null;
    private Dialog mDialog;
    private RelativeLayout btmsheetrealtiveLayout;
    private LinearLayout llcamera, llGallery, llaudio;
    private List<Boolean> viewEnabled;

    /*ImageListDataAdapter itemListDataAdapter;*/
    private static String section;
    private static String points;
    private static String rank;
    String image;
    AlertDepartmentAdapter alertDepartmentAdapter;
    List<DepartmentList> departmentLists;
    Component component;
    List<String> imagesList;
    List<String> locationList;
    List<String> timeStampList;
    List<String> photoTextValueList;
    ImageListDataAdapter finalItemListDataAdapter;
    LinearLayout formlayout;
    TextView tvcol;
    //    Map<String, String> valueDetails = new HashMap<>();
    CheckListDatabase db;
    List<UserDetails> userDetails = new ArrayList<>();
    List<MasterWorkId> masterWorkIdList = new ArrayList<>();
    String wrk_id;


    public RecycleCheckpointAdapter(String points) {
        this.points = points;
    }

    public RecycleCheckpointAdapter(String section, String points, String rank) {
        this.section = section;
        this.points = points;
        this.rank = rank;
    }

    public RecycleCheckpointAdapter(Context context,
                                    String wrk_id,
                                    List<Component> componentList,
                                    List<Boolean> viewEnabled,
                                    List<ChecklistRecordDataList> checklistRecordDataListsm1,
                                    Map<Integer, List<String>> imageMap,
                                    Map<Integer, List<String>> locationMap,
                                    Map<Integer, List<String>> timeStampMap,
                                    Map<Integer, List<String>> photoTextValueMap,
                                    String checklist_type,
                                    Map<Integer, ArrayList<String>> audioFileMap,
                                    ImageInterface imageInterface) {
        this.context = context;
        this.wrk_id = wrk_id;
        this.componentList = componentList;
        this.viewEnabled = viewEnabled;
        this.checklistRecordDataLists = checklistRecordDataListsm1;
        this.imageMap = imageMap;
        this.locationMap = locationMap;
        this.timeStampMap = timeStampMap;
        this.photoTextValueMap = photoTextValueMap;
        this.checklist_type = checklist_type;
        this.audioFileMap = audioFileMap;
        this.imageInterface = imageInterface;

        checkListDatabase = Room.databaseBuilder(context,
                CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        checkLists = checkListDatabase.productDao().getAllCheckList();
        departmentLists = checkListDatabase.productDao().getAllDepartmentList();
        if (componentList != null && componentList.size() > 0) {
            uid = componentList.get(0).getUid();
            rbData = componentList.get(0).getOption_btn_type();
            rbTemp = rbData.split(",", 3);
            rb1 = rbTemp[0];
            rb2 = rbTemp[1];
            rb3 = rbTemp[2];
        }
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public CheckpointViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_room_item, viewGroup, false);
        return new CheckpointViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull CheckpointViewHolder checkpointViewHolder, int i) {

        int listSize = 0;
        if (lastDataSize == 0) {
            listSize = checklistRecordDataLists.size();
        } else {
            listSize = lastDataSize;
        }
        Component component = componentList.get(i);
        imagesList = imageMap.get(i);
        locationList = locationMap.get(i);
        timeStampList = timeStampMap.get(i);
        photoTextValueList = photoTextValueMap.get(i);

        List<CheckList> list;

        db = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        list = db.productDao().getAllCheckList();
        userDetails = db.productDao().getUserDetail();

        ImageListDataAdapter itemListDataAdapter = null;
        if (component.getYesnona().equalsIgnoreCase("false") && component.getAdditional().equalsIgnoreCase("table")) {
            checkpointViewHolder.rlTable.setVisibility(View.VISIBLE);
            checkpointViewHolder.llview.setVisibility(View.VISIBLE);

        } else if(component.getYesnona().equalsIgnoreCase("false") && component.getAdditional().equalsIgnoreCase("textBox")){
            checkpointViewHolder.llview.setVisibility(View.VISIBLE);
            checkpointViewHolder.rlTable.setVisibility(View.GONE);
        }else {
            checkpointViewHolder.llview.setVisibility(View.VISIBLE);
            checkpointViewHolder.rlTable.setVisibility(View.GONE);
        }

        if (radioFlag == i) {
            if (component.getSuggested_ans() != null && component.getSuggested_ans().equalsIgnoreCase(rb2)) {
                checkpointViewHolder.radioButtonNo.setChecked(true);
                radioFlag++;
            } else if (component.getSuggested_ans() != null && component.getSuggested_ans().equalsIgnoreCase(rb3)) {
                checkpointViewHolder.radioButtonNa.setChecked(true);
                radioFlag++;
            } else if (component.getSuggested_ans() != null && component.getSuggested_ans().equalsIgnoreCase(rb1)) {
                checkpointViewHolder.radioButtonYes.setChecked(true);
                radioFlag++;
            } else {
                radioFlag++;
            }
        }

        if (component.getRb_points_description() != null) {
            checkpointViewHolder.radioButtonYes.setText(rb1);
            checkpointViewHolder.radioButtonNo.setText(rb2);
            checkpointViewHolder.radioButtonNa.setText(rb3);
        }

        if (component.getCuserpoint().equalsIgnoreCase("false")) {
            checkpointViewHolder.textViewQuestion.setTextColor(ContextCompat.getColor(context, R.color.darkGray));
            checkpointViewHolder.textViewQuestion.setText(component.getRb_points_description());
            checkpointViewHolder.textViewQuestion.setEnabled(false);
            checkpointViewHolder.radioButtonYes.setEnabled(false);
            checkpointViewHolder.radioButtonNo.setEnabled(false);
            checkpointViewHolder.radioButtonNa.setEnabled(false);
            checkpointViewHolder.tableview.setEnabled(false);
        } else {
            checkpointViewHolder.textViewQuestion.setText(component.getRb_points_description());
            checkpointViewHolder.textViewQuestion.setEnabled(true);
            if (checklist_type.equalsIgnoreCase("sequential")) {

                if (i > listSize) {
                    checkpointViewHolder.radioButtonYes.setEnabled(false);
                    checkpointViewHolder.radioButtonNo.setEnabled(false);
                    checkpointViewHolder.radioButtonNa.setEnabled(false);
                    if (j != 0 && j == i) {
                        if (component.getCuserpoint().equalsIgnoreCase("true")) {
                            checkpointViewHolder.radioButtonYes.setEnabled(true);
                            checkpointViewHolder.radioButtonNo.setEnabled(true);
                            checkpointViewHolder.radioButtonNa.setEnabled(true);
                        } else {
                            checkpointViewHolder.radioButtonYes.setEnabled(false);
                            checkpointViewHolder.radioButtonNo.setEnabled(false);
                            checkpointViewHolder.radioButtonNa.setEnabled(false);
                        }
                    }
                }
            } else {
                checkpointViewHolder.radioButtonYes.setEnabled(true);
                checkpointViewHolder.radioButtonNo.setEnabled(true);
                checkpointViewHolder.radioButtonNa.setEnabled(true);
            }
        }
        if (component.getCuserpoint().equalsIgnoreCase("false")) {
            checkpointViewHolder.edit_cmntBtn.setEnabled(false);

        } else {
            if (checklist_type.equalsIgnoreCase("sequential")) {
                if (i > listSize) {
                    checkpointViewHolder.edit_cmntBtn.setEnabled(false);
                    if (j != 0 && j == i) {
                        if (component.getCuserpoint().equalsIgnoreCase("true")) {
                            checkpointViewHolder.edit_cmntBtn.setEnabled(true);
                        } else {
                            checkpointViewHolder.edit_cmntBtn.setEnabled(false);
                        }
                    }
                }

            } else {
                checkpointViewHolder.edit_cmntBtn.setEnabled(true);
            }

        }
        if (component.getCuserpoint().equalsIgnoreCase("false")) {
            checkpointViewHolder.submitButton1.setEnabled(false);
        } else {
            if (checklist_type.equalsIgnoreCase("sequential")) {
                if (i > listSize) {
                    checkpointViewHolder.submitButton1.setEnabled(false);
                    if (j != 0 && j == i) {
                        if (component.getCuserpoint().equalsIgnoreCase("true")) {
                            checkpointViewHolder.submitButton1.setEnabled(true);
                        } else {
                            checkpointViewHolder.submitButton1.setEnabled(false);
                        }
                    }
                }
            } else {
                checkpointViewHolder.submitButton1.setEnabled(true);
            }
        }

        if (component.getActive_time() != null && component.getCrnt_dt_tm() != null) {
            checkpointViewHolder.Text_submit.setVisibility(View.VISIBLE);
            checkpointViewHolder.Text_submit.setText("Active Time" + component.getActive_time() + "and Submit Time" + component.getCrnt_dt_tm());
        }

        if (component.getTitle_of_section() != null && !component.getTitle_of_section().equalsIgnoreCase("false")) {
            if (tempTitleOfSection.equals("") && i == 0) {
                tempTitleOfSection = component.getTitle_of_section();
                checkpointViewHolder.textViewTitle.setVisibility(View.VISIBLE);
                checkpointViewHolder.textViewTitle.setText(component.getTitle_of_section());
            } else {
                if (tempTitleOfSection.equals(component.getTitle_of_section()) && i > 0) {
                    checkpointViewHolder.textViewTitle.setVisibility(View.GONE);
                } else {
                    checkpointViewHolder.textViewTitle.setText(component.getTitle_of_section());
                    tempTitleOfSection = component.getTitle_of_section();
                }
            }

        } else {
            checkpointViewHolder.textViewTitle.setVisibility(View.GONE);
        }

        if (component.getPhoto() != null && component.getPhoto().equalsIgnoreCase("false")) {
            checkpointViewHolder.photoButton1.setVisibility(View.GONE);
//            checkpointViewHolder.cameraTitleTextView.setVisibility(View.GONE);
            checkpointViewHolder.linearLayoutImageViews.setVisibility(View.GONE);
            checkpointViewHolder.image_recyclerView.setVisibility(View.VISIBLE);
            itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, photoTextValueList, checkpointViewHolder.submitButton1, imageInterface);
            checkpointViewHolder.image_recyclerView.setHasFixedSize(true);
            checkpointViewHolder.image_recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            checkpointViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);
        } else {
            if (component.getCuserpoint().equalsIgnoreCase("false")) {
                checkpointViewHolder.photoButton1.setVisibility(View.VISIBLE);
//                checkpointViewHolder.cameraTitleTextView.setVisibility(View.VISIBLE);
                checkpointViewHolder.photoButton1.setEnabled(false);
                checkpointViewHolder.linearLayoutImageViews.setVisibility(View.GONE);

                checkpointViewHolder.image_recyclerView.setVisibility(View.GONE);
                itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, photoTextValueList, checkpointViewHolder.submitButton1, imageInterface);
                checkpointViewHolder.image_recyclerView.setHasFixedSize(true);
                checkpointViewHolder.image_recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                checkpointViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);
            } else {
                checkpointViewHolder.photoButton1.setVisibility(View.VISIBLE);
//                checkpointViewHolder.cameraTitleTextView.setVisibility(View.VISIBLE);
                checkpointViewHolder.linearLayoutImageViews.setVisibility(View.GONE);
                checkpointViewHolder.image_recyclerView.setVisibility(View.VISIBLE);

                if (checklist_type.equalsIgnoreCase("sequential")) {
                    if (i > listSize) {
                        checkpointViewHolder.photoButton1.setEnabled(false);
                        if (j != 0 && j == i) {
                            if (component.getCuserpoint().equalsIgnoreCase("true")) {
                                checkpointViewHolder.photoButton1.setEnabled(true);
                            } else {
                                checkpointViewHolder.photoButton1.setEnabled(false);
                            }
                        }
                    }
                } else {
                    checkpointViewHolder.photoButton1.setEnabled(true);
                }

                itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, photoTextValueList, checkpointViewHolder.submitButton1, imageInterface);
                checkpointViewHolder.image_recyclerView.setHasFixedSize(true);
                checkpointViewHolder.image_recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                checkpointViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);
            }

        }

        if (component.getAlert() != null && component.getAlert().equalsIgnoreCase("false")) {
            checkpointViewHolder.switch_alert.setVisibility(View.GONE);
            checkpointViewHolder.alertTitleTextView.setVisibility(View.GONE);
        } else {

            if (component.getCuserpoint().equalsIgnoreCase("false")) {
                checkpointViewHolder.switch_alert.setVisibility(View.VISIBLE);
                checkpointViewHolder.alertTitleTextView.setVisibility(View.VISIBLE);
                checkpointViewHolder.switch_alert.setEnabled(false);
            } else {
                checkpointViewHolder.switch_alert.setVisibility(View.VISIBLE);
                checkpointViewHolder.alertTitleTextView.setVisibility(View.VISIBLE);
                if (checklist_type.equalsIgnoreCase("sequential")) {
                    if (i > listSize) {
                        checkpointViewHolder.switch_alert.setEnabled(false);
                        if (j != 0 && j == i) {
                            if (component.getCuserpoint().equalsIgnoreCase("true")) {
                                checkpointViewHolder.switch_alert.setEnabled(true);
                            } else {
                                checkpointViewHolder.switch_alert.setEnabled(false);
                            }
                        }
                    }
                } else {
                    checkpointViewHolder.switch_alert.setEnabled(true);
                }

            }

        }

        if (component.getGuidance() != null && !component.getGuidance().equalsIgnoreCase("false")) {
            checkpointViewHolder.buttonGuidance.setVisibility(View.VISIBLE);
            if (checklist_type.equalsIgnoreCase("sequential")) {
                if (i > listSize) {
                    checkpointViewHolder.buttonGuidance.setEnabled(false);
                    if (j != 0 && j == i) {
                        if (component.getCuserpoint().equalsIgnoreCase("true")) {
                            checkpointViewHolder.buttonGuidance.setEnabled(true);
                        } else {
                            checkpointViewHolder.buttonGuidance.setEnabled(false);
                        }
                    }
                }
            } else {
                checkpointViewHolder.buttonGuidance.setEnabled(true);
            }
        } else {
            checkpointViewHolder.buttonGuidance.setVisibility(View.GONE);
        }

        if (component.getVoice_record() != null && component.getVoice_record().equalsIgnoreCase("false")) {
            checkpointViewHolder.llaudio.setVisibility(View.GONE);
            checkpointViewHolder.llAudioPlay.setVisibility(View.GONE);
        } else {
            if (component.getCuserpoint().equalsIgnoreCase("false")) {
                checkpointViewHolder.llaudio.setVisibility(View.VISIBLE);
                checkpointViewHolder.llaudio.setEnabled(false);
            } else {
                checkpointViewHolder.llaudio.setVisibility(View.VISIBLE);
                if (checklist_type.equalsIgnoreCase("sequential")) {
                    if (i > listSize) {
                        checkpointViewHolder.llaudio.setEnabled(false);
                        if (j != 0 && j == i) {
                            if (component.getCuserpoint().equalsIgnoreCase("true")) {
                                checkpointViewHolder.llaudio.setEnabled(true);
                            } else {
                                checkpointViewHolder.llaudio.setEnabled(false);
                            }
                        }
                    }
                } else {
                    checkpointViewHolder.llaudio.setEnabled(true);
                }

            }

        }

        if (!checklistRecordDataLists.isEmpty()) {
            for (int j = 0; j < checklistRecordDataLists.size(); j++) {
                if (checklistRecordDataLists.get(j).getChecklist_points().equals(component.getPoints())) {
                    if (checklistRecordDataLists.get(j).getAns().equalsIgnoreCase(rb1)) {
                        checkpointViewHolder.radioButtonYes.setChecked(true);
                    } else if (checklistRecordDataLists.get(j).getAns().equalsIgnoreCase(rb2)) {
                        checkpointViewHolder.radioButtonNo.setChecked(true);
                    } else if (checklistRecordDataLists.get(j).getAns().equalsIgnoreCase(rb3)) {
                        checkpointViewHolder.radioButtonNa.setChecked(true);
                    }
                    if (component.getEdit().equals("true") && component.getCuserpoint().equalsIgnoreCase("true")) {
                        checkpointViewHolder.pointEditSubmitedLinearLayout.setVisibility(View.VISIBLE);
                    }
                    checkpointViewHolder.radioButtonYes.setEnabled(false);
                    checkpointViewHolder.radioButtonNo.setEnabled(false);
                    checkpointViewHolder.radioButtonNa.setEnabled(false);
                    checkpointViewHolder.edit_cmntBtn.setEnabled(false);
                    checkpointViewHolder.submitButton1.setEnabled(false);
                    checkpointViewHolder.photoButton1.setEnabled(false);
                    checkpointViewHolder.switch_alert.setEnabled(false);
                    checkpointViewHolder.llaudio.setEnabled(false);
                    checkpointViewHolder.tableview.setEnabled(false);
                    checkpointViewHolder.linearLayoutTimeStamp.setVisibility(View.VISIBLE);
                    if (checklistRecordDataLists.get(j).getAudio_data() != null) {
                        try {
                            JSONArray jsonArray = new JSONArray(checklistRecordDataLists.get(j).getAudio_data());
                            String count = String.valueOf(jsonArray.length());
                            Log.e("size1", String.valueOf(count));
                            checkpointViewHolder.tvAudioCount.setText(count);
                            checkpointViewHolder.tvAudioCount.setVisibility(View.VISIBLE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                    if (checklistRecordDataLists.get(j).getComment() != null) {
                        checkpointViewHolder.Text_comments.setVisibility(View.VISIBLE);
                        String commentTitle = "<b>" + "Comment:" + "</b>" + checklistRecordDataLists.get(j).getComment();
                        checkpointViewHolder.Text_comments.setText(Html.fromHtml(commentTitle));
                    }
                    if (checklistRecordDataLists.get(j).getAlert() != null && checklistRecordDataLists.get(j).getAlert().equalsIgnoreCase("yes")) {
                        checkpointViewHolder.switch_alert.setChecked(true);
                        checkpointViewHolder.switch_alert.setEnabled(false);
                    } else {
                        checkpointViewHolder.switch_alert.setChecked(false);
                        checkpointViewHolder.switch_alert.setEnabled(false);
                    }
                    if (checklistRecordDataLists.get(j).getSeverity() != null) {
                        checkpointViewHolder.Text_Alert.setVisibility(View.VISIBLE);
                        alertDepartment = checklistRecordDataLists.get(j).getDepartment();
                        if (alertDepartment == null) {
                            alertDepartment = "Not available";
                        }
                        String textAlertTitle = "<b>" + "Severity:" + "</b>" + checklistRecordDataLists.get(j).getSeverity()
                                + "<b>" + ", Department:" + "</b>" + alertDepartment
                                + "<b>" + ",\nAlert Comment:" + "</b>" + checklistRecordDataLists.get(j).getAlrt_cmnt();
                        checkpointViewHolder.Text_Alert.setText(Html.fromHtml(textAlertTitle));
                    }
                    String lctn = checklistRecordDataLists.get(j).getCrnt_lctn();
                    if (lctn != null) {
                        String[] temt = lctn.split("_", 2);
                        lctn = temt[0];
                    } else {
                        lctn = "Not available";
                    }
                    String crntTime = "";
                    if (checklistRecordDataLists.get(j).getCrnt_dt_tm() != null) {
                        crntTime = checklistRecordDataLists.get(j).getCrnt_dt_tm();
                    } else {
                        crntTime = checklistRecordDataLists.get(j).getGnrtn_date();
                    }
                    String textSubmitTitle = "<b>" + "Time:" + "</b>" + crntTime + "<b>" + ",\nLocation:" + "</b>" + lctn + "<b>" + ",\nSubmitted By:" + "</b>" +
                            checklistRecordDataLists.get(j).getDone_by();
                    checkpointViewHolder.Text_submit.setText(Html.fromHtml(textSubmitTitle));
                    if (checklistRecordDataLists.get(j).getImage_data() != null /*&& checklistRecordDataLists.get(j).getImage_data().contains("http")*/) {
                        checkpointViewHolder.image_recyclerView.setVisibility(View.VISIBLE);

                        image = checklistRecordDataLists.get(j).getImage_data();
                        Log.e("image...:", image);
                        String[] images = StringUtils.substringsBetween(image, "\"", "\"");
                        if (images != null) {
                            if (imagesList == null) {
                                imagesList = new ArrayList<>();
                            }
                            imagesList.clear();
                            for (String temp : images) {
                                if (temp.contains("https")) {
                                    imagesList.add(temp);

                                    JSONArray imgArray = null;

                                    try {
                                        imgArray = new JSONArray(image);
                                        for (int r = 0; r < imgArray.length(); r++) {
                                            JSONObject imgObj = imgArray.getJSONObject(r);
                                            if (locationList == null || timeStampList == null || photoTextValueList == null) {
                                                locationList = new ArrayList<>();
                                                timeStampList = new ArrayList<>();
                                                photoTextValueList = new ArrayList<>();
                                            }
                                            locationList.add(imgObj.getString("imagelocation"));
                                            timeStampList.add(imgObj.getString("imagetimedate"));
                                            photoTextValueList.add(imgObj.getString("photoTextValue"));
                                        }
                                        location = new String[locationList.size()];
                                        time = new String[timeStampList.size()];
                                        phototextValu = new String[photoTextValueList.size()];
                                        location = locationList.toArray(location);
                                        time = timeStampList.toArray(time);
                                        phototextValu = photoTextValueList.toArray(phototextValu);
                                        checkpointViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    if (temp.contains("storage")) {
                                        imagesList.add(temp.replace("\\", ""));


                                        String t = "";
                                        t = temp.substring(temp.length() - 21);
                                        try {
                                            JSONObject a = new JSONObject(checklistRecordDataLists.get(j).getImageJson());
                                            JSONArray b = new JSONArray();
                                            for (int k = 0; k < a.length(); k++) {
                                                JSONObject c = new JSONObject();
                                                JSONObject d = new JSONObject();
                                                d = a.getJSONObject(t);

                                                c.put("imagepath", temp.replace("\\", ""));
                                                c.put("imagelocation", d.getString("location"));
                                                c.put("imagename", t);
                                                c.put("photoTextValue", d.getString("photoTextValue"));
                                                c.put("imagetimedate", d.getString("timeStamp"));
                                                b.put(c);

                                            }
                                            image = b.toString();

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }
                                }
                            }
                        } else {

                        }
                        itemsList = new String[imagesList.size()];
                        itemsList = imagesList.toArray(itemsList);

//                        ********************************
                        itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, photoTextValueList, checkpointViewHolder.submitButton1, imageInterface);
                        checkpointViewHolder.image_recyclerView.setHasFixedSize(true);
                        checkpointViewHolder.image_recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        checkpointViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);
                        itemListDataAdapter.notifyDataSetChanged();


                    } else {
                        checkpointViewHolder.image_recyclerView.setVisibility(View.VISIBLE);

                    }


                }
            }
        }
//*************************************Filter Points**********************

        if (points != null && points.equals("mypoints") && component.getCuserpoint().equalsIgnoreCase("false")) {
            checkpointViewHolder.lrrootview.setVisibility(View.GONE);
            checkpointViewHolder.constraintLayoutOfCheckpoints.setVisibility(View.GONE);
        } else if (points != null && points.equals("allpoints") && component.getCuserpoint().equalsIgnoreCase("false")) {
            checkpointViewHolder.lrrootview.setVisibility(View.VISIBLE);
            checkpointViewHolder.constraintLayoutOfCheckpoints.setVisibility(View.VISIBLE);
        } else if (points != null && points.equals("SELF") || points != null && points.equals("ALL")) {

            if (section != null && section.equalsIgnoreCase("ALL")) {
                if (points != null && points.equals("SELF") && component.getCuserpoint().equalsIgnoreCase("false")) {
                    checkpointViewHolder.lrrootview.setVisibility(View.GONE);
                    checkpointViewHolder.constraintLayoutOfCheckpoints.setVisibility(View.GONE);
                } else if (points != null && points.equals("ALL") && component.getCuserpoint().equalsIgnoreCase("false")) {
                    checkpointViewHolder.lrrootview.setVisibility(View.VISIBLE);
                    checkpointViewHolder.constraintLayoutOfCheckpoints.setVisibility(View.VISIBLE);
                }
            } else if (section != null && !section.equalsIgnoreCase("ALL")) {
                if (points != null && section.equalsIgnoreCase(component.getTitle_of_section())
                        && points.equals("SELF") && component.getCuserpoint().equalsIgnoreCase("true")) {
                    checkpointViewHolder.lrrootview.setVisibility(View.VISIBLE);
                    checkpointViewHolder.constraintLayoutOfCheckpoints.setVisibility(View.VISIBLE);
                } else if (points != null && !section.equalsIgnoreCase(component.getTitle_of_section())
                        && points.equals("SELF") && component.getCuserpoint().equalsIgnoreCase("true")) {
                    checkpointViewHolder.lrrootview.setVisibility(View.GONE);
                    checkpointViewHolder.constraintLayoutOfCheckpoints.setVisibility(View.GONE);
                } else if (points != null && points.equals("ALL") && component.getCuserpoint().equalsIgnoreCase("false")
                        && section.equalsIgnoreCase(component.getTitle_of_section())) {
                    checkpointViewHolder.lrrootview.setVisibility(View.VISIBLE);
                    checkpointViewHolder.constraintLayoutOfCheckpoints.setVisibility(View.VISIBLE);
                } else if (points != null && points.equals("ALL") && component.getCuserpoint().equalsIgnoreCase("true")
                        && section.equalsIgnoreCase(component.getTitle_of_section())) {
                    checkpointViewHolder.lrrootview.setVisibility(View.VISIBLE);
                    checkpointViewHolder.constraintLayoutOfCheckpoints.setVisibility(View.VISIBLE);
                } else {
                    checkpointViewHolder.lrrootview.setVisibility(View.GONE);
                    checkpointViewHolder.constraintLayoutOfCheckpoints.setVisibility(View.GONE);
                }
            }

        }


        finalItemListDataAdapter = itemListDataAdapter;
        checkpointViewHolder.lrrootview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (component.getCuserpoint().equals("false")) {
                    Toast toast = Toast.makeText(context, "This job is not for you!", Toast.LENGTH_SHORT);
                    /*View view = toast.getView();
                    view.getBackground().setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
                    TextView text = (TextView) view.findViewById(android.R.id.message);
                    text.setTextColor(context.getResources().getColor(R.color.white));*/
                    toast.show();
                } else {

                }
            }
        });
        checkpointViewHolder.tableview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (component.getCuserpoint().equals("false")) {
                    Toast toast = Toast.makeText(context, "This job is not for you!", Toast.LENGTH_SHORT);
                    toast.show();
                } else {

                }
            }
        });

        checkpointViewHolder.tableview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageInterface.onTableViewClickListener(i, component.getPoints());
//                Toast.makeText(context, "test"+i, Toast.LENGTH_SHORT).show();
//                showTableViewDialog(i, checkpointViewHolder, component.getPoints());
            }
        });
        checkpointViewHolder.readview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowAnswerListDialog(i, checkpointViewHolder, component.getPoints());
            }
        });
        checkpointViewHolder.photoButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation;
                ShowPlushAction(i, checkpointViewHolder, component.getPoints());
            }
        });
        checkpointViewHolder.llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkpointViewHolder.llattach.setVisibility(View.GONE);
                ImageView[] imageViews = {checkpointViewHolder.imageView1, checkpointViewHolder.imageView2,
                        checkpointViewHolder.imageView3, checkpointViewHolder.imageView4};
                imageInterface.onPhotoButtonClickListener(i, imageViews);
                imageInterface.onPhotoButtonClickListenerAdapter(i, component.getPoints(), imageViews, finalItemListDataAdapter,
                        imagesList, locationList, timeStampList, photoTextValueList);
            }
        });

        checkpointViewHolder.llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkpointViewHolder.llattach.setVisibility(View.GONE);
//                Intent intent = new Intent(Intent.ACTION_PICK,
//                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                intent.setType("image/*");
//                imageInterface.onAttachPositionClickListener(i, imageMap, locationMap, timeStampMap,
//                        photoTextValueMap, imagesList
//                        , locationList, timeStampList,photoTextValueList);
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                ((Activity) context).startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
            }
        });

        //***********************j******************
        checkpointViewHolder.switch_alert.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkpointViewHolder.switch_alert.isChecked()) {
                    Dialog dialog = new Dialog(context);
                    Window window = dialog.getWindow();
                    dialog.setContentView(R.layout.alert_layout);
                    window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    dialog.show();
                    checkpointViewHolder.rbLeast = dialog.findViewById(R.id.rbLeast);
                    checkpointViewHolder.rbHigh = dialog.findViewById(R.id.rbHigh);
                    checkpointViewHolder.rbVeryHigh = dialog.findViewById(R.id.rbVeryHigh);
                    checkpointViewHolder.alertDepartmentTitle = dialog.findViewById(R.id.alertDepartmentTitle);
                    checkpointViewHolder.alertDepartmentRecycler = dialog.findViewById(R.id.alertDepartmentRecycler);
                    checkpointViewHolder.departmentCmnt = dialog.findViewById(R.id.departmentComment);
                    checkpointViewHolder.alrtSubmit = dialog.findViewById(R.id.alertSubmit);
                    checkpointViewHolder.alrtCancel = dialog.findViewById(R.id.alertCancel);
                    checkpointViewHolder.alertDepartmentRecycler.setLayoutManager(new LinearLayoutManager(context));

                    if (departmentLists.size() == 0) {
                        checkpointViewHolder.alertDepartmentTitle.setVisibility(View.GONE);
                    }
                    alertDepartmentAdapter = new AlertDepartmentAdapter(context, departmentLists);
                    checkpointViewHolder.alertDepartmentRecycler.setAdapter(alertDepartmentAdapter);
//                    alertDepartmentAdapter.notifyDataSetChanged();
                    checkpointViewHolder.alrtSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDepartment = alertDepartmentAdapter.bind();
                            RadioButton[] radioButtons = {checkpointViewHolder.rbLeast, checkpointViewHolder.rbHigh, checkpointViewHolder.rbVeryHigh};
                            EditText editText = checkpointViewHolder.departmentCmnt;
                            Button button = checkpointViewHolder.alrtSubmit;
                            Switch aSwitch = checkpointViewHolder.switch_alert;
                            imageInterface.onSwitchButtonClickListener(i, component.getPoints(), context, aSwitch, radioButtons, alertDepartment, editText, checkpointViewHolder.linearLayoutTimeStamp);
                            String sevrityText = null;
                            if (checkpointViewHolder.rbLeast.isChecked()) {
                                sevrityText = checkpointViewHolder.rbLeast.getText().toString();
                            }
                            if (checkpointViewHolder.rbHigh.isChecked()) {
                                sevrityText = checkpointViewHolder.rbHigh.getText().toString();
                            }
                            if (checkpointViewHolder.rbVeryHigh.isChecked()) {
                                sevrityText = checkpointViewHolder.rbVeryHigh.getText().toString();
                            }
                            if (sevrityText != null) {
                                if (alertDepartment == null) {
                                    alertDepartment = "Not available";
                                }
                                checkpointViewHolder.Text_Alert.setVisibility(View.VISIBLE);
                                String textAlertTitle = "<b>" + "Severity:" + "</b>" + sevrityText.toLowerCase() + "<b>" + ", Department:" + "</b>" + alertDepartment + "<b>" + "\n, Alert Comments:" + "</b>" +
                                        checkpointViewHolder.departmentCmnt.getText().toString();
                                checkpointViewHolder.Text_Alert.setText(Html.fromHtml(textAlertTitle));
                            }
                            hideKeyboardFrom(context, v);
                            dialog.dismiss();
                        }
                    });
                    checkpointViewHolder.alrtCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            checkpointViewHolder.switch_alert.setChecked(false);
                            hideKeyboardFrom(context, v);
                            dialog.dismiss();
                        }
                    });


                }
            }
        });
        checkpointViewHolder.image_recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onItemClick(i);
            }
        });
        checkpointViewHolder.imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkpointViewHolder.imageView1 != null) {
                    imageInterface.onImageClickListener(i, component.getPoints(), checkpointViewHolder.imageView1, context,
                            checkpointViewHolder.submitButton1);
                }
            }
        });

        checkpointViewHolder.submitButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton[] radioButtons = {checkpointViewHolder.radioButtonYes, checkpointViewHolder.radioButtonNo,
                        checkpointViewHolder.radioButtonNa};
                ImageButton editTextComment = checkpointViewHolder.edit_cmntBtn;
                Switch aSwitch = checkpointViewHolder.switch_alert;
                ImageButton button = checkpointViewHolder.submitButton1;
                ImageButton photoButton = checkpointViewHolder.photoButton1;
                LinearLayout pointEditSubmitedLinearLayout = checkpointViewHolder.pointEditSubmitedLinearLayout;

                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String strDate = formatter.format(date);

                points = component.getPoints();

                if (componentList.size() > i) {
                    j = i;
                    j++;
                }
                String isVisible;
                if (component.getEdit().equals("true") && component.getCuserpoint().equalsIgnoreCase("true")) {
                    isVisible = "true";
                } else {
                    isVisible = "false";
                }
                imageInterface.onSubmitButtonClickListener(i, component.getPoints(), radioButtons, editTextComment, aSwitch, button,
                        checkpointViewHolder.textViewQuestion, checkpointViewHolder.Text_submit, strDate,
                        checkpointViewHolder.linearLayoutTimeStamp, checkpointViewHolder.submitButton1, photoButton,
                        finalItemListDataAdapter, imagesList, locationList, timeStampList,
                        photoTextValueList, spinner, pointEditSubmitedLinearLayout, isVisible);

            }
        });

        checkpointViewHolder.buttonGuidance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageInterface.onGuidanceButtonClickListener(i, component.getPoints(), context);

            }
        });

        checkpointViewHolder.edit_cmntBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempComment = checkpointViewHolder.Text_comments.getText().toString();

                if (!tempComment.equals("") || tempComment != null) {
                    temp = tempComment.split(":", 2);
                }
                String commentTitle = tempComment.substring(tempComment.indexOf(":") + 1);

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.comment_layout);
                EditText editText = dialog.findViewById(R.id.editText);
                editText.setText(commentTitle);
                Button submitButton = dialog.findViewById(R.id.button);
                Button cancelButton = dialog.findViewById(R.id.buttonCancel);
                dialog.setCancelable(false);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (component.getComment() != null && component.getComment() != "") {
                            String commenttitle = editText.getText().toString();
                            checkpointViewHolder.Text_comments.setVisibility(View.VISIBLE);
//                            checkpointViewHolder.Text_comments.setText(Html.fromHtml(commenttitle));
                            checkpointViewHolder.Text_comments.setText((Html.fromHtml("<b>" + "Comment:" + "</b>" + commenttitle)));
                        }
                        imageInterface.onCommentButtonClickListener(i, component.getPoints(), context, editText, checkpointViewHolder.linearLayoutTimeStamp);

                        hideKeyboardFrom(context, v);
                        dialog.dismiss();

                    }
                });
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeyboardFrom(context, v);
                        dialog.dismiss();
                    }
                });
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        });

        checkpointViewHolder.pointEditSubmitedData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt = checkpointViewHolder.pointEditSubmitedText.getText().toString();
                if (txt.equalsIgnoreCase("Edit")) {
                    checkpointViewHolder.pointEditSubmitedData.setBackgroundResource(R.drawable.ic_update);
                    checkpointViewHolder.pointEditSubmitedText.setText("Update");
                    checkpointViewHolder.radioButtonYes.setEnabled(true);
                    checkpointViewHolder.radioButtonNo.setEnabled(true);
                    checkpointViewHolder.radioButtonNa.setEnabled(true);
                    checkpointViewHolder.photoButton1.setEnabled(true);
                    checkpointViewHolder.edit_cmntBtn.setEnabled(true);
                    checkpointViewHolder.switch_alert.setEnabled(true);
                    checkpointViewHolder.submitButton1.setVisibility(View.GONE);
//                    checkpointViewHolder.submitTitleTextView.setVisibility(View.GONE);
                    viewEnabled.set(i, true);
                } else {
                    checkpointViewHolder.pointEditSubmitedData.setBackgroundResource(R.drawable.ic_editnew);
                    checkpointViewHolder.pointEditSubmitedText.setText("Edit");
                    checkpointViewHolder.radioButtonYes.setEnabled(false);
                    checkpointViewHolder.radioButtonNo.setEnabled(false);
                    checkpointViewHolder.radioButtonNa.setEnabled(false);
                    checkpointViewHolder.photoButton1.setEnabled(false);
                    checkpointViewHolder.edit_cmntBtn.setEnabled(false);
                    checkpointViewHolder.switch_alert.setEnabled(false);
                    checkpointViewHolder.submitButton1.setVisibility(View.VISIBLE);
//                    checkpointViewHolder.submitTitleTextView.setVisibility(View.VISIBLE);
                    checkpointViewHolder.submitButton1.setEnabled(false);
                    viewEnabled.set(i, false);

                }
                RadioButton[] radioButtons = {checkpointViewHolder.radioButtonYes, checkpointViewHolder.radioButtonNo,
                        checkpointViewHolder.radioButtonNa};

                imageInterface.onEditButtonClickListener(i, context, component.getPoints(), checkpointViewHolder.switch_alert, radioButtons, txt, viewEnabled);
            }
        });
        checkpointViewHolder.llAudioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageInterface.onPlayButtonClickListener(i, component.getPoints());
            }
        });
        checkpointViewHolder.llaudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageInterface.onRecordButtonClickListener(i);
            }
        });

        //**************************************************

    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public int getItemCount() {
        return componentList.size();
    }


    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }

    public void getItemSelected(MenuItem item) {
        if (item.getItemId() == 0) {
            checkpointViewHolder.image_recyclerView.setVisibility(View.VISIBLE);
        } else {
            checkpointViewHolder.image_recyclerView.setVisibility(View.INVISIBLE);
        }

    }

    public void setImageMap(Map<Integer, List<String>> imageMap,
                            Map<Integer, List<String>> locationMap,
                            Map<Integer, List<String>> timeStampMap,
                            Map<Integer, List<String>> photoTextValueMap
//                            ,List<Boolean> viewEnabled
    ) {
        this.imageMap = imageMap;
        this.locationMap = locationMap;
        this.timeStampMap = timeStampMap;
        this.photoTextValueMap = photoTextValueMap;
//        this.viewEnabled = viewEnabled;
    }


    public void setLastData(int lastDataSize) {
        this.lastDataSize = lastDataSize;
    }

    public interface OnItemClicked {
        void onItemClick(int position);
    }

    public void ShowPlushAction(int i, final CheckpointViewHolder checkpointViewHolder, String points) {
//    final  ViewHolderItem v= null;iewHolder;
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_camera_click);
//        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = mDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.translate_utod);
        anim.reset();
        btmsheetrealtiveLayout = mDialog.findViewById(R.id.btmsheetrealtiveLayout);
        llcamera = mDialog.findViewById(R.id.llCamera);
        llGallery = mDialog.findViewById(R.id.llGallery);
        llaudio = mDialog.findViewById(R.id.llaudio);
        btmsheetrealtiveLayout.clearAnimation();
        btmsheetrealtiveLayout.startAnimation(anim);
        llcamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                checkpointViewHolder.llattach.setVisibility(View.GONE);
                ImageView[] imageViews = {checkpointViewHolder.imageView1, checkpointViewHolder.imageView2,
                        checkpointViewHolder.imageView3, checkpointViewHolder.imageView4};
                imageInterface.onPhotoButtonClickListenerAdapter(i, points, imageViews, finalItemListDataAdapter,
                        imageMap.get(i), locationMap.get(i), timeStampMap.get(i), photoTextValueMap.get(i));
                mDialog.dismiss();
            }
        });

        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent();
//                intent.setType("image/*");
                imageInterface.onAttachPositionClickListener(i, imageMap, locationMap, timeStampMap, photoTextValueMap, imagesList
                        , locationList, timeStampList, photoTextValueList);

                mDialog.dismiss();
            }
        });

        mDialog.show();

    }


    //This dialog is for showing table view.

//    public void showTableViewDialog(int i, final CheckpointViewHolder checkpointViewHolder, String points) {
//        mDialog = new Dialog(context);
//        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        mDialog.setContentView(R.layout.dialog_table_view);
//        mDialog.setCancelable(false);
//        Button submitButton1 = mDialog.findViewById(R.id.formSubmit);
//        Button cancelButton1 = mDialog.findViewById(R.id.formCancel);
//        RecyclerView questionRecyclerView = mDialog.findViewById(R.id.table_recycle);
//        questionRecyclerView.setLayoutManager(new LinearLayoutManager(context));
//        Component component = componentList.get(i);
//        String colomn = component.getColumn_name();
//
//        try {
//            //JSONObject jsnobject = new JSONObject(colomn);
//            JSONArray jsonArray = new JSONArray(colomn);
//            for (int h = 0; h < jsonArray.length(); h++) {
//                Log.e("test", jsonArray.getString(h));
//            }
//
//            questionRecyclerView.setAdapter(new FormAdapter(jsonArray, context, new TableFormSubmItInterface() {
//                @Override
//                public void onFormSubmitonButtonClickListener(int post, EditText editText, TextView tvquestion) {
//                    tableForm(post, editText, tvquestion);
//                }
//            }));
//
//
//            cancelButton1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mDialog.dismiss();
//                }
//            });
//            submitButton1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    if (valueDetails.size() == jsonArray.length()) {
//                        JSONObject jsonObject = new JSONObject(valueDetails);
//
//                        Log.e("json", "" + String.valueOf(jsonObject));
////                        String wrkid = String.valueOf(checklistRecordDataLists.get(i).getWrk_id());
//                        Map<String, String> params = new HashMap<>();
//                        params.put("clmval", String.valueOf(jsonObject));
//                        params.put("wrk_id", wrk_id);
//                        params.put("points", component.getPoints());
//                        params.put("dbnm", userDetails.get(0).getDbnm());
//                        params.put("user_id", userDetails.get(0).getUser_id());
//                        params.put("user_name", userDetails.get(0).getFirst_name());
//                        params.put("datetime", "");
//                        Logger.printLog("wrk",wrk_id);
//                        VolleyMethods.makePostJsonObjectRequest(params, context, ApiUrlClass.SubmitTabledata, new PostJsonObjectRequestCallback() {
//                            @Override
//                            public void onSuccessResponse(JSONObject response) {
//                                if (response != null) {
//                                    try {
//                                        String status = response.getString("status");
//                                        Log.e("status", status);
//                                        if (status.equals("1")) {
//                                            mDialog.dismiss();
//                                            Toast.makeText(context, "Form Submited successfully.", Toast.LENGTH_SHORT).show();
//                                        }
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }
//
//                            @Override
//                            public void onVolleyError(VolleyError error) {
//
//                            }
//
//                            @Override
//                            public void onTokenExpire() {
//
//                            }
//                        });
//
//
//                    }
//                }
//            });
//            mDialog.show();
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            Logger.printLog("crash", "" + e.getMessage());
//        }
//    }

//    private void tableForm(int post, EditText editText, TextView textInputLayout) {
//        if (valueDetails.containsKey(post)) {
//            valueDetails.remove(post);
//            valueDetails.put(String.valueOf(post), editText.getText().toString().toUpperCase());
//
//            Log.e("rm", String.valueOf(post));
//            Log.e("put", String.valueOf(valueDetails));
//        } else {
//            valueDetails.put(String.valueOf(post), editText.getText().toString().toUpperCase());
//            Log.e("detailsList", String.valueOf(valueDetails));
//        }
//    }

    public void ShowAnswerListDialog(int pos, final CheckpointViewHolder checkpointViewHolder, String points) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        AlertDialog alertDialog = builder.create();

//        mDialog = new Dialog(context);

//        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dView = LayoutInflater.from(context).inflate(R.layout.dialog_table_answer, null, false);

//        alertDialog.setContentView(R.layout.dialog_table_answer);

        RecyclerView recyclerView = dView.findViewById(R.id.table_view_recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        Component component = componentList.get(pos);
        String json = component.getColumn_name();

        Log.e("json", json);

        String answers = null;
//        for (int c = 0; c < componentList.size(); c++) {
        for (int ch = 0; ch < checklistRecordDataLists.size(); ch++) {
            if (componentList.get(pos).getPoints().equals(checklistRecordDataLists.get(ch).getChecklist_points())
                    && componentList.get(pos).getUid().equals(String.valueOf(checklistRecordDataLists.get(ch).getUid()))) {
                answers = checklistRecordDataLists.get(ch).getClm_value();
                break;
            }
        }
//        }

        try {

            if (answers != null) {
//                Logger.printLog("ANswers list", "::  " + answers);

                JSONArray quesJsonArray = new JSONArray(json);
                JSONArray jsonArray1 = new JSONArray(answers);

                Logger.printLog("ANswers list", "::  " + jsonArray1);
//                Map<Integer, List<String>> answerMap = new HashMap<>();
//                Map<Integer, List<String>> submittedBy = new HashMap<>();

//                for (int q = 0; q < quesJsonArray.length(); q++) {
//                    List<String> answersArray = new ArrayList<>();
//                    List<String> submitted = new ArrayList<>();
//                    for (int a = 0; a < jsonArray1.length(); a++) {
//                        Logger.printLog("ANswers at " + a, "::  " + jsonArray1.getJSONObject(a));
//                        answersArray.add(jsonArray1.getJSONObject(a).getString(String.valueOf(q + 1)));
//                        submitted.add(jsonArray1.getJSONObject(a).getString("name"));
//                    }
//                    answerMap.put(q, answersArray);
//                    submittedBy.put(q, submitted);
//
//                }
//                Logger.printLog("ANswers Map by Position", "::  " + answerMap.toString());
//                Logger.printLog("Submitted by Position", "::  " + submittedBy.toString());
//                recyclerView.setAdapter(new QuestionAnswerAdapter(quesJsonArray, answerMap, submittedBy, context));
                recyclerView.setAdapter(new QuestionAnswerAdapter(quesJsonArray, jsonArray1, context));

                alertDialog.setView(dView);
                alertDialog.show();

            } else {
                Toast.makeText(context, "No answer found!", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            Logger.printLog("QUES JSON EXC", "::  " + e.getMessage());
        }

    }

    class CheckpointViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle, textViewQuestion, textViewcrntLocation1;
        TextView Text_comments, Text_Alert, Text_submit, cameraTitleTextView,
                commentTitleTextView, alertTitleTextView, submitTitleTextView;
        RadioButton radioButtonYes, radioButtonNo, radioButtonNa;
        RadioGroup radioGroup_ans;
        ImageButton edit_cmntBtn;
        ImageButton photoButton1;/*switch1,*/
        ImageButton submitButton1;
        ImageButton buttonGuidance;
        ImageButton pointEditSubmitedData;
        TextView pointEditSubmitedText;
        LinearLayout pointEditSubmitedLinearLayout, llaudio, llAudioList;
        Switch switch_alert;
        ImageView imageView1, imageView2, imageView3, imageView4;
        RecyclerView image_recyclerView, alertDepartmentRecycler;


        //*************************************************************
        RadioButton rbLeast, rbHigh, rbVeryHigh;
        EditText alrtCmnt, departmentCmnt;
        String dialogAlrt, dialogAlrtCmnt, dialogDepartment, dialogDepartmentCmnt;
        Button alrtSubmit, alrtCancel;
        Dialog dialog;
        LinearLayout linearLayoutImageViews, linearLayoutImageViews1, llattach, llCamera, llGallery;
        LinearLayout linearLayoutTimeStamp, lrrootview, llview;
        ConstraintLayout constraintLayoutOfCheckpoints;
        LinearLayout llAudioPlay;
        TextView tvAudioCount, alertDepartmentTitle, tableview, readview;
        RelativeLayout rlTable;
//***************************************************************


        CheckpointViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            buttonGuidance = itemView.findViewById(R.id.buttonGuidance);

            textViewQuestion = itemView.findViewById(R.id.textView_question);

            Text_comments = itemView.findViewById(R.id.editText_comments);
            Text_Alert = itemView.findViewById(R.id.editText_Alert);
            Text_submit = itemView.findViewById(R.id.editText_submit);

            radioGroup_ans = itemView.findViewById(R.id.radioGroup_ans);
            radioButtonYes = itemView.findViewById(R.id.rb1Yes);
            radioButtonNo = itemView.findViewById(R.id.rb1No);
            radioButtonNa = itemView.findViewById(R.id.rb1Na);

            edit_cmntBtn = itemView.findViewById(R.id.edit_cmnt);
            photoButton1 = itemView.findViewById(R.id.photoButton1);
            switch_alert = itemView.findViewById(R.id.switch_alert);
            submitButton1 = itemView.findViewById(R.id.submitButton1);

//            cameraTitleTextView = itemView.findViewById(R.id.cameraTitleTextView);
            commentTitleTextView = itemView.findViewById(R.id.commentTitleTextView);
            alertTitleTextView = itemView.findViewById(R.id.alertTitleTextView);
            submitTitleTextView = itemView.findViewById(R.id.submitTitleTextView);

            imageView1 = itemView.findViewById(R.id.imageView1);
            imageView2 = itemView.findViewById(R.id.imageView2);
            imageView3 = itemView.findViewById(R.id.imageView3);
            imageView4 = itemView.findViewById(R.id.imageView4);

            linearLayoutImageViews = itemView.findViewById(R.id.linearLayoutImageViews);
            image_recyclerView = itemView.findViewById(R.id.image_recyclerView);
            linearLayoutTimeStamp = itemView.findViewById(R.id.linearLayoutTimeStamp);
            constraintLayoutOfCheckpoints = itemView.findViewById(R.id.constraintLayoutOfCheckpoints);
            lrrootview = itemView.findViewById(R.id.lrrootview);
            llattach = itemView.findViewById(R.id.llattach);
            llCamera = itemView.findViewById(R.id.llCamera);
            llGallery = itemView.findViewById(R.id.llGallery);
            pointEditSubmitedData = itemView.findViewById(R.id.buttonEdit);
            pointEditSubmitedText = itemView.findViewById(R.id.editPoint);
            pointEditSubmitedLinearLayout = itemView.findViewById(R.id.llCheckpointEdit);
            llAudioPlay = itemView.findViewById(R.id.llAudioList);
            tvAudioCount = itemView.findViewById(R.id.tvAudioCount);
            tableview = itemView.findViewById(R.id.tableview);
            llaudio = itemView.findViewById(R.id.llaudio);
            llview = itemView.findViewById(R.id.llview);
            rlTable = itemView.findViewById(R.id.rlTable);
            readview = itemView.findViewById(R.id.Readview);

        }
    }

}
