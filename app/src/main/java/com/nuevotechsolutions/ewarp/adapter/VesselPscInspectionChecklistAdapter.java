package com.nuevotechsolutions.ewarp.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.interfaces.VesselImageInterface;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselComponent;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class VesselPscInspectionChecklistAdapter extends RecyclerView.Adapter
        <VesselPscInspectionChecklistAdapter.VesselPscInspectionChecklistViewHolder> {

    static List<VesselChecklistRecordDataList> vesselChecklistRecordDataLists = new ArrayList<>();
    //    ImageListDataAdapter itemListDataAdapter;
    String[] itemsList;
    String[] location, time;
    //    List<String> imagesList;
    VesselPscInspectionChecklistViewHolder vesselPscInspectionChecklistViewHolder;
    private Context context;
    private List<VesselComponent> componentList;
    private VesselImageInterface vesselImageInterface;
    private OnItemClicked onClick;
    private Map<Integer, List<String>> imageMap;
    private Map<Integer, List<String>> locationMap;
    private Map<Integer, List<String>> timeStampMap;
    private static String checklist_type;
    private static int lastDataSize = 0;
    private int j = 0;
    Spinner Tempspinner;
    String tempTitleOfSection = "";

    /*ImageListDataAdapter itemListDataAdapter;*/
    private static String points;
    String image;

    public VesselPscInspectionChecklistAdapter(String points) {
        this.points = points;
        Log.e("ss", points);
    }

    public VesselPscInspectionChecklistAdapter(Context context,
                                               List<VesselComponent> vesselComponentList,
                                               List<VesselChecklistRecordDataList> vesselChecklistRecordDataLists,
                                               Map<Integer, List<String>> imageMap,
                                               Map<Integer, List<String>> locationMap,
                                               Map<Integer, List<String>> timeStampMap,
                                               String checklist_type,
                                               VesselImageInterface vesselImageInterface) {
        this.context = context;
        this.componentList = vesselComponentList;
        this.vesselChecklistRecordDataLists = vesselChecklistRecordDataLists;
        this.imageMap = imageMap;
        this.locationMap = locationMap;
        this.timeStampMap = timeStampMap;
        this.checklist_type = checklist_type;
        this.vesselImageInterface = vesselImageInterface;
        setHasStableIds(true);
        Log.e("componentList", String.valueOf(componentList.size()));
        Log.e("imageMap", String.valueOf(imageMap.size()));
        Log.e("Constructor", String.valueOf(vesselChecklistRecordDataLists.size()));
    }


    @NonNull
    @Override
    public VesselPscInspectionChecklistViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.vessel_psc_inspection_item, viewGroup, false);
        return new VesselPscInspectionChecklistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VesselPscInspectionChecklistViewHolder vesselPscInspectionChecklistViewHolder, int i) {

        int listSize = 0;
        if (lastDataSize == 0) {
            listSize = vesselChecklistRecordDataLists.size();
        } else {
            listSize = lastDataSize;
            Log.e("listSize2", String.valueOf(listSize));
        }
        Log.e("listSize3", String.valueOf(listSize));

        VesselComponent vesselComponent = componentList.get(i);
        List<String> imagesList = imageMap.get(i);
        List<String> locationList = locationMap.get(i);
        List<String> timeStampList = timeStampMap.get(i);

        ImageListDataAdapter itemListDataAdapter = null;

        if (tempTitleOfSection.equals("")) {
            tempTitleOfSection = vesselComponent.getTitle_of_section();
            vesselPscInspectionChecklistViewHolder.textViewTitlePSC.setText(vesselComponent.getTitle_of_section());
        }


        vesselPscInspectionChecklistViewHolder.image_recyclerView.setVisibility(View.VISIBLE);
        vesselPscInspectionChecklistViewHolder.image_recyclerView.setHasFixedSize(true);
        vesselPscInspectionChecklistViewHolder.image_recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        vesselPscInspectionChecklistViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);
        vesselPscInspectionChecklistViewHolder.photoButton1.setVisibility(View.VISIBLE);

        Log.e("checkSizebefore", String.valueOf(vesselChecklistRecordDataLists.size()));
        if (tempTitleOfSection.equals(vesselComponent.getTitle_of_section())) {
            if(i!=0)
            vesselPscInspectionChecklistViewHolder.textViewTitlePSC.setVisibility(View.GONE);
        } else {
            vesselPscInspectionChecklistViewHolder.textViewTitlePSC.setText(vesselComponent.getTitle_of_section());
            tempTitleOfSection = vesselComponent.getTitle_of_section();
        }
        vesselPscInspectionChecklistViewHolder.textView_questionPSC.setText(vesselComponent.getRb_points_description());

        itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, vesselPscInspectionChecklistViewHolder.submitButton1);
        vesselPscInspectionChecklistViewHolder.image_recyclerView.setHasFixedSize(true);
        vesselPscInspectionChecklistViewHolder.image_recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        vesselPscInspectionChecklistViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);

        vesselPscInspectionChecklistViewHolder.image_recyclerView.setVisibility(View.VISIBLE);
        itemListDataAdapter.notifyDataSetChanged();

        if (!vesselChecklistRecordDataLists.isEmpty()) {
            Log.e("checkSize", String.valueOf(vesselChecklistRecordDataLists.size()));
            for (int j = 0; j < vesselChecklistRecordDataLists.size(); j++) {
                Log.e("checkList:", vesselChecklistRecordDataLists.get(j).getAns());
                if (vesselChecklistRecordDataLists.get(j).getChecklist_points().equals(vesselComponent.getPoints())) {


                    vesselPscInspectionChecklistViewHolder.edit_cmntBtn.setEnabled(false);
                    vesselPscInspectionChecklistViewHolder.submitButton1.setEnabled(false);
                    vesselPscInspectionChecklistViewHolder.photoButton1.setEnabled(false);
                    vesselPscInspectionChecklistViewHolder.linearLayoutTimeStamp.setVisibility(View.VISIBLE);
                    Log.e("visible", "visible");
                    if (vesselChecklistRecordDataLists.get(j).getComment() != null) {
                        vesselPscInspectionChecklistViewHolder.Text_comments.setVisibility(View.VISIBLE);
                        String commentTitle = "<b>" + "Comment:" + "</b>" + vesselChecklistRecordDataLists.get(j).getComment();
                        vesselPscInspectionChecklistViewHolder.Text_comments.setText(Html.fromHtml(commentTitle));
                    }

                    String lctn = vesselChecklistRecordDataLists.get(j).getCrnt_lctn();
                    if (lctn != null) {
                        String[] temt = lctn.split("_", 2);
                        lctn = temt[0];
                    }
                    String textSubmitTitle = "<b>" + "Time:" + "</b>" + vesselChecklistRecordDataLists.get(j).getGnrtn_date() + "<b>" + "\nLocation:" + "</b>" + lctn;
                    vesselPscInspectionChecklistViewHolder.Text_submit.setText(Html.fromHtml(textSubmitTitle));

                    if (vesselChecklistRecordDataLists.get(j).getImage_data() != null && vesselChecklistRecordDataLists.get(j).getImage_data().contains("http")) {
                        vesselPscInspectionChecklistViewHolder.image_recyclerView.setVisibility(View.VISIBLE);

                        image = vesselChecklistRecordDataLists.get(j).getImage_data();
                        Log.e("image_data", image);
                        String[] images = StringUtils.substringsBetween(image, "\"", "\"");
                        if (images != null) {
                            imagesList.clear();
                            for (String temp : images) {
                                if (temp.contains("https")) {
                                    imagesList.add(temp);
                                }
                            }
                            Log.e("images", String.valueOf(imagesList));
                        } else {

                        }
                        itemsList = new String[imagesList.size()];
                        itemsList = imagesList.toArray(itemsList);

//                        ********************************

                        JSONArray imgArray = null;

                        try {
                            imgArray = new JSONArray(image);
                            for (int r = 0; r < imgArray.length(); r++) {
                                JSONObject imgObj = imgArray.getJSONObject(r);
                                locationList.add(imgObj.getString("imagelocation"));
                                timeStampList.add(imgObj.getString("imagetimedate"));
                            }
                            location = new String[locationList.size()];
                            time = new String[timeStampList.size()];
                            location = locationList.toArray(location);
                            time = timeStampList.toArray(time);
                            vesselPscInspectionChecklistViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);
//                            itemListDataAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


//                        ********************************

                        itemListDataAdapter = new ImageListDataAdapter(context, imagesList, "", i, locationList, timeStampList, vesselPscInspectionChecklistViewHolder.submitButton1);
                        vesselPscInspectionChecklistViewHolder.image_recyclerView.setHasFixedSize(true);
                        vesselPscInspectionChecklistViewHolder.image_recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        vesselPscInspectionChecklistViewHolder.image_recyclerView.setAdapter(itemListDataAdapter);


                    } else {
                        vesselPscInspectionChecklistViewHolder.image_recyclerView.setVisibility(View.VISIBLE);

                    }


                }
            }
        }

       /* Log.e("mypoints..", String.valueOf(points));
        if (points != null && points.equals("mypoints") && vesselComponent.getCuserpoint().equalsIgnoreCase("false")) {
            vesselPscInspectionChecklistViewHolder.PSCRootView.setVisibility(View.GONE);
        } else if (points != null && points.equals("allpoints") && vesselComponent.getCuserpoint().equalsIgnoreCase("false")) {
            vesselPscInspectionChecklistViewHolder.PSCRootView.setVisibility(View.VISIBLE);
        }
*/
        ImageListDataAdapter finalItemListDataAdapter = itemListDataAdapter;
        vesselPscInspectionChecklistViewHolder.photoButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView[] imageViews = {};
                vesselImageInterface.onPhotoButtonClickListenerAdapter(i, vesselComponent.getPoints(), imageViews, finalItemListDataAdapter, imagesList, locationList, timeStampList,"yes");
            }
        });

        vesselPscInspectionChecklistViewHolder.image_recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onItemClick(i);
            }
        });

        vesselPscInspectionChecklistViewHolder.submitButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton[] radioButtons = {vesselPscInspectionChecklistViewHolder.rb1YesPSC,
                        vesselPscInspectionChecklistViewHolder.rb1NoPSC,
                        vesselPscInspectionChecklistViewHolder.rb1NaPSC};
                ImageButton editTextComment = vesselPscInspectionChecklistViewHolder.edit_cmntBtn;
                ImageButton button = vesselPscInspectionChecklistViewHolder.submitButton1;
                ImageButton photoButton = vesselPscInspectionChecklistViewHolder.photoButton1;
                Switch aSwitch = new Switch(context);
                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String strDate = formatter.format(date);

                points = vesselComponent.getPoints();

                if (componentList.size() > i) {
                    j = i;
                    j++;
                }

                vesselImageInterface.onSubmitButtonClickListener(i, points, radioButtons, editTextComment, aSwitch, button,
                        vesselPscInspectionChecklistViewHolder.textView_questionPSC,
                        vesselPscInspectionChecklistViewHolder.Text_submit, strDate,
                        vesselPscInspectionChecklistViewHolder.linearLayoutTimeStamp,
                        vesselPscInspectionChecklistViewHolder.submitButton1, photoButton,
                        finalItemListDataAdapter, imagesList, locationList, timeStampList, Tempspinner);

            }
        });

        vesselPscInspectionChecklistViewHolder.buttonGuidance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vesselImageInterface.onGuidanceButtonClickListener(i, vesselComponent.getPoints(), context);

            }
        });

        vesselPscInspectionChecklistViewHolder.edit_cmntBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.comment_layout);
                EditText editText = dialog.findViewById(R.id.editText);
                Button submitButton = dialog.findViewById(R.id.button);
                Button cancelButton = dialog.findViewById(R.id.buttonCancel);
                dialog.setCancelable(false);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String commenttitle = "<b>" + "Comment:" + "</b>" + editText.getText().toString();
                        vesselPscInspectionChecklistViewHolder.Text_comments.setVisibility(View.VISIBLE);
                        vesselPscInspectionChecklistViewHolder.Text_comments.setText(Html.fromHtml(commenttitle));

                        vesselImageInterface.onCommentButtonClickListener(i, vesselComponent.getPoints(), context, editText,
                                vesselPscInspectionChecklistViewHolder.linearLayoutTimeStamp);

                        hideKeyboardFrom(context, v);
                        dialog.dismiss();

                    }
                });
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeyboardFrom(context, v);
                        dialog.dismiss();
                    }
                });
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        });

    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public int getItemCount() {
        return componentList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }

    public void setImageMap(Map<Integer, List<String>> imageMap,
                            Map<Integer, List<String>> locationMap,
                            Map<Integer, List<String>> timeStampMap) {
        this.imageMap = imageMap;
        this.locationMap = locationMap;
        this.timeStampMap = timeStampMap;
        Log.e("imageMap", String.valueOf(imageMap.size()));
        Log.e("locatinMap", String.valueOf(locationMap.size()));
        Log.e("timeStamp", String.valueOf(timeStampMap.size()));
    }

    public void setLastData(int lastDataSize) {
        this.lastDataSize = lastDataSize;
        Log.e("timeStamp", String.valueOf(timeStampMap.size()));
    }

    public interface OnItemClicked {
        void onItemClick(int position);
    }

    public class VesselPscInspectionChecklistViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitlePSC, textView_questionPSC;
        TextView Text_comments, Text_submit;
        RadioGroup radioGroup_ans;
        RadioButton rb1YesPSC;
        RadioButton rb1NoPSC;
        RadioButton rb1NaPSC;
        ImageButton edit_cmntBtn;
        ImageButton photoButton1;
        ImageButton submitButton1;
        ImageButton buttonGuidance;
        RecyclerView image_recyclerView;
        LinearLayout linearLayoutImageViews;
        LinearLayout linearLayoutTimeStamp, PSCRootView;
        RelativeLayout relativeLayoutOfCheckpoints;

        public VesselPscInspectionChecklistViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitlePSC = itemView.findViewById(R.id.textViewTitlePSC);
            buttonGuidance = itemView.findViewById(R.id.buttonGuidancePSC);
            textView_questionPSC = itemView.findViewById(R.id.textView_questionPSC);

            Text_comments = itemView.findViewById(R.id.editText_commentsPSC);
            Text_submit = itemView.findViewById(R.id.editText_submitPSC);

            radioGroup_ans = itemView.findViewById(R.id.radioGroup_ansPSC);
            rb1YesPSC = itemView.findViewById(R.id.rb1YesPSC);
            rb1NoPSC = itemView.findViewById(R.id.rb1NoPSC);
            rb1NaPSC = itemView.findViewById(R.id.rb1NaPSC);
            edit_cmntBtn = itemView.findViewById(R.id.edit_cmntPSC);
            photoButton1 = itemView.findViewById(R.id.photoButton1PSC);
            submitButton1 = itemView.findViewById(R.id.submitButton1PSC);
            linearLayoutImageViews = itemView.findViewById(R.id.linearLayoutImageViews);
            image_recyclerView = itemView.findViewById(R.id.image_recyclerViewPSC);
            linearLayoutTimeStamp = itemView.findViewById(R.id.linearLayoutTimeStampPSC);
            relativeLayoutOfCheckpoints = itemView.findViewById(R.id.relativeLayoutOfCheckpoints);
            PSCRootView = itemView.findViewById(R.id.PSCRootView);


        }
    }

}
