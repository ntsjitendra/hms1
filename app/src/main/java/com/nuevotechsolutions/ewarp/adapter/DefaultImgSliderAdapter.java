package com.nuevotechsolutions.ewarp.adapter;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ImageModel;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoViewAttacher;



public class DefaultImgSliderAdapter  extends PagerAdapter {


        private ArrayList<ImageModel> imageModelArrayList;
        private LayoutInflater inflater;
        private Context context;


        public DefaultImgSliderAdapter(Context context, ArrayList<ImageModel> imageModelArrayList) {
            this.context = context;
            this.imageModelArrayList = imageModelArrayList;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return imageModelArrayList.size();
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View imageLayout = inflater.inflate(R.layout.item_slider, view, false);

            assert imageLayout != null;
            final ImageView imageView = (ImageView) imageLayout
                    .findViewById(R.id.image);
            Glide.with(context)
                    .load(imageModelArrayList.get(position).getImage_drawable())
                    .into(imageView);

//        imageView.setImageResource(imageModelArrayList.get(position).getImage_drawable());

            view.addView(imageLayout, 0);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.pop_up_window_image);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    ImageView imageView1 = dialog.findViewById(R.id.imagevewPopup);
                    ImageView popupclose = dialog.findViewById(R.id.closePopup);
                    imageView1.setImageResource(imageModelArrayList.get(position).getImage_drawable());
                    PhotoViewAttacher pAttacher;
                    pAttacher = new PhotoViewAttacher(imageView1);
                    pAttacher.update();
                    popupclose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });
            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }



}
