package com.nuevotechsolutions.ewarp.adapter;


import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.google.gson.JsonObject;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.TeamSelectionInterFace;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;

import java.util.ArrayList;
import java.util.List;

public class TeamSelectionAdapter extends RecyclerView.Adapter<TeamSelectionAdapter.TeamSelectionViewHolder> {

    public Context context;
    private List<TeamSelectionList> list;
    private List<String> engineerCtgry;
    private List<String> engineerName;
    private TeamSelectionInterFace teamSelectionInterFace;
    private SparseBooleanArray itemStateArray = new SparseBooleanArray();
    private JsonObject selectedcheckbox;
    private CheckListDatabase db;
    private static boolean isSelectedAll=false;
    public ArrayList<String> selectedStrings = new ArrayList<String>();



    public TeamSelectionAdapter(List<TeamSelectionList> list, Context context,TeamSelectionInterFace teamSelectionInterFace)
    {
        isSelectedAll=false;
        this.context=context;
        this.list=list;
        this.teamSelectionInterFace=teamSelectionInterFace;
        setHasStableIds(true);
    }

    public List<TeamSelectionList> selectAll(){
        isSelectedAll=true;
        Log.e("testing", String.valueOf(list.size()));
        notifyDataSetChanged();
        return list;
    }
    public List<TeamSelectionList> unselectall(){
        isSelectedAll=false;
        notifyDataSetChanged();
        return list;
    }

    @NonNull
    @Override
    public TeamSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        View view=inflater.inflate(R.layout.team_selection_item,viewGroup,false);
        return new TeamSelectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamSelectionViewHolder teamSelectionViewHolder, int i) {
        TeamSelectionList stringList = list.get(i);

        teamSelectionViewHolder.engineerCtgry.setText(stringList.getRank());
        teamSelectionViewHolder.engineerName.setText(stringList.getFirst_name());
        teamSelectionViewHolder.empId.setText(stringList.getUser_id());
        teamSelectionViewHolder.checkBox.setChecked(stringList.isChecked());
        teamSelectionViewHolder.checkBox.setTag(list.get(i));

        db = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        List<UserDetails> userDetails=new ArrayList<>();
        userDetails = db.productDao().getUserDetail();

        /*if (isSelectedAll==true){
            teamSelectionViewHolder.checkBox.setChecked(true);
        } else {
            teamSelectionViewHolder.checkBox.setChecked(false);
        }*/

        if (stringList.getUser_id()!=null&&stringList.getUser_id().equals(userDetails.get(0).getUser_id())){
            teamSelectionViewHolder.checkBox.setChecked(true);
            teamSelectionViewHolder.checkBox.setTag(list.get(i));
            teamSelectionViewHolder.checkBox.setEnabled(false);
        }

        teamSelectionViewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TeamSelectionList stringlist1 = (TeamSelectionList) teamSelectionViewHolder.checkBox.getTag();


                stringlist1.setChecked(teamSelectionViewHolder.checkBox.isChecked());

                list.get(i).setChecked(teamSelectionViewHolder.checkBox.isChecked());

            }
        });

        teamSelectionViewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                TextView engineerCtgryTextView=teamSelectionViewHolder.engineerCtgry;
                TextView engineerNameTextView=teamSelectionViewHolder.engineerName;
                TextView emplId = teamSelectionViewHolder.empId;
                teamSelectionInterFace.onTeamSelectionButtonClickListener(i,engineerCtgryTextView,engineerNameTextView,emplId);
                if (isChecked) {
                    selectedStrings.add(teamSelectionViewHolder.engineerCtgry.getText().toString());
                    Log.e("selectedstring", String.valueOf(selectedStrings.size()));
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public  class TeamSelectionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView engineerCtgry,engineerName,empId;
        CheckBox checkBox;
        Spinner teamListSpinner;
        LinearLayout linear_team_one;

        public TeamSelectionViewHolder(@NonNull View itemView) {
            super(itemView);
            engineerCtgry=itemView.findViewById(R.id.engineerLabel);
            empId=itemView.findViewById(R.id.empId);
            checkBox=itemView.findViewById(R.id.checkbox);
            engineerName=itemView.findViewById(R.id.empNameTextView);
            linear_team_one = itemView.findViewById(R.id.linear_team_one);
//            this.setIsRecyclable(false);
            itemView.setOnClickListener(this);
//            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    TextView engineerCtgryTextView=engineerCtgry;
//                    TextView engineerNameTextView=engineerName;
//                    int i = 0;
//                    teamSelectionInterFace.onTeamSelectionButtonClickListener(i,engineerCtgryTextView,engineerNameTextView);
//                }
//            });
        }


        @Override
        public void onClick(View v) {

        }
    }

public void updateList(List<TeamSelectionList> teamlist){
    this.list = teamlist;
    notifyDataSetChanged();
}
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
