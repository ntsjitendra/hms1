package com.nuevotechsolutions.ewarp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.nuevotechsolutions.ewarp.R;

public class PendingActivity extends AppCompatActivity {
    TextView tvloginback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending);
        tvloginback = findViewById(R.id.tvloginback);
        tvloginback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PendingActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(PendingActivity.this,LoginActivity.class);
        startActivity(intent);

    }
}
