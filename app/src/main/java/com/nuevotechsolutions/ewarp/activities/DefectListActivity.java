package com.nuevotechsolutions.ewarp.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.DefectListAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.DefectListModel.DefectListDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklist;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselComponent;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselMasterWorkId;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefectListActivity extends AppCompatActivity {
    TextView textView_vessel_header;
    DrawerLayout drawerLayout;

    Toolbar toolbar;


    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    private ImageView filterOption,createDefectList;

    AlertDialog.Builder alertDialog;
    private CheckListDatabase checkListDatabase;
    List<VesselChecklist> vesselChecklists;
    List<VesselComponent> vesselComponentList;
    List<VesselMasterWorkId> vesselMasterWorkIdList;
    ProgressDialog pdLoading;
    private SwipeRefreshLayout CheklistSwiperefresh;

    DefectListAdapter defectlistAdapter;
    RecyclerView defectlist;
    String empId,company,accessToken,wrk_id;
    List<DefectListDataList> defectListDataLists = new ArrayList<>();
    List<VesselChecklistRecordDataList> checklistRecordDataList = new ArrayList<>();
    LinearLayout linearLayout;

    ArrayAdapter<String> arrayAdapterDprtmt;
    ArrayAdapter<String> arrayAdapterPriority;
    ArrayAdapter<String> arrayAdapterStatus;
    String departments[]={"ALL","DECK","ENGINE","GALLEY"};
    String priorities[]={"ALL","LOW","MEDIUM","HIGH"};
    String status[]={"ALL","OPEN","WORKING","CLOSED","DRY-DOCK","CANCEL"};
    int deptFlagPostn=0,prtyFlagPostn=0,statsFlagPostn=0;
    List<UserDetails> userDetails=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_defect_list);

        textView_vessel_header=findViewById(R.id.textView_homePage_header_defectlist);
        textView_vessel_header.setText("Defect List");

        toolbar=findViewById(R.id.toolbarDefect);
        createDefectList=toolbar.findViewById(R.id.createDefectChecklist);
        vesselChecklists=new ArrayList<>();
        vesselComponentList=new ArrayList<>();
        vesselMasterWorkIdList=new ArrayList<>();
        pdLoading = new ProgressDialog(DefectListActivity.this);
        pdLoading.setTitle(getString(R.string.please_wait));
        linearLayout=findViewById(R.id.linearLayoutDefectList);
        defectlist=findViewById(R.id.DefectListRecyclerView);
        CheklistSwiperefresh=findViewById(R.id.CheklistSwiperefreshDefectList);
        arrayAdapterDprtmt=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,departments);
       arrayAdapterPriority=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,priorities);
        arrayAdapterStatus=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,status);

        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
//        View hView =  navigationView.getHeaderView(0);
//        TextView nav_user = (TextView)hView.findViewById(R.id.userNameLabel);


        userDetails=checkListDatabase.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name()!=null){
            String user=userDetails.get(0).getFirst_name();
//            nav_user.setText(user);
        }else{
//            nav_user.setText("Guest User");
        }

        setUpToolbar();
        filterOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterDialog();
            }
        });

        getData();
        CheklistSwiperefresh.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        CheklistSwiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onDefectList(userDetails.get(0).getUser_id(), userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(), userDetails.get(0).getAccessToken());
                Toast.makeText(DefectListActivity.this, "Refreshed", Toast.LENGTH_SHORT).show();
                CheklistSwiperefresh.setRefreshing(false);
                getData();
            }
        });
        createDefectList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = new AlertDialog.Builder(DefectListActivity.this);
                alertDialog.setTitle("Alert Dialog");
                alertDialog.setIcon(R.drawable.ewarpletest);
                alertDialog.setMessage("Are you sure? This will begin a Defect List.");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pdLoading.show();
                        List<UserDetails> userDetails=new ArrayList<>();
                        checkListDatabase = Room.databaseBuilder(getApplicationContext(), CheckListDatabase.class,
                                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                        userDetails=checkListDatabase.productDao().getUserDetail();
                        String user_id=userDetails.get(0).getUser_id();
                        Map<String, String> params = new HashMap<>();
                        params.put("dbnm", userDetails.get(0).getDbnm());
                        params.put("user_id", userDetails.get(0).getUser_id());
                        params.put("employee_id", userDetails.get(0).getEmployee_id());
                        params.put("accessToken", userDetails.get(0).getAccessToken());

                        VolleyMethods.makePostJsonObjectRequest(params, DefectListActivity.this, new ApiUrlClass().CREATE_VESSLE_LIST, new PostJsonObjectRequestCallback() {
                            @Override
                            public void onSuccessResponse(JSONObject response) {
                                Log.e("responsedefect", String.valueOf(response));
                                if (response != null) {

                                    try {
                                        Integer workId = response.getInt("wrk_id");

                                        DefectListDataList defectListDataList = new DefectListDataList();
                                        defectListDataList.setWrk_id(workId);
                                        defectListDataList.setAdded_date(new UtilClassFuntions().currentDateTime());
                                        defectListDataList.setStatus(0);
                                        defectListDataList.setCreated_by(user_id);

                                        Thread threadSetSubmit = new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                checkListDatabase.productDao().setDefectList(defectListDataList);
                                            }
                                        });
                                        threadSetSubmit.start();

                                        Intent intent = new Intent(DefectListActivity.this, DefectListChecklistActivity.class);
                                        intent.putExtra("wrk_id", workId);
                                        startActivity(intent);
                                        pdLoading.hide();
                                        dialog.dismiss();
                                        Log.e("workId", String.valueOf(workId));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                            }

                            @Override
                            public void onVolleyError(VolleyError error) {
                                if (pdLoading != null && pdLoading.isShowing()) {
                                    pdLoading.dismiss();
                                    Toast.makeText(DefectListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onTokenExpire() {
                                if (pdLoading != null && pdLoading.isShowing()) {
                                    pdLoading.dismiss();
                                }
                            }
                        });

                    }
                });

                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(DefectListActivity.this, "No", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });



    }

    public void FilterDialog(){
    filterOption.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
    Dialog filterdialog = new Dialog(DefectListActivity.this);
    filterdialog.setContentView(R.layout.defectlistfilterdialog);
    filterdialog.setTitle("Filter");
    filterdialog.setCancelable(false);
        Spinner deptSpinner =(Spinner)filterdialog.findViewById(R.id.deptSpinner);
        Spinner prioritySpinner =(Spinner)filterdialog.findViewById(R.id.prioritySpinner);
        Spinner statusSpinner =(Spinner)filterdialog.findViewById(R.id.statusSpinner);
        deptSpinner.setAdapter(arrayAdapterDprtmt);
        prioritySpinner.setAdapter(arrayAdapterPriority);
        statusSpinner.setAdapter(arrayAdapterStatus);
        deptSpinner.setSelection(deptFlagPostn);
        prioritySpinner.setSelection(prtyFlagPostn);
        statusSpinner.setSelection(statsFlagPostn);
        Button btnCancel =(Button)filterdialog.findViewById(R.id.btn_Cancel);
        Button btn_ok = (Button)filterdialog.findViewById(R.id.btn_ok);
        deptSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deptFlagPostn=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        prioritySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                prtyFlagPostn=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statsFlagPostn=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterdialog.dismiss();
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DefectListActivity.this,
                        "hello"+deptFlagPostn+prtyFlagPostn+statsFlagPostn, Toast.LENGTH_SHORT).show();
                filterdialog.dismiss();
            }
        });
       filterdialog.show();
        Window window = filterdialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    }
  });

    }

    public void getData() {
        List<DefectListDataList> list = new ArrayList<>();
        List<UserDetails> userDetails = new ArrayList<>();

        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        list = checkListDatabase.productDao().getDefectListData();
        userDetails = checkListDatabase.productDao().getUserDetail();
        empId = String.valueOf(userDetails.get(0).getUser_id());
        company = String.valueOf(userDetails.get(0).getComp_name());

        if (defectlistAdapter == null) {

        } else {
            list.clear();
            for (int j = defectListDataLists.size() - 1; j >= 0; j--) {
                list.add(defectListDataLists.get(j));
            }
        }
        LinearLayoutManager llm = new LinearLayoutManager(DefectListActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        defectlist.setLayoutManager(llm);
        List<UserDetails> finalUserDetails = userDetails;
        List<DefectListDataList> finalList = list;
        defectlistAdapter = new DefectListAdapter(DefectListActivity.this, list, new DefectListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DefectListDataList item, int position) {
                finalList.get(position).getWrk_id();
                String workId = String.valueOf(finalList.get(position).getWrk_id());
                String dbnm = String.valueOf(finalUserDetails.get(0).getDbnm());
                wrk_id=workId;
                String checklist_type = "";

                onResumeItemClick(workId,empId, dbnm,  company, accessToken);

                Intent intent = new Intent(DefectListActivity.this, DefectListChecklistActivity.class);
                intent.putExtra("wrk_id", workId);
                intent.putExtra("checklist_type", checklist_type);
/*
                new SharedPrefancesClearData().ClearDescriptionDataKey(PSCInspectionActivity.this);

                SharedPreferences.Editor editor1 = PSCInspectionActivity.this.getSharedPreferences("descriptionKey", Context.MODE_PRIVATE).edit();

                List<AuxEngLabel> auxEngLabels = new ArrayList<>();
                List<String> auxEngLabels1 = new ArrayList<>();
                auxEngLabels = checkListDatabase.productDao().getAllAuxEngLebels();

                for (int i = 0; i < auxEngLabels.size(); i++) {
                    if (auxEngLabels.get(i).getUid().equals(uId)) {
                        auxEngLabels1.add(auxEngLabels.get(i).getEng_label());
                    }
                }

                List<EnginDescriptionData> enginDescriptionData = new ArrayList<>();
                List<String> enginDescriptionData1 = new ArrayList<>();
                enginDescriptionData = checkListDatabase.productDao().getEnginDescriptionData();

                SharedPreferences prefs = getSharedPreferences("EngDescription", MODE_PRIVATE);

                if (!prefs.contains(workId)) {
                    for (int i = 0; i < enginDescriptionData.size(); i++) {
                        if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                            enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                        }
                    }
                    for (int i = 0; i < auxEngLabels1.size(); i++) {
                        editor1.putString(auxEngLabels1.get(i), enginDescriptionData1.get(i));
                    }

                } else {

                    String ED = prefs.getString(workId, null);
                    String str[] = ED.split(",");
                    Log.e("ed", ED);
                    List<String> al = new ArrayList<String>();
                    al = Arrays.asList(str);
//                    Collections.reverse(al);
                    for (int i = 0; i < auxEngLabels1.size(); i++) {
                        editor1.putString(auxEngLabels1.get(i), al.get(i));
                    }
                    for (int i = 0; i < enginDescriptionData.size(); i++) {
                        try {
                            if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                                enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    for (int i = 0; i < auxEngLabels1.size(); i++) {

                        editor1.putString(auxEngLabels1.get(i), al.get(i));

                    }

                }

                editor1.apply();*/
                startActivity(intent);
            }
        }
        );
        defectlist.setAdapter(defectlistAdapter);
        defectlistAdapter.notifyDataSetChanged();

    }


    private void setUpToolbar() {
//        drawerLayout = findViewById(R.id.drawerLayoutDefect);
        toolbar = findViewById(R.id.toolbarDefect);
        ImageButton backarrowDefectList = findViewById(R.id.backarrowDefectList);
        backarrowDefectList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(DefectListActivity.this);
                finish();
            }
        });
        createDefectList = toolbar.findViewById(R.id.createDefectChecklist);
        filterOption = toolbar.findViewById(R.id.filterOption);
        setSupportActionBar(toolbar);
//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
    }

    private void updateSeen() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().updateVesselMasterWorkId(Integer.parseInt(wrk_id),true);
            }
        });
        threadDeleteAllTables.start();
    }

    public void onResumeItemClick(String wrk_id, String employee_id, String dbnm, String comp_name, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("wrk_id", wrk_id);
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("accessToken", accessToken);
            params.put("seen", "true");

            VolleyMethods.makePostJsonObjectRequest(params, DefectListActivity.this, new ApiUrlClass().resumeApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray checklistRecordData = response.getJSONArray("lastdata");
                            JSONArray enginDescriptionDataArray = response.getJSONArray("engin_data");


                            if (checklistRecordData != null && checklistRecordData.length() > 0) {
                                for (int i = 0; i < checklistRecordData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) checklistRecordData.get(i);
                                    VesselChecklistRecordDataList checklistRecordDataList1 = objectMapper.readValue(jsonObject1.toString(), VesselChecklistRecordDataList.class);
                                    checklistRecordDataList.add(checklistRecordDataList1);
                                }
                            }

//                            if (enginDescriptionDataArray != null && enginDescriptionDataArray.length() > 0) {
//                                for (int i = 0; i < enginDescriptionDataArray.length(); i++) {
//                                    JSONObject jsonObject1 = (JSONObject) enginDescriptionDataArray.get(i);
//                                    EnginDescriptionData enginDescriptionData1 = objectMapper.readValue(jsonObject1.toString(), EnginDescriptionData.class);
//                                    enginDescriptionData.add(enginDescriptionData1);
//                                }
//                            }

                            resumeAndAddData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(DefectListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(DefectListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void resumeAndAddData() {
//        Thread threadDeleteAllTables = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                checkListDatabase.productDao().setResumeDataToTable(checklistRecordDataList/*, enginDescriptionData*/);
//
//            }
//        });
//        threadDeleteAllTables.start();
    }

    public void onResumeChecklist(String employee_id, String dbnm, String comp_name, String statusID, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("statusID", statusID);
            params.put("accessToken", accessToken);

            VolleyMethods.makePostJsonObjectRequest(params, DefectListActivity.this, new ApiUrlClass().checklistApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray massterWorkIdData = response.getJSONArray("masterdata");
                            JSONArray enginDescriptionDataArray = response.getJSONArray("engin_data");

                            if (massterWorkIdData != null && massterWorkIdData.length() > 0) {
                                defectListDataLists.clear();
                                for (int i = 0; i < massterWorkIdData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) massterWorkIdData.get(i);
                                    DefectListDataList defectListDataList = objectMapper.readValue
                                            (jsonObject1.toString(), DefectListDataList.class);
                                    defectListDataLists.add(defectListDataList);
                                }
                            }


//                            CheckListAndAddData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(DefectListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(DefectListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    public void onDefectList(String user_id, String dbnm, String comp_name, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();

        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", user_id);
            params.put("dbnm", dbnm);
            params.put("accessToken", accessToken);

            VolleyMethods.makePostJsonObjectRequest(params, DefectListActivity.this, new ApiUrlClass().DIFECT_LIST, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray defectlistData = response.getJSONArray("masterdata");

                            if (defectlistData != null && defectlistData.length() > 0) {
                                defectListDataLists.clear();
                                for (int i = 0; i < defectlistData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) defectlistData.get(i);
                                    DefectListDataList defectListDataList = objectMapper.readValue
                                            (jsonObject1.toString(), DefectListDataList.class);
                                    defectListDataLists.add(defectListDataList);
                                }
                            }


                            defectListData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(DefectListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(DefectListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void defectListData() {
        Thread threadDeleteAllTables=new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().setDefectListDataList(defectListDataLists);

            }
        });
        threadDeleteAllTables.start();
    }

}
