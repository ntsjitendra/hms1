package com.nuevotechsolutions.ewarp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.VesselShipInspectionAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklist;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselComponent;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselMasterWorkId;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShipInspectionActivity extends AppCompatActivity {

    TextView textView_vessel_header;
    DrawerLayout drawerLayout;
    Toolbar toolbar;

    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;

    ImageView createShipChecklist;
    AlertDialog.Builder alertDialog;
    private CheckListDatabase checkListDatabase;
    List<VesselChecklist> vesselChecklists;
    List<VesselComponent> vesselComponentList;
    List<VesselMasterWorkId> vesselMasterWorkIdList;
    ProgressDialog pdLoading;
    private String uId;
    VesselShipInspectionAdapter vpscAdapter;
    RecyclerView shipList;
    String empId, company, accessToken, wrk_id;
    List<VesselMasterWorkId> vesselMasterWorkIds = new ArrayList<>();
    List<VesselChecklistRecordDataList> checklistRecordDataList = new ArrayList<>();
    LinearLayout linearLayout;
    Integer uid;
    SwipeRefreshLayout CheklistSwiperefreshShip;
    List<UserDetails> userDetails = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ship_inspection);
        getUId();
        textView_vessel_header = findViewById(R.id.textView_homePage_header_shipInsp);
        textView_vessel_header.setText("Ship Inspection");
        toolbar = findViewById(R.id.toolbarShipInsp);
        createShipChecklist = toolbar.findViewById(R.id.createShipChecklist);
        vesselChecklists = new ArrayList<>();
        vesselComponentList = new ArrayList<>();
        vesselMasterWorkIdList = new ArrayList<>();
        pdLoading = new ProgressDialog(ShipInspectionActivity.this);
        pdLoading.setTitle(getString(R.string.please_wait));
        pdLoading.setCancelable(false);
        linearLayout = findViewById(R.id.linearLayoutShipInsp);
        shipList = findViewById(R.id.shipInspectionRecyclerView);
        CheklistSwiperefreshShip = findViewById(R.id.CheklistSwiperefreshShip);

        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();


        userDetails = checkListDatabase.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name() != null) {
            String user = userDetails.get(0).getFirst_name();
//            nav_user.setText(user);
        } else {
//            nav_user.setText("Guest User");
        }

        setUpToolbar();
        getData();
        CheklistSwiperefreshShip.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        CheklistSwiperefreshShip.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onShipInspectionChecklist(userDetails.get(0).getUser_id(), userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(), userDetails.get(0).getAccessToken());
                Toast.makeText(ShipInspectionActivity.this, "Refreshed", Toast.LENGTH_SHORT).show();
                CheklistSwiperefreshShip.setRefreshing(false);
                getData();
            }
        });
        createShipChecklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = new AlertDialog.Builder(ShipInspectionActivity.this);
                alertDialog.setTitle("Alert Dialog");

                alertDialog.setIcon(R.drawable.ewarpletest);
                alertDialog.setMessage("Are you sure? This will begin a Ship Inspection.");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        /*Intent intent=new Intent(PSCInspectionActivity.this,PscInspectionChecklistActivity.class);
                        startActivity(intent);*/
                        pdLoading.show();
                        List<UserDetails> userDetails = new ArrayList<>();
                        checkListDatabase = Room.databaseBuilder(getApplicationContext(), CheckListDatabase.class,
                                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                        userDetails = checkListDatabase.productDao().getUserDetail();
                        List<VesselMasterWorkId> vesellist = new ArrayList<>();
                        vesellist = checkListDatabase.productDao().getVesselMasterWorkIdData();
                        Map<String, String> params = new HashMap<>();
                        params.put("dbnm", userDetails.get(0).getDbnm());
                        params.put("user_id", userDetails.get(0).getUser_id());
                        params.put("employee_id", userDetails.get(0).getEmployee_id());
                        params.put("uid", String.valueOf(vesellist.get(0).getUid()));
                        params.put("accessToken", userDetails.get(0).getAccessToken());
// Change Ship inspection url only .

                        List<UserDetails> finalUserDetails = userDetails;
                        VolleyMethods.makePostJsonObjectRequest(params, ShipInspectionActivity.this, new ApiUrlClass().CREATE_VESSLE_LIST, new PostJsonObjectRequestCallback() {
                            @Override
                            public void onSuccessResponse(JSONObject response) {
                                if (response != null) {
                                    Log.e("vesselResponse", String.valueOf(response));

                                    try {
                                        Integer workId = response.getInt("wrk_id");

                                        VesselMasterWorkId vesselMasterWorkId = new VesselMasterWorkId();
                                        vesselMasterWorkId.setWrk_id(workId);
                                        vesselMasterWorkId.setUid(uid);
                                        vesselMasterWorkId.setGrtd_by(finalUserDetails.get(0).getUser_id());
                                        vesselMasterWorkId.setCrnt_date(new UtilClassFuntions().currentDateTime());
                                        vesselMasterWorkId.setStatus(0);

                                        Thread threadSetSubmit = new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                checkListDatabase.productDao().setVesselMasterWorkId(vesselMasterWorkId);
                                            }
                                        });

                                        threadSetSubmit.start();

                                        Intent intent = new Intent(ShipInspectionActivity.this, ShipInspectionChecklistActivity.class);
                                        intent.putExtra("uid", uId);
                                        intent.putExtra("wrk_id", workId);
                                        startActivity(intent);
                                        pdLoading.hide();
                                        dialog.dismiss();
                                        Log.e("workId", String.valueOf(workId));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        JSONObject jsonObject = response.getJSONObject("vessel_inspection_data");
                                        ObjectMapper objectMapper = new ObjectMapper();


                                        if (jsonObject != null) {
                                            JSONArray list = jsonObject.getJSONArray("list");
                                            JSONArray component = jsonObject.getJSONArray("checklist_points");


                                            if (list != null && list.length() > 0) {
                                                for (int i = 0; i < list.length(); i++) {
                                                    JSONObject jsonObject1 = (JSONObject) list.get(i);
                                                    VesselChecklist vesselChecklist = objectMapper.readValue(jsonObject1.toString(), VesselChecklist.class);
                                                    vesselChecklists.add(vesselChecklist);
                                                }
                                            }

                                            if (component != null && component.length() > 0) {
                                                for (int i = 0; i < component.length(); i++) {
                                                    JSONObject jsonObject1 = (JSONObject) component.get(i);
                                                    VesselComponent vesselComponent = objectMapper.readValue(jsonObject1.toString(), VesselComponent.class);
                                                    vesselComponentList.add(vesselComponent);
                                                }

                                            }
                                            dialog.dismiss();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (JsonParseException e) {
                                        e.printStackTrace();
                                    } catch (JsonMappingException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }

                            @Override
                            public void onVolleyError(VolleyError error) {
                                if (pdLoading != null && pdLoading.isShowing()) {
                                    pdLoading.dismiss();
                                    Toast.makeText(ShipInspectionActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onTokenExpire() {
                                if (pdLoading != null && pdLoading.isShowing()) {
                                    pdLoading.dismiss();
                                }
                            }
                        });

                    }
                });

                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(ShipInspectionActivity.this, "No", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });

    }

    private void getUId() {
        List<VesselChecklist> checklists = new ArrayList<>();
        List<VesselComponent> list = new ArrayList<>();
        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        checklists = checkListDatabase.productDao().getAllVesselCheckList();

        for (int r = 0; r < checklists.size(); r++) {
            if (checklists.get(r).getList().equals("SHIP INSPECTION")) {
                uid = Integer.valueOf(checklists.get(r).getUid());
            }
        }
    }

    private void setUpToolbar() {
//        drawerLayout = findViewById(R.id.drawerLayoutShipInsp);
        toolbar = findViewById(R.id.toolbarShipInsp);
        ImageButton backarrowShipInspection = findViewById(R.id.backarrowShipInspection);
        backarrowShipInspection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(ShipInspectionActivity.this);
                finish();
            }
        });
        setSupportActionBar(toolbar);

    }


    public void getData() {
        List<VesselMasterWorkId> list = new ArrayList<>();
        List<VesselMasterWorkId> list1 = new ArrayList<>();
        List<VesselChecklist> checkList = new ArrayList<>();
        List<UserDetails> userDetails = new ArrayList<>();

        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        list = checkListDatabase.productDao().getVesselMasterWorkIdData();
        Log.e("list", String.valueOf(list.size()));
        checkList = checkListDatabase.productDao().getAllVesselCheckList();
        userDetails = checkListDatabase.productDao().getUserDetail();
        empId = String.valueOf(userDetails.get(0).getUser_id());
        company = String.valueOf(userDetails.get(0).getComp_name());
        accessToken = userDetails.get(0).getAccessToken();
        for (int i = 0; i < checkList.size(); i++) {
            if (checkList.get(i).getList().equals("SHIP INSPECTION")) {
                uId = checkList.get(i).getUid();
            }
        }

        if (vpscAdapter == null) {
            int j = 0;
            for (int i = list.size() - 1; i >= 0; i--) {
                if (list.get(i).getStatus().equals(0)) {
                    list1.add(j, list.get(i));
                    j++;
                }
            }
        } else {
            list1.clear();
            for (int j = vesselMasterWorkIds.size() - 1; j >= 0; j--) {
                list1.add(vesselMasterWorkIds.get(j));
            }
        }
//        uId = String.valueOf(list1.get(0).getUid());
        LinearLayoutManager llm = new LinearLayoutManager(ShipInspectionActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        shipList.setLayoutManager(llm);
        List<UserDetails> finalUserDetails = userDetails;
        vpscAdapter = new VesselShipInspectionAdapter(this, list1, new VesselShipInspectionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(VesselMasterWorkId item, int position) {
                list1.get(position).getWrk_id();
//                 uId = String.valueOf(list1.get(position).getUid());
                String workId = String.valueOf(list1.get(position).getWrk_id());
                String dbnm = String.valueOf(finalUserDetails.get(0).getDbnm());
                wrk_id = workId;

//                List<VesselChecklist> checkLists = new ArrayList<>();
//                List<VesselChecklist> checkLists1 = new ArrayList<>();
//                checkLists = checkListDatabase.productDao().getAllVesselCheckList();

                onShipInspectionItemClick(workId, empId, dbnm, uId, accessToken);
                Log.e("workid", "workiddd" + workId);
                Log.e("empid", "empos" + empId);
                Log.e("dbnm", "dbnm" + dbnm);
                Log.e("uid", "uid" + uId);
                Log.e("accesstoken", "accessToken" + accessToken);

                Intent intent = new Intent(ShipInspectionActivity.this, ShipInspectionChecklistActivity.class);
                intent.putExtra("uid", uId);
                intent.putExtra("wrk_id", workId);
                startActivity(intent);
            }
        }
        );
        shipList.setAdapter(vpscAdapter);
        vpscAdapter.notifyDataSetChanged();

    }

    public void onShipInspectionItemClick(String wrk_id, String employee_id, String dbnm, String uId, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("wrk_id", wrk_id);
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("uid", uId);
//            params.put("comp_name", comp_name);
            params.put("accessToken", accessToken);
//            params.put("seen", "true");
// Ship Inspection Item click API
            VolleyMethods.makePostJsonObjectRequest(params, ShipInspectionActivity.this, new ApiUrlClass().SHIP_INSPECTION_DETAIL_PAGE, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray checklistRecordData = response.getJSONArray("lastdata");

                            if (checklistRecordData != null && checklistRecordData.length() > 0) {
                                for (int i = 0; i < checklistRecordData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) checklistRecordData.get(i);
                                    VesselChecklistRecordDataList checklistRecordDataList1 = objectMapper.readValue(jsonObject1.toString(), VesselChecklistRecordDataList.class);
                                    checklistRecordDataList.add(checklistRecordDataList1);
                                }
                            }

//                            resumeAndAddData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
//                    Toast.makeText(ShipInspectionActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(ShipInspectionActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void resumeAndAddData() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
//                checkListDatabase.productDao().setResumeDataToTable(checklistRecordDataList/*, enginDescriptionData*/);

            }
        });
        threadDeleteAllTables.start();
    }

    public void onShipInspectionChecklist(String employee_id, String dbnm, String comp_name, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();

        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", employee_id);
            params.put("uid", String.valueOf(uid));
            params.put("dbnm", dbnm);
            params.put("accessToken", accessToken);

            VolleyMethods.makePostJsonObjectRequest(params, ShipInspectionActivity.this, new ApiUrlClass().SHIP_INSPECTION_LIST, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray vesselMasterWorkIdData = response.getJSONArray("masterdata");

                            if (vesselMasterWorkIdData != null && vesselMasterWorkIdData.length() > 0) {
                                vesselMasterWorkIds.clear();
                                for (int i = 0; i < vesselMasterWorkIdData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) vesselMasterWorkIdData.get(i);
                                    VesselMasterWorkId masterWorkId = objectMapper.readValue
                                            (jsonObject1.toString(), VesselMasterWorkId.class);
                                    vesselMasterWorkIds.add(masterWorkId);
                                }
                            }


                            shipInspection();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(ShipInspectionActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(ShipInspectionActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void shipInspection() {
        Thread threadDeleteAllTables=new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().setShipInspDataList(vesselMasterWorkIds);

            }
        });
        threadDeleteAllTables.start();
    }

}
