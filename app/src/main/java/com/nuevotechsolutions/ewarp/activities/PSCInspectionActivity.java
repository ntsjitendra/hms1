package com.nuevotechsolutions.ewarp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.VesselPSCInspectionAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklist;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselComponent;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselMasterWorkId;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PSCInspectionActivity extends AppCompatActivity {

    TextView textView_vessel_header;
    DrawerLayout drawerLayout;

    Toolbar toolbar;

    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    ImageView createPscChecklist;
    AlertDialog.Builder alertDialog;
    private CheckListDatabase checkListDatabase;
    List<VesselChecklist> vesselChecklists;
    List<VesselComponent> vesselComponentList;
    List<VesselMasterWorkId> vesselMasterWorkIdList;
    ProgressDialog pdLoading;
    private SwipeRefreshLayout CheklistSwiperefresh;

    VesselPSCInspectionAdapter vpscAdapter;
    RecyclerView pscList;
    String empId, company, accessToken, wrk_id;
    List<VesselMasterWorkId> masterWorkIdList = new ArrayList<>();
    List<VesselChecklistRecordDataList> checklistRecordDataList = new ArrayList<>();
    List<EnginDescriptionData> enginDescriptionData = new ArrayList<>();
    LinearLayout linearLayout;
    Integer uid;
    List<UserDetails> userDetails = new ArrayList<>();
    private String uId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psc_inspection);
        getUId();
        textView_vessel_header = findViewById(R.id.textView_homePage_header_pscInspection);
        textView_vessel_header.setText("PSC Inspection");
        toolbar = findViewById(R.id.toolbarPSCInspction);
        createPscChecklist = toolbar.findViewById(R.id.createPscChecklist);
        vesselChecklists = new ArrayList<>();
        vesselComponentList = new ArrayList<>();
        vesselMasterWorkIdList = new ArrayList<>();
        pdLoading = new ProgressDialog(PSCInspectionActivity.this);
        pdLoading.setTitle(getString(R.string.please_wait));
        linearLayout = findViewById(R.id.linearLayoutPSCInspection);
        pscList = findViewById(R.id.pscInspectionRecyclerView);
        CheklistSwiperefresh = findViewById(R.id.CheklistSwiperefreshPSC);

//        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
//        View hView =  navigationView.getHeaderView(0);
//        TextView nav_user = (TextView)hView.findViewById(R.id.userNameLabel);

        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();


        userDetails = checkListDatabase.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name() != null) {
            String user = userDetails.get(0).getFirst_name();
//            nav_user.setText(user);
        } else {
//            nav_user.setText("Guest User");
        }

        setUpToolbar();

        getData();
        CheklistSwiperefresh.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        CheklistSwiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onPscInspectionChecklist(userDetails.get(0).getUser_id(), userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(), userDetails.get(0).getAccessToken());
                Toast.makeText(PSCInspectionActivity.this, "Refreshed", Toast.LENGTH_SHORT).show();
                CheklistSwiperefresh.setRefreshing(false);
                getData();
            }
        });
        createPscChecklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = new AlertDialog.Builder(PSCInspectionActivity.this);
                alertDialog.setTitle("Alert Dialog");
                alertDialog.setIcon(R.drawable.ewarpletest);
                alertDialog.setMessage("Are you sure? This will begin a PSC Inspection.");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        /*Intent intent=new Intent(PSCInspectionActivity.this,PscInspectionChecklistActivity.class);
                        startActivity(intent);*/
                        pdLoading.show();
                        List<UserDetails> userDetails = new ArrayList<>();
                        checkListDatabase = Room.databaseBuilder(getApplicationContext(), CheckListDatabase.class,
                                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                        List<VesselMasterWorkId> vesellist = new ArrayList<>();
                        vesellist = checkListDatabase.productDao().getVesselMasterWorkIdData();

                        userDetails = checkListDatabase.productDao().getUserDetail();
                        Map<String, String> params = new HashMap<>();
                        params.put("dbnm", userDetails.get(0).getDbnm());
                        params.put("user_id", userDetails.get(0).getUser_id());
                        params.put("employee_id", userDetails.get(0).getEmployee_id());
                        params.put("uid", String.valueOf(vesellist.get(0).getUid()));
                        params.put("accessToken", userDetails.get(0).getAccessToken());

                        List<UserDetails> finalUserDetails = userDetails;
                        VolleyMethods.makePostJsonObjectRequest(params, PSCInspectionActivity.this, new ApiUrlClass().CREATE_VESSLE_LIST, new PostJsonObjectRequestCallback() {
                            @Override
                            public void onSuccessResponse(JSONObject response) {
                                if (response != null) {

                                    Log.e("vesselResponse", String.valueOf(response));

                                    try {
                                        Integer workId = response.getInt("wrk_id");
                                        VesselMasterWorkId vesselMasterWorkId = new VesselMasterWorkId();
                                        vesselMasterWorkId.setWrk_id(workId);
                                        vesselMasterWorkId.setUid(uid);
                                        vesselMasterWorkId.setGrtd_by(finalUserDetails.get(0).getUser_id());
                                        vesselMasterWorkId.setCrnt_date(new UtilClassFuntions().currentDateTime());
                                        vesselMasterWorkId.setStatus(0);

                                        Thread threadSetSubmit = new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                checkListDatabase.productDao().setVesselMasterWorkId(vesselMasterWorkId);
                                            }
                                        });

                                        threadSetSubmit.start();

                                        Intent intent = new Intent(PSCInspectionActivity.this, PscInspectionChecklistActivity.class);
                                        intent.putExtra("uid", uId);
                                        intent.putExtra("wrk_id", workId);
                                        startActivity(intent);
                                        pdLoading.hide();
                                        dialog.dismiss();
                                        Log.e("workId", String.valueOf(workId));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        JSONObject jsonObject = response.getJSONObject("vessel_inspection_data");
                                        ObjectMapper objectMapper = new ObjectMapper();


                                        if (jsonObject != null) {
                                            JSONArray list = jsonObject.getJSONArray("list");
                                            JSONArray component = jsonObject.getJSONArray("checklist_points");


                                            if (list != null && list.length() > 0) {
                                                for (int i = 0; i < list.length(); i++) {
                                                    JSONObject jsonObject1 = (JSONObject) list.get(i);
                                                    VesselChecklist vesselChecklist = objectMapper.readValue(jsonObject1.toString(), VesselChecklist.class);
                                                    vesselChecklists.add(vesselChecklist);
                                                }
                                            }

                                            if (component != null && component.length() > 0) {
                                                for (int i = 0; i < component.length(); i++) {
                                                    JSONObject jsonObject1 = (JSONObject) component.get(i);
                                                    VesselComponent vesselComponent = objectMapper.readValue(jsonObject1.toString(), VesselComponent.class);
                                                    vesselComponentList.add(vesselComponent);
                                                }

                                            }
                                            dialog.dismiss();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (JsonParseException e) {
                                        e.printStackTrace();
                                    } catch (JsonMappingException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                }

                            }


                            @Override
                            public void onVolleyError(VolleyError error) {
                                if (pdLoading != null && pdLoading.isShowing()) {
                                    pdLoading.hide();
                                    Toast.makeText(PSCInspectionActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onTokenExpire() {
                                if (pdLoading != null && pdLoading.isShowing()) {
                                    pdLoading.hide();
                                }
                            }
                        });

                    }
                });

                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(PSCInspectionActivity.this, "No", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });

    }

    private void setUpToolbar() {
//        drawerLayout = findViewById(R.id.drawerLayoutPSCInsp);
        toolbar = findViewById(R.id.toolbarPSCInspction);
        ImageButton backarrowPscInspection = findViewById(R.id.backarrowPscInspection);
        backarrowPscInspection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(PSCInspectionActivity.this);
                finish();
            }
        });
        setSupportActionBar(toolbar);
//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
    }

    private void getUId() {
        List<VesselChecklist> checklists = new ArrayList<>();
        List<VesselComponent> list = new ArrayList<>();
        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        checklists = checkListDatabase.productDao().getAllVesselCheckList();

        for (int r = 0; r < checklists.size(); r++) {
            if (checklists.get(r).getList().equals("SHIP INITIAL INSPECTION CHECKLIST")) {
                uid = Integer.valueOf(checklists.get(r).getUid());
            }
        }
    }

    public void getData() {
        List<VesselMasterWorkId> list = new ArrayList<>();
        List<VesselMasterWorkId> list1 = new ArrayList<>();
        List<VesselChecklist> checklists = new ArrayList<>();
        List<UserDetails> userDetails = new ArrayList<>();

        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        list = checkListDatabase.productDao().getVesselMasterWorkIdData();
        userDetails = checkListDatabase.productDao().getUserDetail();
        empId = String.valueOf(userDetails.get(0).getUser_id());
        company = String.valueOf(userDetails.get(0).getComp_name());
        checklists = checkListDatabase.productDao().getAllVesselCheckList();

        for (int r = 0; r < checklists.size(); r++) {
            if (checklists.get(r).getList().equals("SHIP INITIAL INSPECTION CHECKLIST")) {
                uId = checklists.get(r).getUid();
            }
        }

        if (vpscAdapter == null) {
            int j = 0;
            for (int i = list.size() - 1; i >= 0; i--) {
                if (list.get(i).getStatus().equals(0)) {
                    list1.add(j, list.get(i));
                    j++;
                }
            }
        } else {
            list1.clear();
            for (int j = masterWorkIdList.size() - 1; j >= 0; j--) {
                list1.add(masterWorkIdList.get(j));
            }
        }
        LinearLayoutManager llm = new LinearLayoutManager(PSCInspectionActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        pscList.setLayoutManager(llm);
        List<UserDetails> finalUserDetails = userDetails;
        List<UserDetails> finalUserDetails1 = userDetails;

        vpscAdapter = new VesselPSCInspectionAdapter(this, list1, new VesselPSCInspectionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(VesselMasterWorkId item, int position) {
                list1.get(position).getWrk_id();
                String uId = String.valueOf(list1.get(position).getUid());
                String workId = String.valueOf(list1.get(position).getWrk_id());
                String dbnm = String.valueOf(finalUserDetails.get(0).getDbnm());
                wrk_id = workId;

               /* List<VesselChecklist> checkLists = new ArrayList<>();
                List<VesselChecklist> checkLists1 = new ArrayList<>();
                checkLists = checkListDatabase.productDao().getAllVesselCheckList();
                */


                onPscInspectionItemClick(workId, finalUserDetails1.get(0).getUser_id(), dbnm, company, accessToken);

                Intent intent = new Intent(PSCInspectionActivity.this, PscInspectionChecklistActivity.class);
                intent.putExtra("uid", uId);
                intent.putExtra("unitNo", uId);
                intent.putExtra("wrk_id", workId);
/*
                new SharedPrefancesClearData().ClearDescriptionDataKey(PSCInspectionActivity.this);

                SharedPreferences.Editor editor1 = PSCInspectionActivity.this.getSharedPreferences("descriptionKey", Context.MODE_PRIVATE).edit();

                List<AuxEngLabel> auxEngLabels = new ArrayList<>();
                List<String> auxEngLabels1 = new ArrayList<>();
                auxEngLabels = checkListDatabase.productDao().getAllAuxEngLebels();

                for (int i = 0; i < auxEngLabels.size(); i++) {
                    if (auxEngLabels.get(i).getUid().equals(uId)) {
                        auxEngLabels1.add(auxEngLabels.get(i).getEng_label());
                    }
                }

                List<EnginDescriptionData> enginDescriptionData = new ArrayList<>();
                List<String> enginDescriptionData1 = new ArrayList<>();
                enginDescriptionData = checkListDatabase.productDao().getEnginDescriptionData();

                SharedPreferences prefs = getSharedPreferences("EngDescription", MODE_PRIVATE);

                if (!prefs.contains(workId)) {
                    for (int i = 0; i < enginDescriptionData.size(); i++) {
                        if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                            enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                        }
                    }
                    for (int i = 0; i < auxEngLabels1.size(); i++) {
                        editor1.putString(auxEngLabels1.get(i), enginDescriptionData1.get(i));
                    }

                } else {

                    String ED = prefs.getString(workId, null);
                    String str[] = ED.split(",");
                    Log.e("ed", ED);
                    List<String> al = new ArrayList<String>();
                    al = Arrays.asList(str);
//                    Collections.reverse(al);
                    for (int i = 0; i < auxEngLabels1.size(); i++) {
                        editor1.putString(auxEngLabels1.get(i), al.get(i));
                    }
                    for (int i = 0; i < enginDescriptionData.size(); i++) {
                        try {
                            if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                                enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    for (int i = 0; i < auxEngLabels1.size(); i++) {

                        editor1.putString(auxEngLabels1.get(i), al.get(i));

                    }

                }

                editor1.apply();*/
                startActivity(intent);
            }
        }
        );
        pscList.setAdapter(vpscAdapter);
        vpscAdapter.notifyDataSetChanged();

    }

    public void onPscInspectionItemClick(String wrk_id, String user_id, String dbnm, String comp_name, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("wrk_id", wrk_id);
            params.put("user_id", user_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("accessToken", accessToken);

            VolleyMethods.makePostJsonObjectRequest(params, PSCInspectionActivity.this, new ApiUrlClass().SHIP_INSPECTION_DETAIL_PAGE, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray checklistRecordData = response.getJSONArray("lastdata");
                            JSONArray enginDescriptionDataArray = response.getJSONArray("engin_data");


                            if (checklistRecordData != null && checklistRecordData.length() > 0) {
                                for (int i = 0; i < checklistRecordData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) checklistRecordData.get(i);
                                    VesselChecklistRecordDataList checklistRecordDataList1 = objectMapper.readValue(jsonObject1.toString(), VesselChecklistRecordDataList.class);
                                    checklistRecordDataList.add(checklistRecordDataList1);
                                }
                            }

                            resumeAndAddData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
//                    Toast.makeText(PSCInspectionActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(PSCInspectionActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void resumeAndAddData() {
//        Thread threadDeleteAllTables = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                checkListDatabase.productDao().setResumeDataToTable(checklistRecordDataList/*, enginDescriptionData*/);
//
//            }
//        });
//        threadDeleteAllTables.start();
    }

    public void onPscInspectionChecklist(String user_id, String dbnm, String comp_name, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", user_id);
            params.put("dbnm", dbnm);
            params.put("uid", String.valueOf(uid));
            params.put("comp_name", comp_name);
            params.put("accessToken", accessToken);

            VolleyMethods.makePostJsonObjectRequest(params, PSCInspectionActivity.this, new ApiUrlClass().SHIP_INSPECTION_LIST, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray massterWorkIdData = response.getJSONArray("masterdata");

                            if (massterWorkIdData != null && massterWorkIdData.length() > 0) {
                                masterWorkIdList.clear();
                                for (int i = 0; i < massterWorkIdData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) massterWorkIdData.get(i);
                                    VesselMasterWorkId masterWorkId = objectMapper.readValue
                                            (jsonObject1.toString(), VesselMasterWorkId.class);
                                    masterWorkIdList.add(masterWorkId);
                                }
                            }


                            pscInspection();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(PSCInspectionActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(PSCInspectionActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }
    private void pscInspection() {
        Thread threadDeleteAllTables=new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().setShipInspDataList(masterWorkIdList);

            }
        });
        threadDeleteAllTables.start();
    }

}
