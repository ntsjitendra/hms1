package com.nuevotechsolutions.ewarp.activities;


import android.os.Bundle;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import com.nuevotechsolutions.ewarp.R;

public class ManualsWebViewActivity extends AppCompatActivity {

    WebView manualsWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manuals_web_view);

        manualsWebView=findViewById(R.id.manualsWebView);

    }
}
