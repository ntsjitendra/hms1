package com.nuevotechsolutions.ewarp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.RecordingListAdapter;
import com.nuevotechsolutions.ewarp.interfaces.AudioRemoveInterface;
import com.nuevotechsolutions.ewarp.model.VoiceRecordData;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RecordingListActivity extends AppCompatActivity {

    ArrayList<VoiceRecordData> recordingArraylist;
    RecordingListAdapter recordingListAdapter;
    List<String> recordingList = new ArrayList<>();
    String fileName, mFilePath;
    int position;
    ImageView imgTool;
    ArrayList<String> fileNameList = new ArrayList<>();
    ArrayList<String> addressList = new ArrayList<>();
    private Toolbar toolbar;
    private RecyclerView recyclerViewRecordings;
    File file;
    boolean clicked = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_listdialog);


        initViews();

        fetchRecordings();
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgTool = (ImageView) toolbar.findViewById(R.id.imgTool);
        recordingArraylist = new ArrayList<VoiceRecordData>();
        /** setting up the toolbar  **/
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setTitle("Recording List");
//        toolbar.setTitleTextColor(getResources().getColor(android.R.color.black));
//        setSupportActionBar(toolbar);

        /** enabling back button ***/
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /** setting up recyclerView **/
        recyclerViewRecordings = (RecyclerView) findViewById(R.id.recyclerViewRecordings);
        recyclerViewRecordings.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewRecordings.setHasFixedSize(true);

        imgTool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
                onBackPressed();

            }
        });
//        textViewNoRecordings = (TextView) findViewById(R.id.textViewNoRecordings);

    }

    private void fetchRecordings() {
        Intent intent = getIntent();

//        fileName = intent.getStringExtra("filename");
//        mFilePath = intent.getStringExtra("filepath");
        position = intent.getIntExtra("position", -1);
        fileNameList = intent.getStringArrayListExtra("fileNameList");
        addressList = intent.getStringArrayListExtra("addressList");
//        VoiceRecordData recording = new VoiceRecordData(mFilePath, fileNameList, position, false);
//        recordingArraylist.add(recording);
        for (String audio : fileNameList) {
            File myFile = new File(audio);
            boolean b = false;
            if (myFile.exists()) {
                b = true;
            } else {
                b = false;
            }
            file = new File(audio);
            Log.e("File path", "::  " + file.getParent());
            Log.e("File path", "::  " + file.getName());
            VoiceRecordData recording = new VoiceRecordData(file.getPath(),
                    file.getParent(),
                    file.getName(),
                    false,
                    b);
            recordingArraylist.add(recording);
        }
        Log.e("fileNameList size1", String.valueOf(fileNameList.size()));
        Log.e("size1", String.valueOf(recordingArraylist.size()));
        recyclerViewRecordings.setVisibility(View.VISIBLE);
        recordingListAdapter = new RecordingListAdapter(this, recordingArraylist, addressList,new AudioRemoveInterface() {
            @Override
            public void onRemoveAudioClickListener(int position) {
                clicked = true;

                fileNameList.remove(position);
//                addressList.remove(position);
                recordingArraylist.remove(position);
                recordingListAdapter.notifyDataSetChanged();
                Log.e("mapsizeRemove", String.valueOf(fileNameList.size()));
                Log.e("sizeRemove", String.valueOf(recordingArraylist.size()));

            }
        });
        recyclerViewRecordings.setAdapter(recordingListAdapter);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra("position", position);
//        intent.putStringArrayListExtra("addressList", addressList);
        intent.putStringArrayListExtra("fileNameList", fileNameList);
        setResult(RESULT_OK, intent);

        finish();
    }
}