package com.nuevotechsolutions.ewarp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.android.volley.VolleyError;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.GetJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DemoForNewUserActivity extends AppCompatActivity {
    private LinearLayout lldemo;
    private Button btnDemo,btnNext;
    private TextView tvdemo;
    String userNmae;
    CheckListDatabase cDb;
    List<MasterWorkId> list = new ArrayList<>();
    RelativeLayout rlDemo;
    ProgressDialog pdLoading;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        initView();
        ClickListener();
        cDb = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        list = cDb.productDao().getMasterWorkIdData();
        List<UserDetails> userDetails = new ArrayList<>();
        userDetails = cDb.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name() != null) {
            userNmae = userDetails.get(0).getFirst_name();
            tvdemo.setText("Hey,\n"+ userNmae );
        }
    }

    private void initView(){
        lldemo = findViewById(R.id.lldemo);
        btnDemo = findViewById(R.id.btnDemo);
        tvdemo = findViewById(R.id.tvdemo);
        rlDemo = findViewById(R.id.rlDemo);
        btnNext = findViewById(R.id.btnNext);
    }
    private void ClickListener(){
        btnDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lldemo.setVisibility(View.GONE);
                rlDemo.setVisibility(View.VISIBLE);
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkDetector networkDetector = new NetworkDetector();
                if (networkDetector.isNetworkReachable(DemoForNewUserActivity.this) == true) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (networkDetector.isConnectionFast(DemoForNewUserActivity.this) == true) {
                            pdLoading = new ProgressDialog(DemoForNewUserActivity.this);
                            pdLoading.setTitle("Please wait...");
                            pdLoading.setCancelable(false);
                            pdLoading.show();
                           /* progressbarNewTask.setVisibility(View.VISIBLE);
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);*/
                            Map<String, String> params = new HashMap<>();
                            params.put("abc", "abc");
                            VolleyMethods.makeGetJsonObjectRequest(params, DemoForNewUserActivity.this, new ApiUrlClass().View_Checklist_List_API, new GetJsonObjectRequestCallback() {
                                @Override
                                public void onSuccessResponse(JSONObject response) {
                                    if (response != null) {
                                        Log.e("ok", "ok" + response.toString());
                                        JSONArray arrList = new JSONArray();
                                        JSONArray arrCategory = new JSONArray();
                                        try {
                                            arrList = response.getJSONArray("list");
                                            arrCategory = response.getJSONArray("category");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        if (arrList != null && arrList.length() > 0) {
                                            pdLoading.hide();
                                           /* progressbarNewTask.setVisibility(View.GONE);
                                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);*/
                                            Intent intent = new Intent(DemoForNewUserActivity.this, DownloadChecklistFileActivity.class);
                                            intent.putExtra("list", arrList.toString());
                                            intent.putExtra("category", arrCategory.toString());
                                            startActivity(intent);
                                        }

                                    }
                                }

                                @Override
                                public void onVolleyError(VolleyError error) {
                                    pdLoading.hide();
                                    Log.e("err", error.toString());
                                }

                                @Override
                                public void onTokenExpire() {
                                    pdLoading.hide();
                                    Toast.makeText(DemoForNewUserActivity.this, "expire", Toast.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.checklistHomeLL), "Bad internet connection!", Snackbar.LENGTH_LONG);
                            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                            textView.setTextColor(Color.WHITE);
                            snackbar.show();
                        }
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.checklistHomeLL), "No internet connection!", Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }
            }

        });
    }
}
