package com.nuevotechsolutions.ewarp.activities;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.google.android.material.navigation.NavigationView;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.CheckListDescriptionLabelAdapter;
import com.nuevotechsolutions.ewarp.adapter.CompletedChecklistDetailsAdapter;
import com.nuevotechsolutions.ewarp.adapter.TeamMemberListAdapter;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompletedChecklistDetailsActivity extends AppCompatActivity {

    NavigationView navigationView;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    private TextView textViewTitle;
    private ImageView imagedescription, imageMemberList;
    RecyclerView recyclerView, recyclerViewDescriptionLabel, recyclerViewMemberList;
    CompletedChecklistDetailsAdapter completedChecklistDetailsAdapter;
    TextView textView_checkList_header, tv_eng_type, tv_unit_no, textView_room_no,
            textView_inspe_type, textView_checkList_title;
    TextView tvTitle;
    CheckListDatabase checkListDatabase;

    List<String> labelName;
    List<String> labelValue;
    JSONObject imageJson;
    String uid, wrk_id, title;

    List<Component> componentList;
    List<UserDetails> userDetailsList;
    private Map<Integer, List<String>> imageMap = new HashMap<>();
    private Map<Integer, List<String>> locationMap = new HashMap<>();
    private Map<Integer, List<String>> timeStampMap = new HashMap<>();
    private Map<Integer, List<String>> photoTextValueMap = new HashMap<>();
    private List<ChecklistRecordDataList> lastdata = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_checklist_details);

        labelName = new ArrayList<>();
        labelValue = new ArrayList<>();
        imageJson = new JSONObject();
        SharedPreferences prefs = getSharedPreferences("descriptionKey", MODE_PRIVATE);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
        View hView = navigationView.getHeaderView(0);
        TextView nav_user = (TextView) hView.findViewById(R.id.userNameLabel);

        Map<String, ?> keys = prefs.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
            labelName.add(entry.getKey());
            labelValue.add((String) entry.getValue());
        }
        uid = prefs.getString("uid", uid);
        wrk_id = prefs.getString("wrk_id", wrk_id);
        setUpToolbar();
//        navigationView = findViewById(R.id.navigation_menu);
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//                switch (menuItem.getItemId()) {
//                    case R.id.nav_home:
//                        Intent homintent = new Intent(CompletedChecklistDetailsActivity.this, HomePageActivity.class);
//                        startActivity(homintent);
//                        break;
//                    case R.id.nav_change_pass:
//                        Intent changePass = new Intent(CompletedChecklistDetailsActivity.this, ChangePassword.class);
//                        startActivity(changePass);
//                        break;
//                    case R.id.nav_contact:
//                        Intent contactus = new Intent(CompletedChecklistDetailsActivity.this, ContactUsActivity.class);
//                        startActivity(contactus);
//                        break;
//                    case R.id.nav_logout:
//                        Intent logout = new Intent(CompletedChecklistDetailsActivity.this, LoginActivity.class);
//                        SharedPreferences pref = getSharedPreferences("baseData", MODE_PRIVATE);
//                        SharedPreferences.Editor editor = pref.edit();
//                        editor.clear();
//                        editor.commit();
//                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        logout.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(logout);
//                        finish();
//                        break;
//                }
//                return false;
//            }
//        });

        checkListDatabase = Room.databaseBuilder(getApplicationContext(),
                CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        List<UserDetails> userDetails = new ArrayList<>();
        userDetails = checkListDatabase.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name() != null) {
            String user = userDetails.get(0).getFirst_name();
            nav_user.setText(user);
        } else {
            nav_user.setText("Guest User");
        }

        componentList = new ArrayList<>();
        bindView();
        getUid();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        componentList = checkListDatabase.productDao().getComponentByUid(uid);

        List<ChecklistRecordDataList> temp = new ArrayList<>();
        temp = checkListDatabase.productDao().getChecklistRecordDataList();
        for (int i = 0; i < temp.size(); i++) {
            if (temp.get(i).getWrk_id().equals(Integer.parseInt(wrk_id))) {
                lastdata.add(temp.get(i));
            }
        }


        Log.e("ccd", String.valueOf(lastdata));
        completedChecklistDetailsAdapter = new CompletedChecklistDetailsAdapter(
                CompletedChecklistDetailsActivity.this,
                componentList,
                lastdata,
                imageMap,
                locationMap,
                timeStampMap,
                photoTextValueMap);
        recyclerView.setAdapter(completedChecklistDetailsAdapter);
        completedChecklistDetailsAdapter.notifyDataSetChanged();

    }

    private void getDataFromDatabaseByUid(String uid) {
        Thread threadComponentByUid = new Thread(new Runnable() {
            @Override
            public void run() {
                userDetailsList = checkListDatabase.productDao().getUserDetail();

                //componentList=checkListDatabase.productDao().getAllComponents();
                componentList = checkListDatabase.productDao().getComponentByUid(uid);
                for (int i = 0; i < componentList.size(); i++) {
                    imageMap.put(i, new ArrayList<>());
                    locationMap.put(i, new ArrayList<>());
                    timeStampMap.put(i, new ArrayList<>());
                    photoTextValueMap.put(i, new ArrayList<>());
                }
//                setRecyclerView();
            }
        });
        threadComponentByUid.start();


    }

    private void getUid() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uid = bundle.getString("uid");
           /* engNo = bundle.getString("EngNo");
            engType = bundle.getString("EngType");
            unitNo = bundle.getString("unitNo");*/
            title = bundle.getString("title");
            wrk_id = bundle.getString("wrk_id");

            getDataFromDatabaseByUid(uid);
            textView_checkList_header.setText(title);
           /* tv_eng_type.setText(engType);
            tv_unit_no.setText(unitNo);*/

//**********************Get user data from database***********************

//            List<UserDetails> list;
//            checkListDatabase = Room.databaseBuilder(this, CheckListDatabase.class,
//                    ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
//            list = checkListDatabase.productDao().getUserDetail();
//            rank_id = list.get(0).getRank_id();
//            emp_id = list.get(0).getEmployee_id();

        } else {
            Toast.makeText(this, "something went wrong please try again..", Toast.LENGTH_SHORT).show();
        }
    }

    private void bindView() {
        textView_checkList_header = findViewById(R.id.textView_completed_checkList_header);
        recyclerView = findViewById(R.id.completed_recyclerView);
    }

    private void setUpToolbar() {

        drawerLayout = findViewById(R.id.drawerLayout7);
        toolbar = findViewById(R.id.toolbar7);
        textViewTitle = findViewById(R.id.completed_textViewTitle);
        imagedescription = findViewById(R.id.completedimagedescription);
        imageMemberList = findViewById(R.id.completedimageMemberList);
        ImageButton imgBackarrow = findViewById(R.id.imgbackarrowComplete);
        imgBackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(CompletedChecklistDetailsActivity.this);
                finish();
            }
        });


        imagedescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(CompletedChecklistDetailsActivity.this);
                dialog.setContentView(R.layout.checklist_description_dialog);
                recyclerViewDescriptionLabel = dialog.findViewById(R.id.recyclerViewDescriptionLabel);
                tvTitle = dialog.findViewById(R.id.tvTitle);
                Bundle bundle = getIntent().getExtras();
                if (bundle != null) {
                    String title = bundle.getString("title");
                    tvTitle.setText(title);
                }
                checkListDatabase = Room.databaseBuilder(CompletedChecklistDetailsActivity.this, CheckListDatabase.class,
                        ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                List<AuxEngLabel> auxEngLabels = checkListDatabase.productDao().getAllAuxEngLebels();
                List<String> auxLabelList = new ArrayList<>();
                for (int i = 0; i < auxEngLabels.size(); i++) {
                    if (auxEngLabels.get(i).getUid().equals(uid)) {
                        auxLabelList.add(auxEngLabels.get(i).getEng_label());
                    }
                }
                if (!auxLabelList.isEmpty()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            List<EnginDescriptionData> eDD = new ArrayList<>();
                            recyclerViewDescriptionLabel.setLayoutManager(new LinearLayoutManager(CompletedChecklistDetailsActivity.this));
                            recyclerViewDescriptionLabel.setAdapter(new CheckListDescriptionLabelAdapter(CompletedChecklistDetailsActivity.this, auxLabelList, labelValue, eDD));

                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(CompletedChecklistDetailsActivity.this, "No describtion available for this checklist", Toast.LENGTH_SHORT).show();
                }
            }
        });

        imageMemberList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(CompletedChecklistDetailsActivity.this);
                dialog.setContentView(R.layout.list_tema_member_dialog);
                recyclerViewMemberList = dialog.findViewById(R.id.recyclerViewMemberList);
                List<MasterWorkId> masterWorkIdList = new ArrayList<>();
                List<TeamSelectionList> MemberNamelist = new ArrayList<>();
                List<TeamSelectionList> MemberNamelist1 = new ArrayList<>();
                checkListDatabase = Room.databaseBuilder(CompletedChecklistDetailsActivity.this, CheckListDatabase.class,
                        ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                MemberNamelist = checkListDatabase.productDao().getTeamSelectionData();
                masterWorkIdList = checkListDatabase.productDao().getMasterWorkIdData();

                for (MasterWorkId masterWorkId : masterWorkIdList) {
                    if (masterWorkId.getWrk_id().equals(Integer.parseInt(wrk_id))) {
                        String teamStringJson = masterWorkId.getTeam_list();
                        if (teamStringJson != null) {
                            try {
                                JSONArray teamlistJson = new JSONArray(teamStringJson);

                                for (int i = 0; i < teamlistJson.length(); i++) {
                                    TeamSelectionList team = new TeamSelectionList();
                                    JSONObject obj = teamlistJson.getJSONObject(i);
                                    team.setFirst_name(obj.getString("name"));
                                    team.setRank(obj.getString("rank"));
                                    MemberNamelist1.add(team);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                if (MemberNamelist1.size() == 0) {
                    TeamSelectionList tm = new TeamSelectionList();
                    tm.setRank(userDetailsList.get(0).getRank());
                    tm.setFirst_name(userDetailsList.get(0).getFirst_name());
                    MemberNamelist1.add(tm);
                }
                recyclerViewMemberList.setLayoutManager(new LinearLayoutManager(CompletedChecklistDetailsActivity.this));
                TeamMemberListAdapter teamMemberListAdapter = new TeamMemberListAdapter(CompletedChecklistDetailsActivity.this, MemberNamelist1);
                recyclerViewMemberList.setAdapter(teamMemberListAdapter);
                teamMemberListAdapter.notifyDataSetChanged();
                dialog.show();
            }
        });

        setSupportActionBar(toolbar);
        toolbar.setTitle("");
//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
    }
}