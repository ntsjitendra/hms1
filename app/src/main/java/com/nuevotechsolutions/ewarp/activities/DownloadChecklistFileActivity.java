package com.nuevotechsolutions.ewarp.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.DownloadChecklistFileAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DownloadChecklistFileActivity extends AppCompatActivity {

    Toolbar toolbar;
    JSONArray arrList, arrTemp, arrCategory;
    RecyclerView download_checkList_recycler;
    ProgressBar progressbarViewDownload;
    CheckListDatabase db;
    List<UserDetails> userDetails = new ArrayList<>();
    Spinner categorySpinner;
    List<String> category = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_checklist_file);
        download_checkList_recycler = findViewById(R.id.download_checkList_recyclerView);
        progressbarViewDownload = findViewById(R.id.progressbarViewDownload);
        categorySpinner = findViewById(R.id.categorySpinner);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, category);

        db = Room.databaseBuilder(this, CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = db.productDao().getUserDetail();

        setUpToolbar();

        Intent intent = getIntent();
        if (intent != null) {
            try {
                arrList = new JSONArray(intent.getStringExtra("list"));
                arrCategory = new JSONArray(intent.getStringExtra("category"));
                if (arrCategory.length()>0){
                    category.add("ALL");
                    for (int i=0;i<arrCategory.length();i++){
                        JSONObject obj=arrCategory.getJSONObject(i);
                        category.add(obj.getString("checklist_category"));
                        categorySpinner.setAdapter(arrayAdapter);
                        categorySpinner.setSelection(0);
                        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                String selectedItem = parent.getItemAtPosition(position).toString();
                               if (selectedItem.equals("ALL")){
                                   if (arrList.length() > 0) {
                                       setChecklist(arrList);
                                   }
                               }else {
                                   arrTemp = new JSONArray();
                                   for (int i = 0; i < arrList.length(); i++) {
                                       try {
                                           if (arrList.getJSONObject(i).getString("checklist_category").equals(selectedItem)) {
                                               arrTemp.put(arrList.getJSONObject(i));
                                           }
                                       } catch (JSONException e) {
                                           e.printStackTrace();
                                       }
                                   }
                                   if (arrTemp.length() > 0) {
                                       setChecklist(arrTemp);
                                   }
                               }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void setChecklist(JSONArray arrList) {


        DownloadChecklistFileAdapter downloadChecklistFileAdapter = new DownloadChecklistFileAdapter(DownloadChecklistFileActivity.this, arrList, new DownloadChecklistFileAdapter.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onItemClick(JSONArray arrList, int position) {
                try {
                    progressbarViewDownload.setVisibility(View.VISIBLE);
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    JSONObject obj = arrList.getJSONObject(position);

                    NetworkDetector networkDetector = new NetworkDetector();
                    if (networkDetector.isNetworkReachable(DownloadChecklistFileActivity.this) == true) {
                        if (networkDetector.isConnectionFast(DownloadChecklistFileActivity.this) == true) {
                            Map<String, String> params = new HashMap<>();
                            params.put("uid", String.valueOf(obj.getInt("uid")));
                            params.put("user_id", userDetails.get(0).getUser_id());
                            params.put("dbnm", userDetails.get(0).getDbnm());
                            params.put("accessToken", userDetails.get(0).getAccessToken());
                            VolleyMethods.makePostJsonObjectRequest(params, DownloadChecklistFileActivity.this, new ApiUrlClass().View_Checklist_API, new PostJsonObjectRequestCallback() {
                                @Override
                                public void onSuccessResponse(JSONObject response) {
                                    try {
                                        Log.e("down",response.toString());
                                        if (response!=null){
                                            JSONArray arrChecklist = new JSONArray();
                                            try {
                                                arrChecklist = response.getJSONArray("list");
                                                Log.e("down", String.valueOf(arrChecklist));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            if (arrChecklist!=null && arrChecklist.length()>0){
                                                progressbarViewDownload.setVisibility(View.GONE);
                                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                Intent intent = new Intent(DownloadChecklistFileActivity.this, ViewChecklistActivity.class);
                                                intent.putExtra("checklistName",obj.getString("list"));
                                                intent.putExtra("uid",obj.getString("uid"));
                                                intent.putExtra("user_id",userDetails.get(0).getUser_id());
                                                intent.putExtra("dbnm",userDetails.get(0).getDbnm());
                                                intent.putExtra("accessToken",userDetails.get(0).getAccessToken());
                                                intent.putExtra("list",arrChecklist.toString());
                                                startActivity(intent);
                                            }

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onVolleyError(VolleyError error) {
                                    Log.e("err",error.toString());
                                }

                                @Override
                                public void onTokenExpire() {

                                }
                            });

                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.viewDownloadChecklistLinearLayout), "Bad internet connection!", Snackbar.LENGTH_LONG);
                            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                            textView.setTextColor(Color.WHITE);
                            snackbar.show();
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.viewDownloadChecklistLinearLayout), "No internet connection!", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);
                        snackbar.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        download_checkList_recycler.setLayoutManager(new LinearLayoutManager(DownloadChecklistFileActivity.this));
        download_checkList_recycler.setAdapter(downloadChecklistFileAdapter);
        downloadChecklistFileAdapter.notifyDataSetChanged();

    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbar8);
        setSupportActionBar(toolbar);
        ImageButton imageView = findViewById(R.id.backarrowViewDownload);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(DownloadChecklistFileActivity.this);
                finish();
            }
        });
    }
}
