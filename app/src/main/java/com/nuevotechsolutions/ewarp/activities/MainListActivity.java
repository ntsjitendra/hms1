package com.nuevotechsolutions.ewarp.activities;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.room.Room;
import android.content.Intent;
import android.graphics.Color;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.MainListAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.ManualsModel.MainList;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainListActivity extends AppCompatActivity {

    RecyclerView mainListRecyclerView;
    CheckListDatabase db;
    LinearLayout linearLayout;

    DrawerLayout drawerLayout;
    Toolbar toolbar;

    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;

    List<MainList> mainListList = new ArrayList<>();
    List<UserDetails> userDetails = new ArrayList<>();
    MainListAdapter mainListAdapter;
    String empId, dbnm,  company, accessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);

//        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
//        View hView =  navigationView.getHeaderView(0);
//        TextView nav_user = (TextView)hView.findViewById(R.id.userNameLabel);

        setUpToolbar();
        mainListRecyclerView = findViewById(R.id.mainListRecyclerView);
        linearLayout = findViewById(R.id.mainlistLinearLayout);
        db = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = db.productDao().getUserDetail();

        if (userDetails.get(0).getFirst_name()!=null){
            String user=userDetails.get(0).getFirst_name();
//            nav_user.setText(user);
        }else{
//            nav_user.setText("Guest User");
        }

        onMainListChecklist(userDetails.get(0).getUser_id(),userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(),
                "0", userDetails.get(0).getAccessToken());

        getData();

    }

    private void setUpToolbar() {
//        drawerLayout = findViewById(R.id.drawerLayoutMainList);
        toolbar = findViewById(R.id.toolbarMainList);
        ImageButton bckarwManualMainList = findViewById(R.id.bckarwManualMainList);
        bckarwManualMainList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(MainListActivity.this);
                finish();            }
        });
        setSupportActionBar(toolbar);
//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
    }

    public void getData() {
        List<MainList> list = new ArrayList<>();
        List<MainList> list1 = new ArrayList<>();
        List<UserDetails> userDetails = new ArrayList<>();

        db = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = db.productDao().getUserDetail();
        empId = String.valueOf(userDetails.get(0).getUser_id());
        company = String.valueOf(userDetails.get(0).getComp_name());


            list1.clear();
            for (int j = mainListList.size() - 1; j >= 0; j--) {
                list1.add(mainListList.get(j));
            }

        LinearLayoutManager llm = new LinearLayoutManager(MainListActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mainListRecyclerView.setLayoutManager(llm);
        List<UserDetails> finalUserDetails = userDetails;

        mainListAdapter = new MainListAdapter(this, list1, new MainListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(MainList item, int position) {
                list1.get(position).getMain_list_key();
                String mainListKey = String.valueOf(list1.get(position).getMain_list_key());
                String mainListName = String.valueOf(list1.get(position).getMain_list_name());
                String dbnm = String.valueOf(finalUserDetails.get(0).getDbnm());

                onMainListItemClick(empId, dbnm,  company, accessToken);

                Intent intent = new Intent(MainListActivity.this, IsolationActivity.class);
                intent.putExtra("mainListKey", mainListKey);
                intent.putExtra("mainListName", mainListName);

                startActivity(intent);
            }
        }
        );
        mainListRecyclerView.setAdapter(mainListAdapter);
        mainListAdapter.notifyDataSetChanged();

    }

    public void onMainListItemClick(String employee_id, String dbnm, String comp_name, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("accessToken", accessToken);


            VolleyMethods.makePostJsonObjectRequest(params, MainListActivity.this, new ApiUrlClass().resumeApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray mainListData = response.getJSONArray("lastdata");
                            JSONArray enginDescriptionDataArray = response.getJSONArray("engin_data");


                            if (mainListData != null && mainListData.length() > 0) {
                                for (int i = 0; i < mainListData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) mainListData.get(i);
                                    MainList mainList = objectMapper.readValue(jsonObject1.toString(), MainList.class);
                                    mainListList.add(mainList);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(MainListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(MainListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    public void onMainListChecklist(String employee_id, String dbnm, String comp_name, String statusID, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("statusID", statusID);
            params.put("accessToken", accessToken);

            VolleyMethods.makePostJsonObjectRequest(params, MainListActivity.this, new ApiUrlClass().checklistApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray mainlistData = response.getJSONArray("masterdata");

                            if (mainlistData != null && mainlistData.length() > 0) {
                                mainListList.clear();
                                for (int i = 0; i < mainlistData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) mainlistData.get(i);
                                    MainList mainList = objectMapper.readValue
                                            (jsonObject1.toString(), MainList.class);
                                    mainListList.add(mainList);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(MainListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(MainListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

}
