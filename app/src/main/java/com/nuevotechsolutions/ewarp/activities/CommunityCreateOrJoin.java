package com.nuevotechsolutions.ewarp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.nuevotechsolutions.ewarp.R;

public class CommunityCreateOrJoin extends AppCompatActivity {

    Button join, create;
    Integer user_id=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community_create_or_join);

        join = findViewById(R.id.joinButton);
        create = findViewById(R.id.createButton);
        Bundle data=getIntent().getExtras();
        if (data!=null){
            user_id=data.getInt("user_id");
        }

        join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CommunityCreateOrJoin.this, CommunityCreateOrJoinDetails.class);
                intent.putExtra("buttonKey","join");
                intent.putExtra("user_id",user_id);
                startActivity(intent);
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CommunityCreateOrJoin.this, CommunityCreateOrJoinDetails.class);
                intent.putExtra("buttonKey","create");
                intent.putExtra("user_id",user_id);
                startActivity(intent);
            }
        });

    }
}
