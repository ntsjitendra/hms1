package com.nuevotechsolutions.ewarp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.model.NoticeDataList;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.squareup.picasso.Picasso;

public class NoticeDetailActivity extends AppCompatActivity {

    ImageView noticeImage;
    TextView noticeTitle, noticeAbout;
    NoticeDataList noticeDataList;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_detail);

        noticeImage=findViewById(R.id.noticeImage);
        noticeTitle=findViewById(R.id.noticeTitleText);
        noticeAbout=findViewById(R.id.noticeAboutText);
        setUpToolbar();
        Intent i=getIntent();
        if (i!=null){
            noticeDataList = (NoticeDataList) i.getSerializableExtra("noticekey");
            if (noticeDataList.getFile_path()!=null){
                Picasso.with(this)
                        .load(noticeDataList.getFile_path())
                        .centerCrop()
                        .fit()
                        .error(R.drawable.attach)
                        .into(noticeImage);
            }
            if (noticeDataList.getTitle()!=null){
                noticeTitle.setText(noticeDataList.getTitle());
            }
            if (noticeDataList.getAbout_notice()!=null){
                noticeAbout.setText(noticeDataList.getAbout_notice());
            }
        }

    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbarDetailNotice);
        ImageButton imageView = findViewById(R.id.backarrowDetailsNotice);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(NoticeDetailActivity.this);
                finish();
            }
        });
        setSupportActionBar(toolbar);
//        actionBarDrawerToggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar,app_name,app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
    }

}
