package com.nuevotechsolutions.ewarp.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.AssignedTaskAdapter;
import com.nuevotechsolutions.ewarp.adapter.EngDescriptionAdapter;
import com.nuevotechsolutions.ewarp.adapter.TeamSelectionAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.interfaces.TeamSelectionInterFace;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserPointAccessData;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.LogoutClass;
import com.nuevotechsolutions.ewarp.utils.MultipartUtility;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.nuevotechsolutions.ewarp.utils.SharedPrefancesClearData;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class AssignedTask extends AppCompatActivity {

    String user, id;
    RecyclerView assignedlist;
    DrawerLayout drawerLayout;

    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    CheckListDatabase db;

    List<ChecklistRecordDataList> checklistRecordDataList = new ArrayList<>();
    List<EnginDescriptionData> enginDescriptionData = new ArrayList<>();
    List<MasterWorkId> masterWorkIdList = new ArrayList<>();
    List<UserDetails> userDetails = new ArrayList<>();

    LinearLayout linearLayout;
    private SwipeRefreshLayout swiperefresh;

    AssignedTaskAdapter acAdapter;
    String company, empId, accessToken;

    List<TeamSelectionList> teamSelectionLists;
    List<AuxEngLabel> auxEngLabelList;
    Map<String, String> firstTeamList = new HashMap<>();
    Map<String, String> secondTeamlist = new HashMap<>();
    Map<Integer, String> valueDetails = new HashMap<>();
    Map<Integer, String> lableDetails = new HashMap<>();
    List<String> description = new ArrayList<>();
    private List<String> rankPresent1;
    private Map<String, String> selectionRank = new HashMap<>();

    JSONObject checklistdata1 = new JSONObject();
    JSONObject teammember1 = new JSONObject();
    JSONArray listArray = new JSONArray();
    int team_flag = 0;
    List<String> response;
    TextView tvnoAssignlist;

    private TeamSelectionAdapter teamSelectionAdapter;
    RecyclerView recyclerView;
    List<TeamSelectionList> teamlist = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assigned_task);

        assignedlist = findViewById(R.id.assignedlist);
        linearLayout = findViewById(R.id.assignedTastLinearLayout);
        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefreshAssignedTask);
        setUpToolbar();
        tvnoAssignlist = findViewById(R.id.tvnoAssignlist);
        db = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = db.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name() != null) {
            user = userDetails.get(0).getFirst_name();
//            nav_user.setText(user);
        } else {
//            nav_user.setText("Guest User");
        }
        onAssignedTaskChecklist(userDetails.get(0).getUser_id(), userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(),
                "3", userDetails.get(0).getAccessToken());

        getData();
        swiperefresh.setColorSchemeColors(ContextCompat.getColor(getApplicationContext(),R.color.colorAccent));
        if (new NetworkDetector().isNetworkReachable(AssignedTask.this) == true) {
            swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    onAssignedTaskChecklist(userDetails.get(0).getUser_id(), userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(),
                            "3", userDetails.get(0).getAccessToken());
                    Toast.makeText(AssignedTask.this, "Refreshed", Toast.LENGTH_SHORT).show();
                    swiperefresh.setRefreshing(false);
                    getData();
                }
            });
        }
    }

    private void getData() {

        assignedlist.setLayoutManager(new LinearLayoutManager(AssignedTask.this));
        List<MasterWorkId> list = new ArrayList<>();
        List<MasterWorkId> list1 = new ArrayList<>();

        db = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        list = db.productDao().getMasterWorkIdData();
        userDetails = db.productDao().getUserDetail();
        empId = String.valueOf(userDetails.get(0).getUser_id());
        company = String.valueOf(userDetails.get(0).getComp_name());

        if (acAdapter == null) {
            int j = 0;
            for (int i = list.size() - 1; i >= 0; i--) {
                if (list.get(i).getWrk_status().equals(3) || list.get(i).getWrk_status().equals(5)) {
                    list1.add(j, list.get(i));
                    j++;
                }
            }
        } else {
            list1.clear();
            for (int j = 0; j < masterWorkIdList.size(); j++) {
                list1.add(masterWorkIdList.get(j));
            }
        }


        acAdapter = new AssignedTaskAdapter(AssignedTask.this, list1, new AssignedTaskAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(MasterWorkId item, int position) {
                String assignType = list1.get(position).getAssign_type();
                if (assignType != null) {
                    assignType = "A";
                } else {
                    assignType = "M";
                }
                String assign_type = String.valueOf(list1.get(position).getAssign_type());
                String uId = String.valueOf(list1.get(position).getUid());
                String engNo = String.valueOf(list1.get(position).getEngine_type_nmbr());
                String unitNo = String.valueOf(list1.get(position).getUnit_no());
                String title = list1.get(position).getShort_nm() + "/" + list1.get(position).getWrk_id_crtn_dt().substring(0, 4)
                        + "/" + list1.get(position).getWrk_id() + "/" + assignType;
                String workId = String.valueOf(list1.get(position).getWrk_id());

                List<AuxEngLabel> list1 = new ArrayList<>();
                List<AuxEngLabel> auxEngLabelList = new ArrayList<>();


                db = Room.databaseBuilder(AssignedTask.this.getApplicationContext(), CheckListDatabase.class,
                        ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

                auxEngLabelList = db.productDao().getAllAuxEngLebels();

                for (AuxEngLabel auxEngLabel : auxEngLabelList) {
                    if (auxEngLabel.getUid().equals(uId)) {
                        AuxEngLabel auxEngLabel1 = new AuxEngLabel();
                        auxEngLabel1.setEng_label(auxEngLabel.getEng_label());
                        list1.add(auxEngLabel1);
                    }
                }

                if (assign_type.equalsIgnoreCase("pointwise")) {
                    if (list1.size() > 0) {
                        if (new NetworkDetector().isNetworkReachable(AssignedTask.this) == true) {
                            descriptionDialog(uId, title, workId);
                        } else {
                            offlineDescriptionDialog(uId, title, workId);
                        }

                    } else {

                        if (new NetworkDetector().isNetworkReachable(AssignedTask.this) == true) {
                            try {
                                List<UserDetails> list = new ArrayList<>();
                                db = Room.databaseBuilder(AssignedTask.this, CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                                list = db.productDao().getUserDetail();
                                String strDate = new UtilClassFuntions().currentDateTime();
                                checklistdata1.put("uid", uId);
                                checklistdata1.put("rank_id", list.get(0).getRank_id());
                                checklistdata1.put("grtd_by", list.get(0).getEmployee_id());
                                checklistdata1.put("cmp_name", list.get(0).getComp_name());
                                checklistdata1.put("crtntime", strDate);
                                checklistdata1.put("user_id", list.get(0).getUser_id());
                                checklistdata1.put("wrk_status", 0);
                                checklistdata1.put("wrk_id", workId);
                                checklistdata1.put("dbnm", list.get(0).getDbnm());
                                checklistdata1.put("accessToken", list.get(0).getAccessToken());

                                String dataKey = "checklistdata";
                                String dataKey1 = "teammember";

                                if (team_flag == 0) {

                                    JSONObject teammember_obj = new JSONObject();
                                    JSONObject teammember_done = new JSONObject();
                                    JSONArray team_array = new JSONArray();

                                    teammember_done.put("teammember", "done");
                                    team_array.put(teammember_done);
                                    teammember_obj.put("teamdtls", team_array);
                                    Log.e("doneTeam0", String.valueOf(teammember_obj));
                                    Log.e("doneDescriptio", String.valueOf(checklistdata1));

                                    MultipartUtility multipart = new MultipartUtility(ApiUrlClass.assignApi, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember_obj);
                                    response = multipart.finish();
                                    String wrk_id = "";
                                    for (String line : response) {
                                        System.out.println(line);
                                        Log.e("CheckList:res:wrk_id", line);
                                        JSONObject jsonObject = new JSONObject(line);
                                        Integer res = jsonObject.getInt("response");

                                        if (res.equals(200)) {
                                            wrk_id = jsonObject.getString("wrk_id");
                                            Log.d("wrkId:", wrk_id);
                                            if (!wrk_id.equals("")) {

                                                //******************************************
                                                ObjectMapper objectMapper = new ObjectMapper();
                                                List<UserPointAccessData> userPointAccessDataList = new ArrayList<>();
                                                JSONArray userAccessPoint = jsonObject.getJSONArray("userpoint");


                                                if (userAccessPoint != null && userAccessPoint.length() >= 0) {
                                                    for (int i = 0; i < userAccessPoint.length(); i++) {
                                                        JSONObject jsonObject1 = (JSONObject) userAccessPoint.get(i);
                                                        UserPointAccessData userPointAccessData1 = objectMapper.readValue
                                                                (jsonObject1.toString(), UserPointAccessData.class);
                                                        userPointAccessDataList.add(userPointAccessData1);

                                                    }
                                                }

                                                Log.e("ddata", "saved...");
                                                //*********************************************************

                                                MasterWorkId masterWorkId = new MasterWorkId();
                                                masterWorkId.setWrk_id(Integer.valueOf(workId));
                                                masterWorkId.setGrtd_by(list.get(0).getEmployee_id());
                                                masterWorkId.setWrk_id_crtn_dt(strDate);
                                                masterWorkId.setUid(Integer.valueOf(uId));
                                                masterWorkId.setModified_dt(strDate);
                                                masterWorkId.setWrk_status(0);

                                                List<CheckList> checkLists = new ArrayList<>();
                                                checkLists = db.productDao().getAllCheckList();
                                                for (int i = 0; i < checkLists.size(); i++) {
                                                    if (checkLists.get(i).getUid().equals(uId)) {
                                                        masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                                        masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                                    }
                                                }

                                                Thread threadSetSubmit = new Thread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                                    }
                                                });
                                                threadSetSubmit.start();

                                            }
                                        } else if (res.equals(10003)) {
                                            new LogoutClass().logoutMethod(AssignedTask.this);
                                        }
                                    }
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
//                            if (list1.size() == 0) {
//                                tvnoAssignlist.setVisibility(View.VISIBLE);
//                            }
                        } else {
                            try {
                                List<UserDetails> list = new ArrayList<>();
                                db = Room.databaseBuilder(AssignedTask.this, CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                                list = db.productDao().getUserDetail();
                                String strDate = new UtilClassFuntions().currentDateTime();
                                checklistdata1.put("uid", uId);
                                checklistdata1.put("rank_id", list.get(0).getRank_id());
                                checklistdata1.put("grtd_by", list.get(0).getEmployee_id());
                                checklistdata1.put("cmp_name", list.get(0).getComp_name());
                                checklistdata1.put("crtntime", strDate);
                                checklistdata1.put("user_id", list.get(0).getUser_id());
                                checklistdata1.put("wrk_status", 0);
                                checklistdata1.put("wrk_id", workId);
                                checklistdata1.put("dbnm", list.get(0).getDbnm());
                                checklistdata1.put("accessToken", list.get(0).getAccessToken());

                                String dataKey = "checklistdata";
                                String dataKey1 = "teammember";

                                if (team_flag == 0) {

                                    JSONObject teammember_obj = new JSONObject();
                                    JSONObject teammember_done = new JSONObject();
                                    JSONArray team_array = new JSONArray();

                                    teammember_done.put("teammember", "done");
                                    team_array.put(teammember_done);
                                    teammember_obj.put("teamdtls", team_array);
                                    Log.e("doneTeam0", String.valueOf(teammember_obj));
                                    Log.e("doneDescriptio", String.valueOf(checklistdata1));

                                    MasterWorkId masterWorkId = new MasterWorkId();
                                    masterWorkId.setWrk_id(Integer.valueOf(workId));
                                    masterWorkId.setGrtd_by(list.get(0).getEmployee_id());
                                    masterWorkId.setWrk_id_crtn_dt(strDate);
                                    masterWorkId.setUid(Integer.valueOf(uId));
                                    masterWorkId.setModified_dt(strDate);
                                    masterWorkId.setWrk_status(0);
                                    masterWorkId.setOffline_status(0);
                                    List<CheckList> checkLists = new ArrayList<>();
                                    checkLists = db.productDao().getAllCheckList();
                                    for (int i = 0; i < checkLists.size(); i++) {
                                        if (checkLists.get(i).getUid().equals(uId)) {
                                            masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                            masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                        }
                                    }

                                    Thread threadSetSubmit = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                        }
                                    });
                                    threadSetSubmit.start();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        Intent intent = new Intent(AssignedTask.this, IsolationActivity.class);
                        intent.putExtra("uid", uId);
                        intent.putExtra("title", title);
                        intent.putExtra("wrk_id", workId);
                        intent.putExtra("checklist_type", assign_type);
                        startActivity(intent);
                    }


                } else {
                    if (new NetworkDetector().isNetworkReachable(AssignedTask.this) == true) {
                        teamSelectionDialog(uId, title, workId);
                    } else {
                        offlineTeamSelectionDialog(uId, title, workId);
                    }
                }

            }
        });
        assignedlist.setAdapter(acAdapter);
        acAdapter.notifyDataSetChanged();

    }

    private void offlineTeamSelectionDialog(String uid, String title, String workId) {
        final Dialog dialog1 = new Dialog(AssignedTask.this);
        dialog1.setCancelable(false);
        dialog1.setContentView(R.layout.team_selection_layout);
        LinearLayout rl = (LinearLayout) dialog1.findViewById(R.id.rl);
        Button submitButton = dialog1.findViewById(R.id.teamSelectionSubmitBtn);
        Button cancelButton = dialog1.findViewById(R.id.teamSelectionCancelBtn);
        EditText teamSearchView = dialog1.findViewById(R.id.teamSearchView);
        ImageView imageinfo = dialog1.findViewById(R.id.imageinfo);
        recyclerView = dialog1.findViewById(R.id.recyclerViewTeamSelection);
        CheckBox checkedAllTeam = dialog1.findViewById(R.id.checkedAllTeam);


        recyclerView.setLayoutManager(new LinearLayoutManager(AssignedTask.this));
        db = Room.databaseBuilder(AssignedTask.this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        teamSelectionLists = db.productDao().getTeamSelectionData();

        for (TeamSelectionList teamSelectionList : teamSelectionLists) {
            if (teamSelectionList.getUid().equals(Integer.parseInt(uid))) {
                TeamSelectionList teamSelectionList1 = new TeamSelectionList();
                teamSelectionList1.setRank(String.valueOf(teamSelectionList.getRank()));
                teamSelectionList1.setUser_id(String.valueOf(teamSelectionList.getUser_id()));
                teamSelectionList1.setFirst_name(teamSelectionList.getFirst_name() + " " + teamSelectionList.getLast_name());
                teamlist.add(teamSelectionList1);
            }
        }


        teamSelectionAdapter = new TeamSelectionAdapter(teamlist, AssignedTask.this, new TeamSelectionInterFace() {
            @Override
            public void onTeamSelectionButtonClickListener(int i, TextView engineerCtgryTextView, TextView engineerNameTextView, TextView emplId) {
                teamDetails(i, engineerCtgryTextView, engineerNameTextView, emplId);
            }

            @Override
            public void onDescriptionButtonClickListener(int post, EditText editText, TextView textInputLayout) {

            }
        });

        recyclerView.setAdapter(teamSelectionAdapter);
//                notifyDataSetChanged();

        teamSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filter(charSequence.toString());
//                        teamSelectionAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        checkedAllTeam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkedAllTeam.isChecked()) {
                    teamSelectionAdapter.selectAll();
                } else {
                    teamSelectionAdapter.unselectall();

                }
            }
        });

        imageinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) AssignedTask.this.getSystemService(LAYOUT_INFLATER_SERVICE);

                // Inflate the custom layout/view
                View customView = inflater.inflate(R.layout.checklistinfolpop_up, null);
                PopupWindow mPopupWindow = new PopupWindow(
                        customView,
                        RecyclerView.LayoutParams.WRAP_CONTENT,
                        RecyclerView.LayoutParams.WRAP_CONTENT
                );

                // Set an elevation value for popup window
                // Call requires API level 21
                if (Build.VERSION.SDK_INT >= 21) {
                    mPopupWindow.setElevation(5.0f);
                }
                TextView tvcatlist = (TextView) customView.findViewById(R.id.tvcatlist);
                List<String> ranklist = new ArrayList<>();
                List<String> ranklist1 = new ArrayList<>();
                for (int i = 0; i < teamlist.size(); i++) {
                    ranklist.add(teamlist.get(i).getRank());
                }
                for (int k = 0; k < ranklist.size(); k++) {
                    if (!ranklist1.contains(ranklist.get(k))) {
                        ranklist1.add(ranklist.get(k));
                        Log.e("adddd", ranklist.get(k));
                        StringBuilder stringBuilder = new StringBuilder();
                        for (String teamlist : ranklist1) {
                            stringBuilder.append("* " + teamlist + "\n");
                        }
                        tvcatlist.setText(stringBuilder.toString());
                    }

                }


                ImageButton closeButton = (ImageButton) customView.findViewById(R.id.ib_close);

                // Set a click listener for the popup window close button
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Dismiss the popup window
                        mPopupWindow.dismiss();
                    }
                });
                mPopupWindow.showAtLocation(rl, Gravity.TOP, 0, 100);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> rankpresentList = new ArrayList<>();
                rankPresent1 = new ArrayList<>();
                for (int i = 0; i < teamlist.size(); i++) {
                    rankpresentList.add(teamlist.get(i).getRank());
                    Log.e("listt", teamlist.get(i).getRank());
                }
                for (int k = 0; k < rankpresentList.size(); k++) {
                    int m = 0;
                    if (!rankPresent1.contains(rankpresentList.get(k))) {
                        rankPresent1.add(rankpresentList.get(k));
                        selectionRank.put(String.valueOf(k), rankpresentList.get(k));
                        Log.e("rankpresent", rankpresentList.get(k));

                    }
                }
                List<UserDetails> userDetails = new ArrayList<>();
                db = Room.databaseBuilder(getApplicationContext(), CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                userDetails = db.productDao().getUserDetail();
                Set<String> firstlist = new HashSet<String>(firstTeamList.values());
                Set<String> secondlist = new HashSet<String>(selectionRank.values());

                firstlist.add(userDetails.get(0).getRank());
                if (!rankPresent1.contains(userDetails.get(0).getRank())) {
                    secondlist.add(userDetails.get(0).getRank());
                }
//                       StringBuilder leftRankList = new StringBuilder();
                boolean listt = firstlist.equals(secondlist);
                if (listt == true) {
                    try {

                        List<TeamSelectionList> list = new ArrayList<>();
                        List<TeamSelectionList> list1 = new ArrayList<>();
//                    List<UserDetails> userDetails = new ArrayList<>();
//                    db = Room.databaseBuilder(AssignedTask.this, CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                        list = db.productDao().getTeamSelectionData();
//                    userDetails = db.productDao().getUserDetail();

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getUid().equals(Integer.parseInt(uid))) {
                                list1.add(list.get(i));
                            }
                        }

                      /*for (int i = 0; i < list1.size(); i++) {
                          Log.e("p", list1.get(i).getEmployee_id());
                      }*/
                        //Add teamlead in user access point list.
                        int c_user = 0;
                        JSONObject c_userPoint = new JSONObject();
                        c_userPoint.put("user_id", userDetails.get(0).getUser_id());
                        c_userPoint.put("uid", uid);
                        c_userPoint.put("rank_id", userDetails.get(0).getRank_id());
                        c_userPoint.put("name", userDetails.get(0).getFirst_name());
                        c_userPoint.put("rank", userDetails.get(0).getRank());
                        Log.e("testing...", String.valueOf(firstTeamList.size()));
                        if (c_user == 0) {
                            listArray.put(c_userPoint);
                            c_user = 1;
                        }//End
                        for (Map.Entry<String, String> entry : firstTeamList.entrySet()) {
                            entry.getKey();
                            entry.getValue();
                            JSONObject valueObject1 = new JSONObject();
                            valueObject1.put("user_id", entry.getKey());
                            valueObject1.put("uid", uid);
                            for (int i = 0; i < list.size(); i++) {
                                if (entry.getKey().equals(list.get(i).getUser_id())) {
                                    valueObject1.put("rank_id", list.get(i).getRank_id());
                                    valueObject1.put("name", list.get(i).getFirst_name());
                                    valueObject1.put("rank", list.get(i).getRank());
                                }
                            }
                            listArray.put(valueObject1);
                            Log.e("array...", String.valueOf(listArray));

                        }
                        team_flag = 1;
                        teammember1.put("teamdtls", listArray);
                        Log.e("teamdtls...", String.valueOf(teammember1));


                        Iterator<Map.Entry<String, String>> it3 = secondTeamlist.entrySet().iterator();
                        while (it3.hasNext()) {
                            Map.Entry<String, String> pair = it3.next();
                            System.out.println(pair.getKey() + " = " + pair.getValue());
                        }

                        List<AuxEngLabel> DescripList = new ArrayList<>();
                        db = Room.databaseBuilder(AssignedTask.this.getApplicationContext(), CheckListDatabase.class,
                                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

                        auxEngLabelList = db.productDao().getAllAuxEngLebels();

                        for (AuxEngLabel auxEngLabel : auxEngLabelList) {
                            if (auxEngLabel.getUid().equals(uid)) {
                                AuxEngLabel auxEngLabel1 = new AuxEngLabel();
                                auxEngLabel1.setEng_label(auxEngLabel.getEng_label());
                                DescripList.add(auxEngLabel1);
                            }
                        }

                        if (DescripList.size() == 0) {
                            try {
                                checklistdata1.put("uid", uid);
                                checklistdata1.put("wrk_id", workId);
                                checklistdata1.put("crtntime", new UtilClassFuntions().currentDateTime());
                                checklistdata1.put("user_id", userDetails.get(0).getUser_id());
                                checklistdata1.put("wrk_status", 0);
                                checklistdata1.put("dbnm", userDetails.get(0).getDbnm());
                                checklistdata1.put("accessToken", userDetails.get(0).getAccessToken());

                                String dataKey = "checklistdata";
                                String dataKey1 = "teammember";
                                if (team_flag == 0) {
                                    JSONObject teammember_obj = new JSONObject();
                                    JSONObject teammember_done = new JSONObject();
                                    JSONArray team_array = new JSONArray();

                                    teammember_done.put("teammember", "done");
                                    team_array.put(teammember_done);
                                    teammember_obj.put("teamdtls", team_array);
                                    Log.e("doneTeam", String.valueOf(teammember_obj));

                                    String strDate = new UtilClassFuntions().currentDateTime();
                                    MasterWorkId masterWorkId = new MasterWorkId();
                                    masterWorkId.setWrk_id(Integer.valueOf(workId));
                                    masterWorkId.setGrtd_by(list.get(0).getEmployee_id());
                                    masterWorkId.setWrk_id_crtn_dt(strDate);
                                    masterWorkId.setUid(Integer.valueOf(uid));
                                    masterWorkId.setModified_dt(strDate);
                                    masterWorkId.setWrk_status(0);
                                    masterWorkId.setOffline_status(0);
                                    List<CheckList> checkLists = new ArrayList<>();
                                    checkLists = db.productDao().getAllCheckList();
                                    for (int i = 0; i < checkLists.size(); i++) {
                                        if (checkLists.get(i).getUid().equals(uid)) {
                                            masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                            masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                        }
                                    }

                                    Thread threadSetSubmit = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                        }
                                    });
                                    threadSetSubmit.start();
                                } else {

                                    String strDate = new UtilClassFuntions().currentDateTime();
                                    MasterWorkId masterWorkId = new MasterWorkId();
                                    masterWorkId.setWrk_id(Integer.valueOf(workId));
                                    masterWorkId.setGrtd_by(list.get(0).getEmployee_id());
                                    masterWorkId.setWrk_id_crtn_dt(strDate);
                                    masterWorkId.setUid(Integer.valueOf(uid));
                                    masterWorkId.setModified_dt(strDate);
                                    masterWorkId.setWrk_status(0);
                                    masterWorkId.setOffline_status(0);
                                    List<CheckList> checkLists = new ArrayList<>();
                                    checkLists = db.productDao().getAllCheckList();
                                    for (int i = 0; i < checkLists.size(); i++) {
                                        if (checkLists.get(i).getUid().equals(uid)) {
                                            masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                            masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                        }
                                    }

                                    Thread threadSetSubmit = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                        }
                                    });
                                    threadSetSubmit.start();

                                }

                                Log.e("a", String.valueOf(checklistdata1));

                                List<CheckList> lists = new ArrayList<>();
                                String title = "";
                                String checklist_type = "";
                                lists = db.productDao().getAllCheckList();
                                for (int i = 0; i < lists.size(); i++) {
                                    if (lists.get(i).getUid().equals(uid)) {
                                        title = lists.get(i).getShort_nm().toUpperCase();
                                        checklist_type = lists.get(i).getChecklist_type();
                                    }
                                }
                                String engNo = "", engType = "", unitNo = "";
                                navigateToIsolationActivity(uid, engNo, engType, unitNo, title, workId, checklist_type);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            offlineDescriptionDialog(uid, title, workId);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    dialog1.cancel();
                    teamlist.clear();
                    firstTeamList.clear();
                    secondTeamlist.clear();
                } else {
                    Toast.makeText(AssignedTask.this, "Task cannot begin! Please select at least one user from each rank.", Toast.LENGTH_LONG).show();

                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
                teamlist.clear();
                firstTeamList.clear();
                secondTeamlist.clear();
            }
        });

        dialog1.show();
        Window window = dialog1.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    }

    private void offlineDescriptionDialog(String uid, String title, String workId) {
        Dialog dialog2 = new Dialog(AssignedTask.this);
        dialog2.setCancelable(false);
        dialog2.setContentView(R.layout.description_dialog);
        Button submitButton1 = dialog2.findViewById(R.id.descriptionSubmitBtn1);
        Button cancelButton1 = dialog2.findViewById(R.id.descriptionCancelBtn1);

        RecyclerView descriptionRecyclerView = dialog2.findViewById(R.id.description_recyclerView);

        descriptionRecyclerView.setLayoutManager(new LinearLayoutManager(AssignedTask.this));
        List<AuxEngLabel> list1 = new ArrayList<>();


        db = Room.databaseBuilder(AssignedTask.this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        auxEngLabelList = db.productDao().getAllAuxEngLebels();

        for (AuxEngLabel auxEngLabel : auxEngLabelList) {
            if (auxEngLabel.getUid().equals(uid)) {
                AuxEngLabel auxEngLabel1 = new AuxEngLabel();
                auxEngLabel1.setEng_label(auxEngLabel.getEng_label());
                list1.add(auxEngLabel1);
            }
        }

        descriptionRecyclerView.setAdapter(new EngDescriptionAdapter(list1, AssignedTask.this, new TeamSelectionInterFace() {
            @Override
            public void onTeamSelectionButtonClickListener(int i, TextView engineerCtgryTextView, TextView engineerNameTextView, TextView emplId) {

            }

            @Override
            public void onDescriptionButtonClickListener(int post, EditText editText, TextView textInputLayout) {
                description(post, editText, textInputLayout);
            }
        }));


        submitButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valueDetails.size() == list1.size()) {

                    try {
                        JSONArray jsonArray = new JSONArray();
                        Iterator<Map.Entry<Integer, String>> it = valueDetails.entrySet().iterator();
                        Iterator<Map.Entry<Integer, String>> it1 = lableDetails.entrySet().iterator();
                        while (it.hasNext() && it1.hasNext()) {
                            Map.Entry<Integer, String> label = it1.next();
                            Map.Entry<Integer, String> value = it.next();

                            System.out.println(value.getKey() + " = " + value.getValue());


                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(label.getValue(), value.getValue());
                            jsonArray.put(jsonObject);

                            Log.e("checklistdata", String.valueOf(checklistdata1));

                        }
                        checklistdata1.put("description", jsonArray);
                        List<UserDetails> list = new ArrayList<>();
                        db = Room.databaseBuilder(AssignedTask.this, CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                        list = db.productDao().getUserDetail();

                        Date date = new Date();
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String strDate = formatter.format(date);


                        checklistdata1.put("uid", uid);
                        checklistdata1.put("rank_id", list.get(0).getRank_id());
                        checklistdata1.put("grtd_by", list.get(0).getEmployee_id());
                        checklistdata1.put("cmp_name", list.get(0).getComp_name());
                        checklistdata1.put("crtntime", strDate);
                        checklistdata1.put("device_id", uid);
                        checklistdata1.put("user_id", list.get(0).getUser_id());
                        checklistdata1.put("wrk_status", 0);
                        checklistdata1.put("wrk_id", workId);
                        checklistdata1.put("dbnm", list.get(0).getDbnm());
                        checklistdata1.put("accessToken", list.get(0).getAccessToken());

                        String dataKey = "checklistdata";
                        String dataKey1 = "teammember";

                        Log.e("user_id", list.get(0).getUser_id());
//                    Log.e("rankid", list.get(0).getRank_id());
                        Log.e("id", uid);

                        if (team_flag == 0) {
                            JSONObject teammember_obj = new JSONObject();
                            JSONObject teammember_done = new JSONObject();
                            JSONArray team_array = new JSONArray();

                            teammember_done.put("teammember", "done");
                            team_array.put(teammember_done);
                            teammember_obj.put("teamdtls", team_array);
                            Log.e("doneTeam", String.valueOf(teammember_obj));
                            Log.e("doneDescription", String.valueOf(checklistdata1));

                            MasterWorkId masterWorkId = new MasterWorkId();
                            masterWorkId.setWrk_id(Integer.valueOf(workId));
                            masterWorkId.setGrtd_by(list.get(0).getEmployee_id());
                            masterWorkId.setWrk_id_crtn_dt(strDate);
                            masterWorkId.setUid(Integer.valueOf(uid));
                            masterWorkId.setModified_dt(strDate);
                            masterWorkId.setWrk_status(0);
                            masterWorkId.setOffline_status(0);
                            masterWorkId.setDescription(jsonArray.toString());
                            masterWorkId.setTeam_list(String.valueOf(teammember1.optJSONArray("teamdtls")));
                            Log.e("tl2...", String.valueOf(teammember1.optJSONArray("teamdtls")));
                            Log.e("description....insert","jk+"+jsonArray.toString());

                            List<CheckList> checkLists = new ArrayList<>();
                            checkLists = db.productDao().getAllCheckList();
                            for (int i = 0; i < checkLists.size(); i++) {
                                if (checkLists.get(i).getUid().equals(uid)) {
                                    masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                    masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                }
                            }

                            Thread threadSetSubmit = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                }
                            });
                            threadSetSubmit.start();

                        } else {
                            Log.e("doneTeam1", String.valueOf(teammember1));
                            Log.e("doneDescription1", String.valueOf(checklistdata1));

                            MasterWorkId masterWorkId = new MasterWorkId();
                            masterWorkId.setWrk_id(Integer.valueOf(workId));
                            masterWorkId.setGrtd_by(list.get(0).getEmployee_id());
                            masterWorkId.setWrk_id_crtn_dt(strDate);
                            masterWorkId.setUid(Integer.valueOf(uid));
                            masterWorkId.setModified_dt(strDate);
                            masterWorkId.setWrk_status(0);
                            masterWorkId.setOffline_status(0);
                            masterWorkId.setDescription(jsonArray.toString());
                            masterWorkId.setTeam_list(String.valueOf(teammember1.optJSONArray("teamdtls")));
                            Log.e("tl2...", String.valueOf(teammember1.optJSONArray("teamdtls")));
                            Log.e("description....insert","jk+"+jsonArray.toString());

                            List<CheckList> checkLists = new ArrayList<>();
                            checkLists = db.productDao().getAllCheckList();
                            for (int i = 0; i < checkLists.size(); i++) {
                                if (checkLists.get(i).getUid().equals(uid)) {
                                    masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                    masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                }
                            }

                            Thread threadSetSubmit = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                }
                            });
                            threadSetSubmit.start();

                        }

                        Log.e("a", String.valueOf(checklistdata1));

                        List<CheckList> lists = new ArrayList<>();
//                    String title = "";
                        String checklist_type = "";
                        lists = db.productDao().getAllCheckList();
                        for (int i = 0; i < lists.size(); i++) {
                            if (lists.get(i).getUid().equals(uid)) {
//                            title = lists.get(i).getList().toUpperCase();
                                checklist_type = lists.get(i).getChecklist_type();
                            }
                        }
                        String engNo = "", engType = "", unitNo = "";
                        navigateToIsolationActivity(uid, engNo, engType, unitNo, title, workId, checklist_type);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    dialog2.dismiss();
                } else {
                    Toast.makeText(AssignedTask.this, "Description is mendatory !", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });
        dialog2.show();
        Window window = dialog2.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    }

    private void descriptionDialog(String uid, String title, String workId) {

        Dialog dialog2 = new Dialog(AssignedTask.this);
        dialog2.setCancelable(false);
        dialog2.setContentView(R.layout.description_dialog);
        Button submitButton1 = dialog2.findViewById(R.id.descriptionSubmitBtn1);
        Button cancelButton1 = dialog2.findViewById(R.id.descriptionCancelBtn1);

        RecyclerView descriptionRecyclerView = dialog2.findViewById(R.id.description_recyclerView);

        descriptionRecyclerView.setLayoutManager(new LinearLayoutManager(AssignedTask.this));
        List<AuxEngLabel> list1 = new ArrayList<>();


        db = Room.databaseBuilder(AssignedTask.this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        auxEngLabelList = db.productDao().getAllAuxEngLebels();

        for (AuxEngLabel auxEngLabel : auxEngLabelList) {
            if (auxEngLabel.getUid().equals(uid)) {
                AuxEngLabel auxEngLabel1 = new AuxEngLabel();
                auxEngLabel1.setEng_label(auxEngLabel.getEng_label());
                list1.add(auxEngLabel1);
            }
        }

        descriptionRecyclerView.setAdapter(new EngDescriptionAdapter(list1, AssignedTask.this, new TeamSelectionInterFace() {
            @Override
            public void onTeamSelectionButtonClickListener(int i, TextView engineerCtgryTextView, TextView engineerNameTextView, TextView emplId) {

            }

            @Override
            public void onDescriptionButtonClickListener(int post, EditText editText, TextView textInputLayout) {
                description(post, editText, textInputLayout);
            }
        }));


        submitButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valueDetails.size() == list1.size()) {

                    try {
                        JSONArray jsonArray = new JSONArray();
                        Iterator<Map.Entry<Integer, String>> it = valueDetails.entrySet().iterator();
                        Iterator<Map.Entry<Integer, String>> it1 = lableDetails.entrySet().iterator();
                        while (it.hasNext() && it1.hasNext()) {
                            Map.Entry<Integer, String> label = it1.next();
                            Map.Entry<Integer, String> value = it.next();

                            System.out.println(value.getKey() + " = " + value.getValue());


                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(label.getValue(), value.getValue());
                            jsonArray.put(jsonObject);

                            Log.e("checklistdata", String.valueOf(checklistdata1));

                        }
                        checklistdata1.put("description", jsonArray);
                        List<UserDetails> list = new ArrayList<>();
                        db = Room.databaseBuilder(AssignedTask.this, CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                        list = db.productDao().getUserDetail();

                        Date date = new Date();
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String strDate = formatter.format(date);


                        checklistdata1.put("uid", uid);
                        checklistdata1.put("rank_id", list.get(0).getRank_id());
                        checklistdata1.put("grtd_by", list.get(0).getEmployee_id());
                        checklistdata1.put("cmp_name", list.get(0).getComp_name());
                        checklistdata1.put("crtntime", strDate);
                        checklistdata1.put("device_id", uid);
                        checklistdata1.put("user_id", list.get(0).getUser_id());
                        checklistdata1.put("wrk_status", 0);
                        checklistdata1.put("wrk_id", workId);
                        checklistdata1.put("dbnm", list.get(0).getDbnm());
                        checklistdata1.put("accessToken", list.get(0).getAccessToken());

                        String dataKey = "checklistdata";
                        String dataKey1 = "teammember";

                        Log.e("user_id", list.get(0).getUser_id());
//                    Log.e("rankid", list.get(0).getRank_id());
                        Log.e("id", uid);

                        if (team_flag == 0) {
                            JSONObject teammember_obj = new JSONObject();
                            JSONObject teammember_done = new JSONObject();
                            JSONArray team_array = new JSONArray();

                            teammember_done.put("teammember", "done");
                            team_array.put(teammember_done);
                            teammember_obj.put("teamdtls", team_array);
                            Log.e("doneTeam", String.valueOf(teammember_obj));

                            MultipartUtility multipart = new MultipartUtility(ApiUrlClass.assignApi, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember_obj);
                            response = multipart.finish();
                        } else {
                            MultipartUtility multipart = new MultipartUtility(ApiUrlClass.assignApi, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember1);
                            response = multipart.finish();
                            team_flag = 0;
                        }

                        Log.e("a", String.valueOf(checklistdata1));

                        System.out.println("SERVER REPLIED:");
                        String wrk_id = "";
                        for (String line : response) {
                            System.out.println(line);
                            Log.e("CheckList:res:wrk_id", line);
                            JSONObject jsonObject = new JSONObject(line);
                            Integer res = jsonObject.getInt("response");

                            if (res.equals(200)) {
                                wrk_id = jsonObject.getString("wrk_id");
                                Log.d("wrkId:", wrk_id);
                                if (!wrk_id.equals("")) {

                                    //******************************************
                                    ObjectMapper objectMapper = new ObjectMapper();
                                    List<UserPointAccessData> userPointAccessDataList = new ArrayList<>();
                                    JSONArray userAccessPoint = jsonObject.getJSONArray("userpoint");


                                    if (userAccessPoint != null && userAccessPoint.length() >= 0) {
                                        for (int i = 0; i < userAccessPoint.length(); i++) {
                                            JSONObject jsonObject1 = (JSONObject) userAccessPoint.get(i);
                                            UserPointAccessData userPointAccessData1 = objectMapper.readValue
                                                    (jsonObject1.toString(), UserPointAccessData.class);
                                            userPointAccessDataList.add(userPointAccessData1);

                                        }
                                    }

                                    Log.e("ddata", "saved...");
                                    //*********************************************************

                                    MasterWorkId masterWorkId = new MasterWorkId();
                                    masterWorkId.setWrk_id(Integer.valueOf(workId));
                                    masterWorkId.setGrtd_by(list.get(0).getEmployee_id());
                                    masterWorkId.setWrk_id_crtn_dt(strDate);
                                    masterWorkId.setUid(Integer.valueOf(uid));
                                    masterWorkId.setModified_dt(strDate);
                                    masterWorkId.setWrk_status(0);
                                    masterWorkId.setTeam_list(String.valueOf(teammember1.optJSONArray("teamdtls")));
                                    Log.e("tl2...", String.valueOf(teammember1.optJSONArray("teamdtls")));

                                    List<CheckList> checkLists = new ArrayList<>();
                                    checkLists = db.productDao().getAllCheckList();
                                    for (int i = 0; i < checkLists.size(); i++) {
                                        if (checkLists.get(i).getUid().equals(uid)) {
                                            masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                            masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                        }
                                    }

                                    Thread threadSetSubmit = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                        }
                                    });
                                    threadSetSubmit.start();

                                }
                            } else if (res.equals(10003)) {
                                new LogoutClass().logoutMethod(AssignedTask.this);
                            }

                        }

                        List<CheckList> lists = new ArrayList<>();
//                    String title = "";
                        String checklist_type = "";
                        lists = db.productDao().getAllCheckList();
                        for (int i = 0; i < lists.size(); i++) {
                            if (lists.get(i).getUid().equals(uid)) {
//                            title = lists.get(i).getList().toUpperCase();
                                checklist_type = lists.get(i).getChecklist_type();
                            }
                        }
                        String engNo = "", engType = "", unitNo = "";
                        navigateToIsolationActivity(uid, engNo, engType, unitNo, title, workId, checklist_type);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    dialog2.dismiss();
                } else {
                    Toast.makeText(AssignedTask.this, "Description is mendatory !", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });
        dialog2.show();
        Window window = dialog2.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


    }

    private void navigateToIsolationActivity(String uid, String engNo, String engType, String
            unitNo, String title, String wrk_id, String checklistType) {
        Intent intent = new Intent(AssignedTask.this, IsolationActivity.class);

        intent.putExtra("uid", uid);
       /* intent.putExtra("EngNo", engNo);
        intent.putExtra("EngType", engType);
        intent.putExtra("unitNo", unitNo);*/
        intent.putExtra("title", title);
        intent.putExtra("wrk_id", wrk_id);
        intent.putExtra("checklist_type", checklistType);

        new SharedPrefancesClearData().ClearDescriptionDataKey(this);

        SharedPreferences.Editor editor = this.getSharedPreferences("descriptionKey", MODE_PRIVATE).edit();

        Iterator<Map.Entry<Integer, String>> it = lableDetails.entrySet().iterator();
        Iterator<Map.Entry<Integer, String>> it1 = valueDetails.entrySet().iterator();
        while (it.hasNext() && it1.hasNext()) {
            Map.Entry<Integer, String> pair = it.next();
            Map.Entry<Integer, String> pair1 = it1.next();
            editor.putString(String.valueOf(pair.getValue()), pair1.getValue());
            description.add(pair1.getValue());
        }
        SharedPreferences.Editor editorDes = this.getSharedPreferences("EngDescription", MODE_PRIVATE).edit();
        editorDes.putString(wrk_id, String.valueOf(description));
        Log.e("DescriptionHashMap...", String.valueOf(description));

        editorDes.apply();
        editor.apply();

        this.startActivity(intent);
    }

    private void teamSelectionDialog(String uid, String title, String workId) {
        final Dialog dialog1 = new Dialog(AssignedTask.this);
        dialog1.setCancelable(false);
        dialog1.setContentView(R.layout.team_selection_layout);
        LinearLayout rl = (LinearLayout) dialog1.findViewById(R.id.rl);
        Button submitButton = dialog1.findViewById(R.id.teamSelectionSubmitBtn);
        Button cancelButton = dialog1.findViewById(R.id.teamSelectionCancelBtn);
        EditText teamSearchView = dialog1.findViewById(R.id.teamSearchView);
        ImageView imageinfo = dialog1.findViewById(R.id.imageinfo);
        recyclerView = dialog1.findViewById(R.id.recyclerViewTeamSelection);
        CheckBox checkedAllTeam = dialog1.findViewById(R.id.checkedAllTeam);


        recyclerView.setLayoutManager(new LinearLayoutManager(AssignedTask.this));
        db = Room.databaseBuilder(AssignedTask.this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        teamSelectionLists = db.productDao().getTeamSelectionData();

        for (TeamSelectionList teamSelectionList : teamSelectionLists) {
            if (teamSelectionList.getUid().equals(Integer.parseInt(uid))) {
                TeamSelectionList teamSelectionList1 = new TeamSelectionList();
                teamSelectionList1.setRank(String.valueOf(teamSelectionList.getRank()));
                teamSelectionList1.setUser_id(String.valueOf(teamSelectionList.getUser_id()));
                teamSelectionList1.setFirst_name(teamSelectionList.getFirst_name() + " " + teamSelectionList.getLast_name());
                teamlist.add(teamSelectionList1);
            }
        }


        teamSelectionAdapter = new TeamSelectionAdapter(teamlist, AssignedTask.this, new TeamSelectionInterFace() {
            @Override
            public void onTeamSelectionButtonClickListener(int i, TextView engineerCtgryTextView, TextView engineerNameTextView, TextView emplId) {
                teamDetails(i, engineerCtgryTextView, engineerNameTextView, emplId);
            }

            @Override
            public void onDescriptionButtonClickListener(int post, EditText editText, TextView textInputLayout) {

            }
        });

        recyclerView.setAdapter(teamSelectionAdapter);
//                notifyDataSetChanged();

        teamSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filter(charSequence.toString());
//                        teamSelectionAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        checkedAllTeam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkedAllTeam.isChecked()) {
                    teamSelectionAdapter.selectAll();
                } else {
                    teamSelectionAdapter.unselectall();

                }
            }
        });

        imageinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) AssignedTask.this.getSystemService(LAYOUT_INFLATER_SERVICE);

                // Inflate the custom layout/view
                View customView = inflater.inflate(R.layout.checklistinfolpop_up, null);
                PopupWindow mPopupWindow = new PopupWindow(
                        customView,
                        RecyclerView.LayoutParams.WRAP_CONTENT,
                        RecyclerView.LayoutParams.WRAP_CONTENT
                );

                // Set an elevation value for popup window
                // Call requires API level 21
                if (Build.VERSION.SDK_INT >= 21) {
                    mPopupWindow.setElevation(5.0f);
                }
                TextView tvcatlist = (TextView) customView.findViewById(R.id.tvcatlist);
                List<String> ranklist = new ArrayList<>();
                List<String> ranklist1 = new ArrayList<>();
                for (int i = 0; i < teamlist.size(); i++) {
                    ranklist.add(teamlist.get(i).getRank());
                }
                for (int k = 0; k < ranklist.size(); k++) {
                    if (!ranklist1.contains(ranklist.get(k))) {
                        ranklist1.add(ranklist.get(k));
                        Log.e("adddd", ranklist.get(k));
                        StringBuilder stringBuilder = new StringBuilder();
                        for (String teamlist : ranklist1) {
                            stringBuilder.append("* " + teamlist + "\n");
                        }
                        tvcatlist.setText(stringBuilder.toString());
                    }

                }


                ImageButton closeButton = (ImageButton) customView.findViewById(R.id.ib_close);

                // Set a click listener for the popup window close button
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Dismiss the popup window
                        mPopupWindow.dismiss();
                    }
                });
                mPopupWindow.showAtLocation(rl, Gravity.TOP, 0, 100);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> rankpresentList = new ArrayList<>();
                rankPresent1 = new ArrayList<>();
                for (int i = 0; i < teamlist.size(); i++) {
                    rankpresentList.add(teamlist.get(i).getRank());
                    Log.e("listt", teamlist.get(i).getRank());
                }
                for (int k = 0; k < rankpresentList.size(); k++) {
                    int m = 0;
                    if (!rankPresent1.contains(rankpresentList.get(k))) {
                        rankPresent1.add(rankpresentList.get(k));
                        selectionRank.put(String.valueOf(k), rankpresentList.get(k));
                        Log.e("rankpresent", rankpresentList.get(k));

                    }
                }
                List<UserDetails> userDetails = new ArrayList<>();
                db = Room.databaseBuilder(getApplicationContext(), CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                userDetails = db.productDao().getUserDetail();
                Set<String> firstlist = new HashSet<String>(firstTeamList.values());
                Set<String> secondlist = new HashSet<String>(selectionRank.values());

                firstlist.add(userDetails.get(0).getRank());
                if (!rankPresent1.contains(userDetails.get(0).getRank())) {
                    secondlist.add(userDetails.get(0).getRank());
                }
//                       StringBuilder leftRankList = new StringBuilder();
                boolean listt = firstlist.equals(secondlist);
                if (listt == true) {
                    try {

                        List<TeamSelectionList> list = new ArrayList<>();
                        List<TeamSelectionList> list1 = new ArrayList<>();
//                    List<UserDetails> userDetails = new ArrayList<>();
//                    db = Room.databaseBuilder(AssignedTask.this, CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                        list = db.productDao().getTeamSelectionData();
//                    userDetails = db.productDao().getUserDetail();

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getUid().equals(Integer.parseInt(uid))) {
                                list1.add(list.get(i));
                            }
                        }

                      /*for (int i = 0; i < list1.size(); i++) {
                          Log.e("p", list1.get(i).getEmployee_id());
                      }*/
                        //Add teamlead in user access point list.
                        int c_user = 0;
                        JSONObject c_userPoint = new JSONObject();
                        c_userPoint.put("user_id", userDetails.get(0).getUser_id());
                        c_userPoint.put("uid", uid);
                        c_userPoint.put("rank_id", userDetails.get(0).getRank_id());
                        c_userPoint.put("name", userDetails.get(0).getFirst_name());
                        c_userPoint.put("rank", userDetails.get(0).getRank());
                        Log.e("testing...", String.valueOf(firstTeamList.size()));
                        if (c_user == 0) {
                            listArray.put(c_userPoint);
                            c_user = 1;
                        }//End
                        for (Map.Entry<String, String> entry : firstTeamList.entrySet()) {
                            entry.getKey();
                            entry.getValue();
                            JSONObject valueObject1 = new JSONObject();
                            valueObject1.put("user_id", entry.getKey());
                            valueObject1.put("uid", uid);
                            for (int i = 0; i < list.size(); i++) {
                                if (entry.getKey().equals(list.get(i).getUser_id())) {
                                    valueObject1.put("rank_id", list.get(i).getRank_id());
                                    valueObject1.put("name", list.get(i).getFirst_name());
                                    valueObject1.put("rank", list.get(i).getRank());
                                }
                            }
                            listArray.put(valueObject1);
                            Log.e("array...", String.valueOf(listArray));

                        }
                        team_flag = 1;
                        teammember1.put("teamdtls", listArray);
                        Log.e("teamdtls...", String.valueOf(teammember1));


                        Iterator<Map.Entry<String, String>> it3 = secondTeamlist.entrySet().iterator();
                        while (it3.hasNext()) {
                            Map.Entry<String, String> pair = it3.next();
                            System.out.println(pair.getKey() + " = " + pair.getValue());
                        }

                        List<AuxEngLabel> DescripList = new ArrayList<>();
                        db = Room.databaseBuilder(AssignedTask.this.getApplicationContext(), CheckListDatabase.class,
                                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

                        auxEngLabelList = db.productDao().getAllAuxEngLebels();

                        for (AuxEngLabel auxEngLabel : auxEngLabelList) {
                            if (auxEngLabel.getUid().equals(uid)) {
                                AuxEngLabel auxEngLabel1 = new AuxEngLabel();
                                auxEngLabel1.setEng_label(auxEngLabel.getEng_label());
                                DescripList.add(auxEngLabel1);
                            }
                        }

                        if (DescripList.size() == 0) {
                            try {
                                checklistdata1.put("uid", uid);
                                checklistdata1.put("wrk_id", workId);
                                checklistdata1.put("crtntime", new UtilClassFuntions().currentDateTime());
                                checklistdata1.put("user_id", userDetails.get(0).getUser_id());
                                checklistdata1.put("wrk_status", 0);
                                checklistdata1.put("dbnm", userDetails.get(0).getDbnm());
                                checklistdata1.put("accessToken", userDetails.get(0).getAccessToken());

                                String dataKey = "checklistdata";
                                String dataKey1 = "teammember";
                                if (team_flag == 0) {
                                    JSONObject teammember_obj = new JSONObject();
                                    JSONObject teammember_done = new JSONObject();
                                    JSONArray team_array = new JSONArray();

                                    teammember_done.put("teammember", "done");
                                    team_array.put(teammember_done);
                                    teammember_obj.put("teamdtls", team_array);
                                    Log.e("doneTeam", String.valueOf(teammember_obj));

                                    MultipartUtility multipart = new MultipartUtility(ApiUrlClass.assignApi, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember_obj);
                                    response = multipart.finish();
                                } else {
                                    MultipartUtility multipart = new MultipartUtility(ApiUrlClass.assignApi, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember1);
                                    response = multipart.finish();
                                    team_flag = 0;
                                }

                                Log.e("a", String.valueOf(checklistdata1));

                                System.out.println("SERVER REPLIED:");
                                String wrk_id = "";
                                for (String line : response) {
                                    System.out.println(line);
                                    Log.e("CheckList:res:wrk_id", line);
                                    JSONObject jsonObject = new JSONObject(line);

                                    Integer res = jsonObject.getInt("response");

                                    if (res.equals(200)) {
                                        wrk_id = jsonObject.getString("wrk_id");
                                        Log.d("wrkId:", wrk_id);
                                        if (!wrk_id.equals("")) {

                                            //******************************************
                                            ObjectMapper objectMapper = new ObjectMapper();
                                            List<UserPointAccessData> userPointAccessDataList = new ArrayList<>();
                                            JSONArray userAccessPoint = jsonObject.getJSONArray("userpoint");


                                            if (userAccessPoint != null && userAccessPoint.length() >= 0) {
                                                for (int i = 0; i < userAccessPoint.length(); i++) {
                                                    JSONObject jsonObject1 = (JSONObject) userAccessPoint.get(i);
                                                    UserPointAccessData userPointAccessData1 = objectMapper.readValue
                                                            (jsonObject1.toString(), UserPointAccessData.class);
                                                    userPointAccessDataList.add(userPointAccessData1);

                                                }
                                            }

                                            Log.e("ddata", "saved...");
                                            //*********************************************************

                                            String strDate = new UtilClassFuntions().currentDateTime();
                                            MasterWorkId masterWorkId = new MasterWorkId();
                                            masterWorkId.setWrk_id(Integer.valueOf(workId));
                                            masterWorkId.setGrtd_by(list.get(0).getEmployee_id());
                                            masterWorkId.setWrk_id_crtn_dt(strDate);
                                            masterWorkId.setUid(Integer.valueOf(uid));
                                            masterWorkId.setModified_dt(strDate);
                                            masterWorkId.setWrk_status(0);

                                            List<CheckList> checkLists = new ArrayList<>();
                                            checkLists = db.productDao().getAllCheckList();
                                            for (int i = 0; i < checkLists.size(); i++) {
                                                if (checkLists.get(i).getUid().equals(uid)) {
                                                    masterWorkId.setList(db.productDao().getAllCheckList().get(i).getList());
                                                    masterWorkId.setShort_nm(db.productDao().getAllCheckList().get(i).getShort_nm());
                                                }
                                            }

                                            Thread threadSetSubmit = new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    db.productDao().setMasterWorkIdList(Collections.singletonList(masterWorkId));

                                                }
                                            });
                                            threadSetSubmit.start();

                                        }
                                    } else if (res.equals(10003)) {
                                        new LogoutClass().logoutMethod(AssignedTask.this);
                                    }

                                }

                                List<CheckList> lists = new ArrayList<>();
                                String title = "";
                                String checklist_type = "";
                                lists = db.productDao().getAllCheckList();
                                for (int i = 0; i < lists.size(); i++) {
                                    if (lists.get(i).getUid().equals(uid)) {
                                        title = lists.get(i).getShort_nm().toUpperCase();
                                        checklist_type = lists.get(i).getChecklist_type();
                                    }
                                }
                                String engNo = "", engType = "", unitNo = "";
                                navigateToIsolationActivity(uid, engNo, engType, unitNo, title, workId, checklist_type);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            descriptionDialog(uid, title, workId);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    dialog1.cancel();
                    teamlist.clear();
                    firstTeamList.clear();
                    secondTeamlist.clear();
                } else {
                    Toast.makeText(AssignedTask.this, "Task cannot begin! Please select at least one user from each rank.", Toast.LENGTH_LONG).show();

                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
                teamlist.clear();
                firstTeamList.clear();
                secondTeamlist.clear();
            }
        });

        dialog1.show();
        Window window = dialog1.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    }

    private void setUpToolbar() {
//        drawerLayout = findViewById(R.id.drawerLayout5);
        toolbar = findViewById(R.id.toolbar5);
        ImageButton imageView = findViewById(R.id.backarrowassignTask);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(AssignedTask.this);
                finish();
            }
        });
        setSupportActionBar(toolbar);

//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
    }

    public void onAssignedTaskChecklist(String user_id, String dbnm, String comp_name, String
            statusID, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", user_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("statusID", statusID);
            params.put("accessToken", accessToken);

            VolleyMethods.makePostJsonObjectRequest(params, AssignedTask.this, new ApiUrlClass().checklistApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            Integer res = response.getInt("response");

                            if (res.equals(200)) {
                                ObjectMapper objectMapper = new ObjectMapper();
                                JSONArray massterWorkIdData = response.getJSONArray("masterdata");
                                JSONArray enginDescriptionDataArray = response.getJSONArray("engin_data");

                                if (massterWorkIdData != null && massterWorkIdData.length() > 0) {
                                    masterWorkIdList.clear();
                                    for (int i = 0; i < massterWorkIdData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) massterWorkIdData.get(i);
                                        MasterWorkId masterWorkId = objectMapper.readValue
                                                (jsonObject1.toString(), MasterWorkId.class);
                                        masterWorkIdList.add(masterWorkId);
                                    }
                                }

                                if (enginDescriptionDataArray != null && enginDescriptionDataArray.length() > 0) {
                                    for (int i = 0; i < enginDescriptionDataArray.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) enginDescriptionDataArray.get(i);
                                        EnginDescriptionData enginDescriptionData1 = objectMapper.readValue(jsonObject1.toString(), EnginDescriptionData.class);
                                        enginDescriptionData.add(enginDescriptionData1);

                                    }
                                }

                                CheckListAndAddData();

                            } else if (res.equals(10003)) {
                                new LogoutClass().logoutMethod(AssignedTask.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(AssignedTask.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(AssignedTask.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void CheckListAndAddData() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
                db.productDao().deleteMasterWorkId(3, 5);
                db.productDao().setChecklistDataToTable(masterWorkIdList, enginDescriptionData);
            }
        });
        threadDeleteAllTables.start();
    }

    private void description(int post, EditText editText, TextView textInputLayout) {
        if (valueDetails.containsKey(post)) {
            valueDetails.remove(post);
            lableDetails.remove(post);

            valueDetails.put(post, editText.getText().toString().toUpperCase());
            lableDetails.put(post, textInputLayout.getHint().toString().toUpperCase());
        } else {
            valueDetails.put(post, editText.getText().toString().toUpperCase());
            lableDetails.put(post, textInputLayout.getHint().toString().toUpperCase());
        }
    }


    private void teamDetails(int i, TextView engineerCtgryTextView, TextView
            engineerNameTextView, TextView emplId) {
        String getemplId = emplId.getText().toString();
        Log.e("id", getemplId);
        if (firstTeamList.containsKey(getemplId) && secondTeamlist.containsKey(getemplId)) {
            firstTeamList.remove(getemplId);
            secondTeamlist.remove(getemplId);

        } else {

            firstTeamList.put(getemplId, engineerCtgryTextView.getText().toString());
            secondTeamlist.put(getemplId, engineerNameTextView.getText().toString());

            Log.e("testingFirst...", String.valueOf(firstTeamList.size()));

        }

    }

    void filter(String text) {
        List<TeamSelectionList> temp = new ArrayList();
        temp.clear();
        if (text.length() == 0) {
            temp.addAll(teamlist);
            Log.e("hdsfkuvsk", String.valueOf(teamlist));
        } else {
            for (TeamSelectionList d : teamlist) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.getRank().toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d);
                    Log.e("newlist", String.valueOf(temp));
                }
            }

        }
        teamSelectionAdapter.updateList(temp);
    }

}
