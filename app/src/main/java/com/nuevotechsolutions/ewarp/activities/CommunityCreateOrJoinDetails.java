package com.nuevotechsolutions.ewarp.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.VolleyError;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommunityCreateOrJoinDetails extends AppCompatActivity {

    String buttonName;
    EditText secretKeyEditText, companyNameEditText, companyAddressEditText;
    Button submitButton;
    Integer user_id = 0;
    ProgressBar progressbarCreateJoin;
    private ArrayList<String> timeZoneList;
    private Spinner timeZoneSpinner;
    private ArrayList<String> mNames;
    private ArrayAdapter<String> mSpinnerArrayAdapter;
    private String timeZoneSelected;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community_create_or_join_details);
        secretKeyEditText = findViewById(R.id.secretKeyEditText);
        companyNameEditText = findViewById(R.id.companyNameEditText);
        companyAddressEditText = findViewById(R.id.companyAddressEditText);
        submitButton = findViewById(R.id.submitButton);
        progressbarCreateJoin = findViewById(R.id.progressbarCreateJoin);
        timeZoneSpinner =  findViewById(R.id.spinnerTimeZone);
        mNames = new ArrayList<String>();
//        mNames.add("Select a country code...");
        mNames.addAll(AppConstant.TimeZones);
//        countryCodeList = new ArrayList<String>();
//        countryCodeList.add("+91");
//        countryCodeList.addAll(AppConstant.CountriesCode);
        mSpinnerArrayAdapter = new ArrayAdapter<String>(CommunityCreateOrJoinDetails.this, android.R.layout.simple_spinner_item, mNames); //selected item will look like a spinner set from XML
        mSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeZoneSpinner.setAdapter(mSpinnerArrayAdapter);
        timeZoneSpinner.setPrompt( "Select a TimeZone...");
        timeZoneSpinner.setSelection(0);
        timeZoneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTextColor(Color.BLACK);
               timeZoneSelected = timeZoneSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Bundle data = getIntent().getExtras();
        if (data != null) {
            buttonName = data.getString("buttonKey");
            user_id = data.getInt("user_id");
            Log.e("ButtonName", buttonName);
        }
        if (buttonName.equalsIgnoreCase("join")) {
            secretKeyEditText.setVisibility(View.VISIBLE);
            timeZoneSpinner.setVisibility(View.GONE);
        } else {
            companyNameEditText.setVisibility(View.VISIBLE);
        }
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (buttonName.equalsIgnoreCase("join")) {
                    String secretKey = secretKeyEditText.getText().toString();
                    if (secretKey.equalsIgnoreCase("")) {
                        secretKeyEditText.setError("Please enter secret key!");
                    } else {
                        joinOrCreate(secretKey, "", user_id,"");
                    }
                } else if (buttonName.equalsIgnoreCase("create")) {
                    String companyName = companyNameEditText.getText().toString();
                    if (companyName.equalsIgnoreCase("")) {
                        companyNameEditText.setError("Please enter company name!");
                    } else {
                        joinOrCreate("", companyName, user_id,timeZoneSelected);
                    }

                }
            }
        });
    }

    public void joinOrCreate(String secretKey, String companyName, Integer user_id,String timeZone) {

        Map<String, String> params = new HashMap<>();
        if (!secretKey.equalsIgnoreCase("")) {
            params.put("flag", "join");
            params.put("user_id", String.valueOf(user_id));
            params.put("sacred_key", secretKey);
            params.put("timezone", timeZone);
        } else {
            params.put("flag", "create");
            params.put("user_id", String.valueOf(user_id));
            params.put("companynm", companyName);
            params.put("timezone", timeZone);

        }
        progressbarCreateJoin.setVisibility(View.VISIBLE);
        VolleyMethods.makePostJsonObjectRequest(params, CommunityCreateOrJoinDetails.this, ApiUrlClass.CreateOrJoinAPI, new PostJsonObjectRequestCallback() {
            @Override
            public void onSuccessResponse(JSONObject response) {
                if (response != null) {
                    progressbarCreateJoin.setVisibility(View.GONE);
                    try {
                        Log.e("res", String.valueOf(response));
                        Integer code = response.getInt("response");
                        if (code.equals(200)) {
                            if (buttonName.equalsIgnoreCase("join")) {
                                Toast.makeText(CommunityCreateOrJoinDetails.this, "You request sent successfully,you will get a notification when approved by admin.", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(CommunityCreateOrJoinDetails.this, PendingActivity.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(CommunityCreateOrJoinDetails.this, "Your community created successfully, Please login.", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(CommunityCreateOrJoinDetails.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        } else if (code.equals(202)) {
                            Toast.makeText(CommunityCreateOrJoinDetails.this, "Try again!", Toast.LENGTH_SHORT).show();
                        } else if (code.equals(404)) {
                            Toast.makeText(CommunityCreateOrJoinDetails.this, "Invalid secret key!", Toast.LENGTH_SHORT).show();
                        } else if (code.equals(1062)) {
                            Toast.makeText(CommunityCreateOrJoinDetails.this, "You are already joined!", Toast.LENGTH_SHORT).show();

                        } else if (code.equals(500)) {
                            Toast.makeText(CommunityCreateOrJoinDetails.this, "Server error occurred!", Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(CommunityCreateOrJoinDetails.this, "Response is null!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onVolleyError(VolleyError error) {
                Toast.makeText(CommunityCreateOrJoinDetails.this, error.toString(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onTokenExpire() {

            }
        });

    }

}
