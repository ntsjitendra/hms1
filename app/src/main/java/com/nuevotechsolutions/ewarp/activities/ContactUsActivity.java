package com.nuevotechsolutions.ewarp.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.nuevotechsolutions.ewarp.R;

public class ContactUsActivity extends AppCompatActivity {
    private ImageView backbtnContactUs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_contact_us);
        backbtnContactUs = findViewById(R.id.backbtnContactUs);
        backbtnContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
