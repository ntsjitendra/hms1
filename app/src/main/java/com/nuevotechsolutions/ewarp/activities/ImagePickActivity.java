package com.nuevotechsolutions.ewarp.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.room.Room;

import com.android.volley.BuildConfig;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.DepartmentList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserPointAccessData;
import com.nuevotechsolutions.ewarp.model.NoticeDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklist;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselComponent;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselMasterWorkId;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.LocationHelper;
import com.nuevotechsolutions.ewarp.utils.Logger;
import com.nuevotechsolutions.ewarp.utils.MultipartUtility;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nuevotechsolutions.ewarp.utils.ApiUrlClass.TempLoginData;

public class ImagePickActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 1;
    private Bitmap bitmap, rotatedBitmap;
    private ImageView imageView;
    Button button;
    Uri file;
    File file1, photofile;
    String imageFileName, path;
    private Handler mHandler;
    private String TAG_NOTICED_DATA = "notice_data";
    private String TAG_NOTICE = "notice";
    private String TAG_ABOUT_NOTICE = "about_notice";
    private String TAG_ID = "id";
    private String TAG_NOTICE_TITLE = "title";
    private String TAG_NOTICE_FILE_PATH = "file_path";
    public ProgressBar progressBar_cyclic;
    RelativeLayout rlProgressbar;
    CheckListDatabase checkListDatabase;
    List<CheckList> checkLists;
    List<Component> componentList;
    List<UserDetails> userDetailsList;
    List<AuxEngDetails> auxEngDetailsList;
    List<AuxEngLabel> auxEngLabelList;
    List<TeamSelectionList> teamSelectionLists;
    List<ChecklistRecordDataList> checklistRecordDataList;
    List<MasterWorkId> masterWorkIdList;
    List<UserPointAccessData> userPointAccessDataList;
    List<EnginDescriptionData> enginDescriptionData;

    List<VesselChecklist> vesselChecklists;
    List<VesselComponent> vesselComponentList;
    List<VesselMasterWorkId> vesselMasterWorkIdList;
    List<DepartmentList> departmentLists;
    List<NoticeDataList> noticeLists;

    LocationHelper.LocationResult locationResult;
    LocationHelper locationHelper;
    Geocoder geocoder;
    double latitude, longitude;
    public String crnt_lctn, address;
    int pStatus = 0;
    private Handler handler = new Handler();
    private ImageView checklistgif;
    TextView tvHeader;
    String aboutNotice, id, titleNotice, NoticeimagePath, u;
    int counter = 0;
    int count;
    int remember = 0;
    List<CheckList> list;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_pick);

        mHandler = new Handler();
        imageView = findViewById(R.id.imageView58);
        button = findViewById(R.id.selfeiButton);
        button.setVisibility(View.GONE);
        tvHeader = findViewById(R.id.tvHeader);

//        final Spannable span = new SpannableString("EWARP");
//        span.setSpan(new ForegroundColorSpan(Color.RED), 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        tvHeader.setText(span);
        checklistgif = findViewById(R.id.checklistgif);
        Glide.with(getApplicationContext()).load(R.drawable.checklistgif).into(checklistgif);

//        takePicture(imageView); will optimise later
        Resources res = getResources();
        Drawable drawable = res.getDrawable(R.drawable.circular_progress);
        progressBar_cyclic = (ProgressBar) findViewById(R.id.progressBar_cyclic);
        rlProgressbar = (RelativeLayout) findViewById(R.id.rlProgressbar);
        int colorCodeDark = Color.parseColor("#1384EB");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar_cyclic.setIndeterminateTintList(ColorStateList.valueOf(colorCodeDark));
        }
        progressBar_cyclic.setProgress(0);   // Main Progress
        progressBar_cyclic.setSecondaryProgress(100); // Secondary Progress
        progressBar_cyclic.setMax(100); // Maximum Progress
        progressBar_cyclic.setProgressDrawable(drawable);
        TextView tv = (TextView) findViewById(R.id.tv);

//        new Thread(new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                while (pStatus < 100) {
//                    pStatus += 1;
//
//                    handler.post(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            // TODO Auto-generated method stub
//                            progressBar_cyclic.setProgress(pStatus);
////                            tv.setText(pStatus + "%");
//
//                        }
//                    });
//                    try {
//                        // Sleep for 200 milliseconds.
//                        // Just to display the progress slowly
//                        Thread.sleep(100); //thread will take approx 3 seconds to finish
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }).start();

        checkLists = new ArrayList<>();
        componentList = new ArrayList<>();
        userDetailsList = new ArrayList<>();
        auxEngDetailsList = new ArrayList<>();
        auxEngLabelList = new ArrayList<>();
        teamSelectionLists = new ArrayList<>();
        checklistRecordDataList = new ArrayList<>();
        masterWorkIdList = new ArrayList<>();
        userPointAccessDataList = new ArrayList<>();
        enginDescriptionData = new ArrayList<>();

        vesselChecklists = new ArrayList<>();
        vesselComponentList = new ArrayList<>();
        vesselMasterWorkIdList = new ArrayList<>();
        departmentLists = new ArrayList<>();
        noticeLists = new ArrayList<>();

        LocationAccess();
        locationHelper.getLocation(ImagePickActivity.this, ImagePickActivity.this.locationResult);


        getUserLoginData();
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                button.setEnabled(false);
//                boolean a=storeAllData();
//                uploadImage();
//                Bundle b=getIntent().getExtras();
//                String u=b.getString("username");
//                String id=b.getString("uid");
//                if (a==true) {
//
//                    mHandler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            pdLoading.hide();
//                            Intent intent = new Intent(ImagePickActivity.this, HomePageActivity.class);
//                            intent.putExtra("username", u);
//                            intent.putExtra("uid", id);
//                            startActivity(intent);
//                            finish();                        }
//                    }, 30000);
//
//                }
//            }
//        });


    }

    private void getUserLoginData() {
        boolean a = storeAllData();
//        uploadImage();
        Bundle b = getIntent().getExtras();
        u = b.getString("username");
        String id = b.getString("uid");
        /*if (a == true) {

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressBar_cyclic.setVisibility(View.GONE);
                    rlProgressbar.setVisibility(View.GONE);
                    Intent intent = new Intent(ImagePickActivity.this, ChecklistHomeActivity.class);
                    intent.putExtra("username", u);
                    intent.putExtra("uid", id);
                    intent.putExtra("keynoticeList",(Serializable) noticeLists);
                    startActivity(intent);
                    finish();
                }
            }, 10000);

        }*/
    }

    private void takePicture(View view) {
        if (ActivityCompat.checkSelfPermission(ImagePickActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ImagePickActivity.this, new String[]{Manifest.permission.CAMERA}, 0);
            return;
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.putExtra("android.intent.extras.CAMERA_FACING", android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT);
//        intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
//        intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "IMG_" + timeStamp + ".jpg";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            photofile = getPhotoFileUri(imageFileName);
            file = FileProvider.getUriForFile(ImagePickActivity.this, BuildConfig.APPLICATION_ID + ".provider", photofile);
        } else {
            file = Uri.fromFile(getOutputMediaFile());
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {
                    ExifInterface ei = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        bitmap = BitmapFactory.decodeFile(photofile.getAbsolutePath());
                        ei = new ExifInterface(photofile.getAbsolutePath());
                    } else {
                        bitmap = BitmapFactory.decodeFile(file.getPath());
                        ei = new ExifInterface(file.getPath());
                    }
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_UNDEFINED);


                    switch (orientation) {

                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotatedBitmap = rotateImage(bitmap, 90);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotatedBitmap = rotateImage(bitmap, 180);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotatedBitmap = rotateImage(bitmap, 270);
                            break;

                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                            rotatedBitmap = bitmap;
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        FileOutputStream fos = new FileOutputStream(photofile.getAbsolutePath());
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                        fos.close();
                        path = photofile.getAbsolutePath();
                    } else {
                        FileOutputStream fos = new FileOutputStream(file.getPath());
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                        fos.close();
                        path = file.getPath();

                    }


//                    imageView.setImageBitmap(rotatedBitmap);
                    button.setVisibility(View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private void uploadImage() {
        try {
            List<UserDetails> userDetails = new ArrayList<>();
            checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                    ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
            userDetails = checkListDatabase.productDao().getUserDetail();

            Map<String, String> params = new HashMap<>();
            params.put("dbnm", userDetails.get(0).getDbnm());
            params.put("user_id", userDetails.get(0).getUser_id());
            params.put("accessToken", userDetails.get(0).getAccessToken());
            params.put("crntTimeStamp", new UtilClassFuntions().currentDateTime());
            params.put("location", address);

            JSONObject loginDataObj = new JSONObject(params);

            String dataKey = "loginImgDtls";
            MultipartUtility multipart = new MultipartUtility(ApiUrlClass.loginImgUpload,
                    ApiUrlClass.charset, dataKey, loginDataObj);
            List<File> file1 = new ArrayList<>();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (photofile != null) {
                    file1.add(0, new File(path));
                    try {
                        multipart.addFilePart("files", file1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (file != null) {
                    file1.add(0, new File(path));
                    try {
                        multipart.addFilePart("files", file1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            List<String> response = multipart.finish();
            Log.e("SERVER REPLIED:", String.valueOf(response));
//            Toast.makeText(this, "imageUploaded", Toast.LENGTH_SHORT).show();
            for (String line : response) {
                System.out.println(line);
                Log.e("res", line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static File getOutputMediaFile() {

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "LoginImage");


        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File imgpath = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
        return imgpath;
    }

    public File getPhotoFileUri(String fileName) {
        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "LoginImage");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d("", "failed to create directory");
        }

        // Return the file target for the photo based on filename
        file1 = new File(mediaStorageDir.getPath() + File.separator + fileName);

        return file1;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ImagePickActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    //Calling the loginData api for all data saving for offline mode
    public boolean storeAllData() {

        progressBar_cyclic.setVisibility(View.VISIBLE);
        rlProgressbar.setVisibility(View.VISIBLE);
        List<UserDetails> userDetails = new ArrayList<>();
        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = checkListDatabase.productDao().getUserDetail();
        Map<String, String> params = new HashMap<>();
        params.put("dbnm", userDetails.get(0).getDbnm());
        params.put("user_id", userDetails.get(0).getUser_id());
        params.put("employee_id", userDetails.get(0).getEmployee_id());
        params.put("accessToken", userDetails.get(0).getAccessToken());

        VolleyMethods.makePostJsonObjectRequest(params, ImagePickActivity.this, new ApiUrlClass().user_loginData, new PostJsonObjectRequestCallback() {
            @Override
            public void onSuccessResponse(JSONObject response) {
                Log.d("Responsedata", String.valueOf(response));
                if (response != null) {
//                  Log.d("Responsedata", String.valueOf(response));
                    try {
                        JSONObject jsonObject = response.getJSONObject("data");
                        JSONObject noticeJsonObject = response.getJSONObject("notice_data");
                        JSONObject vesselJsonObject = response.getJSONObject("vessel_inspection_data");
                        ObjectMapper objectMapper = new ObjectMapper();

                        if (jsonObject != null) {
                            JSONArray list = jsonObject.getJSONArray("list");
                            Log.e("list", String.valueOf(list));
//                            JSONArray component = jsonObject.getJSONArray("component");
                            JSONArray auxEngLabellData = jsonObject.getJSONArray("aux_eng-label");
                            JSONArray teamSelectionData = jsonObject.getJSONArray("teamselectionlist");
                            JSONArray checklistRecordData = jsonObject.getJSONArray("lastdata");
                            JSONArray massterWorkIdData = jsonObject.getJSONArray("masterdata");
                            JSONArray enginDescriptionDataArray = jsonObject.getJSONArray("engin_data");
                            JSONArray departmentData = jsonObject.getJSONArray("dept");

                            if (list != null && list.length() > 0) {
                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) list.get(i);
                                    CheckList checkList = objectMapper.readValue(jsonObject1.toString(), CheckList.class);
                                    checkLists.add(checkList);
                                }
                            }

                            /*if (component != null && component.length() > 0) {
                                for (int i = 0; i < component.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) component.get(i);
                                    Component component1 = objectMapper.readValue(jsonObject1.toString(), Component.class);
                                    componentList.add(component1);
                                }

                            }
*/

//                            if (auxEngDetailData != null && auxEngDetailData.length() > 0) {
//                                for (int i = 0; i < auxEngDetailData.length(); i++) {
//                                    JSONObject jsonObject1 = (JSONObject) auxEngDetailData.get(i);
//                                    AuxEngDetails auxEngDetails = objectMapper.readValue(jsonObject1.toString(), AuxEngDetails.class);
//                                    auxEngDetailsList.add(auxEngDetails);
//                                }
//
//                            }

                            if (auxEngLabellData != null && auxEngLabellData.length() > 0) {
                                for (int i = 0; i < auxEngLabellData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) auxEngLabellData.get(i);
                                    AuxEngLabel auxEngLabel = objectMapper.readValue(jsonObject1.toString(), AuxEngLabel.class);
                                    auxEngLabelList.add(auxEngLabel);
                                }

                            }

                            if (teamSelectionData != null && teamSelectionData.length() > 0) {
                                for (int i = 0; i < teamSelectionData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) teamSelectionData.get(i);
                                    TeamSelectionList teamSelectionList1 = objectMapper.readValue
                                            (jsonObject1.toString(), TeamSelectionList.class);
                                    teamSelectionLists.add(teamSelectionList1);
                                }
                            }

                            if (checklistRecordData != null && checklistRecordData.length() > 0) {
                                for (int i = 0; i < checklistRecordData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) checklistRecordData.get(i);
                                    ChecklistRecordDataList checklistRecordDataList1 = objectMapper.readValue(jsonObject1.toString(), ChecklistRecordDataList.class);
                                    checklistRecordDataList.add(checklistRecordDataList1);
                                }
                            }

                            if (massterWorkIdData != null && massterWorkIdData.length() > 0) {
                                for (int i = 0; i < massterWorkIdData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) massterWorkIdData.get(i);
                                    MasterWorkId masterWorkId = objectMapper.readValue
                                            (jsonObject1.toString(), MasterWorkId.class);
                                    masterWorkIdList.add(masterWorkId);
                                }
                            }

                            if (enginDescriptionDataArray != null && enginDescriptionDataArray.length() > 0) {
                                for (int i = 0; i < enginDescriptionDataArray.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) enginDescriptionDataArray.get(i);
                                    EnginDescriptionData enginDescriptionData1 = objectMapper.readValue(jsonObject1.toString(), EnginDescriptionData.class);
                                    enginDescriptionData.add(enginDescriptionData1);

                                }

                            }

                            if (departmentData != null && departmentData.length() > 0) {
                                for (int i = 0; i < departmentData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) departmentData.get(i);
                                    DepartmentList departmentList = objectMapper.readValue(jsonObject1.toString(), DepartmentList.class);
                                    departmentLists.add(departmentList);

                                }

                            }
                        }
                        if (noticeJsonObject != null) {
                            JSONArray noticeList = noticeJsonObject.getJSONArray("notice");

                            if (noticeList != null && noticeList.length() > 0) {
                                for (int i = 0; i < noticeList.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) noticeList.get(i);
                                    NoticeDataList noticeDataList = objectMapper.readValue(jsonObject1.toString(), NoticeDataList.class);
                                    noticeLists.add(noticeDataList);
                                }
                            }
                        }
                        if (vesselJsonObject != null && vesselJsonObject.length() > 0) {
                            JSONArray vesselList = vesselJsonObject.getJSONArray("list");
                            JSONArray vesselComponent = vesselJsonObject.getJSONArray("checklist_points");
                            JSONArray vesselMasterWorkIdData = vesselJsonObject.getJSONArray("masterdata");

                            if (vesselList != null && vesselList.length() > 0) {
                                for (int i = 0; i < vesselList.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) vesselList.get(i);
                                    VesselChecklist vesselChecklist = objectMapper.readValue(jsonObject1.toString(), VesselChecklist.class);
                                    vesselChecklists.add(vesselChecklist);
                                }
                            }

                            if (vesselComponent != null && vesselComponent.length() > 0) {
                                for (int i = 0; i < vesselComponent.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) vesselComponent.get(i);
                                    VesselComponent vesselComponent1 = objectMapper.readValue(jsonObject1.toString(), VesselComponent.class);
                                    vesselComponentList.add(vesselComponent1);
                                }
                            }

                            if (vesselMasterWorkIdData != null && vesselMasterWorkIdData.length() > 0) {
                                for (int i = 0; i < vesselMasterWorkIdData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) vesselMasterWorkIdData.get(i);
                                    VesselMasterWorkId vesselMasterWorkId = objectMapper.readValue(jsonObject1.toString(), VesselMasterWorkId.class);
                                    vesselMasterWorkIdList.add(vesselMasterWorkId);
                                }
                            }
                        }

                        deleteAndAddData();
                        Log.e("data", "...");

//                        if (notificationDataObject != null) {
//                            JSONArray notficationList = notificationDataObject.getJSONArray(TAG_NOTICE);
//                            if (notficationList != null && notficationList.length() > 0) {
//                                for (int i = 0; i < notficationList.length(); i++) {
//                                    JSONObject notificationJsonObject = (JSONObject)notficationList.get(i);
//                                    NoticeDataList noticeDataList = objectMapper.readValue(notificationJsonObject.toString(), NoticeDataList.class);
//                                    noticeLists.add(noticeDataList);
//                                    Log.e("list--", String.valueOf(noticeLists.size()));
////                                    aboutNotice = notficationList.getJSONObject(i).getString(TAG_ABOUT_NOTICE);
////                                    titleNotice = notficationList.getJSONObject(i).getString(TAG_NOTICE_TITLE);
////                                    id = notficationList.getJSONObject(i).getString(TAG_ID);
////                                    NoticeimagePath = notficationList.getJSONObject(i).getString(TAG_NOTICE_FILE_PATH);
//
//
//                                }
//                            }
//
//                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (JsonParseException e) {
                        e.printStackTrace();
                    } catch (JsonMappingException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onVolleyError(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    //This indicates that the reuest has either time out or there is no connection

                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (progressBar_cyclic != null && progressBar_cyclic.isAnimating()) {
                        rlProgressbar.setVisibility(View.GONE);
                        Toast.makeText(ImagePickActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }


            @Override
            public void onTokenExpire() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (progressBar_cyclic != null && progressBar_cyclic.isAnimating()) {
                        rlProgressbar.setVisibility(View.GONE);
                    }
                }
            }
        });


        return true;
    }

    private void deleteAndAddData() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().setDataToTables(checkLists, componentList,
                        auxEngDetailsList, auxEngLabelList, teamSelectionLists,
                        checklistRecordDataList, masterWorkIdList, enginDescriptionData, departmentLists,
                        vesselChecklists, vesselComponentList, vesselMasterWorkIdList, noticeLists);

                getComponentData();
            }
        });
        threadDeleteAllTables.start();
    }

    private void getComponentData() {
        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        list = checkListDatabase.productDao().getAllCheckList1();
        userDetailsList = checkListDatabase.productDao().getUserDetail();

        if (list != null && list.size() > 0) {
            int size = list.size();
            Log.e("count", String.valueOf(count));
//            if (count > 0 && count < 3) {
//                count = 1;
//                Log.e("count1", String.valueOf(count));
//            } else if (count > 2) {
//                if (count % 3 == 0) {
//                    count = count / 3;
//                    Log.e("count2", String.valueOf(count));
//                } else {
//                    count = count / 3;
//                    count = count + 1;
//                    Log.e("count3", String.valueOf(count));
//                }
//            }
            if (size <= 3) {
                count = 1;
            } else {
                count = size / 3;
                if (size % 3 > 0) {
                    count = count + 1;
                }
            }
            callApi();
        } else {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    progressBar_cyclic.setVisibility(View.GONE);
                    rlProgressbar.setVisibility(View.GONE);
                    Intent intent = new Intent(ImagePickActivity.this, ChecklistHomeActivity.class);
                    intent.putExtra("username", u);
                    intent.putExtra("uid", id);
                    intent.putExtra("keynoticeList", (Serializable) noticeLists);
                    startActivity(intent);
                    finish();
                }
            });
        }

    }

    //------ This function is used for fetch componet Api data--------//
    private void callApi() {

        Logger.printLog("COUNT", ":  " + count);
        Logger.printLog("INCREASE COUNT", ":  " + counter);

        if (counter < count) {
            JSONArray arr = new JSONArray();
            for (int i = 0; i < 3; i++) {
                if (remember < list.size()) {
                    arr.put(Integer.parseInt(list.get(remember).getUid()));
                    remember++;
                }
            }
            Map<String, String> params = new HashMap<>();
            params.put("dbnm", userDetailsList.get(0).getDbnm());
            params.put("user_id", userDetailsList.get(0).getUser_id());
            params.put("accessToken", userDetailsList.get(0).getAccessToken());
            params.put("uiddata", arr.toString());
            VolleyMethods.makePostJsonObjectRequest(params, ImagePickActivity.this, TempLoginData, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    Log.e("response:jk", response.toString());
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray component = response.getJSONArray("component");

                            if (component != null && component.length() > 0) {
                                for (int i = 0; i < component.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) component.get(i);
                                    Component component1 = objectMapper.readValue(jsonObject1.toString(), Component.class);
                                    componentList.add(component1);
                                }
                            }

//                            new Thread(new Runnable() {
//                                @Override
//                                public void run() {
                            checkListDatabase.productDao().setComponentToTables(componentList);
                            counter++;
                            callApi();
//                        }
//                            }).start();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Log.e("err", error.toString());
                    callApi();
                }

                @Override
                public void onTokenExpire() {

                }
            });
        } else {
            progressBar_cyclic.setVisibility(View.GONE);
            rlProgressbar.setVisibility(View.GONE);
            Intent intent = new Intent(ImagePickActivity.this, ChecklistHomeActivity.class);
            intent.putExtra("username", u);
            intent.putExtra("uid", id);
            intent.putExtra("keynoticeList", (Serializable) noticeLists);
            startActivity(intent);
            finish();
        }

    }

    public void LocationAccess() {
        this.locationResult = new LocationHelper.LocationResult() {
            @Override
            public void gotLocation(Location location) {

                //Got the location!
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("ab", "lat: " + latitude + ", long: " + longitude);
                    geocoder = new Geocoder(ImagePickActivity.this);
                    try {
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        if (addresses != null && addresses.size() > 0) {
                            address = addresses.get(0).getAddressLine(0);
                            crnt_lctn = address;
                        }
                    } catch (IOException e) {
//                        Toast.makeText(ImagePickActivity.this, "Unable to access the current location !", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e("de", "Location is null.");
                }

            }

        };

        this.locationHelper = new LocationHelper();

    }

    @Override
    protected void onPause() {
        super.onPause();
        rlProgressbar.setVisibility(View.GONE);
    }


}
