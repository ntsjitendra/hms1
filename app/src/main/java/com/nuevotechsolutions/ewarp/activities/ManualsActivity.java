package com.nuevotechsolutions.ewarp.activities;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.room.Room;

import com.google.android.material.navigation.NavigationView;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;

import java.util.ArrayList;
import java.util.List;

public class ManualsActivity extends AppCompatActivity {

    TextView textView_vessel_header;
    DrawerLayout drawerLayout;

    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    CheckListDatabase checkListDatabase;

    CardView manualsButton,documentsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manuals);

        textView_vessel_header=findViewById(R.id.textView_homePage_header_manuals);
        textView_vessel_header.setText("Manuals");
        manualsButton=findViewById(R.id.manualsButton);
        documentsButton=findViewById(R.id.documentsButton);

//        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
//        View hView =  navigationView.getHeaderView(0);
//        TextView nav_user = (TextView)hView.findViewById(R.id.userNameLabel);

        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        List<UserDetails> userDetails=new ArrayList<>();
        userDetails=checkListDatabase.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name()!=null){
            String user=userDetails.get(0).getFirst_name();
//            nav_user.setText(user);
        }else{
//            nav_user.setText("Guest User");
        }

        setUpToolbar();
//        navigationView = findViewById(R.id.navigation_menu);
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//
//                switch (menuItem.getItemId()) {
//                    case R.id.nav_home:
//                        Intent intent=new Intent(ManualsActivity.this,HomePageActivity.class);
//                        startActivity(intent);
//                        break;
//                    case R.id.nav_change_pass:
//                        Intent changePass = new Intent(ManualsActivity.this, ChangePassword.class);
//                        startActivity(changePass);
//                        break;
//                    case R.id.nav_contact:
//                        Intent contactus = new Intent(ManualsActivity.this, ContactUsActivity.class);
//                        startActivity(contactus);
//                        break;
//                    case R.id.nav_logout:
//                        Intent logout = new Intent(ManualsActivity.this, LoginActivity.class);
//                        new SharedPrefancesClearData().ClearBaseData(ManualsActivity.this);
//                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        logout.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(logout);
//                        finish();
//                        break;
//                }
//
//                return false;
//            }
//        });

        manualsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ManualsActivity.this,MainListActivity.class);
                startActivity(intent);
            }
        });

        documentsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ManualsActivity.this,MainListActivity.class);
                startActivity(intent);
            }
        });

    }

    private void setUpToolbar() {
//        drawerLayout = findViewById(R.id.drawerLayoutManuals);
        toolbar = findViewById(R.id.toolbarManuals);
        ImageButton backarrowManual = findViewById(R.id.backarrowManual);
        backarrowManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(ManualsActivity.this);
                finish();
            }
        });
        setSupportActionBar(toolbar);
//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
    }

}
