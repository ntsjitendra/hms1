package com.nuevotechsolutions.ewarp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.CompletedChecklistAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.LogoutClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.nuevotechsolutions.ewarp.utils.SharedPrefancesClearData;
import com.nuevotechsolutions.ewarp.utils.SwipeToDeleteCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class CompleteCheckListActivity extends AppCompatActivity {

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static final String TAG_RESULTS = "result";
    private static final String TAG_ID = "wrk_id";
    private static final String TAG_EMPID = "grtd_by";
    private static final String TAG_COMPANY = "cmp_name";
    private static final String TAG_CATAGORY = "ctgry_name";
    private static final String TAG_DATE = "wrk_id_crtn_dt";
    RecyclerView completedlist;
    ArrayList<String> arrayList;
    ArrayAdapter<String> adapter;
    String company, empId, accessToken, ctgry, year, wrk_id;
    String myJSON, username;
    JSONArray checklist = null;
    URL url;
    Handler mHandler;
    SharedPreferences prefs;
    List<ChecklistRecordDataList> checklistRecordDataList = new ArrayList<>();
    List<EnginDescriptionData> enginDescriptionData = new ArrayList<>();
    List<MasterWorkId> masterWorkIdList = new ArrayList<>();
    List<UserDetails> userDetails = new ArrayList<>();
    List<EnginDescriptionData> descriptionData = new ArrayList<>();
    List<Component> componentList = new ArrayList<>();

    LinearLayout linearLayout;
    CompletedChecklistAdapter ccAdapter;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    CheckListDatabase db;
    String completedlistcount;
    CoordinatorLayout coordinatorLayout;
    TextView tvnoCompletelist;
    ProgressBar progressbarCompleted;
    FloatingActionMenu cancelComFilter;
    FloatingActionButton fab_menu_complete, fab_menu_cancel, fab_menu_all;
    private SwipeRefreshLayout swiperefresh;
    private boolean isdelete = false;
    private boolean isUndoClicked = false;
    private List<MasterWorkId> list1 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_check_list);

        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        completedlist = findViewById(R.id.completedlist);
        linearLayout = findViewById(R.id.linearLayoutComplete);
        progressbarCompleted = findViewById(R.id.progressbarCompleted);
        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefreshComplete);
        cancelComFilter = findViewById(R.id.cancelComFilter);
        fab_menu_cancel = findViewById(R.id.fab_menu_cancel);
        fab_menu_complete = findViewById(R.id.fab_menu_complete);
        fab_menu_all = findViewById(R.id.fab_menu_all);
        setUpToolbar();
        mHandler = new Handler();
        navigationView = findViewById(R.id.navigation_menu);
        tvnoCompletelist = findViewById(R.id.tvnoCompletelist);
        db = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = db.productDao().getUserDetail();
        descriptionData = db.productDao().getEnginDescriptionData();
        if (userDetails.get(0).getFirst_name() != null) {
            String user = userDetails.get(0).getFirst_name();
//            nav_user.setText(user);
        } else {
//            nav_user.setText("Guest User");
        }
        onCompleteChecklist(userDetails.get(0).getUser_id(), userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(),
                "1", userDetails.get(0).getAccessToken(), "", "0");

        getData();
        swiperefresh.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        if (new NetworkDetector().isNetworkReachable(CompleteCheckListActivity.this) == true) {
            swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    onCompleteChecklist(userDetails.get(0).getUser_id(), userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(),
                            "1", userDetails.get(0).getAccessToken(), "", "0");
                    Toast.makeText(CompleteCheckListActivity.this, "Refreshed", Toast.LENGTH_SHORT).show();
                    swiperefresh.setRefreshing(false);
                    getData();
                }
            });
        }
        if (new NetworkDetector().isNetworkReachable(CompleteCheckListActivity.this) == true) {
            enableSwipeToDeleteAndUndo();
            ccAdapter.notifyDataSetChanged();
        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }

        fab_menu_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelComFilter.close(true);
                List<MasterWorkId> filterList = new ArrayList<>();
                List<EnginDescriptionData> filterListDescription = new ArrayList<>();
                for (int i = 0; i < list1.size(); i++) {
                    MasterWorkId workId = list1.get(i);
                    if (workId.getWrk_status() == 2) {
                        filterList.add(workId);
                        filterListDescription.add(descriptionData.get(i));
                    }
                }
                ccAdapter.updateList(filterList, filterListDescription);
            }
        });

        fab_menu_complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelComFilter.close(true);
                List<MasterWorkId> filterList = new ArrayList<>();
                List<EnginDescriptionData> filterListDescription = new ArrayList<>();
                for (int i = 0; i < list1.size(); i++) {
                    MasterWorkId workId = list1.get(i);
                    if (workId.getWrk_status() == 1) {
                        filterList.add(workId);
                        filterListDescription.add(descriptionData.get(i));
                    }
                }
                ccAdapter.updateList(filterList, filterListDescription);
            }
        });

        fab_menu_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelComFilter.close(true);
                ccAdapter.updateList(list1, descriptionData);
            }
        });

    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbar3);
        ImageButton imageView = findViewById(R.id.backarrowCompletedTask);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(CompleteCheckListActivity.this);
                finish();
            }
        });
        setSupportActionBar(toolbar);
//        actionBarDrawerToggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar,app_name,app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
    }

    public void getData() {

        completedlist.setLayoutManager(new LinearLayoutManager(CompleteCheckListActivity.this));
        List<MasterWorkId> list = new ArrayList<>();

        List<UserDetails> userDetails = new ArrayList<>();

        db = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        list = db.productDao().getMasterWorkIdData();
        userDetails = db.productDao().getUserDetail();
        empId = String.valueOf(userDetails.get(0).getUser_id());
        company = String.valueOf(userDetails.get(0).getComp_name());
        accessToken = String.valueOf(userDetails.get(0).getAccessToken());

        if (ccAdapter == null) {
            int j = 0;
            list1.clear();
            for (int i = list.size() - 1; i >= 0; i--) {
                if (list.get(i).getWrk_status().equals(1) || list.get(i).getWrk_status().equals(2)) {
                    if (list.get(i).getWrk_id() < 0) {
                        list1.add(0, list.get(i));
                    } else {
                        list1.add(j, list.get(i));
                    }
                    j++;
                }
            }

        } else {
            list1.clear();
            for (int j = 0; j < masterWorkIdList.size(); j++) {
                list1.add(masterWorkIdList.get(j));
                Log.e("masterlist", String.valueOf(masterWorkIdList.get(j)));
            }
        }
        if (list1.size() == 0) {
            tvnoCompletelist.setVisibility(View.VISIBLE);
        } else {
            tvnoCompletelist.setVisibility(View.GONE);

        }
//        ccAdapter = new CompletedChecklistAdapter(CompleteCheckListActivity.this,list1);
//        completedlist.setAdapter(ccAdapter);
//        ccAdapter.notifyDataSetChanged();

        LinearLayoutManager llm = new LinearLayoutManager(CompleteCheckListActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        completedlist.setLayoutManager(llm);
        List<UserDetails> finalUserDetails = userDetails;
        ccAdapter = new CompletedChecklistAdapter(this, list1, descriptionData, new CompletedChecklistAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(MasterWorkId item, int position) {
                progressbarCompleted.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                if (item.getDelete_id() != null && (item.getDelete_id()).equals(0)) {
                    list1.get(position).getWrk_id();
                    String assignType = list1.get(position).getAssign_type();
                    if (assignType != null) {
                        assignType = "A";
                    } else {
                        assignType = "M";
                    }
                    String refNo = list1.get(position).getShort_nm() + "/" + list1.get(position).getWrk_id_crtn_dt().substring(0, 4)
                            + "/" + list1.get(position).getWrk_id() + "/" + assignType;
                    String uId = String.valueOf(list1.get(position).getUid());
                    String engNo = String.valueOf(list1.get(position).getEngine_type_nmbr());
                    String unitNo = String.valueOf(list1.get(position).getUnit_no());
                    String title = refNo;
                    String workId = String.valueOf(list1.get(position).getWrk_id());
                    String dbnm = String.valueOf(finalUserDetails.get(0).getDbnm());
                    wrk_id = workId;
                    updateSeen();
                    onCompletedItemClick(workId, empId, dbnm, company, accessToken);

                    Intent intent = new Intent(CompleteCheckListActivity.this, CompletedChecklistDetailsActivity.class);
                    intent.putExtra("uid", uId);
                    intent.putExtra("EngNo", engNo);
                    intent.putExtra("EngType", unitNo);
                    intent.putExtra("unitNo", uId);
                    intent.putExtra("title", title);
                    intent.putExtra("wrk_id", workId);

                    new SharedPrefancesClearData().ClearDescriptionDataKey(CompleteCheckListActivity.this);

                    SharedPreferences.Editor editor1 = CompleteCheckListActivity.this.getSharedPreferences("descriptionKey", Context.MODE_PRIVATE).edit();
                    List<AuxEngLabel> auxEngLabels = new ArrayList<>();
                    List<String> auxEngLabels1 = new ArrayList<>();
                    auxEngLabels = db.productDao().getAllAuxEngLebels();

                    for (int i = 0; i < auxEngLabels.size(); i++) {
                        if (auxEngLabels.get(i).getUid().equals(uId)) {
                            auxEngLabels1.add(auxEngLabels.get(i).getEng_label());
                        }
                    }

                    List<EnginDescriptionData> enginDescriptionData = new ArrayList<>();
                    List<String> enginDescriptionData1 = new ArrayList<>();
                    enginDescriptionData = db.productDao().getEnginDescriptionData();

                    SharedPreferences prefs = getSharedPreferences("EngDescription", MODE_PRIVATE);

                    if (!prefs.contains(workId)) {
                        for (int i = 0; i < enginDescriptionData.size(); i++) {
                            if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                                enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                            }
                        }
                        for (int i = 0; i < auxEngLabels1.size(); i++) {
                            editor1.putString(auxEngLabels1.get(i), enginDescriptionData1.get(i));
                        }

                    } else {

                        String ED = prefs.getString(workId, null);
                        String str[] = ED.split(",");
                        Log.e("ed", ED);
                        List<String> al = new ArrayList<String>();
                        al = Arrays.asList(str);
//                    Collections.reverse(al);
                        for (int i = 0; i < auxEngLabels1.size(); i++) {
                            editor1.putString(auxEngLabels1.get(i), al.get(i));
                        }
                        for (int i = 0; i < enginDescriptionData.size(); i++) {
                            try {
                                if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                                    enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        for (int i = 0; i < auxEngLabels1.size(); i++) {

                            editor1.putString(auxEngLabels1.get(i), al.get(i));

                        }

                    }

                    editor1.apply();
                    progressbarCompleted.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                    startActivity(intent);
                } else {
                    if (new NetworkDetector().isNetworkReachable(CompleteCheckListActivity.this) == false) {
                        Snackbar snackbar = Snackbar.make(linearLayout, "Deleted tasks that remain in completed category can be completed upon Internet connectivity.", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);
                        snackbar.show();
                        progressbarCompleted.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                    } else {
                        list1.get(position).getWrk_id();
                        String assignType = list1.get(position).getAssign_type();
                        if (assignType != null) {
                            assignType = "A";
                        } else {
                            assignType = "M";
                        }
                        String refNo = list1.get(position).getShort_nm() + "/" + list1.get(position).getWrk_id_crtn_dt().substring(0, 4)
                                + "/" + list1.get(position).getWrk_id() + "/" + assignType;
                        String uId = String.valueOf(list1.get(position).getUid());
                        String engNo = String.valueOf(list1.get(position).getEngine_type_nmbr());
                        String unitNo = String.valueOf(list1.get(position).getUnit_no());
                        String title = refNo;
                        String workId = String.valueOf(list1.get(position).getWrk_id());
                        String dbnm = String.valueOf(finalUserDetails.get(0).getDbnm());
                        wrk_id = workId;
                        String checklist_type = "";

                        updateSeen();
                        List<CheckList> checkLists = new ArrayList<>();
                        List<CheckList> checkLists1 = new ArrayList<>();
                        checkLists = db.productDao().getAllCheckList();
                        for (int i = 0; i < checkLists.size(); i++) {
                            if (checkLists.get(i).getUid().equals(uId)) {
                                checklist_type = db.productDao().getAllCheckList().get(i).getChecklist_type();
                            }
                        }


                        onCompletedItemClickComponent(uId, empId, dbnm);

                        Intent intent = new Intent(CompleteCheckListActivity.this, CompletedChecklistDetailsActivity.class);
                        intent.putExtra("uid", uId);
                        intent.putExtra("EngNo", engNo);
                        intent.putExtra("EngType", unitNo);
                        intent.putExtra("unitNo", uId);
                        intent.putExtra("title", title);
                        intent.putExtra("wrk_id", workId);
                        intent.putExtra("checklist_type", checklist_type);

                        new SharedPrefancesClearData().ClearDescriptionDataKey(CompleteCheckListActivity.this);

                        SharedPreferences.Editor editor1 = CompleteCheckListActivity.this.getSharedPreferences("descriptionKey", Context.MODE_PRIVATE).edit();

                        List<AuxEngLabel> auxEngLabels = new ArrayList<>();
                        List<String> auxEngLabels1 = new ArrayList<>();
                        auxEngLabels = db.productDao().getAllAuxEngLebels();

                        for (int i = 0; i < auxEngLabels.size(); i++) {
                            if (auxEngLabels.get(i).getUid().equals(uId)) {
                                auxEngLabels1.add(auxEngLabels.get(i).getEng_label());
                            }
                        }

                        List<EnginDescriptionData> enginDescriptionData = new ArrayList<>();
                        List<String> enginDescriptionData1 = new ArrayList<>();
                        enginDescriptionData = db.productDao().getEnginDescriptionData();

                        SharedPreferences prefs = getSharedPreferences("EngDescription", MODE_PRIVATE);

                        if (!prefs.contains(workId)) {
                            for (int i = 0; i < enginDescriptionData.size(); i++) {
                                if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                                    enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                                }
                            }
                            if (enginDescriptionData1.size() != 0 && enginDescriptionData1 != null) {
                                for (int i = 0; i < auxEngLabels1.size(); i++) {
                                    editor1.putString(auxEngLabels1.get(i), enginDescriptionData1.get(i));
                                }
                            }

                        } else {

                            String ED = prefs.getString(workId, null);
                            String str[] = ED.split(",");
                            Log.e("ed", ED);
                            List<String> al = new ArrayList<String>();
                            al = Arrays.asList(str);
//                    Collections.reverse(al);
                            for (int i = 0; i < auxEngLabels1.size(); i++) {
                                editor1.putString(auxEngLabels1.get(i), al.get(i));
                            }
                            for (int i = 0; i < enginDescriptionData.size(); i++) {
                                try {
                                    if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                                        enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            for (int i = 0; i < auxEngLabels1.size(); i++) {

                                editor1.putString(auxEngLabels1.get(i), al.get(i));

                            }

                        }

                        editor1.apply();
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressbarCompleted.setVisibility(View.GONE);
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                startActivity(intent);
                            }
                        }, 4000);

                    }
                }
            }
        });
        completedlist.setAdapter(ccAdapter);
        ccAdapter.notifyDataSetChanged();


    }

    private void onCompletedItemClickComponent(String uid, String empId, String dbnm) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            JSONArray newArr = new JSONArray();
            newArr.put(uid);
            Map<String, String> params = new HashMap<>();
            params.put("user_id", empId);
            params.put("dbnm", dbnm);
            params.put("accessToken", userDetails.get(0).getAccessToken());
            params.put("uiddata", newArr.toString());

            VolleyMethods.makePostJsonObjectRequest(params, CompleteCheckListActivity.this, new ApiUrlClass().TempLoginData, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray component = response.getJSONArray("component");

                            if (component != null && component.length() > 0) {
                                for (int i = 0; i < component.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) component.get(i);
                                    Component component1 = objectMapper.readValue(jsonObject1.toString(), Component.class);
                                    componentList.add(component1);
                                }
                            }

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    db.productDao().setComponentToTables(componentList);
                                }
                            }).start();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(CompleteCheckListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(CompleteCheckListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void onCompletedItemClick(String workId, String empId, String dbnm, String company, String accessToken) {

        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("wrk_id", workId);
            params.put("user_id", empId);
            params.put("dbnm", dbnm);
            params.put("comp_name", company);
            params.put("accessToken", userDetails.get(0).getAccessToken());
            params.put("seen", "true");

            VolleyMethods.makePostJsonObjectRequest(params, CompleteCheckListActivity.this, new ApiUrlClass().resumeApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            Integer res = response.getInt("response");
                            if (res.equals(200)) {
                                ObjectMapper objectMapper = new ObjectMapper();
                                JSONArray checklistRecordData = response.getJSONArray("lastdata");
                                Log.e("res1", String.valueOf(checklistRecordData));
                                JSONArray enginDescriptionDataArray = response.getJSONArray("engin_data");
                                if (checklistRecordData != null && checklistRecordData.length() > 0) {
                                    for (int i = 0; i < checklistRecordData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) checklistRecordData.get(i);
                                        ChecklistRecordDataList checklistRecordDataList1 = objectMapper.readValue(jsonObject1.toString(), ChecklistRecordDataList.class);
                                        checklistRecordDataList.add(checklistRecordDataList1);
                                    }
                                }

//                            if (enginDescriptionDataArray != null && enginDescriptionDataArray.length() > 0) {
//                                for (int i = 0; i < enginDescriptionDataArray.length(); i++) {
//                                    JSONObject jsonObject1 = (JSONObject) enginDescriptionDataArray.get(i);
//                                    EnginDescriptionData enginDescriptionData1 = objectMapper.readValue(jsonObject1.toString(), EnginDescriptionData.class);
//                                    enginDescriptionData.add(enginDescriptionData1);
//                                }
//                            }

                                CheckListAndAddDataItemClick();
                            } else if (res.equals(10003)) {
                                new LogoutClass().logoutMethod(CompleteCheckListActivity.this);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(CompleteCheckListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(CompleteCheckListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    public void onCompleteChecklist(String employee_id, String dbnm, String comp_name, String statusID, String accessToken, String wrk_id, String flag_delete) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("statusID", statusID);
            params.put("accessToken", userDetails.get(0).getAccessToken());
            params.put("wrk_id", wrk_id);
            params.put("flag_delete", flag_delete);


            VolleyMethods.makePostJsonObjectRequest(params, CompleteCheckListActivity.this, new ApiUrlClass().checklistApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            Integer res = response.getInt("response");
                            Log.e("cres", String.valueOf(response));
                            if (res.equals(200)) {
                                ObjectMapper objectMapper = new ObjectMapper();
                                JSONArray massterWorkIdData = response.getJSONArray("masterdata");
                                JSONArray enginDescriptionDataArray = response.getJSONArray("engin_data");

                                if (isdelete == true) {
                                    Log.e("true", String.valueOf(isdelete));
                                    MasterWorkId masterWorkIdOne = new MasterWorkId();
                                    masterWorkIdOne.setWrk_id(Integer.valueOf(wrk_id));
                                    masterWorkIdOne.setWrk_status(8);
                                    masterWorkIdList.add(masterWorkIdOne);
                                    isdelete = false;
                                }

                                if (massterWorkIdData != null && massterWorkIdData.length() > 0) {
                                    masterWorkIdList.clear();

                                    for (int i = 0; i < massterWorkIdData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) massterWorkIdData.get(i);
                                        MasterWorkId masterWorkId = objectMapper.readValue
                                                (jsonObject1.toString(), MasterWorkId.class);
                                        masterWorkIdList.add(masterWorkId);
                                        Log.e("jk>>", String.valueOf(masterWorkIdList.get(i).getWrk_id()));

                                    }
                                    Log.e("size", String.valueOf(masterWorkIdList.size()));
                                    Log.e("wekid", wrk_id);


                                }

                                if (enginDescriptionDataArray != null && enginDescriptionDataArray.length() > 0) {
                                    for (int i = 0; i < enginDescriptionDataArray.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) enginDescriptionDataArray.get(i);
                                        EnginDescriptionData enginDescriptionData1 = objectMapper.readValue(jsonObject1.toString(), EnginDescriptionData.class);
                                        enginDescriptionData.add(enginDescriptionData1);

                                    }
                                }

                                CheckListAndAddData();
                            } else if (res.equals(10003)) {
                                new LogoutClass().logoutMethod(CompleteCheckListActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(CompleteCheckListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(CompleteCheckListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void CheckListAndAddData() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
//                db.productDao().deleteMasterWorkId(1,2);
                db.productDao().setChecklistDataToTable(masterWorkIdList, enginDescriptionData);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ccAdapter.updateList(masterWorkIdList,enginDescriptionData);
                    }
                });


            }
        });
        threadDeleteAllTables.start();
    }

    private void CheckListAndAddDataItemClick() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
                db.productDao().setResumeDataToTable(checklistRecordDataList);

            }
        });
        threadDeleteAllTables.start();
    }

    private void updateSeen() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
                db.productDao().updateMasterWorkId(Integer.parseInt(wrk_id), true);
            }
        });
        threadDeleteAllTables.start();
    }

    private void enableSwipeToDeleteAndUndo() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

                final int position = viewHolder.getAdapterPosition();
                MasterWorkId masterWorkIdrestore = new MasterWorkId();
                masterWorkIdrestore = (ccAdapter.getData(position));
                Log.e("test", String.valueOf(masterWorkIdrestore.getWrk_id()));
//                ccAdapter.removeItem(position);
                list1.remove(position);
                descriptionData.remove(position);
                ccAdapter.updateList(list1, descriptionData);
                MasterWorkId finalMasterWorkIdrestore1 = masterWorkIdrestore;

                MasterWorkId finalMasterWorkIdrestore = masterWorkIdrestore;

                Snackbar.make(coordinatorLayout, "Item was removed from the list.", Snackbar.LENGTH_LONG).setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        switch (event) {
                            case Snackbar.Callback.DISMISS_EVENT_ACTION:
//                                Toast.makeText(CompleteCheckListActivity.this, "Clicked the action", Toast.LENGTH_LONG).show();
                                break;
                            case Snackbar.Callback.DISMISS_EVENT_TIMEOUT:
//                                Toast.makeText(CompleteCheckListActivity.this, "Time out", Toast.LENGTH_LONG).show();
                                isdelete = true;
                                onCompleteChecklist(userDetails.get(0).getUser_id(), userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(),
                                        "1", userDetails.get(0).getAccessToken(), String.valueOf(finalMasterWorkIdrestore1.getWrk_id()), "1");

                                break;
                        }
                    }

                    @Override
                    public void onShown(Snackbar snackbar) {
//                        Toast.makeText(CompleteCheckListActivity.this, "This is my annoying step-brother", Toast.LENGTH_LONG).show();
                    }
                }).setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ccAdapter.restoreItem(finalMasterWorkIdrestore, position);
                        completedlist.scrollToPosition(position);
                        isdelete = false;
                    }
                }).show();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(isUndoClicked!= true) {
//                            onCompleteChecklist(userDetails.get(0).getUser_id(), userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(),
//                                    "1", userDetails.get(0).getAccessToken(), String.valueOf(finalMasterWorkIdrestore1.getWrk_id()), "1");
//                        }
//                    }
//                },2000);


//                ccAdapter.notifyDataSetChanged();
            }
        };

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(completedlist);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
