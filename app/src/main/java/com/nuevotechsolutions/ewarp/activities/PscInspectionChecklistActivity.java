package com.nuevotechsolutions.ewarp.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.nuevotechsolutions.ewarp.BuildConfig;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.ImageListDataAdapter;
import com.nuevotechsolutions.ewarp.adapter.VesselPscInspectionChecklistAdapter;
import com.nuevotechsolutions.ewarp.controller.ImageData;
import com.nuevotechsolutions.ewarp.controller.ImageEventHandler;
import com.nuevotechsolutions.ewarp.controller.ImageHandler;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.interfaces.VesselImageInterface;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecord;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklist;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselComponent;
import com.nuevotechsolutions.ewarp.services.NetworkChangeReceiver;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.ButtonLock;
import com.nuevotechsolutions.ewarp.utils.LocationHelper;
import com.nuevotechsolutions.ewarp.utils.MultipartUtility;
import com.nuevotechsolutions.ewarp.utils.RecyclerSectionItemDecoration;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@SuppressWarnings("all")
public class PscInspectionChecklistActivity extends AppCompatActivity {

    TextView textView_vessel_header;
    DrawerLayout drawerLayout;
    Toolbar toolbar;

    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    Spinner spinner;
    String sections[] = {"Select Sections", "Section 1", "Section 2"};
    private List<String> sections1;

    LocationManager locationManager;
    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1034;
    private static ImageListDataAdapter itemListDataAdapter;
    public final String APP_TAG = "DiGiMon";
    TextView textView_checkList_header, tv_eng_type, tv_unit_no, textView_room_no,
            textView_inspe_type, textView_checkList_title;
    RecyclerView recyclerView, recyclerViewDescriptionLabel;
    EditText roomNumber, inspectionType;
    private CheckListDatabase checkListDatabase;
    List<ChecklistRecord> checklistRecords;
    List<UserDetails> userDetailsList;
    Button cancelBtn, completeBtn;
    FingerprintManager fingerprintManager;
    KeyguardManager keyguardManager;
    File photoFile;
    String imageFileName = "No_Teken_Photo";
    String timeStamp, uid, engNo, engType, unitNo, title;
    Bitmap takenImage, bitmap, bitmapUndo;
    String encodedImage;
    Uri fileProvider;
    File file;
    Bitmap rotatedBitmap = null;
    int postActivity;
    ImageView[] imageViewsActivity;
    List<VesselComponent> componentList;
    String points;
    ImageView imageView;
    AlertDialog.Builder alertDialog;
    List<Component> components;
    List<String> labelName;
    List<String> labelValue;
    List<AuxEngLabel> auxEngLabels;
    String uid1, wrk_id, checklist_type, point = "", tempPoint = "", q, ans, comment, photo, alert, crnt_dt, crnt_lctn, rank_id,
            emp_id, severity, department, alertComment;
    int checkpointId;
    String submitComment;
    int position;
    Intent mServiceIntent;
    List<String> listPhoto = new ArrayList<>();

    JSONObject imageJson;
    private List<String> locationList = new ArrayList<>();
    private List<String> timeStampList = new ArrayList<>();

    //Only for hode data on submit time.
    ImageButton editText, button, b, photoButton;
    Switch aSwitch;
    RadioButton radioButtons[];
    TextView submitEditText;
    LinearLayout linearLayout;
    Dialog dialog, dialogfinger;
    ImageView doneImageView;
    private TextView textViewTitle;
    // private static String[] itemsListData;
    private List<String> itemsListData;
    private List<String> locationListData;
    private List<String> timeStampListData;
    private Map<Integer, List<String>> imageMap = new HashMap<>();
    private Map<Integer, List<String>> locationMap = new HashMap<>();
    private Map<Integer, List<String>> timeStampMap = new HashMap<>();

    private List<VesselChecklistRecordDataList> lastdata = new ArrayList<>();
    private VesselPscInspectionChecklistAdapter vesselPscInspectionChecklistAdapter;
    String chceklisttitle;
    private static final int HIDE_THRESHOLD = 20;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;
    private ImageView imagedescription;
    TextView tvTitle;
    double latitude, longitude;
    public String locality, address, postalCode, county, lat, lon, coordinates;
    LocationHelper.LocationResult locationResult;
    LocationHelper locationHelper;
    Geocoder geocoder;
    private Handler mHandler;
    private SwipeRefreshLayout CheklistSwiperefresh;
    int flag = 0;
    String status = "";
    String submitBtnFlag = "";

    List<VesselChecklistRecordDataList> vesselChecklistRecordDataList = new ArrayList<>();
    int lastDataSize = 0;
    private BroadcastReceiver mNetworkReceiver;
    //    private Sensorservice mSensorService;
    private static Context ctx;

    public static Context getCtx() {
        return ctx;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    NetworkChangeReceiver networkChangeReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psc_inspection_checklist);
        sections1 = new ArrayList<>();
        List<VesselChecklist> checklists = new ArrayList<>();
        List<VesselComponent> list = new ArrayList<>();
        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        checklists = checkListDatabase.productDao().getAllVesselCheckList();
        String uid = "";
        for (int r = 0; r < checklists.size(); r++) {
            if (checklists.get(r).getList().equals("SHIP INITIAL INSPECTION CHECKLIST")) {
                uid = checklists.get(r).getUid();
            }
        }
        list = checkListDatabase.productDao().getVesselComponentByUid(Integer.valueOf(uid));
        sections1.add("Select Section");
        Log.e("list...", String.valueOf(list.size()));
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getTitle_of_section() != null && !sections1.contains(list.get(i).getTitle_of_section())) {
                sections1.add(list.get(i).getTitle_of_section());
            }
        }

        bindView();
        setUpToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        spinner = findViewById(R.id.PSCInspectionChecklistSpinner);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sections1);
        spinner.setAdapter(arrayAdapter);
        spinner.setPrompt("Select Section");

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Toast.makeText(PscInspectionChecklistActivity.this, "0 Position", Toast.LENGTH_SHORT).show();
                }
                if (position == 1) {
                    Toast.makeText(PscInspectionChecklistActivity.this, "1 Position", Toast.LENGTH_SHORT).show();
                }
                if (position == 2) {
                    Toast.makeText(PscInspectionChecklistActivity.this, "2 Position", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        checkListDatabase = Room.databaseBuilder(getApplicationContext(),
                CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetailsList = checkListDatabase.productDao().getUserDetail();

        componentList = new ArrayList<>();
        imageJson = new JSONObject();
        getUid();

        itemListDataAdapter = new ImageListDataAdapter(this, itemsListData, "", position, locationListData, timeStampListData, photoButton);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(PscInspectionChecklistActivity.this);
                dialog.setContentView(R.layout.comment_layout);
                TextView commentTitle = dialog.findViewById(R.id.commentTitleTextView);
                EditText editText = dialog.findViewById(R.id.editText);
                Button submitButton = dialog.findViewById(R.id.button);
                Button cancelButton = dialog.findViewById(R.id.buttonCancel);
                commentTitle.setText("Cancel Comment");
                dialog.setCancelable(false);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        submitComment = editText.getText().toString();
                        status = "1";
                        flag = 1;
                        dialogfinger = new Dialog(PscInspectionChecklistActivity.this);
                        dialogfinger.setContentView(R.layout.fingerprint_dialog);
                        dialogfinger.setCancelable(false);
                        ImageView mfingerprint = dialogfinger.findViewById(R.id.dialogFingerprint);
                        doneImageView = mfingerprint;
                        dialogfinger.show();
                        ImageView imgClose = dialogfinger.findViewById(R.id.imgClose);
                        imgClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogfinger.dismiss();
                            }
                        });
                        Window window = dialogfinger.getWindow();
                        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        verification();
//                        cancelCompleteMethod("2");
//                        submitComment = "";
//                        dialog.dismiss();
//                        dismissKeyboard(IsolationActivity.this);
//                        Toast.makeText(IsolationActivity.this, "Checklist canceled.", Toast.LENGTH_SHORT).show();
//                        finish();
                    }
                });

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        });

        completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(PscInspectionChecklistActivity.this);
                dialog.setContentView(R.layout.comment_layout);
                TextView commentTitle = dialog.findViewById(R.id.commentTitleTextView);
                EditText editText = dialog.findViewById(R.id.editText);
                Button submitButton = dialog.findViewById(R.id.button);
                Button cancelButton = dialog.findViewById(R.id.buttonCancel);
                commentTitle.setText("Complete Comment");
                dialog.setCancelable(false);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        submitComment = editText.getText().toString();
                        status = "2";
                        flag = 1;
                        dialogfinger = new Dialog(PscInspectionChecklistActivity.this);
                        dialogfinger.setContentView(R.layout.fingerprint_dialog);
                        dialogfinger.setCancelable(false);
                        ImageView mfingerprint = dialogfinger.findViewById(R.id.dialogFingerprint);
                        doneImageView = mfingerprint;
                        dialogfinger.show();
                        ImageView imgClose = dialogfinger.findViewById(R.id.imgClose);
                        imgClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogfinger.dismiss();
                            }
                        });
                        Window window = dialogfinger.getWindow();
                        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        verification();
//                        cancelCompleteMethod("1");
//                        submitComment = "";
//                        dialog.dismiss();
//                        dismissKeyboard(IsolationActivity.this);
//                        Toast.makeText(IsolationActivity.this, "Checklist completed.", Toast.LENGTH_SHORT).show();
//                        finish();
                    }
                });
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        });
        setDataOnPoint();
        LocationAccess();
        locationHelper.getLocation(PscInspectionChecklistActivity.this, PscInspectionChecklistActivity.this.locationResult);

        CheklistSwiperefresh.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        CheklistSwiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (new NetworkDetector().isNetworkReachable(PscInspectionChecklistActivity.this) == true) {
                    setDataOnPoint();
                    List<UserDetails> userDetails = new ArrayList<>();
                    userDetails = checkListDatabase.productDao().getUserDetail();
                    checklistDetailSwiperefresh(wrk_id, userDetails.get(0).getUser_id(),
                            userDetails.get(0).getDbnm(),
                            userDetails.get(0).getComp_name(), userDetails.get(0).getAccessToken());
                    CheklistSwiperefresh.setRefreshing(false);
                } else {
                    Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }

            }
        });

//        mNetworkReceiver = new NetworkChangeReceiver();
//        registerNetworkBroadcastForNougat();


    }

    public static void getOfflineData() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                CheckListDatabase db = Room.databaseBuilder(getCtx(),
                        CheckListDatabase.class, ApiUrlClass.roomDatabaseName).build();
                List<ChecklistRecordDataList> checklistdatarecord = new ArrayList<>();
                checklistdatarecord = db.productDao().getChecklistRecordDataListWithFlag();

                if (checklistdatarecord != null) {
                    for (int i = 0; i < checklistdatarecord.size(); i++) {
                        Gson g = new Gson();
                        String json = g.toJson(checklistdatarecord.get(i));

                        Log.e("ggf", json);
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            String imagedata = jsonObject.getString("image_data");
                            imagedata = imagedata.replaceAll("\\\\", "");
                            Log.e("image", imagedata);
                            JSONObject jsonphoto = new JSONObject(imagedata);

                            String dataKey = "submit_dtls";

                            MultipartUtility multipart = new MultipartUtility(ApiUrlClass.submitdata, ApiUrlClass.charset, dataKey, jsonObject);
                            List<File> file1 = new ArrayList<>();

                            if (jsonphoto != null) {
                                for (int j = 0; j < jsonphoto.length(); j++) {
                                    file1.add(j, new File(jsonphoto.getString(String.valueOf(j))));
                                    Log.e("datalist1", String.valueOf(jsonphoto.getString(String.valueOf(j))));
                                }
                                multipart.addFilePart("files", file1);
                            }

                            List<String> response = multipart.finish();
                            Log.e("SERVER REPLIED:", String.valueOf(response));

                            for (String line : response) {
                                System.out.println(line);
                                Log.e("res", line);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        });
        thread.start();

    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public void dismissKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != activity.getCurrentFocus())
            imm.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
    }

    //* @Author: Subhra Priyadarshini;
    //* @Used For : This function is used for fetching current location.
    public void LocationAccess() {
        this.locationResult = new LocationHelper.LocationResult() {
            @Override
            public void gotLocation(Location location) {

                //Got the location!
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("ab", "lat: " + latitude + ", long: " + longitude);
                    geocoder = new Geocoder(PscInspectionChecklistActivity.this);
                    try {
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        if (addresses != null && addresses.size() > 0) {
                            address = addresses.get(0).getAddressLine(0);
                            crnt_lctn = address;
                        }
                    } catch (IOException e) {
                        Toast.makeText(PscInspectionChecklistActivity.this, "Unable to access the current location !", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e("de", "Location is null.");
                }

            }

        };

        this.locationHelper = new LocationHelper();

    }

    public void setDataOnPoint() {
        List<VesselChecklistRecordDataList> checklistRecord = new ArrayList<>();

        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        checklistRecord = checkListDatabase.productDao().getVesselChecklistRecordDataList();
        int j = 0;
        for (int i = 0; i < checklistRecord.size(); i++) {
            if (checklistRecord.get(i).getWrk_id().equals(Integer.parseInt(wrk_id)) && checklistRecord.get(i).getUid().equals(Integer.parseInt(uid))) {
                lastdata.add(j, checklistRecord.get(i));
                j++;
            }
        }

//        Log.e(uid + ":wrkid", wrk_id);
//        Log.e("checklistSIze", String.valueOf(checklistRecord.size()));
//        Log.e("LastchecklistSIze", String.valueOf(lastdata.size()));
//
//        for (int i = 0; i < lastdata.size(); i++) {
//            imageMap.put(i, new ArrayList<>());
//        }

//        RecycleCheckpointAdapter recycleCheckpointAdapter = new RecycleCheckpointAdapter(this, lastdata, imageMap);
//        recycleCheckpointAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.vessel_option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.option_all) {
            item.setChecked(true);
            recyclerView.setAdapter(vesselPscInspectionChecklistAdapter);
            new VesselPscInspectionChecklistAdapter("allpoints").notifyDataSetChanged();
            return true;
        }
        if (id == R.id.option_completed) {
            item.setChecked(true);
            recyclerView.setAdapter(vesselPscInspectionChecklistAdapter);
            new VesselPscInspectionChecklistAdapter("0").notifyDataSetChanged();
            return true;
        }
        if (id == R.id.option_incompleted) {
            item.setChecked(true);
            recyclerView.setAdapter(vesselPscInspectionChecklistAdapter);
            new VesselPscInspectionChecklistAdapter("1").notifyDataSetChanged();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //------This function is used to typecast all the views in xml.-----------//
    private void bindView() {
        textView_checkList_header = findViewById(R.id.textView_homePage_header_pscInspectionChecklist);
        recyclerView = findViewById(R.id.recyclerViewPscInspectionChecklist);
        cancelBtn = findViewById(R.id.cancelledButton);
        completeBtn = findViewById(R.id.completedButton);
        CheklistSwiperefresh = findViewById(R.id.CheklistSwiperefreshPSCChecklist);
        textView_vessel_header = findViewById(R.id.textView_homePage_header_pscInspectionChecklist);
        textView_vessel_header.setText("PSC Inspection");
    }

    //--------This function is used to set the recyclerview in adapter ----//
    private void setRecyclerView() {

        vesselPscInspectionChecklistAdapter = new VesselPscInspectionChecklistAdapter(PscInspectionChecklistActivity.this,
                componentList,
                lastdata,
                imageMap,
                locationMap,
                timeStampMap,
                checklist_type,
                new VesselImageInterface() {

                    @Override
                    public void onPhotoButtonClickListener(int post, ImageView[] imageViews) {

                        if (imageViews[0].getDrawable() == null) {
                            imageView = imageViews[0];
                        } else if (imageViews[1].getDrawable() == null) {
                            imageView = imageViews[1];
                        } else if (imageViews[2].getDrawable() == null) {
                            imageView = imageViews[2];
                        } else if (imageViews[3].getDrawable() == null) {
                            imageView = imageViews[3];
                        }

                        // createImageName();

                    }

                    @Override
                    public void onPhotoButtonClickListenerAdapter(int post,
                                                                  String points,
                                                                  ImageView[] imageViews,
                                                                  ImageListDataAdapter adapter,
                                                                  List<String> imageList,
                                                                  List<String> locationList,
                                                                  List<String> timeStampList,
                                                                  String locationValue) {
                        if (point.equals("") || point.equals(points)) {
                            tempPoint = points;
                            position = post;
                            itemListDataAdapter = adapter;
                            itemsListData = imageList;
                            locationListData = locationList;
                            timeStampListData = timeStampList;

                            createImageName();
                            Log.e("position", String.valueOf(post));
                        } else {
                            Toast.makeText(PscInspectionChecklistActivity.this, "Can't start another checkpoint without submitting the working one!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onSwitchButtonClickListener(int post, String points, Context context, Switch aSwitch, RadioButton[] radioButtons, String alertDepartment, EditText editTexts, LinearLayout linearLayout) {

                        if (point.equals("") || point.equals(points)) {
                            if (aSwitch.isChecked()) {
                                alert = aSwitch.getTextOn().toString();
                            } else {
                                alert = aSwitch.getTextOff().toString();
                            }
                            if (radioButtons[0].isChecked()) {
                                severity = radioButtons[0].getText().toString();
                            }
                            if (radioButtons[1].isChecked()) {
                                severity = radioButtons[1].getText().toString();
                            }
                            if (radioButtons[2].isChecked()) {
                                severity = radioButtons[2].getText().toString();
                            }
                           department = alertDepartment;
                            alertComment = editTexts.getText().toString();
                            linearLayout.setVisibility(View.VISIBLE);
                            if (point.equals("")) {
                                point = points;
                            }
                        } else {
                            aSwitch.setChecked(false);
                            Toast.makeText(PscInspectionChecklistActivity.this, "Can't start another checkpoint without submitting the working one!", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onImageClickListener(int post, String points, ImageView imageView, Context context, ImageButton button) {

                        if (null == imageView.getDrawable()) {
                            Log.e("imageView Is null", "Null");
                        } else {
                            //imageViewDialog(context, imageView, button,recyclerView);
                        }

                    }

                    @Override
                    public void onSubmitButtonClickListener(int post, String points, RadioButton[] radioButtons, ImageButton editText, Switch aSwitch, ImageButton button, TextView textView,
                                                            TextView editTextSubmit, String strDate,
                                                            LinearLayout linearLayout, ImageButton b, ImageButton photoButton,
                                                            ImageListDataAdapter adapter, List<String> imageList,
                                                            List<String> locationList, List<String> timeStampList, Spinner spinner) {
                        if (point.equals("") || point.equals(points)) {
                            itemListDataAdapter = adapter;
                            itemsListData = imageList;
                            locationListData = locationList;
                            timeStampListData = timeStampList;
                            position = post;
                            locationHelper.getLocation(PscInspectionChecklistActivity.this, PscInspectionChecklistActivity.this.locationResult);
                            q = textView.getText().toString();
                            if (point.equals("")) {
                                point = points;
                            }
                            if (radioButtons[0].isChecked()) {
                                ans = radioButtons[0].getText().toString();
                            } else if (radioButtons[1].isChecked()) {
                                ans = radioButtons[1].getText().toString();
                            } else {
                                ans = radioButtons[2].getText().toString();
                            }
                            Log.e("q:ans", q + ":" + ans);
                            crnt_dt = strDate;
                            submitBtnFlag = "clicked";
                            mAlertDialog(points, editText, button, editTextSubmit, linearLayout, b, photoButton,radioButtons);
                        } else {
                            Toast.makeText(PscInspectionChecklistActivity.this, "Can't start another checkpoint without submitting the working one!", Toast.LENGTH_SHORT).show();
                        }

                    }

                    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
                        new AlertDialog.Builder(PscInspectionChecklistActivity.this)
                                .setMessage(message)
                                .setPositiveButton("OK", okListener)
                                .setNegativeButton("Cancel", null)
                                .create()
                                .show();
                    }

                    @Override
                    public void onGuidanceButtonClickListener(int post, String points, Context context) {
                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.guidance_layout);
                        dialog.setCancelable(false);
                        TextView textViewGuidance = dialog.findViewById(R.id.textGuidance);
                        ImageView imageView = dialog.findViewById(R.id.imageViewGuidance);
                        ImageView closeImage = dialog.findViewById(R.id.closeButtonGuidance);


                        getGuidanceData(post, textViewGuidance, imageView);

                        closeImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                        Window window = dialog.getWindow();
                        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    }

                    @Override
                    public void onCommentButtonClickListener(int post, String points, Context context, TextView editText, LinearLayout linearLayout) {
                        if (point.equals("") || point.equals(points)) {
                            comment = editText.getText().toString();
                            linearLayout.setVisibility(View.VISIBLE);
                            if (point.equals("")) {
                                point = points;
                            }
                        } else {
                            Toast.makeText(PscInspectionChecklistActivity.this, "Can't start another checkpoint without submitting the working one!", Toast.LENGTH_SHORT).show();
                        }

                    }

                });

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                RecyclerView.LayoutManager linearLayoutmanager = new LinearLayoutManager(PscInspectionChecklistActivity.this);
                if (linearLayoutmanager != null) {

                    recyclerView.setLayoutManager(linearLayoutmanager);
                    recyclerView.setHasFixedSize(true);

//                    RecyclerSectionItemDecoration sectionItemDecoration =
//                            new RecyclerSectionItemDecoration(getResources().getDimensionPixelSize(R.dimen._15sdp), true, getSectionCallback(componentList));
//                    recyclerView.addItemDecoration(sectionItemDecoration);

                    recyclerView.setAdapter(vesselPscInspectionChecklistAdapter);
//                 chceklisttitle=  componentList.get(position).getTitle_of_section();

                }


            }
        });


    }

    private void mAlertDialog(String points, ImageButton editText, ImageButton button, TextView editTextSubmit, LinearLayout linearLayout, ImageButton b, ImageButton photoButton,RadioButton[] radioButtons) {


        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.fingerprint_dialog);
        dialog.setCancelable(false);
        ImageView mfingerprint = dialog.findViewById(R.id.dialogFingerprint);

        point = points;
        submitEditText = editTextSubmit;
        this.button = button;
        this.photoButton = photoButton;
        this.radioButtons = radioButtons;
        this.editText = editText;
        this.b = b;
        this.linearLayout = linearLayout;
        this.dialog = dialog;
        doneImageView = mfingerprint;
        verification();


        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void getGuidanceData(int post, TextView textView, ImageView imageView) {
        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        List<Component> guidancelist = new ArrayList<>();
        guidancelist = checkListDatabase.productDao().getComponentByUid(uid);

        if (guidancelist.get(post).getGuidance_text().contains(guidancelist.get(post).getGuidance_text())) {
            String guidanceText = guidancelist.get(post).getGuidance_text();
            textView.setText(guidanceText);

        }
        if (guidancelist.get(post).getGuidance_image().contains("https://")) {
            String guidanceImage = guidancelist.get(post).getGuidance_image();
            Picasso.with(this)
                    .load(guidanceImage)
                    .into(imageView);
        }
    }

    private void SubmitDataOnServer() {

        JSONObject submit_dtls = new JSONObject();
        try {
            submit_dtls.put("user_id", userDetailsList.get(0).getUser_id());
            submit_dtls.put("dbnm", userDetailsList.get(0).getDbnm());
            submit_dtls.put("accessToken", userDetailsList.get(0).getAccessToken());
            submit_dtls.put("uid", uid);
            submit_dtls.put("wrk_id", wrk_id);
            submit_dtls.put("checklist_points", point);
            submit_dtls.put("q", q);
            submit_dtls.put("ans", ans);
            submit_dtls.put("comment", comment);
            submit_dtls.put("wrk_id", wrk_id);
            submit_dtls.put("crnt_dt_tm", crnt_dt);
            submit_dtls.put("crnt_lctn", crnt_lctn + "_" + latitude + "_" + longitude);
            submit_dtls.put("rank_id", rank_id);
            submit_dtls.put("user_id", emp_id);
            submit_dtls.put("imageJson", imageJson.toString());
            submit_dtls.put("dbnm", userDetailsList.get(0).getDbnm());

            Log.e("json", String.valueOf(submit_dtls));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {

            String dataKey = "submit_dtls";

            MultipartUtility multipart = new MultipartUtility(ApiUrlClass.submitdata, ApiUrlClass.charset, dataKey, submit_dtls);
            List<File> file1 = new ArrayList<>();
            Log.e("datalist", String.valueOf(itemsListData));
//            for (List<String> lis : imageMap.values()) {
//                itemsListData.addAll(lis);
//            }

            if (itemsListData != null) {
                for (int i = 0; i < itemsListData.size(); i++) {
                    file1.add(i, new File(itemsListData.get(i)));
                    Log.e("datalist1", String.valueOf(itemsListData));
                }
                multipart.addFilePart("files", file1);
            }

            List<String> response = multipart.finish();
            Log.e("SERVER REPLIED:", String.valueOf(response));

            for (String line : response) {
                System.out.println(line);
                Log.e("res", line);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void imageViewDialog(Context context, ImageView imageView, ImageButton button, RecyclerView recyclerView) {
        if (null == imageView.getDrawable()) {
            Log.e("imageView", "imageView is null");
        } else {
            BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
            bitmapUndo = drawable.getBitmap();
        }

        bitmap = new ImageHandler().imgView(imageView, bitmap);
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.image_view_layout);
        final ImageView imageView57 = dialog.findViewById(R.id.imageView57);
        Button closeButton = dialog.findViewById(R.id.closeButton);
        Button editButton = dialog.findViewById(R.id.editButton);
        Button saveButton = dialog.findViewById(R.id.saveButton);
        Button undoButton = dialog.findViewById(R.id.undoButton);
        imageView57.setImageBitmap(bitmap);
        if (button.isEnabled()) {
            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    imageView57.setOnTouchListener(new ImageEventHandler(imageView57, imageView));
                    undoButton.setEnabled(true);
                    saveButton.setEnabled(true);
                }
            });

            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String in1 = imageFileName;
                    new ImageData(imageView57, in1, imageView).saveFile();
                    saveButton.setEnabled(false);
                    undoButton.setEnabled(false);
                    BitmapDrawable drawable = (BitmapDrawable) imageView57.getDrawable();
                    bitmapUndo = drawable.getBitmap();
                    imageView.setImageBitmap(bitmapUndo);
                    dialog.dismiss();
                }
            });

            undoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    imageView57.setImageBitmap(bitmapUndo);
                    imageView.setImageBitmap(bitmapUndo);
                    saveButton.setEnabled(false);
                    undoButton.setEnabled(false);

                }
            });

        }
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void createImageName() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "IMG_" + timeStamp + ".jpg";
        onLaunchCamera();
    }

    //---- This function is used to open camera functionality.-----//
    public void onLaunchCamera() {
        // create Intent to take a picture and return control to the calling application
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Create a File reference to access to future access
        photoFile = getPhotoFileUri(imageFileName);

        // wrap File object into a content provider
        fileProvider = FileProvider.getUriForFile(PscInspectionChecklistActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider);
        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Start the image capture intent to take photo
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
    }

    // Returns the File for a photo stored on disk given the fileName
    public File getPhotoFileUri(String fileName) {

        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);

        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d(APP_TAG, "failed to create directory");
        }

        // Return the file target for the photo based on filename
//        file = new File(mediaStorageDir.getPath() + File.separator + fileName +timeStamp + ".jpg");
        file = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return file;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (flag == 1) {
                    try {
                        takenImage = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
                        FileOutputStream fos = new FileOutputStream(photoFile.getAbsolutePath());
                        Log.e("J", photoFile.getAbsolutePath());
                        takenImage.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                        fos.close();
                        cancelCompleteMethod(status);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        takenImage = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
                        ExifInterface ei = null;
                        try {
                            ei = new ExifInterface(String.valueOf(photoFile.getAbsolutePath()));

                            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_UNDEFINED);


                            switch (orientation) {

                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    rotatedBitmap = rotateImage(takenImage, 90);
                                    break;

                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    rotatedBitmap = rotateImage(takenImage, 180);
                                    break;

                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    rotatedBitmap = rotateImage(takenImage, 270);
                                    break;

                                case ExifInterface.ORIENTATION_NORMAL:
                                default:
                                    rotatedBitmap = takenImage;
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        FileOutputStream fos = new FileOutputStream(photoFile.getAbsolutePath());
                        Log.e("J", photoFile.getAbsolutePath());
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 40, fos);

                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//***********************Bitmap convert base64 and than convert bitmap for resizing*******
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    //decode base64 string to image
                    byte[] imageBytes = Base64.decode(encodedImage, Base64.DEFAULT);
                    Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, options);

                    listPhoto.add(photoFile.getAbsolutePath());
                    JSONObject temp = new JSONObject();
                    try {
                        String strTimeStamp = new UtilClassFuntions().currentDateTime();
                        temp.put("location", crnt_lctn);
                        temp.put("timeStamp", strTimeStamp);
                        temp.put("latitude", latitude);
                        temp.put("longitude", longitude);
                        imageJson.put(imageFileName, temp);
                        locationList.add(crnt_lctn);
                        timeStampList.add(strTimeStamp);
                        Log.e("time1", strTimeStamp);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("temp", String.valueOf(temp));


//                imageView.setImageBitmap(decodedImage);

                    //   File file = new File()

                    try {
//                    List<String> str = new ArrayList<>();
                        itemsListData.add(photoFile.getAbsolutePath());
                        imageMap.put(position, itemsListData);
                        locationMap.put(position, locationList);
                        timeStampMap.put(position, timeStampList);
                        vesselPscInspectionChecklistAdapter.setImageMap(imageMap, locationMap, timeStampMap);
//                        itemListDataAdapter.notifyDataSetChanged();
                        vesselPscInspectionChecklistAdapter.notifyDataSetChanged();
                        photo = String.valueOf(imageMap.values());

                        if (point.equals("")) {
                            point = tempPoint;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (submitBtnFlag.equalsIgnoreCase("start")) {
                        Log.e("position", String.valueOf(position));
                        submitDataMethod();
                    }
                }
            }
        }

    }

    private void submitDataMethod() {

        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(PscInspectionChecklistActivity.this) == true) {
            SubmitDataOnServer();
            String submitTitle = "<b>" + "Time:" + "</b>" + crnt_dt + "<b>" + "\nLocation:" + "</b>" + address;
            submitEditText.setText(Html.fromHtml(submitTitle));
            submitData(points, radioButtons, editText, aSwitch, button);
            dialog.dismiss();
            if (linearLayout.getVisibility() == View.GONE) {
                linearLayout.setVisibility(View.VISIBLE);
            }
            Toast.makeText(PscInspectionChecklistActivity.this, "Submited sucessfully", Toast.LENGTH_SHORT).show();
            mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    new ButtonLock().buttonLock(radioButtons,photoButton,editText,b);

                }
            }, 4000);
        } else {
            String submitTitle = "<b>" + "Time:" + "</b>" + crnt_dt + "<b>" + "\nLocation:" + "</b>" + address;
            submitEditText.setText(Html.fromHtml(submitTitle));
            submitData(points, radioButtons, editText, aSwitch, button);
            dialog.dismiss();
            if (linearLayout.getVisibility() == View.GONE) {
                linearLayout.setVisibility(View.VISIBLE);
            }
            Toast.makeText(PscInspectionChecklistActivity.this, "Submited sucessfully", Toast.LENGTH_SHORT).show();
            mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    new ButtonLock().buttonLock(radioButtons,photoButton,editText,b);
                }
            }, 4000);


        }

    }
    // ---- This function is used to get data from databse.-------//

    private void getDataFromDatabaseByUid(String uid) {
        Thread threadComponentByUid = new Thread(new Runnable() {
            @Override
            public void run() {
                userDetailsList = checkListDatabase.productDao().getUserDetail();

                //componentList=checkListDatabase.productDao().getAllComponents();
                componentList = checkListDatabase.productDao().getVesselComponentByUid(Integer.valueOf(uid));
                for (int i = 0; i < componentList.size(); i++) {
                    imageMap.put(i, new ArrayList<>());
                    locationMap.put(i, new ArrayList<>());
                    timeStampMap.put(i, new ArrayList<>());
                }
                setRecyclerView();
            }
        });
        threadComponentByUid.start();


    }

    private void getUid() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uid1 = uid = bundle.getString("uid");
           /* engNo = bundle.getString("EngNo");
            engType = bundle.getString("EngType");
            unitNo = bundle.getString("unitNo");*/
            title = bundle.getString("title");
            wrk_id = bundle.getString("wrk_id");
            checklist_type = bundle.getString("checklist_type");


            getDataFromDatabaseByUid(uid);
            textView_checkList_header.setText(title);
           /* tv_eng_type.setText(engType);
            tv_unit_no.setText(unitNo);*/

//**********************Get user data from database***********************
            CheckListDatabase db;
            List<UserDetails> list;
            db = Room.databaseBuilder(this, CheckListDatabase.class,
                    ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
            list = db.productDao().getUserDetail();
            rank_id = list.get(0).getRank_id();
            emp_id = list.get(0).getUser_id();

        } else {
            Toast.makeText(this, "something went wrong please try again..", Toast.LENGTH_SHORT).show();
        }
    }

    private void submitData(String post, RadioButton[] radioButtons, ImageButton editText, Switch aSwitch, ImageButton button) {

        //String comment=editText.getText().toString();
        VesselChecklistRecordDataList submitData = new VesselChecklistRecordDataList();

        submitData.setUid(Integer.valueOf(uid));
        submitData.setWrk_id(Integer.valueOf(wrk_id));
        submitData.setChecklist_points(point);
        submitData.setQ(q);
        submitData.setAns(ans);
        submitData.setComment(comment);
        submitData.setCrnt_lctn(crnt_lctn);
        submitData.setEmployee_id(emp_id);
        submitData.setEmployee_id(emp_id);
        submitData.setImageJson(String.valueOf(imageJson));

        Thread threadSetSubmit = new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().setVesselSubmitData(submitData);
                clear();
            }
        });

        threadSetSubmit.start();
    }

    public void clear() {
        point = "";
        q = "";
        ans = "";
        comment = "";
        photo = "";
        crnt_dt = "";
        crnt_lctn = "";
        tempPoint = "";
        listPhoto.clear();
        itemsListData = null;
        submitBtnFlag = "";
    }

    public void cancelCompleteMethod(String status) {
        String crnt_tm = new UtilClassFuntions().currentDateTime();
        ProgressDialog pdLoading = new ProgressDialog(PscInspectionChecklistActivity.this);
        if (pdLoading != null) {
            pdLoading.setTitle("Please wait...");
            pdLoading.show();
        }
        Map<String, String> params = new HashMap<>();
        params.put("comment", submitComment);
        params.put("wrk_id", wrk_id);
        params.put("user_id", userDetailsList.get(0).getUser_id());
        params.put("accessToken", userDetailsList.get(0).getAccessToken());
        params.put("modified_dt", crnt_tm);
        params.put("dbnm", userDetailsList.get(0).getDbnm());
        params.put("checklist_status", status);

        //****************************

        if (!wrk_id.equals("")) {

            MasterWorkId masterWorkId = new MasterWorkId();
            masterWorkId.setWrk_id(Integer.valueOf(wrk_id));
            masterWorkId.setComment(submitComment);
            masterWorkId.setGrtd_by(emp_id);
            masterWorkId.setModified_dt(crnt_tm);
            masterWorkId.setWrk_status(Integer.valueOf(status));

            Thread threadSetSubmit = new Thread(new Runnable() {
                @Override
                public void run() {
                    checkListDatabase.productDao().updateMasterWorkId(wrk_id, comment, emp_id, crnt_tm, status,"",1);
                }
            });
            threadSetSubmit.start();

        }

        //*****************************
        JSONObject jsonObject = new JSONObject(params);
        try {

            String dataKey = "vslckdetailsn";

            MultipartUtility multipart = new MultipartUtility(ApiUrlClass.cancel_complete_url, ApiUrlClass.charset, dataKey, jsonObject);
            List<File> file1 = new ArrayList<>();

            if (photoFile != null) {
                file1.add(0, new File(photoFile.getAbsolutePath()));
                multipart.addFilePart("userimg", file1);
            }

            List<String> response = multipart.finish();
            Log.e("SERVER REPLIED:", String.valueOf(response));

            for (String line : response) {
                System.out.println(line);
                Log.e("res", line);
                pdLoading.dismiss();
                finish();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private RecyclerSectionItemDecoration.SectionCallback getSectionCallback(final List<Component> components) {
        return new RecyclerSectionItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {
                return position == 0
                        || components.get(position)
                        .getTitle_of_section()
                        .charAt(0) != components.get(position - 1)
                        .getTitle_of_section()
                        .charAt(0);
            }

            @Override
            public CharSequence getSectionHeader(int position) {
                return components.get(position).getTitle_of_section()
                        /*.subSequence(0,1)*/;
            }
        };
    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbarPSCInspctionChecklist);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("PSC Inspection");
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.leftarrowwhite));
        getSupportActionBar().getThemedContext();
        toolbar.setTitleTextColor(0xFFFFFFFF);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
                onBackPressed();
        }
        });

    }

    public void verification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

            if (!fingerprintManager.isHardwareDetected()) {
                //Here submit data on submit button
                Toast.makeText(this, "Fingerprint not detected", Toast.LENGTH_SHORT).show();

            } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {

            } else if (!keyguardManager.isKeyguardSecure()) {

            } else if (!fingerprintManager.hasEnrolledFingerprints()) {

            } else {

                PscInspectionChecklistActivity.FingerprintVerification fingerprintVerification = new PscInspectionChecklistActivity.FingerprintVerification(this);
                fingerprintVerification.startAuth(fingerprintManager, null);

            }
        } else {

            //Here submit data on click submit button
            Toast.makeText(this, "Fingerprint not detected", Toast.LENGTH_SHORT).show();

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private class FingerprintVerification extends FingerprintManager.AuthenticationCallback {
        private Context context;

        public FingerprintVerification(Context context) {
            this.context = context;
        }

        public void startAuth(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject) {
            CancellationSignal cancellationSignal = new CancellationSignal();
            fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
        }

        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {
            super.onAuthenticationError(errorCode, errString);
        }

        @Override
        public void onAuthenticationFailed() {
            super.onAuthenticationFailed();
        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
            super.onAuthenticationHelp(helpCode, helpString);
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            super.onAuthenticationSucceeded(result);
            if (flag == 1) {
                doneImageView.setImageResource(R.mipmap.done);
                dialogfinger.dismiss();
                createImageName();
            } else {
                doneImageView.setImageResource(R.mipmap.done);
                Log.e("s", "Authentication");
                submitBtnFlag = "start";
                createImageName();
            }
        }
    }

    public void checklistDetailSwiperefresh(String wrk_id, String employee_id, String dbnm, String comp_name, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("uid", uid);
            params.put("wrk_id", wrk_id);
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("accessToken", accessToken);

            VolleyMethods.makePostJsonObjectRequest(params, PscInspectionChecklistActivity.this, new ApiUrlClass().SHIP_INSPECTION_DETAILS, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray vesselChecklistRecordData = response.getJSONArray("lastdata");
                            List<VesselChecklistRecordDataList> vesselChecklistRecordDataListTemp = new ArrayList<>();

                            Log.e("lastdata", String.valueOf(vesselChecklistRecordData));
                            if (vesselChecklistRecordData != null && vesselChecklistRecordData.length() > 0) {
                                for (int i = 0; i < vesselChecklistRecordData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) vesselChecklistRecordData.get(i);
                                    VesselChecklistRecordDataList vesselChecklistRecordDataList1 = objectMapper.readValue(jsonObject1.toString(), VesselChecklistRecordDataList.class);
                                    vesselChecklistRecordDataList.add(vesselChecklistRecordDataList1);
                                    vesselChecklistRecordDataListTemp.add(vesselChecklistRecordDataList1);
                                }
                                lastDataSize = 0;
                                lastDataSize = vesselChecklistRecordDataListTemp.size();
                                Log.e("data2", String.valueOf(lastDataSize));
                                vesselPscInspectionChecklistAdapter.setLastData(lastDataSize);
                                recyclerView.setAdapter(vesselPscInspectionChecklistAdapter);
                                vesselPscInspectionChecklistAdapter.notifyDataSetChanged();
                            }

                            resumeAndAddData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(PscInspectionChecklistActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(PscInspectionChecklistActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void resumeAndAddData() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().deleteVesselChecklistDataList(Integer.parseInt(wrk_id));
                checkListDatabase.productDao().setVesselResumeDataToTable(vesselChecklistRecordDataList/*, enginDescriptionData*/);
            }
        });
        threadDeleteAllTables.start();
    }

}
