package com.nuevotechsolutions.ewarp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.room.Room;

import com.google.android.material.navigation.NavigationView;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;

import java.util.ArrayList;
import java.util.List;

public class DryDockActivity extends AppCompatActivity {

    TextView textView_vessel_header;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    CheckListDatabase checkListDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dry_dock);

        textView_vessel_header=findViewById(R.id.textView_homePage_header_drydock);
        textView_vessel_header.setText("Dry Dock");
//        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
//        View hView =  navigationView.getHeaderView(0);
//        TextView nav_user = (TextView)hView.findViewById(R.id.userNameLabel);

        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        List<UserDetails> userDetails=new ArrayList<>();
        userDetails=checkListDatabase.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name()!=null){
            String user=userDetails.get(0).getFirst_name();
//            nav_user.setText(user);
        }else{
//            nav_user.setText("Guest User");
        }

        setUpToolbar();


    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbarDryDock);
        setSupportActionBar(toolbar);
        ImageView backarrowDryDock = findViewById(R.id.backarrowDryDock);
        backarrowDryDock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                new OnBackPressed().onBackPressedFunction(DryDockActivity.this);
                finish();
            }
        });
    }

}
