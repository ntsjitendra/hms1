package com.nuevotechsolutions.ewarp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.ViewChecklistRecyclerAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.nuevotechsolutions.ewarp.utils.SyncData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewChecklistActivity extends AppCompatActivity {

    JSONArray arrChecklist;
    String checklistName, uid, user_id, dbnm, accessToken;
    Toolbar toolbar;
    TextView ViewChecklistTitle;
    RecyclerView ViewChecklist_recyclerView;
    ViewChecklistRecyclerAdapter viewChecklistRecyclerAdapter;
    Button download_button_checklist;
    ProgressDialog pdLoading;
    List<Component> componentList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_checklist);
        setUpToolbar();
        ViewChecklist_recyclerView = findViewById(R.id.ViewChecklist_recyclerView);
        download_button_checklist = findViewById(R.id.download_button_checklist);
        componentList = new ArrayList<>();

        Intent intent = getIntent();
        if (intent != null) {
            try {
                checklistName = intent.getStringExtra("checklistName");
                uid = intent.getStringExtra("uid");
                user_id = intent.getStringExtra("user_id");
                dbnm = intent.getStringExtra("dbnm");
                accessToken = intent.getStringExtra("accessToken");
                arrChecklist = new JSONArray(intent.getStringExtra("list"));
                ViewChecklistTitle.setText(checklistName);
                viewChecklistRecyclerAdapter = new ViewChecklistRecyclerAdapter(ViewChecklistActivity.this, arrChecklist,componentList);
                LinearLayoutManager llm = new LinearLayoutManager(ViewChecklistActivity.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                ViewChecklist_recyclerView.setLayoutManager(llm);
                ViewChecklist_recyclerView.setAdapter(viewChecklistRecyclerAdapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        download_button_checklist.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                NetworkDetector networkDetector = new NetworkDetector();
                if (networkDetector.isNetworkReachable(ViewChecklistActivity.this) == true) {
                    if (networkDetector.isConnectionFast(ViewChecklistActivity.this) == true) {
                        pdLoading = new ProgressDialog(ViewChecklistActivity.this);
                        pdLoading.setTitle("Please wait...");
                        pdLoading.setCancelable(false);
                        pdLoading.show();
                       /* progressbarNewTask.setVisibility(View.VISIBLE);
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);*/
                        Map<String, String> params = new HashMap<>();
                        params.put("uid", uid);
                        params.put("user_id", user_id);
                        params.put("dbnm", dbnm);
                        params.put("accessToken", accessToken);
                        VolleyMethods.makePostJsonObjectRequest(params, ViewChecklistActivity.this, new ApiUrlClass().Download_Checklist_API, new PostJsonObjectRequestCallback() {
                            @Override
                            public void onSuccessResponse(JSONObject response) {
                                if (response != null) {
                                    Log.e("ok", "ok" + response.toString());
                                    try {
                                        int code = response.getInt("code");
                                        Log.e("code::", String.valueOf(code));
                                        if (code==200){
                                            pdLoading.hide();
                                            new SyncData().storeAllData(ViewChecklistActivity.this);
                                            Snackbar snackbar = Snackbar.make(findViewById(R.id.VIewChecklistLL), "Checklist downloaded successfully.", Snackbar.LENGTH_LONG);
                                            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                                            textView.setTextColor(Color.WHITE);
                                            snackbar.show();
                                        } else if (code==1062) {
                                            pdLoading.hide();
                                            Snackbar snackbar = Snackbar.make(findViewById(R.id.VIewChecklistLL), "Checklist already downloaded!", Snackbar.LENGTH_LONG);
                                            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                                            textView.setTextColor(Color.WHITE);
                                            snackbar.show();
                                        } else {
                                            pdLoading.hide();
                                            Snackbar snackbar = Snackbar.make(findViewById(R.id.VIewChecklistLL), "Checklist downloading failed!", Snackbar.LENGTH_LONG);
                                            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                                            textView.setTextColor(Color.RED);
                                            snackbar.show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }

                            @Override
                            public void onVolleyError(VolleyError error) {
                                pdLoading.hide();
                                Log.e("err", error.toString());
                            }

                            @Override
                            public void onTokenExpire() {
                                pdLoading.hide();
                                Toast.makeText(ViewChecklistActivity.this, "expire", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.VIewChecklistLL), "Bad internet connection!", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);
                        snackbar.show();
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.VIewChecklistLL), "No internet connection!", Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }
            }
        });

    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbar9);
        ViewChecklistTitle = toolbar.findViewById(R.id.ViewChecklistTitle);
        setSupportActionBar(toolbar);
        ImageButton imageView = findViewById(R.id.backarrowViewChecklist);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(ViewChecklistActivity.this);
                finish();
            }
        });
    }

}
