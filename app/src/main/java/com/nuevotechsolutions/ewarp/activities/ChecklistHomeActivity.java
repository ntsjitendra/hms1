package com.nuevotechsolutions.ewarp.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.VolleyError;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.DefaultImgSliderAdapter;
import com.nuevotechsolutions.ewarp.adapter.NoticeAdapter;
import com.nuevotechsolutions.ewarp.adapter.SliderImageAdapter;
import com.nuevotechsolutions.ewarp.controller.DatabaseHandler;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.fcmservice.MyFirebaseMessagingService;
import com.nuevotechsolutions.ewarp.interfaces.GetJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ImageModel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.NoticeDataList;
import com.nuevotechsolutions.ewarp.services.NetworkChangeReceiver;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.services.TimeChangedReceiver;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.LogoutClass;
import com.nuevotechsolutions.ewarp.utils.SyncData;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.nuevotechsolutions.ewarp.R.string.app_name;


public class ChecklistHomeActivity extends AppCompatActivity {
    CardView assignedButton, newButton, workingButton, completeButton;
    String user, id, Rank;
    TextView textView_homePage_header, imgSyncData;
    TextView view_DownloadChecklist;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    CheckListDatabase cDb;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    public static TextView badge_notification_1, badge_notification_2,
            badge_notification_3, badge_notification_4;
    List<MasterWorkId> list = new ArrayList<>();
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<ImageModel> imageModelArrayList;
    public static final int REQUEST_IMAGE_CAPTURE = 0;
    public static String fileName;
    private TextView notification;
    private RecyclerView noticerecyclerView;
    private ImageView imgNotificationclose;
    private List<NoticeDataList> noticeList = new ArrayList<>();
    List<NoticeDataList> noticeList2 = new ArrayList<>();
    private NoticeAdapter noticeAdapter;
    private JSONObject jsonObject;
    private String isNoticePicAvailable = "";
    private BroadcastReceiver mNetworkReceiver;
    public final String APP_NAME = "EWARP";
    private Handler mHandler;
    private String completedCount, resumeCount, assignCount;
    private int[] myImageList = new int[]{R.drawable.inspectionslide, R.drawable.hospital,
            R.drawable.transport, R.drawable.construction};
    AlertDialog.Builder alertDialog;
    private ImageView imgProfilepic;
    DatabaseHandler db;
    int count = 0;
    Timer swipeTimer;
    TimeChangedReceiver m_timeChangedReceiver;
    //    static boolean checkFirstTime=false;
    ProgressDialog pdLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist_home);
        mHandler = new Handler();

        cDb = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        mHandler = new Handler();
        textView_homePage_header = findViewById(R.id.textView_homePage_header0);
        textView_homePage_header.setText("Home");
        imgSyncData = findViewById(R.id.syncNow);
        notification = findViewById(R.id.notification);
        view_DownloadChecklist = findViewById(R.id.download_checklist);
        swipeTimer = new Timer();

        imageModelArrayList = new ArrayList<>();
        imageModelArrayList = populateListDefault();
        init();
        assignedButton = findViewById(R.id.assignedButton);
        newButton = findViewById(R.id.newButton);
        workingButton = findViewById(R.id.workingButton);
        completeButton = findViewById(R.id.completeButton);
        badge_notification_1 = findViewById(R.id.badge_notification_1);
        badge_notification_2 = findViewById(R.id.badge_notification_2);
        badge_notification_3 = findViewById(R.id.badge_notification_3);
        badge_notification_4 = findViewById(R.id.badge_notification_4);

//        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
//        View hView =  navigationView.getHeaderView(0);
//        TextView nav_user = (TextView)hView.findViewById(R.id.userNameLabel);

        cDb = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        list = cDb.productDao().getMasterWorkIdData();
        List<UserDetails> userDetails = new ArrayList<>();
        userDetails = cDb.productDao().getUserDetail();
//        if (userDetails.get(0).getFirst_name() != null) {
//            user = userDetails.get(0).getFirst_name();
////            nav_user.setText(user);
//        } else {
////            nav_user.setText("Guest User");
//        }
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
        View hView = navigationView.getHeaderView(0);
        TextView nav_user = (TextView) hView.findViewById(R.id.userNameLabel);
        TextView userRank = (TextView) hView.findViewById(R.id.userRank);
        imgProfilepic = (ImageView) hView.findViewById(R.id.imgProfilepic);
        noticeList = cDb.productDao().getAllNoticeList();
        noticeList2 = populateList();
        init();
        list = cDb.productDao().getMasterWorkIdData();
        userDetails = cDb.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name() != null) {
            user = userDetails.get(0).getFirst_name();
            nav_user.setText(user);
        } else {
            nav_user.setText("Guest User");
        }
        if (userDetails.get(0).getRank() != null) {
            Rank = userDetails.get(0).getRank();
            Log.e("rank nanme", Rank);
            userRank.setText(Rank);
        } else {
            userRank.setText("");
        }
        if (userDetails.get(0).getUser_image() != null && userDetails.get(0).getUser_image().contains("http")) {
            Picasso.with(this)
                    .load(userDetails.get(0).getUser_image())
                    .centerCrop()
                    .fit()
                    .error(R.drawable.person)
                    .into(imgProfilepic);
        }
        setUpToolbar();
        db = new DatabaseHandler(this);

        navigationView = findViewById(R.id.navigation_menu);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.nav_change_pass:
                        drawerLayout.closeDrawers();
                        Intent changePass = new Intent(ChecklistHomeActivity.this, ChangePassword.class);
                        startActivity(changePass);
                        break;
                    case R.id.nav_how_it_work:
                        drawerLayout.closeDrawers();
                        Intent howItWorks = new Intent(ChecklistHomeActivity.this, HowItWorksSlideActiivity.class);
                        startActivity(howItWorks);
                        break;
                    case R.id.nav_dashboard:
                        drawerLayout.closeDrawers();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ethicalworking.com/"));
                        startActivity(browserIntent);
                        break;
//                    case R.id.nav_contact:
//                        drawerLayout.closeDrawers();
//                        Intent contactus = new Intent(ChecklistHomeActivity.this, ContactUsActivity.class);
//                        startActivity(contactus);
//                        break;
                    case R.id.nav_free_space:
                        alertDialog = new AlertDialog.Builder(ChecklistHomeActivity.this);
                        alertDialog.setTitle("Gallery Cleanup");
                        alertDialog.setIcon(R.drawable.ewarpletest);
                        alertDialog.setMessage("Do you want to cleanup your gallery photo taken by this app. Are you sure?");
                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                drawerLayout.closeDrawers();
                                getAllFileFromDirectory();
                            }
                        });
                        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                drawerLayout.closeDrawers();
                            }
                        });
                        alertDialog.show();
                        break;
                    case R.id.nav_logout:
                        drawerLayout.closeDrawers();
                        new LogoutClass().logoutMethod(ChecklistHomeActivity.this);
                        break;
                }

                return false;
            }
        });

        this.mHandler.postDelayed(m_Runnable, 2000);
        getData();
        assignedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assignedchecklist();
            }
        });

        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newchecklist();
            }
        });

        workingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                workingchecklist();
            }
        });

        completeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                completechecklist();
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationDialog();
            }
        });
        imgSyncData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(ChecklistHomeActivity.this,Camera2BasicActivity.class);
//                startActivity(intent);

                if (new NetworkDetector().isNetworkReachable(ChecklistHomeActivity.this) == true) {
                    boolean b = new SyncData().storeAllData(ChecklistHomeActivity.this);
                    /*if (b==true){
                        onRestart();

                    }*/

                } else {
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.checklistHomeLL), "No internet connection!", Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                    TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }
            }
        });
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();
            Log.e("newToken", newToken);
            this.getPreferences(Context.MODE_PRIVATE).edit().putString("fb", newToken).apply();
        });
        mNetworkReceiver = new NetworkChangeReceiver();
        Log.d("newToken", this.getPreferences(Context.MODE_PRIVATE).getString("fb", "empty :("));
        try {
            startService(new Intent(ChecklistHomeActivity.this, MyFirebaseMessagingService.class));

        } catch (Exception e) {
            e.printStackTrace();
        }
        m_timeChangedReceiver = new TimeChangedReceiver();
        IntentFilter s_intentFilter = new IntentFilter();
        s_intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        s_intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        registerReceiver(m_timeChangedReceiver, s_intentFilter);

        Log.e("register", "receiver");
        registerNetworkBroadcastForNougat();

        view_DownloadChecklist.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                NetworkDetector networkDetector = new NetworkDetector();
                if (networkDetector.isNetworkReachable(ChecklistHomeActivity.this) == true) {
                    if (networkDetector.isConnectionFast(ChecklistHomeActivity.this) == true) {
                        pdLoading = new ProgressDialog(ChecklistHomeActivity.this);
                        pdLoading.setTitle("Please wait...");
                        pdLoading.setCancelable(false);
                        pdLoading.show();
                        countDownTimer.start();
                       /* progressbarNewTask.setVisibility(View.VISIBLE);
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);*/
                        Map<String, String> params = new HashMap<>();
                        params.put("abc", "abc");
                        VolleyMethods.makeGetJsonObjectRequest(params, ChecklistHomeActivity.this, new ApiUrlClass().View_Checklist_List_API, new GetJsonObjectRequestCallback() {
                            @Override
                            public void onSuccessResponse(JSONObject response) {
                                countDownTimer.cancel();
                                if (response != null) {
                                    Log.e("ok", "ok" + response.toString());
                                    JSONArray arrList = new JSONArray();
                                    JSONArray arrCategory = new JSONArray();
                                    try {
                                        arrList = response.getJSONArray("list");
                                        arrCategory = response.getJSONArray("category");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    if (arrList != null && arrList.length() > 0) {
                                        pdLoading.hide();
                                       /* progressbarNewTask.setVisibility(View.GONE);
                                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);*/
                                        Intent intent = new Intent(ChecklistHomeActivity.this, DownloadChecklistFileActivity.class);
                                        intent.putExtra("list", arrList.toString());
                                        intent.putExtra("category", arrCategory.toString());
                                        startActivity(intent);
                                    }

                                }
                            }

                            @Override
                            public void onVolleyError(VolleyError error) {
                                pdLoading.hide();
                                countDownTimer.cancel();
                                Log.e("err", error.toString());
                            }

                            @Override
                            public void onTokenExpire() {
                                pdLoading.hide();
                                countDownTimer.cancel();
                                Toast.makeText(ChecklistHomeActivity.this, "expire", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.checklistHomeLL), "Bad internet connection!", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);
                        snackbar.show();
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.checklistHomeLL), "No internet connection!", Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                    TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }
            }
        });


    }

    CountDownTimer countDownTimer = new CountDownTimer(40000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            Toast.makeText(ChecklistHomeActivity.this, "Slow Internet Connection", Toast.LENGTH_SHORT).show();
            pdLoading.dismiss();

        }
    };

    public void notificationDialog() {
        Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
        dialog.setContentView(R.layout.dialog_notice);
        imgNotificationclose = dialog.findViewById(R.id.closeNotifictaionBtn);
        noticerecyclerView = dialog.findViewById(R.id.noticerecyclerView);
        noticerecyclerView.setLayoutManager(new LinearLayoutManager(this));
        noticeList = cDb.productDao().getAllNoticeList();
        noticeAdapter = new NoticeAdapter(this, noticeList, new NoticeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(NoticeDataList item, int position) {
                Intent i = new Intent(ChecklistHomeActivity.this, NoticeDetailActivity.class);
                i.putExtra("noticekey", (Serializable) item);
                startActivity(i);
            }
        });
        noticerecyclerView.setAdapter(noticeAdapter);
        imgNotificationclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });
        dialog.show();
    }

    private List<NoticeDataList> populateList() {
        List<NoticeDataList> list = new ArrayList<>();
        List<NoticeDataList> list1 = new ArrayList<>();
        if (noticeList != null) {
            for (int i = 0; i < noticeList.size(); i++) {
                if (noticeList.get(i).getFile_path() != null) {
                    NoticeDataList imageModel = new NoticeDataList();
                    imageModel.setFile_path(noticeList.get(i).getFile_path());
                    Log.e("path--", noticeList.get(i).getFile_path());
                    list.add(imageModel);
                }
            }
        }

        if (list != null && list.size() >= 4) {
            for (int j = list.size(); j > list.size() - 4; j--) {
                list1.add(list.get(j - 1));
            }
        } else {
            switch (list.size()) {

                case 1:
                    for (int j = 0; j < 1; j++) {
                        list1.add(list.get(j));
                    }
                    break;
                case 2:
                    for (int j = 0; j < 2; j++) {
                        list1.add(list.get(j));
                    }
                    break;
                case 3:
                    for (int j = 0; j < 3; j++) {
                        list1.add(list.get(j));
                    }
                    break;
                default:
                    isNoticePicAvailable = "default";

            }
        }
        return list1;
    }

    private ArrayList<ImageModel> populateListDefault() {

        ArrayList<ImageModel> list = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            ImageModel imageModel = new ImageModel();
            imageModel.setImage_drawable(myImageList[i]);
            list.add(imageModel);
        }

        return list;
    }


    private void registerNetworkBroadcastForNougat() {

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//
////            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


//            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    private void checkListNotifications() {
        list.clear();
        list = cDb.productDao().getMasterWorkIdData();
        List<MasterWorkId> workinglist = new ArrayList<>();
        List<MasterWorkId> completedList = new ArrayList<>();
        List<MasterWorkId> assignList = new ArrayList<>();
        int j = 0;
        for (int i = list.size() - 1; i >= 0; i--) {
            if (list.get(i).getWrk_status().equals(0) && list.get(i).getSeen() == false) {
                workinglist.add(j, list.get(i));
                j++;
            }
        }

        int k = 0;
        for (int i = list.size() - 1; i >= 0; i--) {
            if ((list.get(i).getWrk_status().equals(1) || list.get(i).getWrk_status().equals(2))
                    && list.get(i).getSeen() == false) {
                completedList.add(k, list.get(i));
                k++;
            }
        }

        int r = 0;
        for (int i = list.size() - 1; i >= 0; i--) {
            if ((list.get(i).getWrk_status().equals(3) || list.get(i).getWrk_status().equals(5))
                    && list.get(i).getSeen() == false) {
                assignList.add(r, list.get(i));
                r++;
            }
        }
        completedCount = String.valueOf(completedList.size());
        resumeCount = String.valueOf(workinglist.size());
        assignCount = String.valueOf(assignList.size());

        if (completedList.size() > 0) {
            badge_notification_1.setVisibility(View.VISIBLE);
            badge_notification_1.setText(completedCount);
        } else
            badge_notification_1.setVisibility(View.GONE);
        if (workinglist.size() > 0) {
            badge_notification_2.setVisibility(View.VISIBLE);
            badge_notification_2.setText(resumeCount);
        } else
            badge_notification_2.setVisibility(View.GONE);
        if (assignList.size() > 0) {
            badge_notification_4.setVisibility(View.VISIBLE);
            badge_notification_4.setText(assignCount);
        } else
            badge_notification_4.setVisibility(View.GONE);

    }

    private final Runnable m_Runnable = new Runnable() {
        public void run() {
            checkListNotifications();
            ChecklistHomeActivity.this.mHandler.postDelayed(m_Runnable, 2000);
        }
    };

    private void setUpToolbar() {
        drawerLayout = findViewById(R.id.drawerLayout0);
        toolbar = findViewById(R.id.toolbarChecklistPage);
        setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    private void getData() {
        Bundle b = getIntent().getExtras();
//        user = b.getString("username");
//        id = b.getString("uid");

        pref = getSharedPreferences("username", MODE_PRIVATE);
        pref = getSharedPreferences("uid", MODE_PRIVATE);
        user = pref.getString("username", null);
        id = pref.getString("uid", null);


        SharedPreferences prefs = getSharedPreferences("baseData", MODE_PRIVATE);
        String fname = prefs.getString("fNameKey", null);
        //userParaLabel.setText("Welcome:" + user+"\t Id:"+id);
    }

    private void assignedchecklist() {
        Intent intent = new Intent(ChecklistHomeActivity.this, AssignedTask.class);
        intent.putExtra("username", user);
        intent.putExtra("uid", id);
        startActivity(intent);
    }

    private void newchecklist() {
        Intent intent = new Intent(ChecklistHomeActivity.this, NewTaskActivity.class);
        intent.putExtra("username", user);
        intent.putExtra("uid", id);
        startActivity(intent);
    }

    private void workingchecklist() {
        Intent intent = new Intent(ChecklistHomeActivity.this, WorkingCheckListActivity.class);
        intent.putExtra("username", user);
        intent.putExtra("uid", id);
        startActivity(intent);

    }

    private void completechecklist() {
        Intent intent = new Intent(ChecklistHomeActivity.this, CompleteCheckListActivity.class);
        intent.putExtra("username", user);
        startActivity(intent);
    }

    private void init() {
        mPager = (ViewPager) findViewById(R.id.pager);
        if (isNoticePicAvailable.equals("default")) {
            DefaultImgSliderAdapter defaultImgSliderAdapter = new DefaultImgSliderAdapter(ChecklistHomeActivity.this, imageModelArrayList);
            mPager.setAdapter(defaultImgSliderAdapter);
            defaultImgSliderAdapter.notifyDataSetChanged();
        } else {
            SliderImageAdapter sliderImageAdapter = new SliderImageAdapter(ChecklistHomeActivity.this, noticeList2);
            mPager.setAdapter(sliderImageAdapter);
            sliderImageAdapter.notifyDataSetChanged();
        }


        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = 5;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };

        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }


    @Override
    protected void onDestroy() {
        if (swipeTimer != null) {
            swipeTimer.cancel();
        }
        try {
            if (m_timeChangedReceiver != null)
                unregisterReceiver(m_timeChangedReceiver);

        } catch (Exception e) {
        }
        super.onDestroy();
        unregisterNetworkChanges();
    }


    public void getAllFileFromDirectory() {
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_NAME);
        if (!file.exists() && !file.mkdirs()) {
            Log.d(APP_NAME, "failed to create directory");
        } else {
            String path = file.getPath();
            Log.d("Files", "Path: " + path);
            File directory = new File(path);
            File[] files = directory.listFiles();
            Log.d("Files", "Size: " + files.length);
            for (int i = 0; i < files.length; i++) {
                Log.d("Files", "FileName:" + files[i].getName());
            }
            autoDeletePhotofile(files);
        }
    }

    public void autoDeletePhotofile(File[] files) {
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (files[i].exists()) {
                    Calendar time = Calendar.getInstance();
                    time.add(Calendar.DAY_OF_YEAR, -10);
                    //I store the required attributes here and delete them
                    Date lastModified = new Date(files[i].lastModified());
                    if (lastModified.before(time.getTime())) {
                        //file is older than a week
                        files[i].delete();
                    }
                } else {
                    try {
                        files[i].createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            Toast.makeText(this, "Gallery Cleanup", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        new SyncData().storeAllData(ChecklistHomeActivity.this);
//        Intent i=new Intent(ChecklistHomeActivity.this,ChecklistHomeActivity.class);
//        startActivity(i);
//        finish();
    }

    /*@Override
    protected void onStart() {
        super.onStart();
        if (new NetworkDetector().isNetworkReachable(ChecklistHomeActivity.this) == true) {
            boolean b = new SyncData().storeAllData(ChecklistHomeActivity.this);

        }
    }*/

    @Override
    public void onBackPressed() {
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Do you want to exit?");
            builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ChecklistHomeActivity.super.onBackPressed();
                }
            });
            builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.show();
        }
    }

}
