package com.nuevotechsolutions.ewarp.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.room.Room;

import com.android.volley.VolleyError;
import com.crashlytics.android.Crashlytics;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.controller.DatabaseHandler;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.fcmservice.MyFirebaseMessagingService;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecord;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserPointAccessData;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.SharedPrefancesClearData;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity {
    EditText emailEditText, passwordEditText;
    TextView labelTextView, registerButton, forgetButton;
    Button loginButton;
    List<ChecklistRecord> checklistRecordList;
    CheckListDatabase checkListDatabase;
    List<CheckList> checkLists;
    List<Component> componentList;
    List<UserDetails> userDetailsList;
    List<AuxEngDetails> auxEngDetailsList;
    List<AuxEngLabel> auxEngLabelList;
    List<TeamSelectionList> teamSelectionLists;
    List<ChecklistRecordDataList> checklistRecordDataList;
    List<MasterWorkId> masterWorkIdList;
    List<UserPointAccessData> userPointAccessDataList;
    List<EnginDescriptionData> enginDescriptionData;
    private TextView tvWebsitelink;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private String uid = null;
    URL url = null;
    private String username, password;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    TelephonyManager telephonyManager;
    FingerprintManager fingerprintManager;
    private LinearLayout loginll;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    DatabaseHandler db;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private boolean value = true;
    private Button btnOTP;
    String newToken;
    private ProgressBar progressbarLogin;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Fabric.with(this, new Crashlytics());
        progressbarLogin = findViewById(R.id.progressbarLogin);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            newToken = instanceIdResult.getToken();
            Log.e("newToken", newToken);
            this.getPreferences(Context.MODE_PRIVATE).edit().putString("fb", newToken).apply();
        });

        Log.d("newToken", this.getPreferences(Context.MODE_PRIVATE).getString("fb", "empty :("));
        try{
            startService(new Intent(LoginActivity.this, MyFirebaseMessagingService.class));

        }catch (Exception e){
            e.printStackTrace();
        }


        db = new DatabaseHandler(this);
        checkLists = new ArrayList<>();
        componentList = new ArrayList<>();
        userDetailsList = new ArrayList<>();
        auxEngDetailsList = new ArrayList<>();
        auxEngLabelList = new ArrayList<>();
        teamSelectionLists = new ArrayList<>();
        checklistRecordDataList = new ArrayList<>();
        masterWorkIdList = new ArrayList<>();
        userPointAccessDataList = new ArrayList<>();
        enginDescriptionData = new ArrayList<>();

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        checkListDatabase = Room.databaseBuilder(getApplicationContext(),
                CheckListDatabase.class, ApiUrlClass.roomDatabaseName).build();

        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        loginButton = findViewById(R.id.loginButton);
        registerButton = findViewById(R.id.registerbutton);
        forgetButton = findViewById(R.id.forgotpass);
        loginll = (LinearLayout) findViewById(R.id.loginll);
        CheckBox checkbox = findViewById(R.id.checkbox);
        ImageView imageView = findViewById(R.id.imageView);
        btnOTP = findViewById(R.id.btnOTP);

        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        final Spannable span = new SpannableString("New User? Sign Up");
        span.setSpan(bss, 9, 17, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        span.setSpan(new ForegroundColorSpan(Color.WHITE), 9, 17, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        registerButton.setText(span);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent browserIntent = new Intent(LoginActivity.this,IdCardActivity.class);
//                startActivity(browserIntent);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ethicalworking.com/"));
                startActivity(browserIntent);
            }
        });

        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                if (value) {
                    passwordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }

            }
        });
//        loadIMEI();

        //check the user logged in or not.
        pref = getSharedPreferences("baseData", MODE_PRIVATE);
        if (pref.getBoolean("logged", false)) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                if (!fingerprintManager.isHardwareDetected()) {
                    String user = pref.getString("emailKey", null);
//                    loadIMEI();
                    Intent i = new Intent(LoginActivity.this, ChecklistHomeActivity.class);
                    i.putExtra("username", user);
                    i.putExtra("uid", uid);
                    startActivity(i);
                    LoginActivity.this.finish();
                } else {
                    String user = pref.getString("emailKey", null);
//                    loadIMEI();
                    Intent i = new Intent(LoginActivity.this, FingerprintActivity.class);
                    i.putExtra("username", user);
                    i.putExtra("uid", uid);
                    startActivity(i);
                    LoginActivity.this.finish();
                }
            } else {
                String user = pref.getString("emailKey", null);
//                loadIMEI();
                Intent i = new Intent(LoginActivity.this, ChecklistHomeActivity.class);
                i.putExtra("username", user);
                i.putExtra("uid", uid);
                startActivity(i);
                LoginActivity.this.finish();
            }

        }

        checkAndRequestPermissions();
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checkLogin();

                progressbarLogin.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                dismissKeyboard(LoginActivity.this);

                if (emailEditText.getText().toString().isEmpty() || passwordEditText.getText().toString().isEmpty()) {
                    if (emailEditText.getText().toString().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Enter email address", Toast.LENGTH_SHORT).show();
                    } else if (passwordEditText.getText().toString().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Enter password address", Toast.LENGTH_SHORT).show();
                    }
                } else if (!emailEditText.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
                } else {
                    username = emailEditText.getText().toString();
                }
                password = passwordEditText.getText().toString();
                 getLoginData();

            }
        });



        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, UserRegisterActivity.class);
                startActivity(intent);
            }
        });


        forgetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgetPassword.class);
                startActivity(intent);
            }
        });

    }

    private void deleteAndAddData() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.clearAllTables();
                checkListDatabase.productDao().setUserDataToTable(userDetailsList);
            }
        });
        threadDeleteAllTables.start();
    }
    public void getLoginData(){
        Map<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        params.put("device_id", newToken);

        new SharedPrefancesClearData().ClearDescriptionData(LoginActivity.this);

        VolleyMethods.makePostJsonObjectRequest(params, LoginActivity.this, new ApiUrlClass().login_api, new PostJsonObjectRequestCallback() {
            @Override
            public void onSuccessResponse(JSONObject response) {
                if (response != null) {
                    Log.e("response:", String.valueOf(response));
                    Integer errorCredential = 0;
                    Integer user_id = 0;
                    try {
                        errorCredential = response.getInt("response");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (errorCredential!=null && errorCredential.equals(10001)) {
                        progressbarLogin.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        Snackbar snackbar = Snackbar.make(loginll, "Invalid UserName or Password !", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.white));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.RED);

                        snackbar.show();

                    } else if (errorCredential!=null && errorCredential.equals(10003)) {
                        progressbarLogin.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Snackbar snackbar = Snackbar.make(loginll, "Something went wrong!", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.white));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.RED);
                        snackbar.show();

                    } else if (errorCredential!=null && errorCredential.equals(10000)) {
                        progressbarLogin.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Snackbar snackbar = Snackbar.make(loginll, "Currently deactivated by manager!", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.white));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.RED);
                        snackbar.show();

                    } else if (errorCredential!=null && errorCredential.equals(10009)) {
                        progressbarLogin.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Intent intent  = new Intent(LoginActivity.this,PendingActivity.class);
                        startActivity(intent);
//                        Snackbar snackbar = Snackbar.make(loginll, "Please contact your admin!", Snackbar.LENGTH_LONG);
//                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.white));
//                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
//                        textView.setTextColor(Color.RED);
//                        snackbar.show();

                    }else if (errorCredential!=null && errorCredential.equals(502)) {
                        progressbarLogin.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Snackbar snackbar = Snackbar.make(loginll, "User doesn't exist!", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.white));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.RED);
                        snackbar.show();

                    }else if (errorCredential!=null && errorCredential.equals(10015)) {
                        progressbarLogin.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        try {
                            user_id = response.getInt("user_id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent intent=new Intent(LoginActivity.this,CommunityCreateOrJoin.class);
                        intent.putExtra("user_id",user_id);
                        startActivity(intent);

                    } else {

                        try {
                            JSONObject jsonObject = response.getJSONObject("user_details");

                            String srvrDtTm = jsonObject.getString("user_login_time");
                            Date ServerDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(srvrDtTm);
                            String LocalTime = new UtilClassFuntions().currentDateTime();
                            Date LocalDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(LocalTime);
                            String localDtTm = getLocalToUTCDate(LocalDateTime);
                            LocalDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(localDtTm);

                            long serverDiffDtTmInMilisecond = ServerDateTime.getTime() - LocalDateTime.getTime();
                            Log.e("servertime", String.valueOf(ServerDateTime));
                            Log.e("Localtime", String.valueOf(LocalDateTime));
                            Log.e("Localtimedff", String.valueOf(serverDiffDtTmInMilisecond));


//                                    if (ServerDateTime.getDate()== LocalDateTime.getDate()) {
//
//                                        if (serverDiffDtTmInMilisecond >= -300000 && serverDiffDtTmInMilisecond <= 300000) {

                            ObjectMapper objectMapper = new ObjectMapper();

                            UserDetails userDetails = objectMapper.readValue(jsonObject.toString(), UserDetails.class);
                            userDetailsList.add(userDetails);

                            deleteAndAddData();

                            progressbarLogin.setVisibility(View.GONE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                                if (!fingerprintManager.isHardwareDetected()) {
//                                        Intent intent=FingerPrint.openFrontCamera();
//                                        startActivityForResult(intent,100);
                                    Intent intent = new Intent(LoginActivity.this, ImagePickActivity.class);
                                    intent.putExtra("username", username);
                                    intent.putExtra("uid", uid);
                                    startActivity(intent);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    Intent intent = new Intent(LoginActivity.this, FingerprintActivity.class);
                                    intent.putExtra("username", username);
                                    intent.putExtra("uid", uid);
                                    startActivity(intent);
                                    LoginActivity.this.finish();
                                }
                            } else {

//                                    Intent intent=FingerPrint.openFrontCamera();
//                                    startActivityForResult(intent,100);
                                Intent intent = new Intent(LoginActivity.this, ImagePickActivity.class);
                                intent.putExtra("username", username);
                                intent.putExtra("uid", uid);
                                startActivity(intent);
                                startActivity(intent);
                                finish();
                            }

//                                        }
//                                        else {
//                                            Toast.makeText(LoginActivity.this, "Time doesn't matched!", Toast.LENGTH_SHORT).show();
//                                        }
//                                    }
//                                  else {
//                                        Toast.makeText(LoginActivity.this, "Date doesn't matched!", Toast.LENGTH_SHORT).show();
//                                    }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                }

            }

            @Override
            public void onVolleyError(VolleyError error) {
                progressbarLogin.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onTokenExpire() {
                progressbarLogin.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            }
        });
    }


    public void checkLogin() {
        if (emailEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "enter email address", Toast.LENGTH_SHORT).show();
        } else {
            if (emailEditText.getText().toString().trim().matches(emailPattern)) {
                Toast.makeText(getApplicationContext(), "valid email address", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
            }
        }


    }

    public void loadIMEI() {
        try {
            // Check if the READ_PHONE_STATE permission is already available.
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                uid = telephonyManager.getDeviceId();

            } else {
                // READ_PHONE_STATE permission is already been granted.
                telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                uid = telephonyManager.getDeviceId();

            }
        } catch (Exception e) {
        }
    }


    private void checkAndRequestPermissions() {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.INTERNET)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
//                            Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        }
                        if (!report.areAllPermissionsGranted()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();

                        }
                        // check for permanent denial of any permission

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
                finish();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Do you want to exit?");
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                LoginActivity.super.onBackPressed();
            }
        });
        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    public void dismissKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != activity.getCurrentFocus())
            imm.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
    }


    public String getLocalToUTCDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date time = calendar.getTime();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        return outputFmt.format(time);
    }

}
