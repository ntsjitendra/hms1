package com.nuevotechsolutions.ewarp.activities;


import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.CheckListAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.GetJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.nuevotechsolutions.ewarp.utils.SyncData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewTaskActivity extends AppCompatActivity {

    RecyclerView recyclerViewCheckList;
    List<CheckList> checkLists;
    List<CheckList> checkLists1;
    List<AuxEngDetails> auxEngDetails;
    CheckListDatabase checkListDatabase;
    List<TeamSelectionList> teamSelectionLists;
    CheckListAdapter checkListAdapter;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    RecyclerView.LayoutManager layoutManager;
    private SwipeRefreshLayout newtaskswipeRefresh;
    CheckListDatabase db;
    TextView tvnolist;
    ProgressBar progressbarNewTask;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_check_list);
        checkLists = new ArrayList<>();
        checkLists1 = new ArrayList<>();
        auxEngDetails = new ArrayList<>();
        teamSelectionLists = new ArrayList<>();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
//        View hView =  navigationView.getHeaderView(0);
//        TextView nav_user = (TextView)hView.findViewById(R.id.userNameLabel);
        db= Room.databaseBuilder(this, CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        List<UserDetails> userDetails=new ArrayList<>();
        userDetails=db.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name()!=null){
            String user=userDetails.get(0).getFirst_name();
//            nav_user.setText(user);
        }else{
//            nav_user.setText("Guest User");
        }


        setUpToolbar();
        progressbarNewTask=findViewById(R.id.progressbarNewTask);
        newtaskswipeRefresh = (SwipeRefreshLayout)findViewById(R.id.newtaskswipeRefresh);
        tvnolist = findViewById(R.id.tvnolist);
        if (new NetworkDetector().isNetworkReachable(NewTaskActivity.this) == true) {

            newtaskswipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
            newtaskswipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (new NetworkDetector().isNetworkReachable(NewTaskActivity.this) == true) {
                        boolean b = new SyncData().storeAllData(NewTaskActivity.this);

                        newtaskswipeRefresh.setRefreshing(false);
                        if (b == true) {
                            getCheckListFromDatabase();
                            Log.e("nnnn", String.valueOf(checkLists.size()));
                            checkListAdapter.notifyDataSetChanged();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    getCheckListFromDatabase();
                                    checkListAdapter.notifyDataSetChanged();
                                }
                            }, 3000);

                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.newTaskChecklist), "No internet connection!", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);
                        snackbar.show();
                    }
                }
            });
        }else{
            newtaskswipeRefresh.setEnabled(false);

        }
        checkListDatabase = Room.databaseBuilder(this, CheckListDatabase.class, ApiUrlClass.roomDatabaseName).build();
        bindView();
        getCheckListFromDatabase();
        getAuxEngDetailsFromDataBAse();
        getTeamDataFromDatabase();

    }

    private void setUpToolbar() {

//        drawerLayout = findViewById(R.id.drawerLayout1);
        toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        ImageButton imageView = findViewById(R.id.backarrowNewTask);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(NewTaskActivity.this);
                finish();
            }
        });
//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
    }


    private void bindView() {
        recyclerViewCheckList = findViewById(R.id.checkList_recyclerView);
    }

    private void setAdapterToRecyclerView() {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                checkListAdapter = new CheckListAdapter(NewTaskActivity.this, checkLists1);
                LinearLayoutManager llm = new LinearLayoutManager(NewTaskActivity.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerViewCheckList.setLayoutManager(llm);
                recyclerViewCheckList.setAdapter(checkListAdapter);
                if(checkLists1.size()==0){
                    tvnolist.setVisibility(View.VISIBLE);

                }

            }
        });


    }

    private void getCheckListFromDatabase() {
        Thread threadCheckList = new Thread(new Runnable() {
            @Override
            public void run() {
                if (checkLists != null) {
                    checkLists = checkListDatabase.productDao().getAllCheckList();
                    Log.e("Checklist...", String.valueOf(checkLists.size()));
                    checkLists1.clear();
                    for(int i=0;i<checkLists.size();i++){
                       if (checkLists.get(i).getStatus().equals(1)){
                           checkLists1.add(checkLists.get(i));
                       }
                   }
                    setAdapterToRecyclerView();
                }
            }
        });

        threadCheckList.start();

    }

    private void getAuxEngDetailsFromDataBAse() {
        Thread threadCheckList = new Thread(new Runnable() {
            @Override
            public void run() {
                if (auxEngDetails != null) {
                    auxEngDetails = checkListDatabase.productDao().getAllAuxEngDtls();
                    setAdapterToRecyclerView();
                }
            }
        });

        threadCheckList.start();

    }

    private void getTeamDataFromDatabase() {

        Thread threadUserPointAccess = new Thread(new Runnable() {
            @Override
            public void run() {
                if (teamSelectionLists != null) {
                    teamSelectionLists = checkListDatabase.productDao().getTeamSelectionData();
                    setAdapterToRecyclerView();
                }
            }
        });

        threadUserPointAccess.start();

    }


}
