package com.nuevotechsolutions.ewarp.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.Rational;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraX;
import androidx.camera.core.CaptureRequestParameter;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureConfig;
import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.CustomCameraAdapter;
import com.nuevotechsolutions.ewarp.utils.LocationHelper;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomCameraActivity extends AppCompatActivity implements CustomCameraAdapter.AdapterCallback {
    private int REQUEST_CODE_PERMISSIONS = 101;
    private final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};
    TextureView textureView;
    RecyclerView cameraReclerView;
    private JSONObject imageJson = new JSONObject();
    private Map<Integer, List<String>> imgMap = new HashMap<>();
    private Map<Integer, List<String>> timeStaMap = new HashMap<>();
    private Map<Integer, List<String>> locationMap = new HashMap<>();
    private Map<Integer, List<String>> photoTextValueMap = new HashMap<>();
    private List<String> listPhoto = new ArrayList<>();
    private List<String> timeStampList = new ArrayList<>();
    private List<String> locationList = new ArrayList<>();
    private List<String> photoTextValueList = new ArrayList<>();
    private CustomCameraAdapter customCameraAdapter;
    private ImageView imgClose, imgDone;
    private double latitude, longitude;
    private String address, currentTime;
    private String currentlocation;
    ImageButton imgCapture;
    LocationHelper.LocationResult locationResult;
    private LocationHelper locationHelper;
    private Geocoder geocoder;
    int position;
    String locationValue = "";
    LocationManager locationManager;
    private ImageView imgLargeView, imgcross;
    private RelativeLayout imgrl;
    File file;
    String imgFile = "NO_IMG_TAKEN";
    private Bitmap rotatedBitmap;
    String encodedImage;
    public final String APP_TAG = "EWARP";
    int clickCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_camera_activity);
        locationManager = (LocationManager) getSystemService(CustomCameraActivity.this.LOCATION_SERVICE);
        Intent intent = getIntent();
        position = intent.getIntExtra("position", 0);
        locationValue = intent.getStringExtra("locationValue");
        Log.e("po", "test" + position);

        String imageJsonString = intent.getStringExtra("imageJson");
        try {
            if (imageJsonString != null) {
                imageJson = new JSONObject(imageJsonString);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        imgMap = (HashMap<Integer, List<String>>) intent.getSerializableExtra("imageMap");
        locationMap = (HashMap<Integer, List<String>>) intent.getSerializableExtra("locationMap");
        timeStaMap = (HashMap<Integer, List<String>>) intent.getSerializableExtra("timeStampMap");
        photoTextValueMap = (HashMap<Integer, List<String>>) intent.getSerializableExtra("photoTextValueMap");

//        listPhoto = (List<String>) intent.getSerializableExtra("photolist");
//        locationList = (List<String>) intent.getSerializableExtra("locationList");
//        timeStampList = (List<String>) intent.getSerializableExtra("timeStampList");
//        photoTextValueList = (List<String>) intent.getSerializableExtra("photoTextValueList");
        listPhoto.addAll(imgMap.get(position));
        locationList.addAll(locationMap.get(position));
        timeStampList.addAll(timeStaMap.get(position));
        if (photoTextValueMap.get(position) != null) {
            photoTextValueList.addAll(photoTextValueMap.get(position));
        }
        textureView = findViewById(R.id.view_finder);
        imgClose = findViewById(R.id.imgClose);
        imgDone = findViewById(R.id.imgDonne);
        cameraReclerView = findViewById(R.id.cameraRecyclerview);
        imgLargeView = findViewById(R.id.imgLargeView);
        imgrl = findViewById(R.id.imgrl);
        imgcross = findViewById(R.id.imgcross);
        imgCapture = findViewById(R.id.imgCapture);

        clickCount = listPhoto.size();

        if (allPermissionsGranted()) {
            startCamera(); //start camera if permission has been granted by user
        } else {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }

        imgDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent();
                intent1.putExtra("post", position);
                intent1.putExtra("imageJson", String.valueOf(imageJson));
                intent1.putExtra("imageMap", (Serializable) imgMap);
                intent1.putExtra("locationMap", (Serializable) locationMap);
                intent1.putExtra("timeStampMap", (Serializable) timeStaMap);
                intent1.putExtra("photoTextValueMap", (Serializable) photoTextValueMap);
//                intent1.putExtra("photolist", (Serializable) listPhoto);
                intent1.putExtra("locationList", (Serializable) locationList);
                intent1.putExtra("timeStampList", (Serializable) timeStampList);
                intent1.putExtra("photoTextValueList", (Serializable) photoTextValueList);

                Log.e("imgObj:", String.valueOf(imageJson));
                Log.e("map:", String.valueOf(imgMap));
                Log.e("map1:", String.valueOf(locationMap));
                Log.e("map2:", String.valueOf(timeStaMap));
                Log.e("map3:", String.valueOf(photoTextValueMap));
                setResult(Activity.RESULT_OK, intent1);
                finish();


            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                listPhoto.clear();
                finish();
            }
        });

        LocationAccess();
        locationHelper.getLocation(CustomCameraActivity.this, CustomCameraActivity.this.locationResult);

        imgcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgrl.setVisibility(View.GONE);
                textureView.setVisibility(View.VISIBLE);
            }
        });


    }


    private void startCamera() {

        CameraX.unbindAll();
        Rational aspectRatio = null;
        Size screen = new Size(640, 640);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            aspectRatio = new Rational(textureView.getWidth(), textureView.getHeight());
            screen = new Size(textureView.getWidth(), textureView.getHeight());

        }

        PreviewConfig pConfig = new PreviewConfig.Builder()
                .setTargetAspectRatio(aspectRatio)
                .setTargetResolution(screen)
//                .setLensFacing(lensFacing)
                .build();

        Preview preview = new Preview(pConfig);
        preview.enableTorch(true);


        preview.setOnPreviewOutputUpdateListener(
                new Preview.OnPreviewOutputUpdateListener() {
                    @Override
                    public void onUpdated(Preview.PreviewOutput output) {
                        ViewGroup parent = (ViewGroup) textureView.getParent();
                        parent.removeView(textureView);
                        parent.addView(textureView, 0);
                        textureView.setSurfaceTexture(output.getSurfaceTexture());
                        updateTransform();
                    }
                });
        ImageCaptureConfig imageCaptureConfig = new ImageCaptureConfig.Builder().setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
                .setTargetRotation(getWindowManager().getDefaultDisplay().getRotation()).build();
        final ImageCapture imgCap = new ImageCapture(imageCaptureConfig);
        imgCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickCount < 5) {
                    clickCount++;
                    imgCapture.setClickable(false);

                    imgFile = "IMG_" + System.currentTimeMillis() + ".png";
                    file = getPhotoFileUri(imgFile);
                    Log.d("campath", String.valueOf(file));
//file = new File(Environment.getExternalStorageDirectory()+"/" + System.currentTimeMillis() + ".png");
                    imgCap.takePicture(file, new ImageCapture.OnImageSavedListener() {
                        @Override
                        public void onImageSaved(@NonNull File file) {
                            decodeFile(file);
                            currentTime = new UtilClassFuntions().currentDateTime();
                            listPhoto.add(file.getAbsolutePath());
                            JSONObject subJson = new JSONObject();
                            try {
                                subJson.put("location", currentlocation);
                                subJson.put("timeStamp", currentTime);
                                subJson.put("photoTextValue", "");
                                subJson.put("longitude", longitude);
                                subJson.put("latitude", latitude);
                                imageJson.put(file.getName(), subJson);
                                locationList.add(currentlocation);
                                Log.e("locationlist", "test" + locationList.get(0));
                                Log.e("currentlocation", "test" + currentlocation);
                                timeStampList.add(currentTime);
                                if (photoTextValueList == null) {
                                    photoTextValueList = new ArrayList<>();
                                }
                                photoTextValueList.add("");
                                imgMap.put(position, listPhoto);
                                locationMap.put(position, locationList);
                                timeStaMap.put(position, timeStampList);
                                photoTextValueMap.put(position, photoTextValueList);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            cameraReclerView.setLayoutManager(new LinearLayoutManager(CustomCameraActivity.this, LinearLayoutManager.HORIZONTAL, false));
                            customCameraAdapter = new CustomCameraAdapter(CustomCameraActivity.this, listPhoto);
                            cameraReclerView.setAdapter(customCameraAdapter);
                            customCameraAdapter.notifyDataSetChanged();
                            imgCapture.setClickable(true);

                        }

                        @Override
                        public void onError(@NonNull ImageCapture.UseCaseError useCaseError, @NonNull String message, @Nullable Throwable cause) {
                            String msg = "Pic capture failed : " + message;
                            Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
                            if (cause != null) {
                                cause.printStackTrace();
                            }
                        }
                    });
                } else {
                    Toast.makeText(CustomCameraActivity.this, "5 Photos are allowed per check point. Thanks.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //bind to lifecycle:
        CameraX.bindToLifecycle((LifecycleOwner) this, preview, imgCap);
    }

    public File getPhotoFileUri(String fileName) {

        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);

        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d(APP_TAG, "failed to create directory");
        }

        // Return the file target for the photo based on filename
//        file = new File(mediaStorageDir.getPath() + File.separator + fileName +timeStamp + ".jpg");
        file = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return file;
    }

    private void updateTransform() {
        Matrix mx = new Matrix();
        float w = textureView.getMeasuredWidth();
        float h = textureView.getMeasuredHeight();

        float cX = w / 2f;
        float cY = h / 2f;

        int rotationDgr;
        int rotation = (int) textureView.getRotation();

        switch (rotation) {
            case Surface.ROTATION_0:
                rotationDgr = 0;
                break;
            case Surface.ROTATION_90:
                rotationDgr = 90;
                break;
            case Surface.ROTATION_180:
                rotationDgr = 180;
                break;
            case Surface.ROTATION_270:
                rotationDgr = 270;
                break;
            default:
                return;
        }

        mx.postRotate((float) rotationDgr, cX, cY);
        textureView.setTransform(mx);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera();
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private boolean allPermissionsGranted() {

        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    //* @Author: Subhra Priyadarshini;
    //* @Used For : This function is used for fetching current location.
    public void LocationAccess() {
        this.locationResult = new LocationHelper.LocationResult() {
            @Override
            public void gotLocation(Location location) {

                //Got the location!
                if (location != null) {

                    try {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.e("ab", "lat: " + latitude + ", long: " + longitude);
                        geocoder = new Geocoder(CustomCameraActivity.this);
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        if (addresses != null && addresses.size() > 0) {
                            address = addresses.get(0).getAddressLine(0);
                            if (locationValue.equalsIgnoreCase("true")) {
                                currentlocation = address+ "_"+latitude+"_"+longitude;
                            } else {
                                currentlocation = "Not available";
                            }

                            Log.e("de..", currentlocation);

                        }
                    } catch (IOException e) {
//                        Toast.makeText(IsolationActivity.this, "Unable to access the current location !", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e("de", "Location is null.");
                }

            }

        };

        this.locationHelper = new LocationHelper();

    }


    @Override
    public void onMethodCallback() {
        Glide.with(this)
                .load(file)
                .centerCrop().fitCenter()
                .into(imgLargeView);
        imgrl.setVisibility(View.VISIBLE);
        textureView.setVisibility(View.GONE);

    }

    private Bitmap decodeFile(File f) {
        Bitmap b = null;
        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int IMAGE_MAX_SIZE = 1080;
        int scale = 2;
        if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
            scale = (int) Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE /
                    (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        try {
            Matrix m = new Matrix();
            m.postRotate(90);
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("TAG", "Width :" + b.getWidth() + " Height :" + b.getHeight());
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);


        try {
            FileOutputStream out = new FileOutputStream(file);
            b.compress(Bitmap.CompressFormat.JPEG, 80, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

}
