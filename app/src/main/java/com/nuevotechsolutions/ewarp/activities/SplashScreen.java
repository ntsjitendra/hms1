package com.nuevotechsolutions.ewarp.activities;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.room.Room;

import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.SyncData;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class SplashScreen extends AppCompatActivity {
    private static int SPLASH_TIME_OUT=3000;
    TextView textView;
    ImageView imageview;
    CheckListDatabase db;
    List<UserDetails> userDetails;
    FingerprintManager fingerprintManager;
    KeyguardManager keyguardManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        if (new NetworkDetector().isNetworkReachable(SplashScreen.this) == true) {
//            boolean b = new SyncData().storeAllData(SplashScreen.this);
        }
        
        userDetails=new ArrayList<>();
        if (!isTaskRoot()
                && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_MAIN)) {

            finish();
            return;
        }
        Fabric.with(this, new Crashlytics());
        textView=findViewById(R.id.textView3);
        imageview = findViewById(R.id.imageview);
        textView.setText("Ethical Working with"+" Accountability,"+"\n"+" Responsibility &"+"Profitability");
        Animation animation;
        animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        textView.startAnimation(animation);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                db = Room.databaseBuilder(SplashScreen.this, CheckListDatabase.class,
                        ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                userDetails = db.productDao().getUserDetail();
                if (userDetails!=null && !userDetails.isEmpty()){

                    SharedPreferences prefs = getSharedPreferences("FingerprintKey", MODE_PRIVATE);
                    int fingerprintFlagKey =prefs.getInt("fingerprintFlagKey",0);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
                        keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

                        if (!fingerprintManager.isHardwareDetected()) {
                            Intent splashScreen=new Intent(SplashScreen.this,ChecklistHomeActivity.class);
                            startActivity(splashScreen);
                        } else {
                            if (fingerprintFlagKey==1){
                                Intent splashScreen=new Intent(SplashScreen.this,ChecklistHomeActivity.class);
                                startActivity(splashScreen);
                            }else{
                                Intent splashScreen=new Intent(SplashScreen.this,FingerprintActivity.class);
                                splashScreen.putExtra("username",userDetails.get(0).getEmail());
                                splashScreen.putExtra("uid","");
                                startActivity(splashScreen);
                            }
                        }
                    }

                    SplashScreen.this.finish();
                }else{
                    Intent splashScreen=new Intent(SplashScreen.this,LoginActivity.class);
                    startActivity(splashScreen);
                    SplashScreen.this.finish();
                }

            }
        },SPLASH_TIME_OUT);
    }
}
