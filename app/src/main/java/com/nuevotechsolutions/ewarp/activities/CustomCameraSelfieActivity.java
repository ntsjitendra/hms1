package com.nuevotechsolutions.ewarp.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Rational;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraX;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureConfig;
import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.bumptech.glide.Glide;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.CustomCameraAdapter;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.LocationHelper;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomCameraSelfieActivity extends AppCompatActivity{
    private int REQUEST_CODE_PERMISSIONS = 101;
    private final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};
    TextureView textureView;
    private ImageView imgCapture, imgDone,cameraclick;
    private double latitude, longitude;
    private String address, currentTime;
    private String currentlocation;
    LocationHelper.LocationResult locationResult;
    private LocationHelper locationHelper;
    private Geocoder geocoder;
    int position;
    String locationValue = "";
    LocationManager locationManager;
    private ImageView imgLargeView, imgcross;
    File file;
    String imgFile = "NO_IMG_TAKEN";
    private Bitmap rotatedBitmap;
    String encodedImage;
    public final String APP_TAG = "EWARP";
    private CameraX.LensFacing lensFacing = CameraX.LensFacing.FRONT;
    Bitmap myBitmap;
    String photofilePath;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    List<UserDetails> userDetailsList;
    private CheckListDatabase checkListDatabase;
    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cameratwo);
        checkListDatabase = Room.databaseBuilder(getApplicationContext(),
                CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetailsList = checkListDatabase.productDao().getUserDetail();
        locationManager = (LocationManager) getSystemService(CustomCameraSelfieActivity.this.LOCATION_SERVICE);
        Intent intent = getIntent();
        textureView = findViewById(R.id.camera_two);
        cameraclick = findViewById(R.id.imgCaptureTwo);
//        imgClose = findViewById(R.id.imgClose);
        imgDone = findViewById(R.id.txtDone);
//        imgLargeView = findViewById(R.id.imgLargeView);
        imgCapture = findViewById(R.id.imgCapture);
//        imgcross = findViewById(R.id.imgcross);

        if (allPermissionsGranted()) {
            startCamera(); //start camera if permission has been granted by user
        } else {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }

        imgDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(photofilePath!= null) {
                    Intent intent = new Intent(CustomCameraSelfieActivity.this, IsolationActivity.class);
                    intent.putExtra("BitmapImage", photofilePath);
                    Log.e("file", photofilePath);
                    setResult(RESULT_OK, intent);
                    Toast.makeText(CustomCameraSelfieActivity.this, "Please wait while submiting your data!", Toast.LENGTH_LONG).show();
                    finish();
                }else{
                    Toast.makeText(CustomCameraSelfieActivity.this,"Please click a photo",Toast.LENGTH_LONG).show();
                }
//
//                Intent intent = new Intent(CustomCameraSelfieActivity.this,IsolationActivity.class);
//                intent.putExtra("BitmapImage", photofilePath);
//                setResult(RESULT_OK,intent);
//                Toast.makeText(CustomCameraSelfieActivity.this,"Please wait while submitting your data!",Toast.LENGTH_LONG).show();
//                finish();

            }
        });
//        imgClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////
//            }
//        });

        LocationAccess();
        locationHelper.getLocation(CustomCameraSelfieActivity.this, CustomCameraSelfieActivity.this.locationResult);

//        imgcross.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                textureView.setVisibility(View.VISIBLE);
//            }
//        });


    }


    private void startCamera() {

        CameraX.unbindAll();
        Rational aspectRatio = null;
        Size screen = new Size(640, 640);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            aspectRatio = new Rational(textureView.getWidth(), textureView.getHeight());
            screen = new Size(textureView.getWidth(), textureView.getHeight());

        }

        PreviewConfig pConfig = new PreviewConfig.Builder()
                .setTargetAspectRatio(aspectRatio)
                .setTargetResolution(screen)
                .setLensFacing(lensFacing)
                .build();

        Preview preview = new Preview(pConfig);
        preview.enableTorch(true);



        preview.setOnPreviewOutputUpdateListener(
                new Preview.OnPreviewOutputUpdateListener() {
                    @Override
                    public void onUpdated(Preview.PreviewOutput output) {
                        ViewGroup parent = (ViewGroup) textureView.getParent();
                        parent.removeView(textureView);
                        parent.addView(textureView, 0);
                        textureView.setSurfaceTexture(output.getSurfaceTexture());
//                        updateTransform();
                    }
                });
        ImageCaptureConfig imageCaptureConfig = new ImageCaptureConfig.Builder().setLensFacing(CameraX.LensFacing.FRONT).setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
                .setTargetRotation(getWindowManager().getDefaultDisplay().getRotation()).build();
        final ImageCapture imgCap = new ImageCapture(imageCaptureConfig);

        findViewById(R.id.imgCaptureTwo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgFile = "IMG_" + System.currentTimeMillis() + ".png";

                file = getPhotoFileUri(imgFile);
//file = new File(Environment.getExternalStorageDirectory()+"/" + System.currentTimeMillis() + ".png");
                imgCap.takePicture(file, new ImageCapture.OnImageSavedListener() {
                    @Override
                    public void onImageSaved(@NonNull File file) {
                        myBitmap = decodeFile(file);
                        ExifInterface ei = null;
                        try {
                            ei = new ExifInterface(file.getAbsolutePath());

                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                        myBitmap = getRotatedBitmap(myBitmap,orientation,1);
                        currentTime = new UtilClassFuntions().currentDateTime();
                        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                        float scaledTextSize = myBitmap.getWidth() * 60 / displayMetrics.widthPixels;
                        Canvas cs = new Canvas(myBitmap);
                        Paint paint = new Paint();
                        paint.setTextSize(scaledTextSize);
                        paint.setFakeBoldText(true);
                        paint.setColor(Color.RED);
                        paint.setStyle(Paint.Style.FILL);
                        cs.drawBitmap(myBitmap, 0f, 0f, null);
                        float height = paint.measureText("yY");
                        float width = paint.measureText("Done by: " + userDetailsList.get(0).getFirst_name());
                        float startXPosition = (myBitmap.getWidth() - width) / 9;
                        cs.drawText("Done by: " + userDetailsList.get(0).getFirst_name(), startXPosition, myBitmap.getHeight() / 4 - height + 15f, paint);
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(file.getAbsolutePath());
                            myBitmap.compress(Bitmap.CompressFormat.JPEG,75,fos);
                            fos.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        } } catch (IOException e) {
                            e.printStackTrace();
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // TODO Auto-generated method stub

                                imgCapture.setImageBitmap(myBitmap);
                            }
                        });

                        photofilePath= file.getAbsolutePath();
                    }




                    @Override
                    public void onError(@NonNull ImageCapture.UseCaseError useCaseError, @NonNull String message, @Nullable Throwable cause) {
                        String msg = "Pic capture failed : " + message;
                        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
                        if (cause != null) {
                            cause.printStackTrace();
                        }
                    }
                });
            }
        });

        //bind to lifecycle:
        CameraX.bindToLifecycle((LifecycleOwner) this, preview, imgCap);
//        this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

    }

    public File getPhotoFileUri(String fileName) {

        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);

        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d(APP_TAG, "failed to create directory");
        }

        // Return the file target for the photo based on filename
//        file = new File(mediaStorageDir.getPath() + File.separator + fileName +timeStamp + ".jpg");
        file = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return file;
    }

    private void updateTransform() {
        Matrix mx = new Matrix();
        float w = textureView.getMeasuredWidth();
        float h = textureView.getMeasuredHeight();

        float cX = w / 2f;
        float cY = h / 2f;

        int rotationDgr;
        int rotation = (int) textureView.getRotation();

        switch (rotation) {
            case Surface.ROTATION_0:
                rotationDgr = 0;
                break;
            case Surface.ROTATION_90:
                rotationDgr = 90;
                break;
            case Surface.ROTATION_180:
                rotationDgr = 180;
                break;
            case Surface.ROTATION_270:
                rotationDgr = 270;
                break;
            default:
                return;
        }

        mx.postRotate((float) rotationDgr, cX, cY);
        textureView.setTransform(mx);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera();
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private boolean allPermissionsGranted() {

        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    //* @Author: Subhra Priyadarshini;
    //* @Used For : This function is used for fetching current location.
    public void LocationAccess() {
        this.locationResult = new LocationHelper.LocationResult() {
            @Override
            public void gotLocation(Location location) {

                //Got the location!
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.e("ab", "lat: " + latitude + ", long: " + longitude);
                    geocoder = new Geocoder(CustomCameraSelfieActivity.this);
                    try {
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        if (addresses != null && addresses.size() > 0) {
                            address = addresses.get(0).getAddressLine(0);
                            if (locationValue.equalsIgnoreCase("true")) {
                                currentlocation = address;
                            } else {
                                currentlocation = "Not available";
                            }

                            Log.e("de..", currentlocation);

                        }
                    } catch (IOException e) {
//                        Toast.makeText(IsolationActivity.this, "Unable to access the current location !", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e("de", "Location is null.");
                }

            }

        };

        this.locationHelper = new LocationHelper();

    }
    private static Bitmap getRotatedBitmap(Bitmap bitmap, int orientation, int cameraid) {


        switch (orientation) {
            case ExifInterface.ORIENTATION_UNDEFINED:
                bitmap = rotateImage(bitmap, 180);
                Log.e("angle","ok1");
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                bitmap = rotateImage(bitmap, 90);
                Log.e("angle","ok2");
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                bitmap = rotateImage(bitmap, 180);
                Log.e("angle","ok3");
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                bitmap = rotateImage(bitmap, 270);
                Log.e("angle","ok4");
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                bitmap = rotateImage(bitmap, 270);
                Log.e("angle","default");
                break;
        }
        return bitmap;
    }


//    @Override
//    public void onMethodCallback() {
//        Glide.with(this)
//                .load(file)
//                .centerCrop().fitCenter()
//                .into(imgLargeView);
//        textureView.setVisibility(View.GONE);
//
//    }

    private Bitmap decodeFile(File f) {
        Bitmap b = null;
        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int IMAGE_MAX_SIZE = 1080;
        int scale = 2;
        if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
            scale = (int) Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE /
                    (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        try {
            Matrix m = new Matrix();
            m.postRotate(90);
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("TAG", "Width :" + b.getWidth() + " Height :" + b.getHeight());
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);


        try {
            FileOutputStream out = new FileOutputStream(file);
            b.compress(Bitmap.CompressFormat.JPEG, 80, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

    @Override
    public void onBackPressed() {

    }
}
