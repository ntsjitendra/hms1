package com.nuevotechsolutions.ewarp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.room.Room;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.LogoutClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ChangePassword extends AppCompatActivity {

    EditText confirmEditText, newEditText, oldEditText;
    TextView labelChangePass;
    Button changePassButton;
    LinearLayout changePassLinearLayout;
    private String email,accessToken,dbnm;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    SharedPreferences pref;
    CheckListDatabase db;
    List<UserDetails> userDetails = new ArrayList<>();
    RequestQueue mQueue;
    ProgressBar progressbaChangepass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        progressbaChangepass= findViewById(R.id.progressbaChangepass);

        db = Room.databaseBuilder(this, CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = db.productDao().getUserDetail();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
//        View hView =  navigationView.getHeaderView(0);
//        TextView nav_user = (TextView)hView.findViewById(R.id.userNameLabel);

        if (userDetails.get(0).getFirst_name() != null) {
            String user = userDetails.get(0).getFirst_name();
//            nav_user.setText(user);
        } else {
//            nav_user.setText("Guest User");
        }
        email = userDetails.get(0).getUser_id();
        accessToken = userDetails.get(0).getAccessToken();
        dbnm = userDetails.get(0).getDbnm();

        pref = getSharedPreferences("baseData", MODE_PRIVATE);
        String data = pref.getString("emailKey", null);
        if (data != null) {
            //email = pref.getString("emailKey", null);

        }

        oldEditText = findViewById(R.id.passwordChangePass);
        newEditText = findViewById(R.id.passwordChangePass1);
        confirmEditText = findViewById(R.id.confirmChangePass);
        changePassButton = findViewById(R.id.changePassButton);
        labelChangePass = findViewById(R.id.labelChangePass);
        changePassLinearLayout = findViewById(R.id.changePassLinearLayout);

        mQueue = Volley.newRequestQueue(this);

        setUpToolbar();

        changePassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissKeyboard(ChangePassword.this);
                if (new NetworkDetector().isNetworkReachable(ChangePassword.this)==true) {
                    changePass();
                }else {
                    Snackbar snackbar = Snackbar.make(changePassLinearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.white));
                    TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                    textView.setTextColor(Color.RED);
                    snackbar.show();
                }
            }
        });

    }

    public void changePass() {

        String oldPassword = oldEditText.getText().toString();
        String newPassword = newEditText.getText().toString();
        String confirmPassword = confirmEditText.getText().toString();
        if (oldPassword.equals("") || newPassword.equals("") || confirmPassword.equals("")) {
            if (oldPassword.equals("")) {
                oldEditText.setError("Enter Old Password!");
            } else if (newPassword.equals("")) {
                newEditText.setError("Enter new Password!");
            } else if (confirmPassword.equals("")) {
                confirmEditText.setError("Enter Confirm Password!");
            } else if (oldPassword.equals("") && newPassword.equals("") && confirmPassword.equals("")) {
                oldEditText.setError("Enter Old Password!");
                newEditText.setError("Enter new Password!");
                confirmEditText.setError("Enter Confirm Password!");
            } else if (oldPassword.equals("") && newPassword.equals("")) {
                oldEditText.setError("Enter Old Password!");
                newEditText.setError("Enter new Password!");
            } else if (newPassword.equals("") && confirmPassword.equals("")) {
                newEditText.setError("Enter new Password!");
                confirmEditText.setError("Enter Confirm Password!");
            } else if (oldPassword.equals("") && confirmPassword.equals("")) {
                oldEditText.setError("Enter Old Password!");
                confirmEditText.setError("Enter Confirm Password!");
            }
        }else if (newPassword.length()<6) {
            newEditText.setError("Password must contain min 6 character.");
        } else {
            if (newPassword.equals(confirmPassword)) {
                progressbaChangepass.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                //********************************

                Map<String, String> params = new HashMap<>();
                params.put("user_id", email);
                params.put("password", oldPassword);
                params.put("new_password", newPassword);
                params.put("accessToken", accessToken);
                params.put("dbnm", dbnm);

                Log.e("params", String.valueOf(params));


                VolleyMethods.makePostJsonObjectRequest(params, ChangePassword.this, ApiUrlClass.changepass_url, new PostJsonObjectRequestCallback() {
                    @Override
                    public void onSuccessResponse(JSONObject response) {
                        if (response != null) {
                            try {
                                Integer res = response.getInt("response");
                                if (res.equals(200)){
                                    progressbaChangepass.setVisibility(View.GONE);
                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);                                    clearField();
                                    Snackbar snackbar = Snackbar.make(changePassLinearLayout, "Password updated successfully!", Snackbar.LENGTH_LONG);
                                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.white));
                                    TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                                    textView.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.dark_green));
                                    snackbar.show();
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent intent = new Intent(ChangePassword.this,ChecklistHomeActivity.class);
                                            startActivity(intent);
                                        }
                                    },2000);

                                }else if (res.equals(500)){
                                    progressbaChangepass.setVisibility(View.GONE);
                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);                                     Snackbar snackbar = Snackbar.make(changePassLinearLayout, "Password doesn't updated!", Snackbar.LENGTH_LONG);
                                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.white));
                                    TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                                    textView.setTextColor(Color.RED);
                                    snackbar.show();
                                }else if (res.equals(10002)){
                                    progressbaChangepass.setVisibility(View.GONE);
                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);                                     Snackbar snackbar = Snackbar.make(changePassLinearLayout, "Something wrong!", Snackbar.LENGTH_LONG);
                                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.white));
                                    TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                                    textView.setTextColor(Color.RED);
                                    snackbar.show();
                                }else if (res.equals(10003)){
                                    progressbaChangepass.setVisibility(View.GONE);
                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);                                     new LogoutClass().logoutMethod(ChangePassword.this);
                                }else if (res.equals(10004)){
                                    progressbaChangepass.setVisibility(View.GONE);
                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);                                     Snackbar snackbar = Snackbar.make(changePassLinearLayout, "Failed, Check your old password!", Snackbar.LENGTH_LONG);
                                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.white));
                                    TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                                    textView.setTextColor(Color.RED);
                                    snackbar.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        progressbaChangepass.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Log.e("error", String.valueOf(error));
                            Toast.makeText(ChangePassword.this, error.toString(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onTokenExpire() {
                        progressbaChangepass.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }
                });


                //************************************

            } else {
                newEditText.setText("");
                confirmEditText.setText("");
                Snackbar snackbar = Snackbar.make(changePassLinearLayout, "Confirm password  mismatch, Please try again!", Snackbar.LENGTH_LONG);
                snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.white));
                TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                textView.setTextColor(Color.RED);
                snackbar.show();
            }
        }


    }

    private void setUpToolbar() {
//        drawerLayout=findViewById(R.id.drawerLayout4);
//        TextView textView_changepass_header = (TextView) toolbar.findViewById(R.id.textView_changepass_header);
//        textView_changepass_header.setText("Change Password");
        ImageView bacakarrowChangepass = findViewById(R.id.bacakarrowChangepass);
        bacakarrowChangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        actionBarDrawerToggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar,app_name,app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
    }

    public void clearField(){
        oldEditText.setText("");
        newEditText.setText("");
        confirmEditText.setText("");
    }

    public void dismissKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != activity.getCurrentFocus())
            imm.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
    }

}
