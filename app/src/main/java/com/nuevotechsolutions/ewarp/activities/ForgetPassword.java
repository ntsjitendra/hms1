package com.nuevotechsolutions.ewarp.activities;


import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.VolleyError;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgetPassword extends AppCompatActivity {
    private RelativeLayout rlSendButton;
    private ImageView imgbackarrowPass;
    private EditText emailForgetEditText;
    private LinearLayout forgetLinearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        emailForgetEditText = findViewById(R.id.emailForgetEditText);
        rlSendButton = findViewById(R.id.rlSendButton);
        imgbackarrowPass = findViewById(R.id.imgbackarrowPass);
        forgetLinearLayout = findViewById(R.id.forgetLinearLayout);

        imgbackarrowPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        rlSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=emailForgetEditText.getText().toString();
                if (email.equals("")){
                    emailForgetEditText.setError("Please enter email!");
                }else {
                    forgetPass(email,emailForgetEditText);
                }

            }
        });
    }

    public void forgetPass(String email,EditText emailForgetEditText){
        Map<String, String> params = new HashMap<>();
        params.put("username", email);
        VolleyMethods.makePostJsonObjectRequest(params, ForgetPassword.this, ApiUrlClass.ForgetApi, new PostJsonObjectRequestCallback() {
            @Override
            public void onSuccessResponse(JSONObject response) {
                if (response!=null){
                    Log.e("response:", String.valueOf(response));
                    Integer code = 0;
                    try {
                        code = response.getInt("response");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (code.equals(10001)) {
                        Snackbar snackbar = Snackbar.make(forgetLinearLayout, "Email does not exist!", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.white));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.RED);
                        snackbar.show();
                    }else if (code.equals(10002)){
                            Snackbar snackbar = Snackbar.make(forgetLinearLayout, "Invailid email!", Snackbar.LENGTH_LONG);
                            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.white));
                            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                            textView.setTextColor(Color.RED);
                            snackbar.show();
                    }else if (code.equals(500)){
                        Snackbar snackbar = Snackbar.make(forgetLinearLayout, "Server error occurred!", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.white));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.RED);
                        snackbar.show();
                    }else if (code.equals(200)){
                        Snackbar snackbar = Snackbar.make(forgetLinearLayout, "Link send to your registered email!", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.white));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(getResources().getColor(R.color.dark_green));
                        snackbar.show();
                        emailForgetEditText.setText("");
                    }

                }
            }

            @Override
            public void onVolleyError(VolleyError error) {

            }

            @Override
            public void onTokenExpire() {

            }
        });
    }
}
