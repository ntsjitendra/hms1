package com.nuevotechsolutions.ewarp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.WorkingCheckListAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.services.NetworkChangeReceiver;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.LogoutClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.nuevotechsolutions.ewarp.utils.SharedPrefancesClearData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkingCheckListActivity extends AppCompatActivity {
    RecyclerView workinglist;
    ArrayList<String> arrayList;
    ArrayAdapter<String> adapter;

    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;

    String temp[], temp1, cmp, username, ctgry, res;
    SharedPreferences prefs;
    CheckListDatabase db;

    String company, empId, accessToken, ctgry1, year, wrk_id, last_wrk_id;
    String myJSON, myJSON1, username1, id;
    JSONArray checklist = null;
    URL url;
    ProgressBar pbHeaderProgress;
    LinearLayout linlaHeaderProgress;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;

    List<ChecklistRecordDataList> checklistRecordDataList = new ArrayList<>();
    List<EnginDescriptionData> enginDescriptionData = new ArrayList<>();
    List<MasterWorkId> masterWorkIdList = new ArrayList<>();
    List<UserDetails> userDetails = new ArrayList<>();
    List<MasterWorkId> list1 = new ArrayList<>();
    List<EnginDescriptionData> descriptionData = new ArrayList<>();
    List<Component> componentList = new ArrayList<>();

    LinearLayout linearLayout;
    private SwipeRefreshLayout swiperefresh;
    Handler mHandler;
    WorkingCheckListAdapter wcAdapter;
    private BroadcastReceiver mNetworkReceiver;
    private TextView tvnoResumelist;
    ProgressBar progressbarResumeTask;
  private FloatingActionButton fabUp, fabDown;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_working_check_list);
        mHandler = new Handler();
        setUpToolbar();
        workinglist = findViewById(R.id.workinglist);
        linearLayout = findViewById(R.id.resumeLinearLayout);
        tvnoResumelist = findViewById(R.id.tvnoResumelist);
        progressbarResumeTask = findViewById(R.id.progressbarResume);
//        navigationView = findViewById(R.id.navigation_menu);
        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        fabDown = findViewById(R.id.fabDown);
        fabUp = findViewById(R.id.fabUP);

        db = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = db.productDao().getUserDetail();
        descriptionData = db.productDao().getEnginDescriptionData();

//        this.mHandler.postDelayed(m_Runnable, 60000);
        onResumeChecklist(userDetails.get(0).getUser_id(), userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(),
                "0", userDetails.get(0).getAccessToken());

        getData();
        swiperefresh.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        if (new NetworkDetector().isNetworkReachable(WorkingCheckListActivity.this) == true) {
            swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    onResumeChecklist(userDetails.get(0).getUser_id(), userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(),
                            "0", userDetails.get(0).getAccessToken());
                    Toast.makeText(WorkingCheckListActivity.this, "Refreshed", Toast.LENGTH_SHORT).show();
                    swiperefresh.setRefreshing(false);
                    getData();
                }
            });
        }
        if (list1.size() == 0) {
            tvnoResumelist.setVisibility(View.VISIBLE);
        }
        mNetworkReceiver = new NetworkChangeReceiver();
        registerNetworkBroadcastForNougat();
        fabDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                workinglist  .post(new Runnable() {
                    @Override
                    public void run() {
                        workinglist.scrollToPosition(wcAdapter.getItemCount() - 1);
                    }
                });
                fabUp.show();
                fabDown.hide();
            }
        });
        fabUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                workinglist  .post(new Runnable() {
                    @Override
                    public void run() {
                        workinglist.scrollToPosition(0);
                    }
                });
                fabDown.show();
                fabUp.hide();
            }
        });
    }


    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private void setUpToolbar() {
//        drawerLayout = findViewById(R.id.drawerLayout2);
        toolbar = findViewById(R.id.toolbar2);
        ImageButton backarrowResumeTask = findViewById(R.id.backarrowResumeTask);
        backarrowResumeTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(WorkingCheckListActivity.this);
                finish();
            }
        });
//        imgbtn = findViewById(R.id.imgbtn);

        setSupportActionBar(toolbar);
//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
//        imgbtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//              workinglist  .post(new Runnable() {
//                    @Override
//                    public void run() {
////                        workinglist.scrollToPosition(wcAdapter.getItemCount() - 1);
//                        workinglist.scrollToPosition(wcAdapter.getItemCount() - 1);
//                    }
//                });
//            }
//        });
    }


    public void getData() {
        List<MasterWorkId> list = new ArrayList<>();
        List<UserDetails> userDetails = new ArrayList<>();

        db = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        list = db.productDao().getMasterWorkIdData();
        userDetails = db.productDao().getUserDetail();
        empId = String.valueOf(userDetails.get(0).getUser_id());
        company = String.valueOf(userDetails.get(0).getComp_name());

        if (wcAdapter == null) {
            int j = 0;
            for (int i = list.size() - 1; i >= 0; i--) {
                if (list.get(i).getWrk_status().equals(0)) {
                    if (list.get(i).getWrk_id() < 0) {
                        list1.add(0, list.get(i));
                    } else {
                        list1.add(j, list.get(i));
                    }
                    j++;
                }
            }
        } else {
            list1.clear();
            for (int j = 0; j < masterWorkIdList.size(); j++) {
                list1.add(masterWorkIdList.get(j));
            }
        }
        LinearLayoutManager llm = new LinearLayoutManager(WorkingCheckListActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        workinglist.setLayoutManager(llm);
        List<UserDetails> finalUserDetails = userDetails;
        wcAdapter = new WorkingCheckListAdapter(this, list1, descriptionData, new WorkingCheckListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(MasterWorkId item, int position) {
                progressbarResumeTask.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                Log.e("deseled_id", "jk:" + item.getDelete_id());

                    if (item.getDelete_id()!= null &&(item.getDelete_id()).equals(0)) {
                        list1.get(position).getWrk_id();
                        String assignType = list1.get(position).getAssign_type();
                        if (assignType != null) {
                            assignType = "A";
                        } else {
                            assignType = "M";
                        }
                        String refNo = list1.get(position).getShort_nm() + "/" + list1.get(position).getWrk_id_crtn_dt().substring(0, 4)
                                + "/" + list1.get(position).getWrk_id() + "/" + assignType;
                        String uId = String.valueOf(list1.get(position).getUid());
                        String engNo = String.valueOf(list1.get(position).getEngine_type_nmbr());
                        String unitNo = String.valueOf(list1.get(position).getUnit_no());
                        String title = refNo;
                        String workId = String.valueOf(list1.get(position).getWrk_id());
                        String dbnm = String.valueOf(finalUserDetails.get(0).getDbnm());
                        wrk_id = workId;
                        String checklist_type = "";

                        updateSeen();
                        List<CheckList> checkLists = new ArrayList<>();
                        List<CheckList> checkLists1 = new ArrayList<>();
                        checkLists = db.productDao().getAllCheckList();
                        for (int i = 0; i < checkLists.size(); i++) {
                            if (checkLists.get(i).getUid().equals(uId)) {
                                checklist_type = db.productDao().getAllCheckList().get(i).getChecklist_type();
                            }
                        }


                        onResumeItemClick(workId, empId, dbnm, company, accessToken);

                        Intent intent = new Intent(WorkingCheckListActivity.this, IsolationActivity.class);
                        intent.putExtra("uid", uId);
                        intent.putExtra("EngNo", engNo);
                        intent.putExtra("EngType", unitNo);
                        intent.putExtra("unitNo", uId);
                        intent.putExtra("title", title);
                        intent.putExtra("wrk_id", workId);
                        intent.putExtra("checklist_type", checklist_type);

                        new SharedPrefancesClearData().ClearDescriptionDataKey(WorkingCheckListActivity.this);

                        SharedPreferences.Editor editor1 = WorkingCheckListActivity.this.getSharedPreferences("descriptionKey", Context.MODE_PRIVATE).edit();

                        List<AuxEngLabel> auxEngLabels = new ArrayList<>();
                        List<String> auxEngLabels1 = new ArrayList<>();
                        auxEngLabels = db.productDao().getAllAuxEngLebels();

                        for (int i = 0; i < auxEngLabels.size(); i++) {
                            if (auxEngLabels.get(i).getUid().equals(uId)) {
                                auxEngLabels1.add(auxEngLabels.get(i).getEng_label());
                            }
                        }

                        List<EnginDescriptionData> enginDescriptionData = new ArrayList<>();
                        List<String> enginDescriptionData1 = new ArrayList<>();
                        enginDescriptionData = db.productDao().getEnginDescriptionData();

                        SharedPreferences prefs = getSharedPreferences("EngDescription", MODE_PRIVATE);

                        if (!prefs.contains(workId)) {
                            for (int i = 0; i < enginDescriptionData.size(); i++) {
                                if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                                    enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                                }
                            }
                            if (enginDescriptionData1.size() != 0 && enginDescriptionData1 != null) {
                                for (int i = 0; i < auxEngLabels1.size(); i++) {
                                    editor1.putString(auxEngLabels1.get(i), enginDescriptionData1.get(i));
                                }
                            }

                        } else {

                            String ED = prefs.getString(workId, null);
                            String str[] = ED.split(",");
                            Log.e("ed", ED);
                            List<String> al = new ArrayList<String>();
                            al = Arrays.asList(str);
//                    Collections.reverse(al);
                            for (int i = 0; i < auxEngLabels1.size(); i++) {
                                editor1.putString(auxEngLabels1.get(i), al.get(i));
                            }
                            for (int i = 0; i < enginDescriptionData.size(); i++) {
                                try {
                                    if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                                        enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            for (int i = 0; i < auxEngLabels1.size(); i++) {

                                editor1.putString(auxEngLabels1.get(i), al.get(i));

                            }

                        }

                        editor1.apply();
                        progressbarResumeTask.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        startActivity(intent);
                    } else {
                        if (new NetworkDetector().isNetworkReachable(WorkingCheckListActivity.this) == false) {
                            Snackbar snackbar = Snackbar.make(linearLayout, "Deleted tasks that remain in resume category can be completed upon Internet connectivity.", Snackbar.LENGTH_LONG);
                            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                            textView.setTextColor(Color.WHITE);
                            snackbar.show();
                            progressbarResumeTask.setVisibility(View.GONE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        } else {
                            list1.get(position).getWrk_id();
                            String assignType = list1.get(position).getAssign_type();
                            if (assignType != null) {
                                assignType = "A";
                            } else {
                                assignType = "M";
                            }
                            String refNo = list1.get(position).getShort_nm() + "/" + list1.get(position).getWrk_id_crtn_dt().substring(0, 4)
                                    + "/" + list1.get(position).getWrk_id() + "/" + assignType;
                            String uId = String.valueOf(list1.get(position).getUid());
                            String engNo = String.valueOf(list1.get(position).getEngine_type_nmbr());
                            String unitNo = String.valueOf(list1.get(position).getUnit_no());
                            String title = refNo;
                            String workId = String.valueOf(list1.get(position).getWrk_id());
                            String dbnm = String.valueOf(finalUserDetails.get(0).getDbnm());
                            wrk_id = workId;
                            String checklist_type = "";

                            updateSeen();
                            List<CheckList> checkLists = new ArrayList<>();
                            List<CheckList> checkLists1 = new ArrayList<>();
                            checkLists = db.productDao().getAllCheckList();
                            for (int i = 0; i < checkLists.size(); i++) {
                                if (checkLists.get(i).getUid().equals(uId)) {
                                    checklist_type = db.productDao().getAllCheckList().get(i).getChecklist_type();
                                }
                            }


                            onResumeItemClickComponent(uId, empId, dbnm);

                            Intent intent = new Intent(WorkingCheckListActivity.this, IsolationActivity.class);
                            intent.putExtra("uid", uId);
                            intent.putExtra("EngNo", engNo);
                            intent.putExtra("EngType", unitNo);
                            intent.putExtra("unitNo", uId);
                            intent.putExtra("title", title);
                            intent.putExtra("wrk_id", workId);
                            intent.putExtra("checklist_type", checklist_type);

                            new SharedPrefancesClearData().ClearDescriptionDataKey(WorkingCheckListActivity.this);

                            SharedPreferences.Editor editor1 = WorkingCheckListActivity.this.getSharedPreferences("descriptionKey", Context.MODE_PRIVATE).edit();

                            List<AuxEngLabel> auxEngLabels = new ArrayList<>();
                            List<String> auxEngLabels1 = new ArrayList<>();
                            auxEngLabels = db.productDao().getAllAuxEngLebels();

                            for (int i = 0; i < auxEngLabels.size(); i++) {
                                if (auxEngLabels.get(i).getUid().equals(uId)) {
                                    auxEngLabels1.add(auxEngLabels.get(i).getEng_label());
                                }
                            }

                            List<EnginDescriptionData> enginDescriptionData = new ArrayList<>();
                            List<String> enginDescriptionData1 = new ArrayList<>();
                            enginDescriptionData = db.productDao().getEnginDescriptionData();

                            SharedPreferences prefs = getSharedPreferences("EngDescription", MODE_PRIVATE);

                            if (!prefs.contains(workId)) {
                                for (int i = 0; i < enginDescriptionData.size(); i++) {
                                    if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                                        enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                                    }
                                }
                                if (enginDescriptionData1.size() != 0 && enginDescriptionData1 != null) {
                                    for (int i = 0; i < auxEngLabels1.size(); i++) {
                                        editor1.putString(auxEngLabels1.get(i), enginDescriptionData1.get(i));
                                    }
                                }

                            } else {

                                String ED = prefs.getString(workId, null);
                                String str[] = ED.split(",");
                                Log.e("ed", ED);
                                List<String> al = new ArrayList<String>();
                                al = Arrays.asList(str);
//                    Collections.reverse(al);
                                for (int i = 0; i < auxEngLabels1.size(); i++) {
                                    editor1.putString(auxEngLabels1.get(i), al.get(i));
                                }
                                for (int i = 0; i < enginDescriptionData.size(); i++) {
                                    try {
                                        if (enginDescriptionData.get(i).getWrk_id().equals(Integer.parseInt(workId))) {
                                            enginDescriptionData1.add(enginDescriptionData.get(i).getDec_answer());
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }

                                for (int i = 0; i < auxEngLabels1.size(); i++) {

                                    editor1.putString(auxEngLabels1.get(i), al.get(i));

                                }

                            }

                            editor1.apply();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    progressbarResumeTask.setVisibility(View.GONE);
                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    startActivity(intent);
                                }
                            }, 4000);

                        }

                    }
                }

        });
        workinglist.setAdapter(wcAdapter);
        wcAdapter.notifyDataSetChanged();


    }

    private void onResumeItemClickComponent(String uid, String empId, String dbnm) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            JSONArray newArr = new JSONArray();
            newArr.put(uid);
            Map<String, String> params = new HashMap<>();
            params.put("user_id", empId);
            params.put("dbnm", dbnm);
            params.put("accessToken", userDetails.get(0).getAccessToken());
            params.put("uiddata", newArr.toString());

            VolleyMethods.makePostJsonObjectRequest(params, WorkingCheckListActivity.this, new ApiUrlClass().TempLoginData, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray component = response.getJSONArray("component");

                            if (component != null && component.length() > 0) {
                                for (int i = 0; i < component.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) component.get(i);
                                    Component component1 = objectMapper.readValue(jsonObject1.toString(), Component.class);
                                    componentList.add(component1);
                                }
                            }

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    db.productDao().setComponentToTables(componentList);
                                }
                            }).start();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(WorkingCheckListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(WorkingCheckListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    public void onResumeItemClick(String wrk_id, String employee_id, String dbnm, String comp_name, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("wrk_id", wrk_id);
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("accessToken", userDetails.get(0).getAccessToken());
            params.put("seen", "true");

            VolleyMethods.makePostJsonObjectRequest(params, WorkingCheckListActivity.this, new ApiUrlClass().resumeApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            Integer res = response.getInt("response");

                            if (res.equals(200)) {
                                ObjectMapper objectMapper = new ObjectMapper();
                                JSONArray checklistRecordData = response.getJSONArray("lastdata");
                                Log.e("check+", String.valueOf(checklistRecordData));
                                JSONArray enginDescriptionDataArray = response.getJSONArray("engin_data");


                                if (checklistRecordData != null && checklistRecordData.length() > 0) {
                                    for (int i = 0; i < checklistRecordData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) checklistRecordData.get(i);
                                        ChecklistRecordDataList checklistRecordDataList1 = objectMapper.readValue(jsonObject1.toString(), ChecklistRecordDataList.class);
                                        checklistRecordDataList.add(checklistRecordDataList1);
                                    }
                                }

//                            if (enginDescriptionDataArray != null && enginDescriptionDataArray.length() > 0) {
//                                for (int i = 0; i < enginDescriptionDataArray.length(); i++) {
//                                    JSONObject jsonObject1 = (JSONObject) enginDescriptionDataArray.get(i);
//                                    EnginDescriptionData enginDescriptionData1 = objectMapper.readValue(jsonObject1.toString(), EnginDescriptionData.class);
//                                    enginDescriptionData.add(enginDescriptionData1);
//                                }
//                            }

                                resumeAndAddData();

                            } else if (res.equals(10003)) {
                                new LogoutClass().logoutMethod(WorkingCheckListActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(WorkingCheckListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(WorkingCheckListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    public void onResumeChecklist(String employee_id, String dbnm, String comp_name, String statusID, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("statusID", statusID);
            params.put("accessToken", userDetails.get(0).getAccessToken());


            VolleyMethods.makePostJsonObjectRequest(params, WorkingCheckListActivity.this, new ApiUrlClass().checklistApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            Integer res = response.getInt("response");
                            if (res.equals(200)) {
                                ObjectMapper objectMapper = new ObjectMapper();
                                JSONArray massterWorkIdData = response.getJSONArray("masterdata");
                                Log.e("masterdata+", String.valueOf(massterWorkIdData));
                                JSONArray enginDescriptionDataArray = response.getJSONArray("engin_data");

                                if (massterWorkIdData != null && massterWorkIdData.length() > 0) {
                                    masterWorkIdList.clear();
                                    for (int i = 0; i < massterWorkIdData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) massterWorkIdData.get(i);
                                        MasterWorkId masterWorkId = objectMapper.readValue
                                                (jsonObject1.toString(), MasterWorkId.class);
                                        masterWorkIdList.add(masterWorkId);
                                    }

                                }

                                if (enginDescriptionDataArray != null && enginDescriptionDataArray.length() > 0) {
                                    for (int i = 0; i < enginDescriptionDataArray.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) enginDescriptionDataArray.get(i);
                                        EnginDescriptionData enginDescriptionData1 = objectMapper.readValue(jsonObject1.toString(), EnginDescriptionData.class);
                                        enginDescriptionData.add(enginDescriptionData1);

                                    }
                                }
                                CheckListAndAddData();

//                            list1.addAll(0,masterWorkIdList);
//                            wcAdapter.notifyItemRangeInserted(0,masterWorkIdList.size());

                            } else if (res.equals(10003)) {
                                new LogoutClass().logoutMethod(WorkingCheckListActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(WorkingCheckListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(WorkingCheckListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void CheckListAndAddData() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
//                db.productDao().deleteMasterWorkId(0);
                db.productDao().setChecklistDataToTable(masterWorkIdList, enginDescriptionData);
            }
        });
        threadDeleteAllTables.start();
    }

    private void resumeAndAddData() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
                db.productDao().setResumeDataToTable(checklistRecordDataList);

            }
        });
        threadDeleteAllTables.start();
    }

    /*private final Runnable m_Runnable = new Runnable() {
        public void run() {
            onResumeChecklist(userDetails.get(0).getUser_id(),userDetails.get(0).getDbnm() , userDetails.get(0).getComp_name(),
                    "0", userDetails.get(0).getAccessToken());
            WorkingCheckListActivity.this.mHandler.postDelayed(m_Runnable, 60000);
        }
    };*/

    private void updateSeen() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
                db.productDao().updateMasterWorkId(Integer.parseInt(wrk_id), true);
            }
        });
        threadDeleteAllTables.start();
    }

//   @Override
//    protected void onPause() {
//        super.onPause();
//        mHandler.removeCallbacks(m_Runnable);
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

}
