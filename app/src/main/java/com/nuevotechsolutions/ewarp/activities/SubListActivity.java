package com.nuevotechsolutions.ewarp.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.SubListAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.ManualsModel.SubList;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.SharedPrefancesClearData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nuevotechsolutions.ewarp.R.string.app_name;

public class SubListActivity extends AppCompatActivity {

    RecyclerView subListRecyclerView;
    CheckListDatabase db;
    LinearLayout linearLayout;

    DrawerLayout drawerLayout;
    Toolbar toolbar;

    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;

    List<SubList> subListList = new ArrayList<>();
    List<UserDetails> userDetails = new ArrayList<>();
    SubListAdapter subListAdapter;
    String empId, dbnm,  company, accessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_list);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
        View hView =  navigationView.getHeaderView(0);
        TextView nav_user = (TextView)hView.findViewById(R.id.userNameLabel);

        setUpToolbar();
        subListRecyclerView = findViewById(R.id.subListRecyclerView);
        linearLayout = findViewById(R.id.sublistLinearLayout);
        navigationView = findViewById(R.id.navigation_menu);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        Intent homeintent = new Intent(SubListActivity.this, ChecklistHomeActivity.class);
                        startActivity(homeintent);
                        break;
                    case R.id.nav_change_pass:
                        Intent changePass = new Intent(SubListActivity.this, ChangePassword.class);
                        startActivity(changePass);
                        break;
//                    case R.id.nav_contact:
//                        Intent contactus = new Intent(SubListActivity.this, ContactUsActivity.class);
//                        startActivity(contactus);
//                        break;
                    case R.id.nav_logout:
                        Intent logout = new Intent(SubListActivity.this, LoginActivity.class);
                        new SharedPrefancesClearData().ClearBaseData(SubListActivity.this);
                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        logout.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(logout);
                        finish();
                        break;
                }

                return false;
            }
        });

        db = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = db.productDao().getUserDetail();

        if (userDetails.get(0).getFirst_name()!=null){
            String user=userDetails.get(0).getFirst_name();
            nav_user.setText(user);
        }else{
            nav_user.setText("Guest User");
        }

        onMainListChecklist(userDetails.get(0).getUser_id(),userDetails.get(0).getDbnm(), userDetails.get(0).getComp_name(),
                "0", userDetails.get(0).getAccessToken());

        getData();
    }

    private void setUpToolbar() {
        drawerLayout = findViewById(R.id.drawerLayoutSubList);
        toolbar = findViewById(R.id.toolbarSubList);
        setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void getData() {
        List<SubList> list = new ArrayList<>();
        List<SubList> list1 = new ArrayList<>();
        List<UserDetails> userDetails = new ArrayList<>();

        db = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = db.productDao().getUserDetail();
        empId = String.valueOf(userDetails.get(0).getUser_id());
        company = String.valueOf(userDetails.get(0).getComp_name());


        list1.clear();
        for (int j = subListList.size() - 1; j >= 0; j--) {
            list1.add(subListList.get(j));
        }

        LinearLayoutManager llm = new LinearLayoutManager(SubListActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        subListRecyclerView.setLayoutManager(llm);
        List<UserDetails> finalUserDetails = userDetails;

        subListAdapter = new SubListAdapter(this, list1, new SubListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(SubList item, int position) {
                list1.get(position).getMain_list_key();
                String mainListKey = String.valueOf(list1.get(position).getMain_list_key());
                String subListUrl = String.valueOf(list1.get(position).getUrl());
                String dbnm = String.valueOf(finalUserDetails.get(0).getDbnm());

                onSubListItemClick(empId, dbnm,  company, accessToken);

                Intent intent = new Intent(SubListActivity.this, IsolationActivity.class);
                intent.putExtra("mainListKey", mainListKey);
                intent.putExtra("mainListName", subListUrl);

                startActivity(intent);
            }
        }
        );
        subListRecyclerView.setAdapter(subListAdapter);
        subListAdapter.notifyDataSetChanged();

    }

    public void onSubListItemClick(String employee_id, String dbnm, String comp_name, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("accessToken", accessToken);


            VolleyMethods.makePostJsonObjectRequest(params, SubListActivity.this, new ApiUrlClass().resumeApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray subListData = response.getJSONArray("lastdata");
                            JSONArray enginDescriptionDataArray = response.getJSONArray("engin_data");


                            if (subListData != null && subListData.length() > 0) {
                                for (int i = 0; i < subListData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) subListData.get(i);
                                    SubList subList = objectMapper.readValue(jsonObject1.toString(), SubList.class);
                                    subListList.add(subList);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(SubListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(SubListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    public void onMainListChecklist(String employee_id, String dbnm, String comp_name, String statusID, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("statusID", statusID);
            params.put("accessToken", accessToken);

            VolleyMethods.makePostJsonObjectRequest(params, SubListActivity.this, new ApiUrlClass().checklistApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray sublistData = response.getJSONArray("masterdata");

                            if (sublistData != null && sublistData.length() > 0) {
                                subListList.clear();
                                for (int i = 0; i < sublistData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) sublistData.get(i);
                                    SubList subList = objectMapper.readValue
                                            (jsonObject1.toString(), SubList.class);
                                    subListList.add(subList);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(SubListActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(SubListActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

}
