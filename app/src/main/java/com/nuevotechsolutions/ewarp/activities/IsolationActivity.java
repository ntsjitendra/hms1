package com.nuevotechsolutions.ewarp.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.BuildConfig;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.CheckListDescriptionLabelAdapter;
import com.nuevotechsolutions.ewarp.adapter.FormAdapter;
import com.nuevotechsolutions.ewarp.adapter.ImageListDataAdapter;
import com.nuevotechsolutions.ewarp.adapter.RecycleCheckpointAdapter;
import com.nuevotechsolutions.ewarp.adapter.TeamMemberListAdapter;
import com.nuevotechsolutions.ewarp.controller.ImageData;
import com.nuevotechsolutions.ewarp.controller.ImageEventHandler;
import com.nuevotechsolutions.ewarp.controller.ImageHandler;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.ImageInterface;
import com.nuevotechsolutions.ewarp.interfaces.MultiPartResponseInerface;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.interfaces.TableFormSubmItInterface;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecord;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.retrofit.RetrofitInstance;
import com.nuevotechsolutions.ewarp.services.NetworkChangeReceiver;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.services.RefreshDataOnSubmit;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.ButtonLock;
import com.nuevotechsolutions.ewarp.utils.FileUtil;
import com.nuevotechsolutions.ewarp.utils.LocationHelper;
import com.nuevotechsolutions.ewarp.utils.Logger;
import com.nuevotechsolutions.ewarp.utils.LogoutClass;
import com.nuevotechsolutions.ewarp.utils.MultipartUtility;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.nuevotechsolutions.ewarp.utils.PathUtil;
import com.nuevotechsolutions.ewarp.utils.RecyclerSectionItemDecoration;
import com.nuevotechsolutions.ewarp.utils.SyncTableData;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


@SuppressWarnings("All")
public class IsolationActivity extends AppCompatActivity {

    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1034;
    public static final int REQUEST_AUDIO_PERMISSION_CODE = 1;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;
    private static final int AUDIO_LIST_UPSATE = 121;
    private static final int HIDE_THRESHOLD = 20;
    private static final int PERMISSION_REQUEST_CODE = 100;
    public static RecyclerView recyclerView, recyclerViewDescriptionLabel, recyclerViewMemberList;
    private static ImageListDataAdapter itemListDataAdapter;
    //    private Sensorservice mSensorService;
    private static Context ctx;
    private static Bundle mBundleRecyclerViewState;
    public final String APP_TAG = "EWARP";
    private final String KEY_RECYCLER_STATE = "recycler_state";
    public String locality, address, postalCode, county, lat, lon, coordinates;
    LocationManager locationManager;
    TextView textView_checkList_header, tv_eng_type, tv_unit_no, textView_room_no,
            textView_inspe_type, textView_checkList_title;
    EditText roomNumber, inspectionType;
    List<ChecklistRecord> checklistRecords;
    List<UserDetails> userDetailsList;
    Button cancelBtn, completeBtn;
    FingerprintManager fingerprintManager;
    KeyguardManager keyguardManager;
    File photoFile;
    String imageFileName = "No_Teken_Photo";
    String timeStamp, uid, engNo, engType, unitNo, title;
    Bitmap takenImage, bitmap, bitmapUndo;
    String encodedImage;
    Uri fileProvider;
    File file;
    Bitmap rotatedBitmap = null;
    int postActivity;
    ImageView[] imageViewsActivity;
    List<Component> componentList;
    String points;
    ImageView imageView;
    AlertDialog.Builder alertDialog;
    List<Component> components;
    List<String> labelName;
    List<String> labelValue;
    List<AuxEngLabel> auxEngLabels;
    DrawerLayout drawerLayout;
    androidx.appcompat.widget.Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    String uid1, wrk_id, checklist_type, point = "", tempPoint = "", q, ans, comment, photo, alert, crnt_dt, crnt_lctn = "Not available", rank_id,
            emp_id, severity, department, alertComment;
    int checkpointId;
    String submitComment;
    int position, posti;
    TextView tvAudioCount1;
    Intent mServiceIntent;
    List<String> listPhoto = new ArrayList<>();
    JSONObject imageJson;
    //Only for hode data on submit time.
    ImageButton editText, button, b, photoButton, beforeLockedButton;
    int editUpdate;
    Switch aSwitch;
    RadioButton radioButtons[];
    TextView submitEditText;
    LinearLayout linearLayout, pointEditSubmitedLinearLayout;
    Dialog dialog, dialogfinger;
    ImageView doneImageView;
    String chceklisttitle, isVisibleEditBtn;
    TextView tvTitle;
    double latitude, longitude;
    LocationHelper.LocationResult locationResult;
    LocationHelper locationHelper;
    Geocoder geocoder;
    int flag = 0;
    String status = "";
    String submitBtnFlag = "", locationValue = "", selfy_on_submit = "";
    List<ChecklistRecordDataList> checklistRecordDataList = new ArrayList<>();
    int lastDataSize = 0;
    int handlerDelayTime = 4000;
    String imageEncoded;
    String imagePath = "";
    int sectionFlagPostn = 0, userFlagPostn = 0, rankFlagPostn = 0;
    String photofilepath;
    ImageInterface imageInterface;
    Date date;
    SimpleDateFormat formatter;
    Dialog recordDialog;
    RecyclerView.LayoutManager lm;
    List<Boolean> viewEnabled = new ArrayList<>();
    //    List<String> fileNameList = new ArrayList<>();
    Map<Integer, ArrayList<String>> audioFileMap = new HashMap<>();
    Map<Integer, ArrayList<String>> audioFileMapAddress = new HashMap<>();
    List<ChecklistRecordDataList> submitDataTemp1 = new ArrayList<>();
    NetworkChangeReceiver networkChangeReceiver;
    String mFilePath;
    //    String mFileName;
    String recordingUri;
    String fileName;
    File[] files;
    ArrayList<String> audioList;
    ArrayList<String> audioListAddress;
    private int SELECT_IMAGE = 101;
    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;
    private CheckListDatabase checkListDatabase;
    private List<String> locationList = new ArrayList<>();
    private List<String> timeStampList = new ArrayList<>();
    private List<String> photoTextValueList = new ArrayList<>();
    private TextView textViewTitle;
    // private static String[] itemsListData;
    private List<String> itemsListData;
    private List<String> locationListData;
    private List<String> timeStampListData;
    private List<String> photoTextValueData;
    private Map<Integer, List<String>> imageMap = new HashMap<>();
    private Map<Integer, List<String>> locationMap = new HashMap<>();
    private Map<Integer, List<String>> timeStampMap = new HashMap<>();
    private Map<Integer, List<String>> photoTextValueMap = new HashMap<>();
    private List<ChecklistRecordDataList> lastdata = new ArrayList<>();
    private RecycleCheckpointAdapter recycleCheckpointAdapter;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;
    private ImageView imagedescription, imgTeamMember;
    private Handler mHandler;
    private SwipeRefreshLayout CheklistSwiperefresh;
    private BroadcastReceiver mNetworkReceiver;
    private boolean frontCamera;
    private ProgressBar progressbar;
    private Parcelable mListState;
    private LinearLayout llaudioplay1;
    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    Map<String, String> valueDetails = new HashMap<>();
    SharedPreferences prefs;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView ShowImgView;
    File imgFile1 = null;

    public static Context getCtx() {
        return ctx;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chech_list_info);
        Intent intent = getIntent();
        ctx = IsolationActivity.this;
        mHandler = new Handler();
        cancelBtn = findViewById(R.id.cancelledButton);
        completeBtn = findViewById(R.id.completedButton);
        CheklistSwiperefresh = findViewById(R.id.CheklistSwiperefresh);
        linearLayout = findViewById(R.id.isolationLinearLayout);
        progressbar = findViewById(R.id.progressbar);
        labelName = new ArrayList<>();
        labelValue = new ArrayList<>();
        imageJson = new JSONObject();

//*******************************************Description Data*******************
        prefs = getSharedPreferences("descriptionKey", MODE_PRIVATE);
        if (prefs != null) {
            Map<String, ?> keys = prefs.getAll();
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
                labelName.add(entry.getKey());
                labelValue.add((String) entry.getValue());
            }
            uid1 = prefs.getString("uid", uid);
            wrk_id = prefs.getString("wrk_id", wrk_id);
        }
        setUpToolbar();

        if (new NetworkDetector().isNetworkReachable(IsolationActivity.this) == true) {
            new SyncTableData().syncTableData(IsolationActivity.this);
        }
//NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();
//networkChangeReceiver.onReceive(this,getIntent());
        checkListDatabase = Room.databaseBuilder(getApplicationContext(),
                CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetailsList = checkListDatabase.productDao().getUserDetail();

        componentList = new ArrayList<>();
        bindView();
        getUid();
        itemListDataAdapter = new ImageListDataAdapter(this, itemsListData, "", position, locationListData, timeStampListData, photoTextValueList, photoButton, imageInterface);

        executor = ContextCompat.getMainExecutor(IsolationActivity.this);
        biometricPrompt = new BiometricPrompt(IsolationActivity.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
                if (progressbar.getVisibility() == View.VISIBLE)
                    progressbar.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                if (flag == 1) {
                                        /*doneImageView.setImageResource(R.mipmap.done);
                                        dialogfinger.dismiss();*/
//                createImageName();
                    Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                } else {
//                    doneImageView.setImageResource(R.mipmap.done);
                    Log.e("s", "Authentication");
                    submitBtnFlag = "start";
//                createImageName();
                    Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Biometric Authentication")
                .setSubtitle("Verify using your biometric credential")
                .setNegativeButtonText("Cancel")
                .build();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(IsolationActivity.this);
                dialog.setContentView(R.layout.comment_layout);
                TextView commentTitle = dialog.findViewById(R.id.commentTitleTextView);
                EditText editText = dialog.findViewById(R.id.editText);
                Button submitButton = dialog.findViewById(R.id.button);
                Button cancelButton = dialog.findViewById(R.id.buttonCancel);
                commentTitle.setText("Cancel Comment");
                dialog.setCancelable(false);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        submitComment = editText.getText().toString();
                        status = "2";

                        if (selfy_on_submit.equalsIgnoreCase("true")) {
                            flag = 1;

                            BiometricManager biometricManager = BiometricManager.from(IsolationActivity.this);
                            switch (biometricManager.canAuthenticate()) {
                                case BiometricManager.BIOMETRIC_SUCCESS:
                                    alertDialog = new AlertDialog.Builder(IsolationActivity.this);
                                    alertDialog.setTitle("Alert Dialog");
                                    alertDialog.setIcon(R.drawable.ewarpletest);
                                    alertDialog.setMessage("Do you want to cancel checklist ?");
                                    alertDialog.setCancelable(false);
                                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
//                                            dialogfinger = new Dialog(IsolationActivity.this);
//                                            dialogfinger.setContentView(R.layout.fingerprint_dialog);
//                                            dialogfinger.setCancelable(false);
//                                            ImageView mfingerprint = dialogfinger.findViewById(R.id.dialogFingerprint);
//                                            doneImageView = mfingerprint;
//                                            dialogfinger.show();
                            /*ImageView imgClose = dialogfinger.findViewById(R.id.imgClose);
                            imgClose.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogfinger.dismiss();
                                }
                            });*/
//                                            Window window = dialogfinger.getWindow();
//                                            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                                            verification();
                                            biometricPrompt.authenticate(promptInfo);
                                        }
                                    });
                                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                    alertDialog.show();
                                    break;
                                case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                                    Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                                    break;
                                case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                                    Intent intent1 = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                                    startActivityForResult(intent1, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                                    break;
                                case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                                    Log.e("MY_APP_TAG", "The user hasn't associated " +
                                            "any biometric credentials with their account.");
                                    break;
                            }










                            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
                                if (fingerprintManager.isHardwareDetected()) {
                                    alertDialog = new AlertDialog.Builder(IsolationActivity.this);
                                    alertDialog.setTitle("Alert Dialog");
                                    alertDialog.setIcon(R.drawable.ewarpletest);
                                    alertDialog.setMessage("Do you want to cancel checklist ?");
                                    alertDialog.setCancelable(false);
                                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialogfinger = new Dialog(IsolationActivity.this);
                                            dialogfinger.setContentView(R.layout.fingerprint_dialog);
                                            dialogfinger.setCancelable(false);
                                            ImageView mfingerprint = dialogfinger.findViewById(R.id.dialogFingerprint);
                                            doneImageView = mfingerprint;
                                            dialogfinger.show();
                            *//*ImageView imgClose = dialogfinger.findViewById(R.id.imgClose);
                            imgClose.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogfinger.dismiss();
                                }
                            });*//*
                                            Window window = dialogfinger.getWindow();
                                            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                            verification();
                                        }
                                    });
                                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                    alertDialog.show();


                                } else {
                                    Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                                }
                            } else {
                                Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                            }*/
                        } else {
                            cancelCompleteMethod("2");
                            submitComment = "";
                            dialog.dismiss();
                            dismissKeyboard(IsolationActivity.this);
                            Toast.makeText(IsolationActivity.this, "Checklist canceled.", Toast.LENGTH_SHORT).show();
                            Intent intent1 = new Intent(IsolationActivity.this, WorkingCheckListActivity.class);
                            startActivity(intent1);
                        }

                    }
                });

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        });

        completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(IsolationActivity.this);
                dialog.setContentView(R.layout.comment_layout);
                TextView commentTitle = dialog.findViewById(R.id.commentTitleTextView);
                EditText editText = dialog.findViewById(R.id.editText);
                Button submitButton = dialog.findViewById(R.id.button);
                Button cancelButton = dialog.findViewById(R.id.buttonCancel);
                commentTitle.setText("Complete Comment");
                dialog.setCancelable(false);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        submitComment = editText.getText().toString();
                        status = "1";

                        if (selfy_on_submit.equalsIgnoreCase("true")) {
                            flag = 1;

                            BiometricManager biometricManager = BiometricManager.from(IsolationActivity.this);
                            switch (biometricManager.canAuthenticate()) {
                                case BiometricManager.BIOMETRIC_SUCCESS:
                                    alertDialog = new AlertDialog.Builder(IsolationActivity.this);
                                    alertDialog.setTitle("Alert Dialog");
                                    alertDialog.setIcon(R.drawable.ewarpletest);
                                    alertDialog.setMessage("Do you want to complete checklist ?");
                                    alertDialog.setCancelable(false);
                                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
//                                            dialogfinger = new Dialog(IsolationActivity.this);
//                                            dialogfinger.setContentView(R.layout.fingerprint_dialog);
//                                            dialogfinger.setCancelable(false);
//                                            ImageView mfingerprint = dialogfinger.findViewById(R.id.dialogFingerprint);
//                                            doneImageView = mfingerprint;
//                                            dialogfinger.show();
                            /*ImageView imgClose = dialogfinger.findViewById(R.id.imgClose);
                            imgClose.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogfinger.dismiss();
                                }
                            });*/
//                                            Window window = dialogfinger.getWindow();
//                                            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                                            verification();
                                            biometricPrompt.authenticate(promptInfo);
                                        }
                                    });
                                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                    alertDialog.show();
                                    break;
                                case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                                    Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                                    break;
                                case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                                    Intent intent1 = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                                    startActivityForResult(intent1, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                                    break;
                                case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                                    Log.e("MY_APP_TAG", "The user hasn't associated " +
                                            "any biometric credentials with their account.");
                                    break;
                            }


                            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
                                if (fingerprintManager.isHardwareDetected()) {
                                    alertDialog = new AlertDialog.Builder(IsolationActivity.this);
                                    alertDialog.setTitle("Alert Dialog");
                                    alertDialog.setIcon(R.drawable.ewarpletest);
                                    alertDialog.setMessage("Do you want to complete checklist ?");
                                    alertDialog.setCancelable(false);
                                    alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialogfinger = new Dialog(IsolationActivity.this);
                                            dialogfinger.setContentView(R.layout.fingerprint_dialog);
                                            dialogfinger.setCancelable(false);
                                            ImageView mfingerprint = dialogfinger.findViewById(R.id.dialogFingerprint);
                                            doneImageView = mfingerprint;
                                            dialogfinger.show();
                            *//*ImageView imgClose = dialogfinger.findViewById(R.id.imgClose);
                            imgClose.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogfinger.dismiss();
                                }
                            });*//*
                                            Window window = dialogfinger.getWindow();
                                            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                            verification();
                                        }
                                    });
                                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                    alertDialog.show();


                                } else {
                                    Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                                }
                            } else {
                                Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                            }*/
                        } else {
                            cancelCompleteMethod("1");
                            submitComment = "";
                            dialog.dismiss();
                            dismissKeyboard(IsolationActivity.this);
                            Toast.makeText(IsolationActivity.this, "Checklist completed.", Toast.LENGTH_SHORT).show();
                            Intent intent1 = new Intent(IsolationActivity.this, ChecklistHomeActivity.class);
                            startActivity(intent1);
                        }
                    }
                });
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        });
        setDataOnPoint();
        LocationAccess();
        locationHelper.getLocation(IsolationActivity.this, IsolationActivity.this.locationResult);

        if (new NetworkDetector().isNetworkReachable(IsolationActivity.this) == true) {
            CheklistSwiperefresh.setColorSchemeColors(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));

            CheklistSwiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (new NetworkDetector().isNetworkReachable(IsolationActivity.this) == true) {
                        setDataOnPoint();
                        List<UserDetails> userDetails = new ArrayList<>();
                        userDetails = checkListDatabase.productDao().getUserDetail();
                        checklistDetailSwiperefresh(wrk_id, userDetails.get(0).getUser_id(),
                                userDetails.get(0).getDbnm(),
                                userDetails.get(0).getComp_name(), userDetails.get(0).getAccessToken());
                        CheklistSwiperefresh.setRefreshing(false);
                    } else {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.isolationLinearLayout), "No internet connection!", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);
                        snackbar.show();
                    }

                }
            });
        } else {
            CheklistSwiperefresh.setEnabled(false);
        }

//        mNetworkReceiver = new NetworkChangeReceiver();
//        registerNetworkBroadcastForNougat();

//        Auto Selected all point filter option
        recyclerView.setAdapter(recycleCheckpointAdapter);
        new RecycleCheckpointAdapter("allpoints").notifyDataSetChanged();
    }


    private void registerNetworkBroadcastForNougat() {

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    //
    public void dismissKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != activity.getCurrentFocus())
            imm.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
    }

    //* @Author: Subhra Priyadarshini;
    //* @Used For : This function is used for fetching current location.
    public void LocationAccess() {
        this.locationResult = new LocationHelper.LocationResult() {
            @Override
            public void gotLocation(Location location) {

                //Got the location!
                if (location != null) {
                    if (locationValue.equalsIgnoreCase("true")) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    } else {
                        latitude = 0;
                        longitude = 0;
                    }

                    geocoder = new Geocoder(IsolationActivity.this);
                    try {
                        if (locationValue.equalsIgnoreCase("true")) {
                            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                            if (addresses != null && addresses.size() > 0) {
                                address = addresses.get(0).getAddressLine(0);
                                crnt_lctn = address + "_" + latitude + "_" + longitude;
                                Log.e("test+", crnt_lctn);
                            }
                        } else {
                            address = "Not available";
                            crnt_lctn = "Not available";
                        }

                    } catch (IOException e) {
//                        Toast.makeText(IsolationActivity.this, "Unable to access the current location !", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e("de", "Location is null.");
                }

            }

        };

        this.locationHelper = new LocationHelper();

    }

    public void setDataOnPoint() {
        List<ChecklistRecordDataList> checklistRecord = new ArrayList<>();

        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        checklistRecord = checkListDatabase.productDao().getChecklistRecordDataList();
        int j = 0;
        Log.e("tempId", wrk_id);
        for (int i = 0; i < checklistRecord.size(); i++) {
            if (checklistRecord.get(i).getWrk_id().equals(Integer.parseInt(wrk_id)) && checklistRecord.get(i).getUid().equals(Integer.parseInt(uid))) {
                lastdata.add(j, checklistRecord.get(i));
                j++;
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.option_self) {
            item.setChecked(true);
            recyclerView.setAdapter(recycleCheckpointAdapter);
            new RecycleCheckpointAdapter("mypoints").notifyDataSetChanged();
            return true;
        }
        if (id == R.id.option_all) {
            item.setChecked(true);
            recyclerView.setAdapter(recycleCheckpointAdapter);
            new RecycleCheckpointAdapter("allpoints").notifyDataSetChanged();
            return true;
        }
        if (id == R.id.option_customize) {
            item.setChecked(true);
            customizeFilterDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void customizeFilterDialog() {
        List<String> section = new ArrayList<>();
        String user[] = {"ALL", "SELF"};
        List<String> ranks = new ArrayList<>();
        ranks.add("ALL");
        if (componentList != null) {
            String temp = "ALL";
            section.add(temp);
            for (int i = 0; i < componentList.size(); i++) {
                if (!temp.equalsIgnoreCase(componentList.get(i).getTitle_of_section())) {
                    if (!section.contains(componentList.get(i).getTitle_of_section())) {
                        section.add(componentList.get(i).getTitle_of_section());
                    }
                    temp = componentList.get(i).getTitle_of_section();
                }
            }
        }

        List<MasterWorkId> masterWorkIdList = new ArrayList<>();
        masterWorkIdList = checkListDatabase.productDao().getMasterWorkIdData();

        for (MasterWorkId masterWorkId : masterWorkIdList) {
            if (masterWorkId.getWrk_id().equals(Integer.parseInt(wrk_id))) {
                String teamStringJson = masterWorkId.getTeam_list();
                try {
                    JSONArray teamlistJson = new JSONArray(teamStringJson);
                    for (int i = 0; i < teamlistJson.length(); i++) {
                        TeamSelectionList team = new TeamSelectionList();
                        JSONObject obj = teamlistJson.getJSONObject(i);
                        if (!ranks.contains(obj.getString("rank"))) {
                            ranks.add(obj.getString("rank"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        ArrayAdapter<String> arrayAdapterSection = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, section);
        ArrayAdapter<String> arrayAdapterUser = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, user);
        ArrayAdapter<String> arrayAdapterRank = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, ranks);
        Dialog filterdialog = new Dialog(IsolationActivity.this);
        filterdialog.setContentView(R.layout.customize_checklist_points_filter_dialog);
        filterdialog.setTitle("Filter");
        filterdialog.setCancelable(false);
        Spinner sectionSpinner = (Spinner) filterdialog.findViewById(R.id.sectionSpinner);
        Spinner userSpinner = (Spinner) filterdialog.findViewById(R.id.userSpinner);
        Spinner rankSpinner = (Spinner) filterdialog.findViewById(R.id.rankSpinner);
        sectionSpinner.setAdapter(arrayAdapterSection);
        userSpinner.setAdapter(arrayAdapterUser);
        rankSpinner.setAdapter(arrayAdapterRank);
        sectionSpinner.setSelection(sectionFlagPostn);
        userSpinner.setSelection(userFlagPostn);
        rankSpinner.setSelection(rankFlagPostn);
        Button btnCancel = (Button) filterdialog.findViewById(R.id.btn_Cancel_filter);
        Button btn_ok = (Button) filterdialog.findViewById(R.id.btn_ok_filter);
        sectionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sectionFlagPostn = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        userSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userFlagPostn = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        rankSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rankFlagPostn = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterdialog.dismiss();
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String section1 = sectionSpinner.getSelectedItem().toString();
                String user1 = userSpinner.getSelectedItem().toString();
                String rank1 = rankSpinner.getSelectedItem().toString();
                recyclerView.setAdapter(recycleCheckpointAdapter);
                new RecycleCheckpointAdapter(section1, user1, rank1).notifyDataSetChanged();
                filterdialog.dismiss();
            }
        });
        filterdialog.show();
        Window window = filterdialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


    }

    private void setUpToolbar() {

        toolbar = findViewById(R.id.toolbar6);
        textViewTitle = findViewById(R.id.textViewTitle);
        ImageButton backarrowChecklist = toolbar.findViewById(R.id.backarrowChecklist);
        backarrowChecklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(IsolationActivity.this);
                finish();
            }
        });
        imagedescription = findViewById(R.id.imagedescription);
        imgTeamMember = findViewById(R.id.imageMemberList);
        imgTeamMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(IsolationActivity.this);
                dialog.setContentView(R.layout.list_tema_member_dialog);
                recyclerViewMemberList = dialog.findViewById(R.id.recyclerViewMemberList);
                List<MasterWorkId> masterWorkIdList = new ArrayList<>();
                List<TeamSelectionList> MemberNamelist = new ArrayList<>();
                List<TeamSelectionList> MemberNamelist1 = new ArrayList<>();
                checkListDatabase = Room.databaseBuilder(IsolationActivity.this, CheckListDatabase.class,
                        ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                MemberNamelist = checkListDatabase.productDao().getTeamSelectionData();
                masterWorkIdList = checkListDatabase.productDao().getMasterWorkIdData();

                for (MasterWorkId masterWorkId : masterWorkIdList) {
                    if (masterWorkId.getWrk_id().equals(Integer.parseInt(wrk_id))) {
                        String teamStringJson = masterWorkId.getTeam_list();
                        if (teamStringJson != null) {
                            try {
                                JSONArray teamlistJson = new JSONArray(teamStringJson);

                                for (int i = 0; i < teamlistJson.length(); i++) {
                                    TeamSelectionList team = new TeamSelectionList();
                                    JSONObject obj = teamlistJson.getJSONObject(i);
                                    team.setFirst_name(obj.getString("name"));
                                    team.setRank(obj.getString("rank"));
                                    MemberNamelist1.add(team);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                if (MemberNamelist1.size() == 0) {
                    TeamSelectionList tm = new TeamSelectionList();
                    tm.setRank(userDetailsList.get(0).getRank());
                    tm.setFirst_name(userDetailsList.get(0).getFirst_name());
                    MemberNamelist1.add(tm);
                }
                recyclerViewMemberList.setLayoutManager(new LinearLayoutManager(IsolationActivity.this));
                TeamMemberListAdapter teamMemberListAdapter = new TeamMemberListAdapter(IsolationActivity.this, MemberNamelist1);
                recyclerViewMemberList.setAdapter(teamMemberListAdapter);
                teamMemberListAdapter.notifyDataSetChanged();
                dialog.show();
            }
        });


        imagedescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkListDatabase = Room.databaseBuilder(IsolationActivity.this, CheckListDatabase.class,
                        ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
                List<String> auxLabelList = new ArrayList<>();
                List<EnginDescriptionData> eDD = new ArrayList<>();
                List<EnginDescriptionData> eDD1 = new ArrayList<>();
                eDD1 = checkListDatabase.productDao().getEnginDescriptionData();
                for (int i = 0; i < eDD1.size(); i++) {
                    if (eDD1.get(i).getWrk_id() == Integer.parseInt(wrk_id)) {
                        eDD.add(eDD1.get(i));
                    }
                }
                if (eDD.size() == 0) {
                    List<AuxEngLabel> auxEngLabels = checkListDatabase.productDao().getAllAuxEngLebels();
                    for (int i = 0; i < auxEngLabels.size(); i++) {
                        if (auxEngLabels.get(i).getUid().equals(uid)) {
                            auxLabelList.add(auxEngLabels.get(i).getEng_label());
                        }
                    }
                } else {
                    List<EnginDescriptionData> enginDescriptionData = checkListDatabase.productDao().getEnginDescriptionData();
                    labelValue.clear();
                    for (int i = 0; i < enginDescriptionData.size(); i++) {
                        Log.e("UID..", uid);
                        if (enginDescriptionData.get(i).getWrk_id() == Integer.parseInt(wrk_id)) {
                            if (!auxLabelList.contains(enginDescriptionData.get(i).getDecription())) {
                                auxLabelList.add(enginDescriptionData.get(i).getDecription());
                                labelValue.add(enginDescriptionData.get(i).getDec_answer());
                            }
                        }
                    }
                }

                if (!auxLabelList.isEmpty()) {
                    final Dialog dialog = new Dialog(IsolationActivity.this);
                    dialog.setContentView(R.layout.checklist_description_dialog);
                    recyclerViewDescriptionLabel = dialog.findViewById(R.id.recyclerViewDescriptionLabel);
                    tvTitle = dialog.findViewById(R.id.tvTitle);
                    Bundle bundle = getIntent().getExtras();
                    if (bundle != null) {
                        String title = bundle.getString("title");
                        tvTitle.setText(title);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            recyclerViewDescriptionLabel.setLayoutManager(new LinearLayoutManager(IsolationActivity.this));
                            recyclerViewDescriptionLabel.setAdapter(new CheckListDescriptionLabelAdapter(IsolationActivity.this, auxLabelList, labelValue, eDD));

                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(IsolationActivity.this, "No describtion available for this checklist", Toast.LENGTH_SHORT).show();
                }
            }
        });
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }

    private void bindView() {
        textView_checkList_header = findViewById(R.id.textView_checkList_header);
        recyclerView = findViewById(R.id.recyclerView);

    }


    private void setRecyclerView() {
        recycleCheckpointAdapter = new RecycleCheckpointAdapter(IsolationActivity.this, wrk_id,
                componentList,
                viewEnabled,
                lastdata,
                imageMap,
                locationMap,
                timeStampMap,
                photoTextValueMap,
                checklist_type,
                audioFileMap,
                imageInterface = new ImageInterface() {

                    @Override
                    public void onPhotoButtonClickListener(int post, ImageView[] imageViews) {

                        if (imageViews[0].getDrawable() == null) {
                            imageView = imageViews[0];
                        } else if (imageViews[1].getDrawable() == null) {
                            imageView = imageViews[1];
                        } else if (imageViews[2].getDrawable() == null) {
                            imageView = imageViews[2];
                        } else if (imageViews[3].getDrawable() == null) {
                            imageView = imageViews[3];
                        }

                        // createImageName();

                    }

                    @Override
                    public void onPhotoButtonClickListenerAdapter(int post,
                                                                  String points,
                                                                  ImageView[] imageViews,
                                                                  ImageListDataAdapter adapter,
                                                                  List<String> imageList,
                                                                  List<String> locationList,
                                                                  List<String> timeStampList,
                                                                  List<String> photoTextValueList) {
//                        if (point.equals("") || point.equals(points)) {
                        tempPoint = points;
                        position = post;
                        itemListDataAdapter = adapter;
                        itemsListData = imageList;
                        locationListData = locationList;
                        timeStampListData = timeStampList;
                        photoTextValueData = photoTextValueList;
//                            createImageName();
                        Intent intent = new Intent(IsolationActivity.this, CustomCameraActivity.class);
                        intent.putExtra("position", post);
                        intent.putExtra("locationValue", locationValue);
                        intent.putExtra("imageJson", imageJson.toString());

                        intent.putExtra("imageMap", (Serializable) imageMap);
                        intent.putExtra("locationMap", (Serializable) locationMap);
                        intent.putExtra("timeStampMap", (Serializable) timeStampMap);
                        intent.putExtra("photoTextValueMap", (Serializable) photoTextValueMap);

                        intent.putExtra("photolist", (Serializable) listPhoto);
                        intent.putExtra("locationList", (Serializable) locationList);
                        intent.putExtra("timeStampList", (Serializable) timeStampList);
                        intent.putExtra("photoTextValueList", (Serializable) photoTextValueList);
                        Log.e("location", String.valueOf(locationList));

                        startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
                        /*} else {
                            clear();
                            recycleCheckpointAdapter.notifyItemChanged(position);
                            tempPoint = points;
                            position = post;
                            itemListDataAdapter = adapter;
                            itemsListData = imageList;
                            locationListData = locationList;
                            timeStampListData = timeStampList;
                            Intent intent = new Intent(IsolationActivity.this, CustomCameraActivity.class);
                            intent.putExtra("position", post);
                            startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);

//                            createImageName();
                        }
*/
                    }

                    @Override
                    public void onSwitchButtonClickListener(int post, String points, Context context, Switch aSwitch, RadioButton[] radioButtons, String alertDeparment, EditText editTexts, LinearLayout linearLayout) {

                        position = post;
                        if (aSwitch.isChecked()) {
                            submitDataTemp1.get(post).setAlert(aSwitch.getTextOn().toString());
                        } else {
                            submitDataTemp1.get(post).setAlert(aSwitch.getTextOff().toString());
                        }
                        if (radioButtons[0].isChecked()) {
                            submitDataTemp1.get(post).setSeverity(radioButtons[0].getText().toString());
                        }
                        if (radioButtons[1].isChecked()) {
                            submitDataTemp1.get(post).setSeverity(radioButtons[1].getText().toString());
                        }
                        if (radioButtons[2].isChecked()) {
                            submitDataTemp1.get(post).setSeverity(radioButtons[2].getText().toString());
                        }
                        submitDataTemp1.get(post).setDepartment(alertDeparment);
                        submitDataTemp1.get(post).setAlrt_cmnt(editTexts.getText().toString());

                        linearLayout.setVisibility(View.VISIBLE);
                        if (point.equals("")) {
                            point = points;
                        }

                        /*if (point.equals("") || point.equals(points)) {
                            if (aSwitch.isChecked()) {
                                alert = aSwitch.getTextOn().toString();
                            } else {
                                alert = aSwitch.getTextOff().toString();
                            }
                            if (radioButtons[0].isChecked()) {
                                severity = radioButtons[0].getText().toString();
                            }
                            if (radioButtons[1].isChecked()) {
                                severity = radioButtons[1].getText().toString();
                            }
                            if (radioButtons[2].isChecked()) {
                                severity = radioButtons[2].getText().toString();
                            }
                            department = alertDeparment;

                            alertComment = editTexts.getText().toString();
                            linearLayout.setVisibility(View.VISIBLE);
                            if (point.equals("")) {
                                point = points;
                            }
                        } else {
                            clear();
                            recycleCheckpointAdapter.notifyItemChanged(position);
                            position = post;
                            if (aSwitch.isChecked()) {
                                alert = aSwitch.getTextOn().toString();
                            } else {
                                alert = aSwitch.getTextOff().toString();
                            }
                            if (radioButtons[0].isChecked()) {
                                severity = radioButtons[0].getText().toString();
                            }
                            if (radioButtons[1].isChecked()) {
                                severity = radioButtons[1].getText().toString();
                            }
                            if (radioButtons[2].isChecked()) {
                                severity = radioButtons[2].getText().toString();
                            }
                            department = alertDeparment;

                            alertComment = editTexts.getText().toString();
                            linearLayout.setVisibility(View.VISIBLE);
                            if (point.equals("")) {
                                point = points;
                            }
                        }*/

                    }

                    @Override
                    public void onImageClickListener(int post, String points, ImageView imageView, Context context, ImageButton button) {

                        if (null == imageView.getDrawable()) {
                            Log.e("imageView Is null", "Null");
                        } else {
                            //imageViewDialog(context, imageView, button,recyclerView);
                        }

                    }

                    @Override
                    public void onSubmitButtonClickListener(int post, String points, RadioButton[] radioButtons,
                                                            ImageButton editText, Switch aSwitch, ImageButton button, TextView textView,
                                                            TextView editTextSubmit, String strDate,
                                                            LinearLayout linearLayout, ImageButton b, ImageButton photoButton,
                                                            ImageListDataAdapter adapter, List<String> imageList,
                                                            List<String> locationList, List<String> timeStampList,
                                                            List<String> photoTextValueList, Spinner spinner,
                                                            LinearLayout pointEditSubmitedLinearLayout, String isVisibleEditBtn) {
                        position = post;

                        if (aSwitch.isChecked()) {
                            submitDataTemp1.get(post).setAlert(aSwitch.getTextOn().toString());
                        } else {
                            submitDataTemp1.get(post).setAlert(aSwitch.getTextOff().toString());
                        }

                        if (radioButtons[0].isChecked()) {
                            submitDataTemp1.get(post).setAns(radioButtons[0].getText().toString());
                        } else if (radioButtons[1].isChecked()) {
                            submitDataTemp1.get(post).setAns(radioButtons[1].getText().toString());
                        } else {
                            submitDataTemp1.get(post).setAns(radioButtons[2].getText().toString());
                        }
                        submitDataTemp1.get(post).setChecklist_points(points);
                        submitDataTemp1.get(post).setQ(textView.getText().toString());
                        submitDataTemp1.get(post).setCrnt_dt_tm(strDate);
                        submitDataTemp1.get(post).setUid(Integer.valueOf(uid));
                        submitDataTemp1.get(post).setWrk_id(Integer.valueOf(wrk_id));
                        submitDataTemp1.get(post).setCrnt_lctn(crnt_lctn);
                        submitDataTemp1.get(post).setUser_id(userDetailsList.get(0).getUser_id());
                        submitDataTemp1.get(post).setSbmt_dtals(userDetailsList.get(0).getUser_id());
                        submitDataTemp1.get(post).setDone_by(userDetailsList.get(0).getFirst_name());

                        beforeLockedButton = b;
                        progressbar.setVisibility(View.VISIBLE);
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        itemListDataAdapter = adapter;
                        if (imageList != null && imageList.size() > 0) {
                            itemsListData = imageList;
                            locationListData = locationList;
                            timeStampListData = timeStampList;
                            photoTextValueData = photoTextValueList;
                        }


                        position = post;
//                                locationHelper.getLocation(IsolationActivity.this, IsolationActivity.this.locationResult);
                        q = textView.getText().toString();
                        if (point.equals("")) {
                            point = points;
                        }
                        if (radioButtons[0].isChecked()) {
                            ans = radioButtons[0].getText().toString();
                        } else if (radioButtons[1].isChecked()) {
                            ans = radioButtons[1].getText().toString();
                        } else {
                            ans = radioButtons[2].getText().toString();
                        }

                        crnt_dt = strDate;
                        submitBtnFlag = "clicked";
                        mAlertDialog(points, radioButtons, editText, aSwitch, button, editTextSubmit, linearLayout, b, photoButton, pointEditSubmitedLinearLayout, isVisibleEditBtn);

                    }

                    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
                        new AlertDialog.Builder(IsolationActivity.this)
                                .setMessage(message)
                                .setPositiveButton("OK", okListener)
                                .setNegativeButton("Cancel", null)
                                .create()
                                .show();
                    }

                    @Override
                    public void onGuidanceButtonClickListener(int post, String points, Context context) {
                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.guidance_layout);
                        dialog.setCancelable(false);
                        TextView textViewGuidance = dialog.findViewById(R.id.textGuidance);
                        ImageView imageView = dialog.findViewById(R.id.imageViewGuidance);
                        ImageView closeImage = dialog.findViewById(R.id.closeButtonGuidance);
                        getGuidanceData(post, textViewGuidance, imageView);

                        closeImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                        Window window = dialog.getWindow();
                        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    }

                    @Override
                    public void onRecordButtonClickListener(int post) {
                        posti = post;
                        VoiceRecordDialog(posti);
                    }

                    @Override
                    public void onTableViewClickListener(int post, String Cpoints) {
                        Toast.makeText(IsolationActivity.this, "clcik", Toast.LENGTH_SHORT).show();
                        showTableViewDialog(post, Cpoints);
                    }

                    @Override
                    public void onPlayButtonClickListener(int post, String Cpoints) {
                        posti = post;
                        showAudioList(posti, Cpoints);

                    }


                    @Override
                    public void onCommentButtonClickListener(int post, String points, Context context, TextView editText, LinearLayout linearLayout) {

                        position = post;
                        submitDataTemp1.get(post).setComment(editText.getText().toString());

                        if (point.equals("") || point.equals(points)) {
                            comment = editText.getText().toString();
                            linearLayout.setVisibility(View.VISIBLE);
                            if (point.equals("")) {
                                point = points;
                            }
                        } else {
                            clear();
                            recycleCheckpointAdapter.notifyItemChanged(position);
                            position = post;
                            comment = editText.getText().toString();
                            linearLayout.setVisibility(View.VISIBLE);
                            if (point.equals("")) {
                                point = points;
                            }
                        }

                    }

                    @Override
                    public void onAttachPositionClickListener(int post, Map<Integer, List<String>> imageMapLocal,
                                                              Map<Integer, List<String>> locationMapLocal,
                                                              Map<Integer, List<String>> timestampMapLocal,
                                                              Map<Integer, List<String>> photoTextValueMapLocal,
                                                              List<String> listPhotoLocal,
                                                              List<String> locationListLocal,
                                                              List<String> timeStampListLocal,
                                                              List<String> photoTextValueListLocal) {
                        position = post;
                        //Will do later photo attch issue //
//                        imageMap = imageMapLocal;
//                        locationMap = locationMapLocal;
//                        timeStampMap = timestampMapLocal;
//                        listPhoto = listPhotoLocal;
//                        locationList = locationListLocal;
//                        timeStampList = timeStampListLocal;
                        String state = Environment.getExternalStorageState();
                        if (Environment.MEDIA_MOUNTED.equals(state)) {
                            if (Build.VERSION.SDK_INT >= 23) {
                                if (checkPermission()) {
                                    String path = Environment.getExternalStorageDirectory().toString();
                                    File file = new File(path, "UniqueFileName" + ".jpg");
                                    if (!file.exists()) {
                                        Log.d("path", file.toString());

                                        try {
                                            FileOutputStream fos = new FileOutputStream(file);
                                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                                            Log.e("bitmap", String.valueOf(bitmap));
                                            fos.flush();
                                            fos.close();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } else {
                                    requestPermission(); // Code for permission
                                }
                            } else {
                                String path = Environment.getExternalStorageDirectory().toString();
                                File file = new File(path, "UniqueFileName" + ".jpg");
                                if (!file.exists()) {
                                    Log.d("path", file.toString());
                                    FileOutputStream fos = null;
                                    try {
                                        fos = new FileOutputStream(file);
                                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                                        fos.flush();
                                        fos.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                    }

                    @Override
                    public void onPhotoTextValue(int post, String photoTextValue, String imageName, Context context, int k) {
                        photoTextValueList.add(k, photoTextValue);
                        photoTextValueMap.put(post, photoTextValueList);
                        Log.e("photoTextValueMap", String.valueOf(photoTextValueMap));
                        try {
                            imageJson.getJSONObject(imageName).put("photoTextValue", photoTextValue);
                            itemListDataAdapter.notifyDataSetChanged();
                            Log.e("eeeee", String.valueOf(imageJson));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onEditButtonClickListener(int post, Context context, String points, Switch aSwitch, RadioButton[] radioButtons, String txt, List<Boolean> booleanList) {


                        JSONObject jsonObject = new JSONObject();
                        beforeLockedButton = null;
                        editUpdate = post;
                        viewEnabled = booleanList;

                        if (txt.equalsIgnoreCase("update")) {
                            date = new Date();
                            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String strDate = formatter.format(date);
                            if (radioButtons[0].isChecked()) {
                                submitDataTemp1.get(post).setAns(radioButtons[0].getText().toString());
                            } else if (radioButtons[1].isChecked()) {
                                submitDataTemp1.get(post).setAns(radioButtons[1].getText().toString());
                            } else {
                                submitDataTemp1.get(post).setAns(radioButtons[2].getText().toString());
                            }
                            if (aSwitch != null) {
                                if (aSwitch.isChecked()) {
                                    submitDataTemp1.get(post).setAlert(aSwitch.getTextOn().toString());
                                } else {
                                    submitDataTemp1.get(post).setAlert(aSwitch.getTextOff().toString());
                                }
                            }

                            submitDataTemp1.get(editUpdate).setChecklist_points(points);

                            String point = submitDataTemp1.get(editUpdate).getChecklist_points();
                            String q = submitDataTemp1.get(editUpdate).getQ();
                            String ans = submitDataTemp1.get(editUpdate).getAns();
                            String alert = submitDataTemp1.get(editUpdate).getAlert();
                            String alertSevrity = submitDataTemp1.get(editUpdate).getSeverity();
                            String alertDepartment = submitDataTemp1.get(editUpdate).getDepartment();
                            String alertComment = submitDataTemp1.get(editUpdate).getAlrt_cmnt();
                            String cmnt = submitDataTemp1.get(editUpdate).getComment();
                            String crnt_dt = submitDataTemp1.get(editUpdate).getCrnt_dt_tm();
                            String user_id = submitDataTemp1.get(editUpdate).getUser_id();
                            String sbmtDtals = submitDataTemp1.get(editUpdate).getSbmt_dtals();
                            String done_by = submitDataTemp1.get(editUpdate).getDone_by();

                            try {
                                jsonObject.put("uid", uid);
                                jsonObject.put("wrk_id", wrk_id);
                                jsonObject.put("checklist_points", points);
                                jsonObject.put("q", q);
                                jsonObject.put("ans", ans);
                                jsonObject.put("comment", cmnt);
                                jsonObject.put("alert", submitDataTemp1.get(editUpdate).getAlert());
                                jsonObject.put("severity", submitDataTemp1.get(editUpdate).getSeverity());
                                jsonObject.put("department", submitDataTemp1.get(editUpdate).getDepartment());
                                jsonObject.put("alrt_cmnt", submitDataTemp1.get(editUpdate).getAlrt_cmnt());
                                jsonObject.put("crnt_dt_tm", strDate);
                                jsonObject.put("crnt_lctn", crnt_lctn);
                                jsonObject.put("rank_id", submitDataTemp1.get(editUpdate).getRank_id());
                                jsonObject.put("user_id", userDetailsList.get(0).getUser_id());
                                jsonObject.put("imageJson", submitDataTemp1.get(editUpdate).getImageJson());
                                jsonObject.put("dbnm", userDetailsList.get(0).getDbnm());
                                jsonObject.put("accessToken", userDetailsList.get(0).getAccessToken());
                                jsonObject.put("done_by", submitDataTemp1.get(editUpdate).getDone_by());

                                String dataKey = "update_details";
                                MultipartUtility multipart = new MultipartUtility(ApiUrlClass.updateData, ApiUrlClass.charset, dataKey, jsonObject);
                                List<File> file1 = new ArrayList<>();

                                if (itemsListData != null) {
                                    for (int i = 0; i < itemsListData.size(); i++) {
                                        file1.add(i, new File(itemsListData.get(i)));
                                    }
                                    multipart.addFilePart("files", file1);
                                }


                                List<String> response = multipart.finish();
                                Log.e("SERVER REPLIED:", String.valueOf(response));

                                for (String line : response) {
                                    System.out.println(line);
                                    JSONObject jsonObject1 = new JSONObject(line);
                                    Integer res = jsonObject1.getInt("response");
                                    if (res.equals(200)) {
                                        if (alert.equalsIgnoreCase("yes")) {
                                            setAlertDataInRow(wrk_id, alertSevrity, alertDepartment, alertComment, alert, points);
                                        } else {
                                            setAlertDataInRow(wrk_id, alertSevrity, alertDepartment, alertComment, alert, points);
                                        }
                                        recycleCheckpointAdapter.notifyItemChanged(position);
                                    } else if (res.equals(10003)) {
                                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    }

                });

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                RecyclerView.LayoutManager linearLayoutmanager = new LinearLayoutManager(IsolationActivity.this);
                if (linearLayoutmanager != null) {

                    recyclerView.setLayoutManager(linearLayoutmanager);
                    recyclerView.setHasFixedSize(true);

//                    RecyclerSectionItemDecoration sectionItemDecoration =
//                            new RecyclerSectionItemDecoration(getResources().getDimensionPixelSize(R.dimen._15sdp), true, getSectionCallback(componentList));
//                    recyclerView.addItemDecoration(sectionItemDecoration);

                    recyclerView.setAdapter(recycleCheckpointAdapter);
//                 chceklisttitle=  componentList.get(position).getTitle_of_section();

                }


            }
        });


    }

    private void setAlertDataInRow(String wrk_id, String severity, String depert, String arltCommnt, String alert, String points) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().updateRecord4(Integer.valueOf(wrk_id), severity, depert, arltCommnt, alert, points);
            }
        }).start();
    }

    private void getGuidanceData(int post, TextView textView, ImageView imageView) {
        checkListDatabase = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        List<Component> guidancelist = new ArrayList<>();
        guidancelist = checkListDatabase.productDao().getComponentByUid(uid);

        if (guidancelist.get(post).getGuidance_text().contains(guidancelist.get(post).getGuidance_text())) {
            String guidanceText = guidancelist.get(post).getGuidance_text();
            textView.setText(guidanceText);

        }
        if (guidancelist.get(post).getGuidance_image().contains("https://")) {
            String guidanceImage = guidancelist.get(post).getGuidance_image();
            Picasso.with(this)
                    .load(guidanceImage)
                    .into(imageView);
        }
    }

    private void mAlertDialog(String points, RadioButton[] radioButtons, ImageButton editText, Switch
            aSwitch, ImageButton button, TextView editTextSubmit, LinearLayout linearLayout,
                              ImageButton b, ImageButton photoButton, LinearLayout pointEditSubmitedLinearLayout, String isVisibleEditBtn) {


        point = points;
        submitEditText = editTextSubmit;
        this.button = button;
        this.photoButton = photoButton;
        this.radioButtons = radioButtons;
        this.editText = editText;
        this.aSwitch = aSwitch;
        this.b = b;
        this.linearLayout = linearLayout;
        this.pointEditSubmitedLinearLayout = pointEditSubmitedLinearLayout;
        this.isVisibleEditBtn = isVisibleEditBtn;
        if (selfy_on_submit.equalsIgnoreCase("true")) {

            BiometricManager biometricManager = BiometricManager.from(this);
            switch (biometricManager.canAuthenticate()) {
                case BiometricManager.BIOMETRIC_SUCCESS:
                    alertDialog = new AlertDialog.Builder(IsolationActivity.this);
                    alertDialog.setTitle("Alert Dialog");
                    alertDialog.setIcon(R.drawable.ewarpletest);
                    alertDialog.setMessage("Do you want to submit ?");
                    alertDialog.setCancelable(false);

                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
//                            dialog = new Dialog(IsolationActivity.this);
//                            dialog.setContentView(R.layout.fingerprint_dialog);
//                            dialog.setCancelable(false);
//                            ImageView mfingerprint = dialog.findViewById(R.id.dialogFingerprint);
//                            doneImageView = mfingerprint;
//
//                            dialog.show();
//                            Window window = dialog.getWindow();
//                            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//
//                            ImageView imgClose = dialog.findViewById(R.id.imgClose);
//                            imgClose.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    dialog.dismiss();
//                                }
//                            });
//                            verification();
                            biometricPrompt.authenticate(promptInfo);
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progressbar.setVisibility(View.GONE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            beforeLockedButton = null;
                        }
                    });
                    alertDialog.show();
                    break;
                case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                    submitBtnFlag = "start";
                    Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                    break;
                case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                    submitBtnFlag = "start";
                    Intent intent1 = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                    startActivityForResult(intent1, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                    break;
                case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                    Log.e("MY_APP_TAG", "The user hasn't associated " +
                            "any biometric credentials with their account.");
                    break;
            }

          /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
                if (fingerprintManager.isHardwareDetected()) {
                    alertDialog = new AlertDialog.Builder(IsolationActivity.this);
                    alertDialog.setTitle("Alert Dialog");
                    alertDialog.setIcon(R.drawable.ewarpletest);
                    alertDialog.setMessage("Do you want to submit ?");
                    alertDialog.setCancelable(false);

                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            dialog = new Dialog(IsolationActivity.this);
                            dialog.setContentView(R.layout.fingerprint_dialog);
                            dialog.setCancelable(false);
                            ImageView mfingerprint = dialog.findViewById(R.id.dialogFingerprint);
                            doneImageView = mfingerprint;

                            dialog.show();
                            Window window = dialog.getWindow();
                            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            ImageView imgClose = dialog.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

                            verification();
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progressbar.setVisibility(View.GONE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            beforeLockedButton = null;
                        }
                    });
                    alertDialog.show();

                } else {
                    submitBtnFlag = "start";
                    Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }
            } else {
                submitBtnFlag = "start";
                Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

            }*/
        } else {
            submitDataMethod();
        }

    }

    private void SubmitDataOnServer() {
        JSONObject submit_dtls = new JSONObject();
        try {
            /*submit_dtls.put("uid", uid);
            submit_dtls.put("wrk_id", wrk_id);
            submit_dtls.put("checklist_points", point);
            submit_dtls.put("q", q);
            submit_dtls.put("ans", ans);
            submit_dtls.put("comment", comment);
            submit_dtls.put("alert", alert);
            submit_dtls.put("severity", severity);
            submit_dtls.put("department", department);
            submit_dtls.put("alrt_cmnt", alertComment);
            submit_dtls.put("crnt_dt_tm", crnt_dt);
            submit_dtls.put("crnt_lctn", crnt_lctn + "_" + latitude + "_" + longitude);
            submit_dtls.put("rank_id", rank_id);
            submit_dtls.put("user_id", emp_id);
            submit_dtls.put("imageJson", imageJson.toString());
            submit_dtls.put("dbnm", userDetailsList.get(0).getDbnm());
            submit_dtls.put("accessToken", userDetailsList.get(0).getAccessToken());
            submit_dtls.put("done_by", userDetailsList.get(0).getFirst_name());
*/


            submit_dtls.put("uid", submitDataTemp1.get(position).getUid());
            submit_dtls.put("wrk_id", submitDataTemp1.get(position).getWrk_id());
            submit_dtls.put("checklist_points", submitDataTemp1.get(position).getChecklist_points());
            submit_dtls.put("q", submitDataTemp1.get(position).getQ());
            submit_dtls.put("ans", submitDataTemp1.get(position).getAns());
            submit_dtls.put("comment", submitDataTemp1.get(position).getComment());
            submit_dtls.put("alert", submitDataTemp1.get(position).getAlert());
            submit_dtls.put("severity", submitDataTemp1.get(position).getSeverity());
            submit_dtls.put("department", submitDataTemp1.get(position).getDepartment());
            submit_dtls.put("alrt_cmnt", submitDataTemp1.get(position).getAlrt_cmnt());
            submit_dtls.put("crnt_dt_tm", submitDataTemp1.get(position).getCrnt_dt_tm());
            submit_dtls.put("crnt_lctn", submitDataTemp1.get(position).getCrnt_lctn());
            submit_dtls.put("rank_id", submitDataTemp1.get(position).getRank_id());
            submit_dtls.put("user_id", submitDataTemp1.get(position).getUser_id());
            submit_dtls.put("imageJson", submitDataTemp1.get(position).getImageJson());
            submit_dtls.put("dbnm", userDetailsList.get(0).getDbnm());
            submit_dtls.put("accessToken", userDetailsList.get(0).getAccessToken());
            submit_dtls.put("done_by", submitDataTemp1.get(position).getDone_by());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("submit", String.valueOf(submit_dtls));
        try {

            RequestBody json = RequestBody.create(MediaType.parse("application/json"), String.valueOf(submit_dtls));

            MultipartBody.Part[] surveyImagesParts = null;
            if (itemsListData != null && itemsListData.size() > 0) {
                surveyImagesParts = new MultipartBody.Part[itemsListData.size()];
                for (int index = 0; index < itemsListData.size(); index++) {
                    Logger.printLog("IMAGE_PATH" + index, ":: " + "  " + itemsListData.get(index));
                    File file = new File(itemsListData.get(index));
//                    RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
                    surveyImagesParts[index] = MultipartBody.Part.createFormData("files", file.getName(), surveyBody);
                }
            } else {
//                surveyImagesParts = new MultipartBody.Part[0];
            }

            ArrayList<String> audioList1112 = audioFileMap.get(position);
            MultipartBody.Part[] surveyAudioParts = null;
            if (audioList1112 != null && audioList1112.size() > 0) {
                surveyAudioParts = new MultipartBody.Part[audioList1112.size()];
                for (int index = 0; index < audioList1112.size(); index++) {
                    Logger.printLog("AUDIO_PATH" + index, ":: " + "  " + audioList1112.get(index));
                    File file = new File(audioList1112.get(index));
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    surveyAudioParts[index] = MultipartBody.Part.createFormData("audioFiles", file.getName(), surveyBody);
                }
            } else {
//                surveyAudioParts = new MultipartBody.Part[0];
            }

            RetrofitInstance.getRetrofitInstance()
                    .uploadFile(json,
                            surveyImagesParts,
                            surveyAudioParts)
                    .enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            if (response.isSuccessful()) {
                                try {
                                    Logger.printLog("RESPONSE", response.body().toString());
                                    String line = response.body().toString();

//                                    System.out.println(line);
                                    JSONObject jsonObject = new JSONObject(line);
//                                    Integer res = jsonObject.getInt("response");
                                    if (jsonObject.has("response")
                                            && jsonObject.getInt("response") == 200) {

                                        if (linearLayout.getVisibility() == View.GONE) {
                                            linearLayout.setVisibility(View.VISIBLE);
                                        }
                                        if (selfy_on_submit.equalsIgnoreCase("false")) {
                                            handlerDelayTime = 2000;
                                        }

//                                        mHandler.postDelayed(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                        progressbar.setVisibility(View.GONE);
                                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                        new ButtonLock().buttonLock(radioButtons, editText, button, aSwitch, b, photoButton, pointEditSubmitedLinearLayout, isVisibleEditBtn);
                                        Intent i = new Intent(IsolationActivity.this, RefreshDataOnSubmit.class);
                                        i.putExtra("wrk_id", wrk_id);
                                        i.putExtra("uid", uid);
                                        i.putExtra("checklist_type", checklist_type);
                                        startService(i);
//                                            }
//                                        }, handlerDelayTime);
                                        submitData(points, radioButtons, editText, aSwitch, button, 1);
                                        Toast.makeText(IsolationActivity.this, "Sumited sucessfully", Toast.LENGTH_SHORT).show();
                                    } else if (jsonObject.has("response")
                                            && jsonObject.getInt("response") == 10003) {
                                        new LogoutClass().logoutMethod(IsolationActivity.this);
                                    } else {
                                        if (jsonObject.has("error")) {
                                            Logger.showToast(IsolationActivity.this, jsonObject.getString("error"));
                                        }
                                    }

                                } catch (Exception e) {
                                    progressbar.setVisibility(View.GONE);
                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    Logger.printLog("submit resp exc", "" + e.getMessage());
                                }
                            } else {
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                progressbar.setVisibility(View.GONE);
                                Logger.printLog("submit 500 Resp", "" + response.errorBody());
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Logger.printLog("OnSubmit failed", t.getMessage());
                            Logger.showToast(IsolationActivity.this, "Your internet connection is very slow. We are saving the data in offline and sync when the internet connection will good");
                            if (linearLayout.getVisibility() == View.GONE) {
                                linearLayout.setVisibility(View.VISIBLE);
                            }
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                            progressbar.setVisibility(View.GONE);
                            submitData(points, radioButtons, editText, aSwitch, button, 0);
                            new ButtonLock().buttonLock(radioButtons, editText, button, aSwitch, b, photoButton, pointEditSubmitedLinearLayout, isVisibleEditBtn);
                        }
                    });

//            String dataKey = "submit_dtls";
//            MultipartUtility multipart = new MultipartUtility(ApiUrlClass.submitdata, ApiUrlClass.charset, dataKey, submit_dtls);
//            Log.e("submit1", String.valueOf(multipart));
//            List<File> file1 = new ArrayList<>();
//            List<File> audioFile = new ArrayList<>();
////            for (List<String> lis : imageMap.values()) {
////                itemsListData.addAll(lis);
////            }
//            if (itemsListData != null && itemsListData.size() > 0) {
//                for (int i = 0; i < itemsListData.size(); i++) {
//                    file1.add(i, new File(itemsListData.get(i)));
//                }
//                multipart.addFilePart("files", file1);
//                Log.e("file", String.valueOf(file1));
//            }
//            ArrayList<String> audioList111 = audioFileMap.get(position);
//            if (audioList111 != null && audioList111.size() > 0) {
//                for (int i = 0; i < audioList111.size(); i++) {
//                    audioFile.add(i, new File(audioList111.get(i)));
//                }
//                multipart.addFilePart("audioFiles", audioFile);
//            }

//            if (audioList != null) {
//                for (int i = 0; i < audioList.size(); i++) {
//                    audioFile.add(i, new File(audioList.get(i)));

//                }
//                multipart.addFilePart("audioFiles", audioFile);
//            }


//            List<String> response = multipart.finish();
//            Log.e("SERVER REPLIED:", String.valueOf(response));

//            for (String line : response) {
//                System.out.println(line);
//                JSONObject jsonObject = new JSONObject(line);
//                Integer res = jsonObject.getInt("response");
//                if (res.equals(200)) {
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (linearLayout.getVisibility() == View.GONE) {
//                                linearLayout.setVisibility(View.VISIBLE);
//                            }
//                        }
//                    });
//
//                    if (selfy_on_submit.equalsIgnoreCase("false")) {
//                        handlerDelayTime = 2000;
//                    }
////                    Toast.makeText(IsolationActivity.this, "Submitted sucessfully", Toast.LENGTH_SHORT).show();
//
//                    mHandler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            progressbar.setVisibility(View.GONE);
//                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                            new ButtonLock().buttonLock(radioButtons, editText, button, aSwitch, b, photoButton, pointEditSubmitedLinearLayout, isVisibleEditBtn);
//                            Intent i = new Intent(IsolationActivity.this, RefreshDataOnSubmit.class);
//                            i.putExtra("wrk_id", wrk_id);
//                            i.putExtra("uid", uid);
//                            i.putExtra("checklist_type", checklist_type);
//                            startService(i);
//                        }
//                    }, handlerDelayTime);
//                } else if (res.equals(10003)) {
//                    new LogoutClass().logoutMethod(IsolationActivity.this);
//                }
//
//            }

        } catch (Exception e) {
            e.printStackTrace();
            progressbar.setVisibility(View.GONE);
            Logger.printLog("Submit Exc", "" + e.getMessage());
            if (linearLayout.getVisibility() == View.GONE) {
                linearLayout.setVisibility(View.VISIBLE);
            }
        }


    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(IsolationActivity.this, WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(IsolationActivity.this, WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(IsolationActivity.this, "Write External Storage permission allows us to save files. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(IsolationActivity.this, new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                    Log.e("bitmap1", String.valueOf(bitmap));


                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
            case REQUEST_AUDIO_PERMISSION_CODE:
                if (grantResults.length > 0) {
                    boolean permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean permissionToStore = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (permissionToRecord && permissionToStore) {
                        Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    private void imageViewDialog(Context context, ImageView imageView, ImageButton button, RecyclerView recyclerView) {
        if (null == imageView.getDrawable()) {
            Log.e("imageView", "imageView is null");
        } else {
            BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
            bitmapUndo = drawable.getBitmap();
        }

        bitmap = new ImageHandler().imgView(imageView, bitmap);
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.image_view_layout);
        final ImageView imageView57 = dialog.findViewById(R.id.imageView57);
        Button closeButton = dialog.findViewById(R.id.closeButton);
        Button editButton = dialog.findViewById(R.id.editButton);
        Button saveButton = dialog.findViewById(R.id.saveButton);
        Button undoButton = dialog.findViewById(R.id.undoButton);
        imageView57.setImageBitmap(bitmap);
        if (button.isEnabled()) {
            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    imageView57.setOnTouchListener(new ImageEventHandler(imageView57, imageView));
                    undoButton.setEnabled(true);
                    saveButton.setEnabled(true);
                }
            });

            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String in1 = imageFileName;
                    new ImageData(imageView57, in1, imageView).saveFile();
                    saveButton.setEnabled(false);
                    undoButton.setEnabled(false);
                    BitmapDrawable drawable = (BitmapDrawable) imageView57.getDrawable();
                    bitmapUndo = drawable.getBitmap();
                    imageView.setImageBitmap(bitmapUndo);
                    dialog.dismiss();
                }
            });

            undoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    imageView57.setImageBitmap(bitmapUndo);
                    imageView.setImageBitmap(bitmapUndo);
                    saveButton.setEnabled(false);
                    undoButton.setEnabled(false);

                }
            });

        }
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void createImageName() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "IMG_" + timeStamp + ".jpg";
//        onLaunchCamera();
    }


    public void onLaunchCamera() {

        // create Intent to take a picture and return control to the calling application
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Create a File reference to access to future access
        photoFile = getPhotoFileUri(imageFileName);

        // wrap File object into a content provider
        fileProvider = FileProvider.getUriForFile(IsolationActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider);
        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Start the image capture intent to take photo
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
    }

    // Returns the File for a photo stored on disk given the fileName
    public File getPhotoFileUri(String fileName) {

        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);

        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d(APP_TAG, "failed to create directory");
        }

        // Return the file target for the photo based on filename
//        file = new File(mediaStorageDir.getPath() + File.separator + fileName +timeStamp + ".jpg");
        file = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return file;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SECOND_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK && data != null) {

            // TODO Extract the data returned from the child Activity.
            try {
                imageJson = new JSONObject(data.getStringExtra("imageJson"));
                position = data.getIntExtra("post", 0);
                imageMap = (HashMap<Integer, List<String>>) data.getSerializableExtra("imageMap");
                locationMap = (HashMap<Integer, List<String>>) data.getSerializableExtra("locationMap");
                timeStampMap = (HashMap<Integer, List<String>>) data.getSerializableExtra("timeStampMap");
                photoTextValueMap = (HashMap<Integer, List<String>>) data.getSerializableExtra("photoTextValueMap");
//                listPhoto = (List<String>) data.getSerializableExtra("photolist");
                locationList = (List<String>) data.getSerializableExtra("locationList");
                timeStampList = (List<String>) data.getSerializableExtra("timeStampList");
                photoTextValueList = (List<String>) data.getSerializableExtra("photoTextValueList");

//                Log.e("imageJson..", String.valueOf(imageJson));

                submitDataTemp1.get(position).setImageJson(String.valueOf(imageJson));

                recycleCheckpointAdapter.setImageMap(imageMap, locationMap, timeStampMap, photoTextValueMap);
                recycleCheckpointAdapter.notifyItemChanged(position);
                photo = String.valueOf(imageMap.values());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(IsolationActivity.this, "It will take a while, don't go back", Toast.LENGTH_LONG).show();
                photofilepath = data.getStringExtra("BitmapImage");
//                Log.e("filepat", "test" + photofilepath);
                if (flag == 1) {
                    try {
                        takenImage = BitmapFactory.decodeFile(photofilepath);
                        FileOutputStream fos = new FileOutputStream(photofilepath);
                        Log.e("J", photofilepath);
                        takenImage.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                        fos.close();
                        cancelCompleteMethod(status);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {

//                    if(photofilepath!=null){
//                        Intent intent = getIntent();
//                        photofilepath = intent.getStringExtra("BitmapImage");
//                        Log.e("filepat",photofilepath);
//
//                    }
//                        takenImage = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
//                        ExifInterface ei = null;
//                        try {
//                            ei = new ExifInterface(String.valueOf(photoFile.getAbsolutePath()));
//
//                            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
//                                    ExifInterface.ORIENTATION_UNDEFINED);
//
//
//                            switch (orientation) {
//
//                                case ExifInterface.ORIENTATION_ROTATE_90:
//                                    rotatedBitmap = rotateImage(takenImage, 90);
//                                    break;
//
//                                case ExifInterface.ORIENTATION_ROTATE_180:
//                                    rotatedBitmap = rotateImage(takenImage, 180);
//                                    break;
//
//                                case ExifInterface.ORIENTATION_ROTATE_270:
//                                    rotatedBitmap = rotateImage(takenImage, 270);
//                                    break;
//
//                                case ExifInterface.ORIENTATION_NORMAL:
//                                default:
//                                    rotatedBitmap = takenImage;
//                            }
//

//                        String timeStamp = new SimpleDateFormat("yyyy/MM/dd_HH:mm:ss").format(new Date());
//                        Bitmap dest = Bitmap.createBitmap(rotatedBitmap.getWidth(), rotatedBitmap.getHeight(), Bitmap.Config.ARGB_8888);
//                        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
//                        float scaledTextSize = rotatedBitmap.getWidth() * 70 / displayMetrics.widthPixels;
//                        Canvas cs = new Canvas(dest);
//                        Paint paint = new Paint();
//                        paint.setTextSize(scaledTextSize);
//                        paint.setFakeBoldText(true);
//                        paint.setColor(Color.WHITE);
//                        paint.setStyle(Paint.Style.FILL);
//                        cs.drawBitmap(rotatedBitmap, 0f, 0f, null);
//                        float height = paint.measureText("yY");
//                        float width = paint.measureText("Done by: " + userDetailsList.get(0).getFirst_name());
//                        float startXPosition = (rotatedBitmap.getWidth() - width) / 9;
//                        cs.drawText("Done by: " + userDetailsList.get(0).getFirst_name(), startXPosition, rotatedBitmap.getHeight() / 9 - height + 15f, paint);
//                        //Bitmap bitmap  = MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
//                        FileOutputStream fos = new FileOutputStream(photoFile.getAbsolutePath());
//                        dest.compress(Bitmap.CompressFormat.JPEG, 40, fos);
//
//                        fos.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
////***********************Bitmap convert base64 and than convert bitmap for resizing*******
//                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
//                    byte[] byteArray = byteArrayOutputStream.toByteArray();
//                    encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                    BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inSampleSize = 4;
//                    //decode base64 string to image
//                    byte[] imageBytes = Base64.decode(encodedImage, Base64.DEFAULT);
//                    Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, options);
                    Log.e("test", photofilepath);
                    listPhoto.add(photofilepath);
                    imageFileName = new File(photofilepath).getName();

                    itemsListData = imageMap.get(position);
                    locationList = locationMap.get(position);
                    timeStampList = timeStampMap.get(position);
                    photoTextValueList = photoTextValueMap.get(position);

//                    listPhoto.add(photoFile.getAbsolutePath());
                    JSONObject temp = new JSONObject();
                    try {
                        String strTimeStamp = new UtilClassFuntions().currentDateTime();
                        temp.put("location", crnt_lctn);
                        temp.put("timeStamp", strTimeStamp);
                        temp.put("photoTextValue", "");
                        temp.put("latitude", latitude);
                        temp.put("longitude", longitude);
                        imageJson.put(imageFileName, temp);

//                        itemsListData.add(photofilepath);
                        locationList.add(crnt_lctn);
                        timeStampList.add(strTimeStamp);
                        if (photoTextValueList == null) {
                            photoTextValueList = new ArrayList<>();
                        }
                        photoTextValueList.add("");

                        submitDataTemp1.get(position).setImageJson(imageJson.toString());
//                        Log.e("Imagejsn", String.valueOf(imageJson));
//                        Log.e("temp", String.valueOf(temp));
//                        Log.e("imageFileName", String.valueOf(imageFileName));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        if (itemsListData == null) {
                            itemsListData = new ArrayList<>();
                        }
                        itemsListData.add(photofilepath);
                        imageMap.put(position, itemsListData);
                        locationMap.put(position, locationList);
                        timeStampMap.put(position, timeStampList);
                        photoTextValueMap.put(position, photoTextValueList);
                        recycleCheckpointAdapter.setImageMap(imageMap, locationMap, timeStampMap, photoTextValueMap);
                        recycleCheckpointAdapter.notifyDataSetChanged();
                        photo = String.valueOf(imageMap.values());

                        if (point.equals("")) {
                            point = tempPoint;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (submitBtnFlag.equalsIgnoreCase("start")) {
                        submitDataMethod();
                    }
                }
            }
        }
        if (requestCode == SELECT_IMAGE && resultCode == RESULT_OK && data != null) {
//            Log.e("image", String.valueOf(imageMap));
//            Log.e("image1", String.valueOf(locationMap));
//            Log.e("image2", String.valueOf(timeStamp));
//            Log.e("image3", String.valueOf(photoTextValueMap));
            ArrayList<String> mArrayUri = new ArrayList<String>();

            if (imageMap.get(position).size() == 5) {
                return;
            }
            itemsListData = new ArrayList<>();
            itemsListData.addAll(imageMap.get(position));
            locationList.addAll(locationMap.get(position));
            timeStampList.addAll(timeStampMap.get(position));
            if (photoTextValueMap.get(position) != null) {
                photoTextValueList.addAll(photoTextValueMap.get(position));
            }

            if (data.getClipData() != null) {
                try {

                    String imagePath = "";

                    ClipData clipData = data.getClipData();
                    int count = clipData.getItemCount();
                    if (count > 5) {
                        count = 5;
                        Toast.makeText(IsolationActivity.this, "5 Photos are allowed per check point. Thanks.", Toast.LENGTH_SHORT).show();
                    }
                    if (count >= (5 - itemsListData.size())) {
                        count = 5 - itemsListData.size();
                    }
                    for (int i = 0; i < count; i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        Uri imageUri = item.getUri();
                        //-------------------Decrease large image uri size -------------------
                        File file = FileUtil.from(this, imageUri);

//                        File compressedImgFile = new Compressor(this).compressToFile(file);
//                        long length = compressedImgFile.length();
//                        length = length / 1024;
//                        Uri imageurione = Uri.fromFile(new File(compressedImgFile.getAbsolutePath()));
                        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);
                        Compressor compressor = new Compressor(this);
                        compressor.setDestinationDirectoryPath(mediaStorageDir.getPath()); // set path
                        File newFile = compressor.compressToFile(file);
                        Uri imageurione = Uri.fromFile(newFile);

//---------------------------------------
                        imagePath = PathUtil.getPath(this, imageurione);
                        Log.e("tst", imagePath);
//                      String subimagePath = imagePath.substring(imagePath.lastIndexOf("/"));
//                      file   = getPhotoFilePath(subimagePath);
//                        imagePath = file.getAbsolutePath();

                        Log.d("pathh", String.valueOf(imagePath));
                        mArrayUri.add(imagePath);
                        imageFileName = imagePath.substring(imagePath.lastIndexOf("/") + 1);
                        listPhoto.add(imagePath);
                        itemsListData.add(imagePath);
                        locationList.add(address);
                        timeStampList.add(new UtilClassFuntions().currentDateTime());
//                            cursor.close();
                        JSONObject temp = new JSONObject();
                        try {
                            String strTimeStamp = new UtilClassFuntions().currentDateTime();
                            temp.put("location", crnt_lctn);
                            temp.put("timeStamp", strTimeStamp);
                            temp.put("photoTextValue", "");
                            temp.put("latitude", latitude);
                            temp.put("longitude", longitude);
                            imageJson.put(imageFileName, temp);
                            locationList.add(crnt_lctn);
                            timeStampList.add(strTimeStamp);
                            photoTextValueList.add("");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    submitDataTemp1.get(position).setImageJson(imageJson.toString());
                    imageMap.put(position, itemsListData);
                    timeStampMap.put(position, timeStampList);
                    locationMap.put(position, locationList);
                    photoTextValueMap.put(position, photoTextValueList);

                    recycleCheckpointAdapter.setImageMap(imageMap, locationMap, timeStampMap, photoTextValueMap);
                    recycleCheckpointAdapter.notifyItemChanged(position);
                    photo = String.valueOf(imageMap.values());

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                if (data.getData() != null) {
                    Uri imgUri = data.getData();
//                content://com.android.providers.media.documents/document/image%3A63216
//                content://media/external/images/media/58064
                    try {
                        File file = FileUtil.from(this, imgUri);
                        File SinglecompressedImgFile = null;
//                        SinglecompressedImgFile = new Compressor(this).compressToFile(file);
//                        long length = SinglecompressedImgFile.length();
//                        length = length / 1024;
//                        Uri imageurione = Uri.fromFile(new File(SinglecompressedImgFile.getAbsolutePath()));

                        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);
                        Compressor compressor = new Compressor(this);
                        compressor.setDestinationDirectoryPath(mediaStorageDir.getPath()); // set path
                        SinglecompressedImgFile = compressor.compressToFile(file);
                        Uri imageurione = Uri.fromFile(SinglecompressedImgFile);

                        String filePath = PathUtil.getPath(this, imageurione);
                        imageFileName = filePath.substring(filePath.lastIndexOf("/") + 1);

//                        String subimagePath = filePath.substring(filePath.lastIndexOf("/"));
//                        String imagepathh = getPhotoFileUriPath(subimagePath).getAbsolutePath();
//                        imageFileName = imagepathh.substring(filePath.lastIndexOf("/") + 1);

//                        Log.e("pathone",imagepathh);

                        listPhoto.add(filePath);
                        itemsListData.add(filePath);
                    /*locationList.add(address);
                    timeStampList.add(new UtilClassFuntions().currentDateTime());*/
                        JSONObject temp = new JSONObject();
                        try {
                            String strTimeStamp = new UtilClassFuntions().currentDateTime();
                            temp.put("location", crnt_lctn);
                            temp.put("timeStamp", strTimeStamp);
                            temp.put("photoTextValue", "");
                            temp.put("latitude", latitude);
                            temp.put("longitude", longitude);
                            imageJson.put(imageFileName, temp);
                            locationList.add(crnt_lctn);
                            timeStampList.add(strTimeStamp);
                            photoTextValueList.add("");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        submitDataTemp1.get(position).setImageJson(imageJson.toString());
                        imageMap.put(position, itemsListData);
                        timeStampMap.put(position, timeStampList);
                        locationMap.put(position, locationList);
                        photoTextValueMap.put(position, photoTextValueList);
                        recycleCheckpointAdapter.setImageMap(imageMap, locationMap, timeStampMap, photoTextValueMap);
                        recycleCheckpointAdapter.notifyDataSetChanged();
                        photo = String.valueOf(imageMap.values());

//                    mArrayUri.add(filePath);
//                    getImageFilePath(mArrayUri);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

        }

        if (requestCode == AUDIO_LIST_UPSATE && resultCode == RESULT_OK && data != null) {
            data.getStringArrayListExtra("fileNameList");
            data.getStringArrayListExtra("addressList");
            int pos = data.getIntExtra("position", -1);
            audioFileMap.put(pos, data.getStringArrayListExtra("fileNameList"));
//            audioFileMapAddress.put(pos, data.getStringArrayListExtra("addressList"));
//            Log.e("adress", audioFileMap.toString());
        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Bitmap TableBitmap = (Bitmap) data.getExtras().get("data");
            ShowImgView.setImageBitmap(TableBitmap);
            Uri tempUri = getImageUri(getApplicationContext(), TableBitmap);
            File imgfile = null;

            try {
                imgfile = FileUtil.from(this, tempUri);
                File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);
                Compressor compressor = new Compressor(this);
                compressor.setDestinationDirectoryPath(mediaStorageDir.getPath()); // set path
                imgFile1 = compressor.compressToFile(imgfile);
                Uri imageurione = Uri.fromFile(imgFile1);
                String filePath = PathUtil.getPath(this, tempUri);
                Log.e("file+", String.valueOf(imgFile1));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

//            tableFile = new File(mediaStorageDir.getPath() + File.separator + imgfile.getName());
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Image", null);
        return Uri.parse(path);
    }


    public File getPhotoFileUriPath(String fileName) {

        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);

        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d(APP_TAG, "failed to create directory");
        }

        // Return the file target for the photo based on filename
//        file = new File(mediaStorageDir.getPath() + File.separator + fileName +timeStamp + ".jpg");
        file = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return file;
    }

    //    public String getRealPathFromURI(Uri uri) {
//        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
//        cursor.moveToFirst();
//        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
//        return cursor.getString(idx);
//    }
    private void submitDataMethod() {

        if (address == null) {
            address = "Not available";
        }
        String submitTitle = "<b>" + "Time:" + "</b>" + crnt_dt + "<b>" + "\nLocation:" + "</b>" + address + "<b>" + "\nSubmitted By:" + "</b>" + userDetailsList.get(0).getFirst_name();
        submitEditText.setText(Html.fromHtml(submitTitle));

        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isConnectionFast(IsolationActivity.this) == true) {
//            SubmitDataOnServer();

//            AsyncData data = new AsyncData();
//            data.execute();

//            submitData(points, radioButtons, editText, aSwitch, button, 1);

            /*if (selfy_on_submit.equalsIgnoreCase("true")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
                    if (fingerprintManager.isHardwareDetected()) {
                        dialog.dismiss();
                    }
                }
            }*/
/*
            if (linearLayout.getVisibility() == View.GONE) {
                linearLayout.setVisibility(View.VISIBLE);
            }
            if (selfy_on_submit.equalsIgnoreCase("false")) {
                handlerDelayTime = 2000;
            }
            Toast.makeText(IsolationActivity.this, "Submitted sucessfully", Toast.LENGTH_SHORT).show();

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressbar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    new ButtonLock().buttonLock(radioButtons, editText, button, aSwitch, b, photoButton, pointEditSubmitedLinearLayout, isVisibleEditBtn);
                    Intent i = new Intent(IsolationActivity.this, RefreshDataOnSubmit.class);
                    i.putExtra("wrk_id", wrk_id);
                    i.putExtra("uid", uid);
                    i.putExtra("checklist_type", checklist_type);
                    startService(i);
                }
            }, handlerDelayTime);*/

            SubmitDataOnServer();

        } else {
//            if (address == null) {
//                address = "Not available";
//            }
//            String submitTitle = "<b>" + "Time:" + "</b>" + crnt_dt + "<b>" + "\nLocation:" + "</b>" + address + "<b>" + "\nSubmitted By:" + "</b>" + userDetailsList.get(0).getFirst_name();
//            submitEditText.setText(Html.fromHtml(submitTitle));

            submitData(points, radioButtons, editText, aSwitch, button, 0);

            if (selfy_on_submit.equalsIgnoreCase("true")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
                    /*if (fingerprintManager.isHardwareDetected()) {
                        dialog.dismiss();
                    }*/
                }
            }

            if (linearLayout.getVisibility() == View.GONE) {
                linearLayout.setVisibility(View.VISIBLE);
            }
            if (selfy_on_submit.equalsIgnoreCase("false")) {
                handlerDelayTime = 2000;
            }

            Toast.makeText(IsolationActivity.this, "Submitted sucessfully", Toast.LENGTH_SHORT).show();

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressbar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    new ButtonLock().buttonLock(radioButtons, editText, button, aSwitch, b, photoButton, pointEditSubmitedLinearLayout, isVisibleEditBtn);
                }
            }, handlerDelayTime);


        }

    }

    private void getDataFromDatabaseByUid(String uid) {
        Thread threadComponentByUid = new Thread(new Runnable() {
            @Override
            public void run() {
                userDetailsList = checkListDatabase.productDao().getUserDetail();

                //componentList=checkListDatabase.productDao().getAllComponents();
                componentList = checkListDatabase.productDao().getComponentByUid(uid);

                Log.e("componentSize", String.valueOf(componentList.size()));
                for (Component c : componentList) {
                    if (c.getCuserpoint().equalsIgnoreCase("true")) {
                        viewEnabled.add(false);
                    } else {
                        viewEnabled.add(true);
                    }
                }
                for (int i = 0; i < componentList.size(); i++) {
                    audioFileMap.put(i, new ArrayList<>());
                    submitDataTemp1.add(new ChecklistRecordDataList());
                }

                if (componentList != null && componentList.size() > 0) {
                    locationValue = componentList.get(0).getPhoto_location();
                    selfy_on_submit = componentList.get(0).getSelfy_on_submit();
                }
                for (int i = 0; i < componentList.size(); i++) {
                    imageMap.put(i, new ArrayList<>());
                    locationMap.put(i, new ArrayList<>());
                    timeStampMap.put(i, new ArrayList<>());
                }

//                setRecyclerView();
                Executors.newSingleThreadExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        setRecyclerView();
                    }
                });
            }
        });
        threadComponentByUid.start();


    }

    private void getUid() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uid1 = uid = bundle.getString("uid");

            title = bundle.getString("title");
            wrk_id = bundle.getString("wrk_id");
            checklist_type = bundle.getString("checklist_type");


            getDataFromDatabaseByUid(uid);
            textView_checkList_header.setText(title);

//**********************Get user data from database***********************
            CheckListDatabase db;
            List<UserDetails> list;
            db = Room.databaseBuilder(this, CheckListDatabase.class,
                    ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
            list = db.productDao().getUserDetail();
            rank_id = list.get(0).getRank_id();
            emp_id = list.get(0).getUser_id();

        } else {
            Toast.makeText(this, "something went wrong please try again..", Toast.LENGTH_SHORT).show();
        }
    }

    private void submitData(String post, RadioButton[] radioButtons, ImageButton
            editText, Switch aSwitch, ImageButton button, Integer flag) {

        ChecklistRecordDataList checklistRecordDataList = new ChecklistRecordDataList();

        /*checklistRecordDataList.setWrk_id(Integer.parseInt(wrk_id));
        checklistRecordDataList.setGnrtn_date(crnt_dt);
        checklistRecordDataList.setAns(ans);
        checklistRecordDataList.setDepartment(department);
        checklistRecordDataList.setChecklist_points(point);
        checklistRecordDataList.setAlrt_cmnt(alertComment);
        checklistRecordDataList.setUid(Integer.parseInt(uid));
        checklistRecordDataList.setQ(q);
        checklistRecordDataList.setCrnt_dt_tm(crnt_dt);
        checklistRecordDataList.setCrnt_lctn(crnt_lctn);
        checklistRecordDataList.setAlert(alert);
        checklistRecordDataList.setUser_id(emp_id);
        checklistRecordDataList.setDone_by(userDetailsList.get(0).getFirst_name());
        checklistRecordDataList.setImageJson(String.valueOf(imageJson));

        JSONObject photoJson = new JSONObject();
        for (int i = 0; i < listPhoto.size(); i++) {
            try {
                photoJson.put(String.valueOf(i), listPhoto.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        checklistRecordDataList.setImage_data(String.valueOf(photoJson));
        checklistRecordDataList.setSbmt_dtals(emp_id);
        checklistRecordDataList.setComment(comment);
        checklistRecordDataList.setSeverity(severity);
        checklistRecordDataList.setFlag(flag);*/

        checklistRecordDataList.setWrk_id(Integer.parseInt(wrk_id));
        checklistRecordDataList.setAns(submitDataTemp1.get(position).getAns());
        checklistRecordDataList.setDepartment(submitDataTemp1.get(position).getDepartment());
        checklistRecordDataList.setChecklist_points(submitDataTemp1.get(position).getChecklist_points());
        checklistRecordDataList.setAlrt_cmnt(submitDataTemp1.get(position).getAlrt_cmnt());
        checklistRecordDataList.setUid(submitDataTemp1.get(position).getUid());
        checklistRecordDataList.setQ(submitDataTemp1.get(position).getQ());
        checklistRecordDataList.setCrnt_dt_tm(submitDataTemp1.get(position).getCrnt_dt_tm());
        checklistRecordDataList.setCrnt_lctn(submitDataTemp1.get(position).getCrnt_lctn());
        checklistRecordDataList.setAlert(submitDataTemp1.get(position).getAlert());
        checklistRecordDataList.setUser_id(submitDataTemp1.get(position).getUser_id());
        checklistRecordDataList.setDone_by(submitDataTemp1.get(position).getDone_by());
        checklistRecordDataList.setImageJson(submitDataTemp1.get(position).getImageJson());

        JSONArray audioArray = new JSONArray();
        ArrayList<String> audioList111 = audioFileMap.get(position);
        for (int i = 0; i < audioList111.size(); i++) {
            JSONObject j = new JSONObject();
            try {
                j.put("audionm", audioList111.get(i).substring(87));
//                Log.e("java::", String.valueOf(audioList111.get(i).substring(87)));
                j.put("audio", "");
                audioArray.put(j);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        checklistRecordDataList.setAudio_data(audioArray.toString());

        JSONObject photoJson = new JSONObject();
        List<String> listPhoto = imageMap.get(position);
        for (int i = 0; i < listPhoto.size(); i++) {
            try {
                photoJson.put(String.valueOf(i), listPhoto.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        Log.e("ImageJson:", ":" + submitDataTemp1.get(position).getImageJson());
//        Log.e("OfflinePhoto:", photoJson.toString());
        checklistRecordDataList.setImage_data(String.valueOf(photoJson));
        checklistRecordDataList.setSbmt_dtals(submitDataTemp1.get(position).getSbmt_dtals());
        checklistRecordDataList.setComment(submitDataTemp1.get(position).getComment());
        checklistRecordDataList.setSeverity(submitDataTemp1.get(position).getSeverity());
        checklistRecordDataList.setFlag(flag);

        Thread threadSetSubmit = new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().setChecklistRecordDataList1(checklistRecordDataList);
                setDataOnPoint();
                clear();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressbar.setVisibility(View.GONE);
                    }
                });
            }
        });

        threadSetSubmit.start();
    }

    public void clear() {
        point = "";
        q = "";
        ans = "";
        comment = "";
        photo = "";
        alert = "";
        severity = "";
        department = "";
        alertComment = "";
        crnt_dt = "";
        crnt_lctn = "";
        tempPoint = "";
        listPhoto.clear();
        itemsListData = null;
        submitBtnFlag = "";
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recycleCheckpointAdapter.notifyDataSetChanged();
                itemListDataAdapter.notifyDataSetChanged();
            }
        });


    }

    public void cancelCompleteMethod(String status) {
        NetworkDetector networkDetector = new NetworkDetector();
        String crnt_tm = new UtilClassFuntions().currentDateTime();
        if (networkDetector.isNetworkReachable(this) == true) {
            progressbar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            Map<String, String> params = new HashMap<>();
            params.put("comment", submitComment);
            params.put("wrk_id", wrk_id);
            params.put("user_id", userDetailsList.get(0).getUser_id());
            params.put("accessToken", userDetailsList.get(0).getAccessToken());
            params.put("modified_dt", new UtilClassFuntions().currentDateTime());
            params.put("dbnm", userDetailsList.get(0).getDbnm());
            params.put("wrk_status", status);

            //****************************

            if (!wrk_id.equals("")) {

                MasterWorkId masterWorkId = new MasterWorkId();
                masterWorkId.setWrk_id(Integer.valueOf(wrk_id));
                masterWorkId.setComment(submitComment);
                masterWorkId.setGrtd_by(emp_id);
                masterWorkId.setModified_dt(crnt_tm);
                masterWorkId.setWrk_status(Integer.valueOf(status));
                masterWorkId.setUser_img(photofilepath);

                /*Thread threadSetSubmit = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        checkListDatabase.productDao().updateMasterWorkId(wrk_id, comment, emp_id, crnt_tm, status);
                    }
                });
                threadSetSubmit.start();*/

            }

            //*****************************
            JSONObject jsonObject = new JSONObject(params);
            try {
                String dataKey = "details";
                MultipartUtility multipart = new MultipartUtility(ApiUrlClass.cancel_complete_url, ApiUrlClass.charset, dataKey, jsonObject);
                List<File> file1 = new ArrayList<>();

                if (photofilepath != null) {
                    file1.add(0, new File(photofilepath));
                    multipart.addFilePart("userimg", file1);
                }

                List<String> response = multipart.finish();
                Log.e("Completed SERVER REPLIED:", String.valueOf(response));

                for (String line : response) {
                    System.out.println(line);
                    JSONObject jsonObjectRes = new JSONObject(line);
                    Integer res = jsonObjectRes.getInt("response");
                    if (res.equals(200)) {
                        cancelCompleteCechlistUpdate(wrk_id, comment, emp_id, crnt_tm, status, photofilepath, 1);
                        progressbar.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        finish();
                    } else if (res.equals(201)) {
                        cancelCompleteCechlistUpdate(wrk_id, comment, emp_id, crnt_tm, status, photofilepath, 0);
                        progressbar.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(this, "Try again!", Toast.LENGTH_SHORT).show();
                    } else if (res.equals(10003)) {
                        cancelCompleteCechlistUpdate(wrk_id, comment, emp_id, crnt_tm, status, photofilepath, 0);
                        progressbar.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        finish();
                        new LogoutClass().logoutMethod(IsolationActivity.this);
                    } else {
                        cancelCompleteCechlistUpdate(wrk_id, comment, emp_id, crnt_tm, status, photofilepath, 0);
                        progressbar.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(this, "Checklist canceled successfully!", Toast.LENGTH_SHORT).show();

                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            cancelCompleteCechlistUpdate(wrk_id, comment, emp_id, crnt_tm, status, photofilepath, 0);
            finish();
        }
    }

    public void cancelCompleteCechlistUpdate(String wrk_id, String comment, String emp_id, String crnt_tm, String status, String user_img, Integer flag) {
        Thread threadSetSubmit = new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().updateMasterWorkId(wrk_id, comment, emp_id, crnt_tm, status, user_img, flag);
            }
        });
        threadSetSubmit.start();
    }

    private RecyclerSectionItemDecoration.SectionCallback getSectionCallback(
            final List<Component> components) {
        return new RecyclerSectionItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {
                return position == 0
                        || components.get(position)
                        .getTitle_of_section()
                        .charAt(0) != components.get(position - 1)
                        .getTitle_of_section()
                        .charAt(0);
            }

            @Override
            public CharSequence getSectionHeader(int position) {
                return components.get(position).getTitle_of_section()
                        /*.subSequence(0,1)*/;
            }
        };
    }


    public void verification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

            if (!fingerprintManager.isHardwareDetected()) {
                //Here submit data on submit button
                submitBtnFlag = "start";
                createImageName();
            } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {

            } else if (!keyguardManager.isKeyguardSecure()) {

            } else if (!fingerprintManager.hasEnrolledFingerprints()) {

            } else {

                FingerprintVerification fingerprintVerification = new FingerprintVerification(this);
                fingerprintVerification.startAuth(fingerprintManager, null);
            }
        } else {

            //Here submit data on click submit button
            submitBtnFlag = "start";
            createImageName();

        }
    }

    public void checklistDetailSwiperefresh(String wrk_id, String employee_id, String
            dbnm, String comp_name, String accessToken) {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("wrk_id", wrk_id);
            params.put("user_id", employee_id);
            params.put("dbnm", dbnm);
            params.put("comp_name", comp_name);
            params.put("accessToken", accessToken);

            VolleyMethods.makePostJsonObjectRequest(params, IsolationActivity.this, new ApiUrlClass().resumeApi, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            Integer res = response.getInt("response");

                            if (res.equals(200)) {
                                ObjectMapper objectMapper = new ObjectMapper();
                                JSONArray checklistRecordData = response.getJSONArray("lastdata");
                                JSONArray enginDescriptionDataArray = response.getJSONArray("engin_data");
                                List<ChecklistRecordDataList> checklistRecordDataListTemp = new ArrayList<>();

                                if (checklistRecordData != null && checklistRecordData.length() > 0) {
                                    for (int i = 0; i < checklistRecordData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) checklistRecordData.get(i);
                                        ChecklistRecordDataList checklistRecordDataList1 = objectMapper.readValue(jsonObject1.toString(), ChecklistRecordDataList.class);
                                        checklistRecordDataList.add(checklistRecordDataList1);
                                        checklistRecordDataListTemp.add(checklistRecordDataList1);
                                    }
                                    lastDataSize = 0;
                                    lastDataSize = checklistRecordDataListTemp.size();
                                    recycleCheckpointAdapter.setLastData(lastDataSize);
                                    recyclerView.setAdapter(recycleCheckpointAdapter);
                                    recycleCheckpointAdapter.notifyDataSetChanged();
                                }
                                resumeAndAddData();
                            } else if (res.equals(10003)) {
                                new LogoutClass().logoutMethod(IsolationActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(IsolationActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(IsolationActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void resumeAndAddData() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().deleteChecklistDataList(Integer.parseInt(wrk_id));
                checkListDatabase.productDao().setResumeDataToTable(checklistRecordDataList/*, enginDescriptionData*/);
            }
        });
        threadDeleteAllTables.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    //--------------- This dialog is used for  show the the dialog for recording voice .-------------//
    public void VoiceRecordDialog(int postion) {
        LinearLayout linearLayoutPlay;
        Chronometer chronometerTimer;
        ImageView imageViewpause, imageViewStop, imageMicrophone, imageViewresume, imgClose;
        TextView tvpause, tvStop, tvMicrophone, tvresume;
        LinearLayout llstoprecording, llimgmicrophone, llresumerecording, llpauserecording;
        SeekBar seekBar;
        MediaRecorder myAudioRecorder;
        final long[] mLastStopTime = {0};

        recordDialog = new Dialog(IsolationActivity.this);
        recordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        recordDialog.setContentView(R.layout.layout_audio_recoder);
        recordDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        recordDialog.setCancelable(false);


        linearLayoutPlay = recordDialog.findViewById(R.id.linearLayoutPlay);
        chronometerTimer = recordDialog.findViewById(R.id.chronometerTimer);
        imageMicrophone = recordDialog.findViewById(R.id.imageMicrophone);
        imageViewpause = recordDialog.findViewById(R.id.imagePause);
        imageViewStop = recordDialog.findViewById(R.id.imageStopRecording);
        imageViewresume = recordDialog.findViewById(R.id.imageResume);
        tvMicrophone = recordDialog.findViewById(R.id.tvMicrophone);
        tvStop = recordDialog.findViewById(R.id.tvStopRecording);
        tvpause = recordDialog.findViewById(R.id.tvPause);
        tvresume = recordDialog.findViewById(R.id.tvResume);
        llstoprecording = recordDialog.findViewById(R.id.llstoprecording);
        llimgmicrophone = recordDialog.findViewById(R.id.llimgmicrophone);
        llresumerecording = recordDialog.findViewById(R.id.llresumerecording);
        llpauserecording = recordDialog.findViewById(R.id.llpauserecording);
        imgClose = recordDialog.findViewById(R.id.imgClose);


        imageViewpause.setEnabled(false);

        File root = new File(getExternalFilesDir(Environment.DIRECTORY_MUSIC), "/EWARPRecoder");
        if (!root.exists()) {
            root.mkdirs();
        }

        audioList = audioFileMap.get(postion);


        imageMicrophone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckPermissionsAudioRecord()) {
                    mFilePath = root.getAbsolutePath();
//                    mFileName = mFilePath + String.valueOf(System.currentTimeMillis() + ".mp3");
//                    File directory = new File(mFileName);
//                    fileName = mFileName.substring(mFileName.lastIndexOf("/") + 1);
                    SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss", Locale.US);
                    Date now = new Date();
                    fileName = formatter.format(now) + ".mp3";
//                    fileName = new UtilClassFuntions().currentDateTime() + ".mp3";

//                    fileName = String.valueOf(System.currentTimeMillis() + ".mp3");
//                    fileName = directory.getName();

//                    Log.e("name", mFileName);
                    mRecorder = new MediaRecorder();
                    imageMicrophone.setEnabled(false);
                    imageViewpause.setEnabled(true);
                    imageMicrophone.setVisibility(View.GONE);
                    tvMicrophone.setVisibility(View.GONE);
                    llimgmicrophone.setVisibility(View.GONE);
                    imageViewStop.setVisibility(View.VISIBLE);
                    llstoprecording.setVisibility(View.VISIBLE);
                    tvStop.setVisibility(View.VISIBLE);
                    mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                    mRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                    mRecorder.setOutputFile(mFilePath + File.separator + fileName);
                    try {
                        mRecorder.prepare();
                        mRecorder.start();
                    } catch (IOException e) {
                        Log.e("LOG_TAG", "prepare() failed");
                    }
                    chronometerTimer.setBase(SystemClock.elapsedRealtime());
                    chronometerTimer.start();
                    Toast.makeText(getApplicationContext(), "Recording Started", Toast.LENGTH_LONG).show();

                } else {
                    RequestPermissions();
                }
            }
        });

        imageViewpause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewresume.setVisibility(View.VISIBLE);
                tvresume.setVisibility(View.VISIBLE);
                llresumerecording.setVisibility(View.VISIBLE);
                imageViewpause.setVisibility(View.GONE);
                tvpause.setVisibility(View.GONE);
                llpauserecording.setVisibility(View.GONE);
                mRecorder.pause();
                chronometerTimer.stop();
                mLastStopTime[0] = SystemClock.elapsedRealtime();
            }
        });
        imageViewresume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewresume.setVisibility(View.GONE);
                tvresume.setVisibility(View.GONE);
                llresumerecording.setVisibility(View.GONE);
                imageViewpause.setVisibility(View.VISIBLE);
                tvpause.setVisibility(View.VISIBLE);
                llpauserecording.setVisibility(View.VISIBLE);
                mRecorder.resume();
                chronometerTimer.start();
                long intervalOnPause = (SystemClock.elapsedRealtime() - mLastStopTime[0]);
                chronometerTimer.setBase(chronometerTimer.getBase() + intervalOnPause);
            }
        });
        imageViewStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageMicrophone.setVisibility(View.VISIBLE);
                tvMicrophone.setVisibility(View.VISIBLE);
                llimgmicrophone.setVisibility(View.VISIBLE);
                imageViewStop.setVisibility(View.GONE);
                tvStop.setVisibility(View.GONE);
                llstoprecording.setVisibility(View.GONE);
                imageMicrophone.setEnabled(true);
                try {
                    mRecorder.stop();
                    mRecorder.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mRecorder = null;
                //starting the chronometer
                chronometerTimer.stop();
                chronometerTimer.setBase(SystemClock.elapsedRealtime());
                //showing the play button
                Toast.makeText(IsolationActivity.this, "Recording saved successfully.", Toast.LENGTH_SHORT).show();
//                    recordDialog.dismiss();
//                    fileNameList.add(fileName);
//                    audioList.add(fileName);
                audioList.add(mFilePath + File.separator + fileName);
                audioFileMap.put(postion, audioList);

            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRecorder == null) {
                    recordDialog.dismiss();
                } else {
                    imageMicrophone.setEnabled(true);
                    mRecorder.stop();
                    mRecorder.release();
                    mRecorder = null;
                    //starting the chronometer
                    chronometerTimer.stop();
                    chronometerTimer.setBase(SystemClock.elapsedRealtime());
                    recordDialog.dismiss();
                }
            }
        });
        recordDialog.show();

    }

    //-------- This dialog is used for Show recording List.----------//

    private void showAudioList(int position, String CPoints) {
        Log.e("positioRec", String.valueOf(position));
        for (int i = 0; i < componentList.size(); i++) {
            Component c = componentList.get(i);
            for (int j = 0; j < lastdata.size(); j++) {
                if (lastdata.get(j).getChecklist_points().equals(CPoints)) {
//                        audioList.clear();
                    String audio = lastdata.get(j).getAudio_data();
                    try {
                        if (audio != null) {
                            audioList = new ArrayList<>();
                            audioListAddress = new ArrayList<>();
                            File root = new File(getExternalFilesDir(Environment.DIRECTORY_MUSIC), "/EWARPRecoder");
                            if (!root.exists()) {
                                root.mkdirs();
                            }
                            String mPath = root.getAbsolutePath();
                            JSONArray audioArray = new JSONArray(audio);
                            for (int k = 0; k < audioArray.length(); k++) {
                                JSONObject audioObj = new JSONObject();
                                audioObj = audioArray.getJSONObject(k);
//                                Log.e("jj", audioObj.getString("audionm"));
//                                Log.e("jjk", mPath + File.separator + audioObj.getString("audionm"));
//                                Log.e("jjkk", audioObj.getString("audio"));

                                if (!audioList.contains(mPath + File.separator + audioObj.getString("audionm"))) {
                                    audioList.add(mPath + File.separator + audioObj.getString("audionm"));
                                }
                                if (!audioListAddress.contains(audioObj.getString("audio"))) {
                                    audioListAddress.add(audioObj.getString("audio"));
                                }

                            }
                            audioFileMap.put(position, audioList);
                            audioFileMapAddress.put(position, audioListAddress);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        if (audioFileMap.get(position).size() > 0) {
            Intent intent = new Intent(IsolationActivity.this, RecordingListActivity.class);
//            intent.putExtra("filename", fileName);
//            intent.putExtra("filepath", mFilePath);
            intent.putExtra("position", position);
            intent.putStringArrayListExtra("fileNameList", audioFileMap.get(position));
            intent.putStringArrayListExtra("addressList", audioFileMapAddress.get(position));
            Log.e("sizea", String.valueOf(audioFileMap.get(position)));
            Log.e("sizea", String.valueOf(audioFileMap.get(position)));
            startActivityForResult(intent, AUDIO_LIST_UPSATE);
        } else {
            Toast.makeText(IsolationActivity.this, "No recording available!", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean CheckPermissionsAudioRecord() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void RequestPermissions() {
        ActivityCompat.requestPermissions(IsolationActivity.this, new String[]{RECORD_AUDIO, WRITE_EXTERNAL_STORAGE}, REQUEST_AUDIO_PERMISSION_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private class FingerprintVerification extends FingerprintManager.AuthenticationCallback {
        private Context context;

        public FingerprintVerification(Context context) {
            this.context = context;
        }

        public void startAuth(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject) {
            CancellationSignal cancellationSignal = new CancellationSignal();
            fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
        }

        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {
            super.onAuthenticationError(errorCode, errString);
        }

        @Override
        public void onAuthenticationFailed() {
            super.onAuthenticationFailed();
        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
            super.onAuthenticationHelp(helpCode, helpString);
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            super.onAuthenticationSucceeded(result);
            if (flag == 1) {
                doneImageView.setImageResource(R.mipmap.done);
                dialogfinger.dismiss();
//                createImageName();
                Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            } else {
                doneImageView.setImageResource(R.mipmap.done);
                Log.e("s", "Authentication");
                submitBtnFlag = "start";
//                createImageName();
                Intent intent = new Intent(IsolationActivity.this, CustomCameraSelfieActivity.class);
                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        }

    }

    private class AsyncData extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

    }

    public void showTableViewDialog(int i, String Cpoints) {
       Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_table_view);
        dialog.setCancelable(false);

        Button submitButton1 = dialog.findViewById(R.id.formSubmit);
        Button cancelButton1 = dialog.findViewById(R.id.formCancel);
        RecyclerView questionRecyclerView = dialog.findViewById(R.id.table_recycle);
        EditText tableComment = dialog.findViewById(R.id.tableComment);
        ImageView tableimg = dialog.findViewById(R.id.tableimg);
        ShowImgView = dialog.findViewById(R.id.ShowImgView);
        questionRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        Component component = componentList.get(i);
        String colomn = component.getColumn_name();
        tableimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                } else {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);

                }
            }
        });

        try {
            //JSONObject jsnobject = new JSONObject(colomn);
            JSONArray jsonArray = new JSONArray(colomn);
            for (int h = 0; h < jsonArray.length(); h++) {
                Log.e("test", jsonArray.getString(h));
            }

            questionRecyclerView.setAdapter(new FormAdapter(jsonArray, this, new TableFormSubmItInterface() {
                @Override
                public void onFormSubmitonButtonClickListener(int post, EditText editText, TextView tvquestion) {
                    tableForm(post, editText, tvquestion);
                }
            }));


            cancelButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            submitButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Date currentTime = Calendar.getInstance().getTime();
                    Log.e("time", String.valueOf(currentTime));


                    if (valueDetails.size() == jsonArray.length()) {
                        JSONObject jsonObject = new JSONObject(valueDetails);

                        if (new NetworkDetector().isNetworkReachable(IsolationActivity.this) == true) {
                            Log.e("json", "" + String.valueOf(jsonObject));
                            JSONObject jsonObject1 = new JSONObject();
                            try {
                                jsonObject1.put("clmval", String.valueOf(jsonObject));
                                jsonObject1.put("wrk_id", wrk_id);
                                jsonObject1.put("points", component.getPoints());
                                jsonObject1.put("dbnm", userDetailsList.get(0).getDbnm());
                                jsonObject1.put("user_id", userDetailsList.get(0).getUser_id());
                                jsonObject1.put("user_name", userDetailsList.get(0).getFirst_name());
                                jsonObject1.put("datetime", currentTime.toString());
                                jsonObject1.put("comment", tableComment.getText().toString());
                                Logger.printLog("wrk", wrk_id);
                                Logger.printLog("json", String.valueOf(jsonObject1));
                                String dataKey = "details";
                                MultipartUtility multipart = new MultipartUtility(ApiUrlClass.SubmitTabledata, ApiUrlClass.charset, dataKey, jsonObject1, new MultiPartResponseInerface() {
                                    @Override
                                    public void onResponseSucess(String response) {
                                        Log.e("SERVER response:", String.valueOf(response));
                                        try {
                                            JSONArray jsonArray1 = new JSONArray(response);
                                            if (jsonArray1.getJSONObject(0).getInt("status") == 1) {
                                                dialog.dismiss();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onResponseError(String response) {
                                        Log.e("SERVER response:", String.valueOf(response));
//                                        JSONArray jsonArray1 = new JSONArray();
//                                        if (jsonArray1.getJSONObject(0).getString("status").equalsIgnoreCase("1")) {
//                                            dialog.dismiss();
//                                        }
                                    }
                                });
                                List<File> tablefile1 = new ArrayList<>();
                                tablefile1.add(imgFile1);
                                multipart.addFilePart("files", tablefile1);
                                Log.e("img", String.valueOf(tablefile1));
                                String response = String.valueOf(multipart.finish());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                        String wrkid = String.valueOf(checklistRecordDataLists.get(i).getWrk_id());
//                            Map<String, String> params = new HashMap<>();
//                            params.put("clmval", String.valueOf(jsonObject));
//                            params.put("wrk_id", wrk_id);
//                            params.put("points", component.getPoints());
//                            params.put("dbnm", userDetailsList.get(0).getDbnm());
//                            params.put("user_id", userDetailsList.get(0).getUser_id());
//                            params.put("user_name", userDetailsList.get(0).getFirst_name());
//                            params.put("datetime", "");
//                            params.put("comment", tableComment.getText().toString());
//                            Logger.printLog("wrk", wrk_id);
//                            VolleyMethods.makePostJsonObjectRequest(params, IsolationActivity.this, ApiUrlClass.SubmitTabledata, new PostJsonObjectRequestCallback() {
//                                @Override
//                                public void onSuccessResponse(JSONObject response) {
//                                    if (response != null) {
//                                        try {
//                                            String status = response.getString("status");
//                                            Log.e("status", status);
//                                            if (status.equals("1")) {
//                                                dialog.dismiss();
//                                                Toast.makeText(IsolationActivity.this, "Form Submited successfully.", Toast.LENGTH_SHORT).show();
//                                            }
//                                        } catch (JSONException e) {
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                }
//
//                                @Override
//                                public void onVolleyError(VolleyError error) {
//
//                                }
//
//                                @Override
//                                public void onTokenExpire() {
//
//                                }
//                            });


                        } else {
                            try {
                                SharedPreferences preferences = getSharedPreferences("qiestion_db", MODE_PRIVATE);
                                String savedJson = preferences.getString("questions", "[]");

                                JSONArray saveJsonArray = new JSONArray(savedJson);

                                JSONObject saveObject = new JSONObject();

                                saveObject.put("wrk_id", wrk_id);
                                saveObject.put("points", component.getPoints());
                                saveObject.put("dbnm", userDetailsList.get(0).getDbnm());
                                saveObject.put("user_id", userDetailsList.get(0).getUser_id());
                                saveObject.put("user_name", userDetailsList.get(0).getFirst_name());
                                saveObject.put("user_name", userDetailsList.get(0).getFirst_name());
                                saveObject.put("clmval", jsonObject);
                                saveObject.put("comment", tableComment.getText().toString());
                                saveObject.put("files",imgFile1.getAbsolutePath());
                                saveJsonArray.put(saveObject);

                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("questions", saveJsonArray.toString());
                                editor.apply();
                                Toast.makeText(IsolationActivity.this, "Saved your data in offline", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();

                                Logger.printLog("saved Question", ":--   " + preferences.getString("questions", "[]"));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    }
                }
            });
            dialog.show();

        } catch (JSONException e) {
            e.printStackTrace();
            Logger.printLog("crash", "" + e.getMessage());
        }
    }

    private void tableForm(int post, EditText editText, TextView textInputLayout) {

        if (valueDetails.containsKey(String.valueOf(post + 1))) {
            valueDetails.remove(String.valueOf(post + 1));
            valueDetails.put(String.valueOf(post + 1), editText.getText().toString().toUpperCase());

            Log.e("rm", String.valueOf(post + 1));
            Log.e("put", String.valueOf(valueDetails));
        } else {
            valueDetails.put(String.valueOf(post + 1), editText.getText().toString().toUpperCase());
            Log.e("detailsList", String.valueOf(valueDetails));
        }
    }
}