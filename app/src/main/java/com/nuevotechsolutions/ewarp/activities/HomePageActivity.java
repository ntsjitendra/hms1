package com.nuevotechsolutions.ewarp.activities;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.DefaultImgSliderAdapter;
import com.nuevotechsolutions.ewarp.adapter.NoticeAdapter;
import com.nuevotechsolutions.ewarp.adapter.SliderImageAdapter;
import com.nuevotechsolutions.ewarp.controller.DatabaseHandler;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ImageModel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.ManualsModel.MainList;
import com.nuevotechsolutions.ewarp.model.NoticeDataList;
import com.nuevotechsolutions.ewarp.services.NetworkChangeReceiver;
import com.nuevotechsolutions.ewarp.services.NetworkDetector;
import com.nuevotechsolutions.ewarp.services.TimeChangedReceiver;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.LogoutClass;
import com.nuevotechsolutions.ewarp.utils.SyncData;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.nuevotechsolutions.ewarp.R.string.app_name;

public class HomePageActivity extends AppCompatActivity {

    CardView checklistButton, vesselButton, defectButton, dryDockButton, manualsButton;
    String user, id, Rank;
    TextView textView_homePage_header;
    DrawerLayout drawerLayout;

    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    DatabaseHandler db;
    CheckListDatabase cDb;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    AlertDialog.Builder alertDialog;
    public static TextView badge_notification_h1, badge_notification_h2,
            badge_notification_h3, badge_notification_h4, badge_notification_h5;
    List<MasterWorkId> list = new ArrayList<>();
    List<UserDetails> userDetails = new ArrayList<>();
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    //    private List<NoticeDataList> imageModelArrayList;
    private Handler mHandler;
    Uri file;
    File file1, photofile;
    String imageFileName, path;
    private static final int REQUEST_CODE = 1;
    private Bitmap bitmap, rotatedBitmap;
    private ImageView imgProfilepic;
    LinearLayout linearLayoutHomePage;

    private int[] myImageList = new int[]{R.drawable.navyslide, R.drawable.flipimagee,
            R.drawable.slideimage, R.drawable.slideimagee};
    private ArrayList<ImageModel> imageModelArrayList;
    public static final int REQUEST_IMAGE_CAPTURE = 0;
    public static String fileName;
    private ImageView imgSyncData, notification;
    private RecyclerView noticerecyclerView;
    private ImageView imgNotificationclose;
    private List<NoticeDataList> noticeList = new ArrayList<>();
    List<NoticeDataList> noticeList2 = new ArrayList<>();
    private NoticeAdapter noticeAdapter;
    private JSONObject jsonObject;
    private String isNoticePicAvailable = "";
    private BroadcastReceiver mNetworkReceiver;
    public final String APP_TAG = "EWARP";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        cDb = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        mHandler = new Handler();
        textView_homePage_header = findViewById(R.id.textView_homePage_header0);
        textView_homePage_header.setText("Home");
        imgSyncData = findViewById(R.id.syncNow);
        notification = findViewById(R.id.notification);

        imageModelArrayList = new ArrayList<>();
        imageModelArrayList = populateListDefault();

        checklistButton = findViewById(R.id.checklistButton);
        vesselButton = findViewById(R.id.VesselInspectionButton);
        defectButton = findViewById(R.id.defectListButton);
        dryDockButton = findViewById(R.id.dryDockButton);
        manualsButton = findViewById(R.id.manualButton);
        linearLayoutHomePage = findViewById(R.id.linearLayoutHomePage);
        badge_notification_h1 = findViewById(R.id.badge_notification_h1);
        badge_notification_h2 = findViewById(R.id.badge_notification_h2);
        badge_notification_h3 = findViewById(R.id.badge_notification_h3);
        badge_notification_h4 = findViewById(R.id.badge_notification_h4);
        badge_notification_h5 = findViewById(R.id.badge_notification_h5);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_menu);
        View hView = navigationView.getHeaderView(0);
        TextView nav_user = (TextView) hView.findViewById(R.id.userNameLabel);
        TextView userRank = (TextView) hView.findViewById(R.id.userRank);
        imgProfilepic = (ImageView) hView.findViewById(R.id.imgProfilepic);
//        Intent intent = getIntent();
//        noticeList = (List<NoticeDataList>) intent.getSerializableExtra("keynoticeList");
        noticeList = cDb.productDao().getAllNoticeList();
        noticeList2 = populateList();
        init();
        list = cDb.productDao().getMasterWorkIdData();
        userDetails = cDb.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name() != null) {
            user = userDetails.get(0).getFirst_name();
            nav_user.setText(user);
        } else {
            nav_user.setText("Guest User");
        }
        if (userDetails.get(0).getRank() != null) {
            Rank = userDetails.get(0).getRank().toLowerCase();
            Log.e("rank nanme", Rank);
            userRank.setText(Rank);
        } else {
            userRank.setText("");
        }
        if (userDetails.get(0).getUser_image() != null && userDetails.get(0).getUser_image().contains("http")) {
            Picasso.with(this)
                    .load(userDetails.get(0).getUser_image())
                    .centerCrop()
                    .fit()
                    .error(R.drawable.person)
                    .into(imgProfilepic);
        }

        db = new DatabaseHandler(this);

        setUpToolbar();
        navigationView = findViewById(R.id.navigation_menu);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.nav_change_pass:
                        drawerLayout.closeDrawers();
                        Intent changePass = new Intent(HomePageActivity.this, ChangePassword.class);
                        startActivity(changePass);
                        break;
//                    case R.id.nav_contact:
//                        drawerLayout.closeDrawers();
//                        Intent contactus = new Intent(HomePageActivity.this, ContactUsActivity.class);
//                        startActivity(contactus);
//                        break;
                    case R.id.nav_free_space:
                        alertDialog = new AlertDialog.Builder(HomePageActivity.this);
                        alertDialog.setTitle("Gallery Cleanup");
                        alertDialog.setIcon(R.drawable.ewarpletest);
                        alertDialog.setMessage("Do you want to cleanup your gallery photo taken by this app. Are you sure?");
                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                drawerLayout.closeDrawers();
                                getAllFileFromDirectory();
                            }
                        });
                        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                drawerLayout.closeDrawers();
                            }
                        });
                        alertDialog.show();
                        break;
                    case R.id.nav_logout:
                        drawerLayout.closeDrawers();
                        new LogoutClass().logoutMethod(HomePageActivity.this);
                        break;
                }

                return false;
            }
        });


        this.mHandler.postDelayed(m_Runnable, 2000);

        checklistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePageActivity.this, ChecklistHomeActivity.class);
                startActivity(intent);
            }
        });

        vesselButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePageActivity.this, VesselInspectionActivity.class);
                startActivity(intent);
            }
        });

        defectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePageActivity.this, DefectListActivity.class);
                startActivity(intent);
            }
        });

        dryDockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar = Snackbar
                        .make(linearLayoutHomePage, "Feature Comming Soon..", Snackbar.LENGTH_SHORT);
                View sbView = snackbar.getView();
                sbView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
//                Intent intent=new Intent(HomePageActivity.this,DryDockActivity.class);
//                startActivity(intent);
            }
        });

        manualsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onManualsClick();
                Intent intent = new Intent(HomePageActivity.this, ManualsActivity.class);
                startActivity(intent);
            }
        });

        TimeChangedReceiver m_timeChangedReceiver = new TimeChangedReceiver();
        IntentFilter s_intentFilter = new IntentFilter();
        s_intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        s_intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        registerReceiver(m_timeChangedReceiver, s_intentFilter);


        imgSyncData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(HomePageActivity.this,SlidingDotActivity.class);
//                startActivity(intent);
                if (new NetworkDetector().isNetworkReachable(HomePageActivity.this) == true) {
                    boolean b = new SyncData().storeAllData(HomePageActivity.this);
                } else {
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.linearLayoutHomePage), "No internet connection!", Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }
            }
        });
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationDialog();
            }
        });

        mNetworkReceiver = new NetworkChangeReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mNetworkReceiver, intentFilter);
        registerNetworkBroadcastForNougat();

    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public void onManualsClick() {
        NetworkDetector networkDetector = new NetworkDetector();
        if (networkDetector.isNetworkReachable(this) == true) {
            Map<String, String> params = new HashMap<>();
            params.put("user_id", userDetails.get(0).getUser_id());
            params.put("dbnm", userDetails.get(0).getDbnm());
            params.put("comp_name", userDetails.get(0).getComp_name());
            params.put("accessToken", userDetails.get(0).getAccessToken());


            VolleyMethods.makePostJsonObjectRequest(params, HomePageActivity.this, new ApiUrlClass().MANUALS_DATA, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray mainListData = response.getJSONArray("manuals");


                            if (mainListData != null && mainListData.length() > 0) {
                                for (int i = 0; i < mainListData.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) mainListData.get(i);
                                    MainList mainList = objectMapper.readValue(jsonObject1.toString(), MainList.class);
//                                    mainListList.add(mainList);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Toast.makeText(HomePageActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTokenExpire() {
                    Toast.makeText(HomePageActivity.this, "Session Expired!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(linearLayoutHomePage, "No internet connection!", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorAccent));
            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {
                    ExifInterface ei = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        bitmap = BitmapFactory.decodeFile(photofile.getAbsolutePath());
                        ei = new ExifInterface(photofile.getAbsolutePath());
                    } else {
                        bitmap = BitmapFactory.decodeFile(file.getPath());
                        ei = new ExifInterface(file.getPath());
                    }
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_UNDEFINED);
                    switch (orientation) {

                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotatedBitmap = rotateImage(bitmap, 90);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotatedBitmap = rotateImage(bitmap, 180);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotatedBitmap = rotateImage(bitmap, 270);
                            break;

                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                            rotatedBitmap = bitmap;
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        FileOutputStream fos = new FileOutputStream(photofile.getAbsolutePath());
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                        fos.close();
                        path = photofile.getAbsolutePath();
                    } else {
                        FileOutputStream fos = new FileOutputStream(file.getPath());
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                        fos.close();
                        path = file.getPath();

                    }

                    imgProfilepic.setImageBitmap(rotatedBitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private List<NoticeDataList> populateList() {
        List<NoticeDataList> list = new ArrayList<>();
        List<NoticeDataList> list1 = new ArrayList<>();
        if (noticeList != null) {
            for (int i = 0; i < noticeList.size(); i++) {
                if (noticeList.get(i).getFile_path() != null) {
                    NoticeDataList imageModel = new NoticeDataList();
                    imageModel.setFile_path(noticeList.get(i).getFile_path());
                    Log.e("path--", noticeList.get(i).getFile_path());
                    list.add(imageModel);
                }
            }
        }

        if (list != null && list.size() == 4) {
            for (int j = 0; j < 4; j++) {
                list1.add(list.get(j));
            }
        } else {
            switch (list.size()) {

                case 1:
                    for (int j = 0; j < 1; j++) {
                        list1.add(list.get(j));
                    }
                    break;
                case 2:
                    for (int j = 0; j < 2; j++) {
                        list1.add(list.get(j));
                    }
                    break;
                case 3:
                    for (int j = 0; j < 3; j++) {
                        list1.add(list.get(j));
                    }
                default:
                    isNoticePicAvailable = "default";

            }
        }
        return list1;
    }

    private ArrayList<ImageModel> populateListDefault() {

        ArrayList<ImageModel> list = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            ImageModel imageModel = new ImageModel();
            imageModel.setImage_drawable(myImageList[i]);
            list.add(imageModel);
        }

        return list;
    }

    private void checkListNotifications() {

        List<MasterWorkId> workinglist = new ArrayList<>();
        List<MasterWorkId> completedList = new ArrayList<>();
        List<MasterWorkId> assignList = new ArrayList<>();
        int j = 0;
        for (int i = list.size() - 1; i >= 0; i--) {
            if (list.get(i).getWrk_status().equals(0) && list.get(i).getSeen() == false) {
                workinglist.add(j, list.get(i));
                j++;
            }
        }

        int k = 0;
        for (int i = list.size() - 1; i >= 0; i--) {
            if ((list.get(i).getWrk_status().equals(1) || list.get(i).getWrk_status().equals(2))
                    && list.get(i).getSeen() == false) {
                completedList.add(k, list.get(i));
                k++;
            }
        }

        int r = 0;
        for (int i = list.size() - 1; i >= 0; i--) {
            if ((list.get(i).getWrk_status().equals(3) || list.get(i).getWrk_status().equals(5))
                    && list.get(i).getSeen() == false) {
                assignList.add(r, list.get(i));
                r++;
            }
        }
        String completedCount = String.valueOf(completedList.size());
        String resumeCount = String.valueOf(workinglist.size());
        String assignCount = String.valueOf(assignList.size());

        if (completedList.size() > 0) {
            badge_notification_h1.setVisibility(View.VISIBLE);
            badge_notification_h1.setText(completedCount);
        }
        if (workinglist.size() > 0) {
            badge_notification_h2.setVisibility(View.VISIBLE);
            badge_notification_h2.setText(resumeCount);
        }
        if (assignList.size() > 0) {
            badge_notification_h4.setVisibility(View.VISIBLE);
            badge_notification_h4.setText(assignCount);
        }

    }

    private final Runnable m_Runnable = new Runnable() {
        public void run() {
            if (list.size() > 0) {
                checkListNotifications();
            }
            HomePageActivity.this.mHandler.postDelayed(m_Runnable, 2000);
        }
    };

    private void setUpToolbar() {
        drawerLayout = findViewById(R.id.drawerLayout0);
        toolbar = findViewById(R.id.toolbar0);
        setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Do you want to exit?");
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                HomePageActivity.super.onBackPressed();
            }
        });
        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void init() {

        mPager = (ViewPager) findViewById(R.id.pager0);
        if (isNoticePicAvailable.equals("default")) {
            DefaultImgSliderAdapter defaultImgSliderAdapter = new DefaultImgSliderAdapter(HomePageActivity.this, imageModelArrayList);
            mPager.setAdapter(defaultImgSliderAdapter);
            defaultImgSliderAdapter.notifyDataSetChanged();
        } else {
            SliderImageAdapter sliderImageAdapter = new SliderImageAdapter(HomePageActivity.this, noticeList2);
            mPager.setAdapter(sliderImageAdapter);
            sliderImageAdapter.notifyDataSetChanged();
        }


        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator0);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = 5;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }

    public void notificationDialog() {
        Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
        dialog.setContentView(R.layout.dialog_notice);
        imgNotificationclose = dialog.findViewById(R.id.closeNotifictaionBtn);
        noticerecyclerView = dialog.findViewById(R.id.noticerecyclerView);
        noticerecyclerView.setLayoutManager(new LinearLayoutManager(this));
        noticeAdapter = new NoticeAdapter(this, noticeList, new NoticeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(NoticeDataList item, int position) {
                Intent i = new Intent(HomePageActivity.this, NoticeDetailActivity.class);
                i.putExtra("noticekey", (Serializable) item);
                startActivity(i);
            }
        });
        noticerecyclerView.setAdapter(noticeAdapter);
        imgNotificationclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });
        dialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    public void getAllFileFromDirectory() {
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);
        if (!file.exists() && !file.mkdirs()) {
            Log.d(APP_TAG, "failed to create directory");
        } else {
            String path = file.getPath();
            Log.d("Files", "Path: " + path);
            File directory = new File(path);
            File[] files = directory.listFiles();
            Log.d("Files", "Size: " + files.length);
            for (int i = 0; i < files.length; i++) {
                Log.d("Files", "FileName:" + files[i].getName());
            }
            autoDeletePhotofile(files);
        }
    }

    public void autoDeletePhotofile(File[] files) {
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (files[i].exists()) {
                    Calendar time = Calendar.getInstance();
                    time.add(Calendar.DAY_OF_YEAR, -10);
                    //I store the required attributes here and delete them
                    Date lastModified = new Date(files[i].lastModified());
                    if (lastModified.before(time.getTime())) {
                        //file is older than a week
                        files[i].delete();
                    }
                } else {
                    try {
                        files[i].createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            Toast.makeText(this, "Gallery Cleanup", Toast.LENGTH_SHORT).show();
        }
    }

}
