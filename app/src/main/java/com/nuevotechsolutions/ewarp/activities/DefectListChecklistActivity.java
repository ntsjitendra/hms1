package com.nuevotechsolutions.ewarp.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.nuevotechsolutions.ewarp.BuildConfig;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.DefectListModel.DefectListDataList;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.LocationHelper;
import com.nuevotechsolutions.ewarp.utils.MultipartUtility;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.nuevotechsolutions.ewarp.utils.UtilClassFuntions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefectListChecklistActivity extends AppCompatActivity {
private EditText etDescription,etdeficiency,etActionReqShip,etActionReqOffice,etRemarks,
        etClosedDate,etRevisedTaeget,etCompletionDate,etRemarkDealys;
private RecyclerView listBeforePhoto,listAfterPhoto;
private ImageView imgAfterCamera,imgBeforeCamera,imgCompletionDate,imgClosedDateDate;
    private int year, month, day;
    private Calendar calendar;
    private Button cancelBtn, completeBtn;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private String submitComment;
    private int flag = 0;
    private String status = "";
    private String submitBtnFlag = "";
    private Dialog dialogfinger;
    private ImageView doneImageView;
    private String imageFileName = "No_Teken_Photo";
    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1034;
    public final String APP_TAG = "DefectList";
    private File photoFile;
    private File file;
    private Uri fileProvider;
    private Bitmap takenImage;
    private Bitmap rotatedBitmap = null;
    private String encodedImage;
    List<String> listPhoto = new ArrayList<>();
    double latitude, longitude;
    public String locality, address, postalCode, county, lat, lon, coordinates;
    private LocationHelper.LocationResult locationResult;
    private LocationHelper locationHelper;
    private Geocoder geocoder;
    private String crnt_lctn,photo,point,tempPoint,wrk_id,emp_id;
    private JSONObject imageJson;
    private List<String> locationList = new ArrayList<>();
    private List<String> timeStampList = new ArrayList<>();
    private List<String> itemsListData;
    private List<String> locationListData;
    private List<String> timeStampListData;
    private Map<Integer, List<String>> imageMap = new HashMap<>();
    private Map<Integer, List<String>> locationMap = new HashMap<>();
    private Map<Integer, List<String>> timeStampMap = new HashMap<>();
    List<UserDetails> userDetailsList;
    private CheckListDatabase checkListDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_defect_list_checklist);

        cancelBtn = findViewById(R.id.cancelledButton);
        completeBtn = findViewById(R.id.completedButton);
        setUpToolbar();
        IdTypeCasting();

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

//        etCompletionDate.setText(new StringBuilder().append(year).append("-").append(month + 1).append("-").append(day));
        imgCompletionDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(DefectListChecklistActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                etCompletionDate.setText(day + "/" + (month + 1) + "/" + year);

                            }
                        }, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                datePickerDialog.show();
            }

        });

        imgClosedDateDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(DefectListChecklistActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                etClosedDate.setText(day + "/" + (month + 1) + "/" + year);

                            }
                        }, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                datePickerDialog.show();
            }
        });

        checkListDatabase = Room.databaseBuilder(getApplicationContext(),
                CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetailsList = checkListDatabase.productDao().getUserDetail();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(DefectListChecklistActivity.this);
                dialog.setContentView(R.layout.comment_layout);
                dialog.show();
                TextView commentTitle = dialog.findViewById(R.id.commentTitleTextView);
                EditText editText = dialog.findViewById(R.id.editText);
                Button submitButton = dialog.findViewById(R.id.button);
                Button cancelButton = dialog.findViewById(R.id.buttonCancel);
                commentTitle.setText("Complete Comment");
                dialog.setCancelable(false);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        submitComment = editText.getText().toString();
                        status = "1";
                        flag = 1;
                        dialogfinger = new Dialog(DefectListChecklistActivity.this);
                        dialogfinger.setContentView(R.layout.fingerprint_dialog);
                        dialogfinger.setCancelable(false);
                        ImageView mfingerprint = dialogfinger.findViewById(R.id.dialogFingerprint);
                        doneImageView = mfingerprint;
                        dialogfinger.show();
                        ImageView imgClose = dialogfinger.findViewById(R.id.imgClose);
                        imgClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogfinger.dismiss();
                            }
                        });
                        Window window = dialogfinger.getWindow();
                        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        verification();
                    }
                });
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            }
        });

        completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(DefectListChecklistActivity.this);
                dialog.setContentView(R.layout.comment_layout);
                dialog.show();
                TextView commentTitle = dialog.findViewById(R.id.commentTitleTextView);
                EditText editText = dialog.findViewById(R.id.editText);
                Button submitButton = dialog.findViewById(R.id.button);
                Button cancelButton = dialog.findViewById(R.id.buttonCancel);
                commentTitle.setText("Complete Comment");
                dialog.setCancelable(false);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        submitComment = editText.getText().toString();
                        status = "1";
                        flag = 1;
                        dialogfinger = new Dialog(DefectListChecklistActivity.this);
                        dialogfinger.setContentView(R.layout.fingerprint_dialog);
                        dialogfinger.setCancelable(false);
                        ImageView mfingerprint = dialogfinger.findViewById(R.id.dialogFingerprint);
                        doneImageView = mfingerprint;
                        dialogfinger.show();
                        ImageView imgClose = dialogfinger.findViewById(R.id.imgClose);
                        imgClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogfinger.dismiss();
                            }
                        });
                        Window window = dialogfinger.getWindow();
                        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        verification();
                    }
                });
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            }
        });

    }

    //* This function is used for typecasting all the xml id's. *//
    private void IdTypeCasting() {
        etDescription = findViewById(R.id.etDescription);
        etdeficiency = findViewById(R.id.etdeficiency);
        etActionReqShip = findViewById(R.id.etActionReqShip);
        etActionReqOffice = findViewById(R.id.etActionReqOffice);
        etRemarks = findViewById(R.id.etRemarks);
        etClosedDate = findViewById(R.id.etClosedDate);
        etRevisedTaeget = findViewById(R.id.etRevisedTaeget);
        etCompletionDate = findViewById(R.id.etCompletionDate);
        etRemarkDealys = findViewById(R.id.etRemarkDealys);
        listBeforePhoto = findViewById(R.id.listBeforePhoto);
        listAfterPhoto = findViewById(R.id.listAfterPhoto);
        imgAfterCamera = findViewById(R.id.imgAfterCamera);
        imgBeforeCamera = findViewById(R.id.imgBeforeCamera);
        imgCompletionDate = findViewById(R.id.imgCompletionDate);
        imgClosedDateDate = findViewById(R.id.imgClosedDateDate);

    }

    private void setUpToolbar() {

       Toolbar toolbar = findViewById(R.id.toolbarDefect);
       ImageButton imageView = toolbar.findViewById(R.id.backArrowDefectList);
       ImageView checkSubmit = toolbar.findViewById(R.id.defectListSubmit);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
       imageView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               new OnBackPressed().onBackPressedFunction(DefectListChecklistActivity.this);
               finish();
           }
       });

       checkSubmit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               finish();
           }
       });

        setSupportActionBar(toolbar);

    }

    public void verification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

            if (!fingerprintManager.isHardwareDetected()) {
                //Here submit data on submit button
                Toast.makeText(this, "Fingerprint not detected", Toast.LENGTH_SHORT).show();

            } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {

            } else if (!keyguardManager.isKeyguardSecure()) {

            } else if (!fingerprintManager.hasEnrolledFingerprints()) {

            } else {

                DefectListChecklistActivity.FingerprintVerification fingerprintVerification = new DefectListChecklistActivity.FingerprintVerification(this);
                fingerprintVerification.startAuth(fingerprintManager, null);

            }
        } else {

            //Here submit data on click submit button
            Toast.makeText(this, "Fingerprint not detected", Toast.LENGTH_SHORT).show();

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private class FingerprintVerification extends FingerprintManager.AuthenticationCallback {
        private Context context;

        public FingerprintVerification(Context context) {
            this.context = context;
        }

        public void startAuth(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject) {
            CancellationSignal cancellationSignal = new CancellationSignal();
            fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
        }

        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {
            super.onAuthenticationError(errorCode, errString);
        }

        @Override
        public void onAuthenticationFailed() {
            super.onAuthenticationFailed();
        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
            super.onAuthenticationHelp(helpCode, helpString);
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            super.onAuthenticationSucceeded(result);
            Toast.makeText(context, "Clicked..", Toast.LENGTH_SHORT).show();
            if (flag == 1) {
                doneImageView.setImageResource(R.mipmap.done);
                dialogfinger.dismiss();
                createImageName();
            } else {
                doneImageView.setImageResource(R.mipmap.done);
                Log.e("s", "Authentication");

                submitBtnFlag = "start";
                finish();
//                createImageName();
            }
        }
    }
    private void createImageName() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "IMG_" + timeStamp + ".jpg";
        onLaunchCamera();
    }
    //---- This function is used to open camera functionality.-----//
    public void onLaunchCamera() {
        // create Intent to take a picture and return control to the calling application
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Create a File reference to access to future access
        photoFile = getPhotoFileUri(imageFileName);

        // wrap File object into a content provider
        fileProvider = FileProvider.getUriForFile(DefectListChecklistActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider);
        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Start the image capture intent to take photo
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
    }

    // Returns the File for a photo stored on disk given the fileName
    public File getPhotoFileUri(String fileName) {

        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);

        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d(APP_TAG, "failed to create directory");
        }

        // Return the file target for the photo based on filename
//        file = new File(mediaStorageDir.getPath() + File.separator + fileName +timeStamp + ".jpg");
        file = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return file;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (flag == 1) {
                    try {
                        takenImage = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
                        FileOutputStream fos = new FileOutputStream(photoFile.getAbsolutePath());
                        Log.e("J", photoFile.getAbsolutePath());
                        takenImage.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                        fos.close();
                        cancelCompleteMethod(status);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        takenImage = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
                        ExifInterface ei = null;
                        try {
                            ei = new ExifInterface(String.valueOf(photoFile.getAbsolutePath()));

                            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_UNDEFINED);


                            switch (orientation) {

                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    rotatedBitmap = rotateImage(takenImage, 90);
                                    break;

                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    rotatedBitmap = rotateImage(takenImage, 180);
                                    break;

                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    rotatedBitmap = rotateImage(takenImage, 270);
                                    break;

                                case ExifInterface.ORIENTATION_NORMAL:
                                default:
                                    rotatedBitmap = takenImage;
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        FileOutputStream fos = new FileOutputStream(photoFile.getAbsolutePath());
                        Log.e("J", photoFile.getAbsolutePath());
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 40, fos);

                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//***********************Bitmap convert base64 and than convert bitmap for resizing*******
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    //decode base64 string to image
                    byte[] imageBytes = Base64.decode(encodedImage, Base64.DEFAULT);
                    Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, options);

                    listPhoto.add(photoFile.getAbsolutePath());
                    JSONObject temp = new JSONObject();
                    try {
                        String strTimeStamp = new UtilClassFuntions().currentDateTime();
                        temp.put("location", crnt_lctn);
                        temp.put("timeStamp", strTimeStamp);
                        temp.put("latitude", latitude);
                        temp.put("longitude", longitude);
                        imageJson.put(imageFileName, temp);
                        locationList.add(crnt_lctn);
                        timeStampList.add(strTimeStamp);
                        Log.e("time1", strTimeStamp);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("temp", String.valueOf(temp));


                   /* try {
                        itemsListData.add(photoFile.getAbsolutePath());
                        imageMap.put(position, itemsListData);
                        locationMap.put(position, locationList);
                        timeStampMap.put(position, timeStampList);
                        vesselPscInspectionChecklistAdapter.setImageMap(imageMap, locationMap, timeStampMap);
                        vesselPscInspectionChecklistAdapter.notifyDataSetChanged();
                        photo = String.valueOf(imageMap.values());

                        if (point.equals("")) {
                            point = tempPoint;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (submitBtnFlag.equalsIgnoreCase("start")) {
                        Log.e("position", String.valueOf(position));
                        submitDataMethod();
                    }*/
                }
            }
        }

    }
    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public void cancelCompleteMethod(String status) {
        ProgressDialog pdLoading = new ProgressDialog(DefectListChecklistActivity.this);
        if (pdLoading != null) {
            pdLoading.setTitle("Please wait...");
            pdLoading.show();
        }
        Map<String, String> params = new HashMap<>();
        params.put("comment", submitComment);
        params.put("wrk_id", wrk_id);
        params.put("user_id", userDetailsList.get(0).getUser_id());
        params.put("accessToken", userDetailsList.get(0).getAccessToken());
        params.put("modified_dt", new UtilClassFuntions().currentDateTime());
        params.put("dbnm", userDetailsList.get(0).getDbnm());
        params.put("checklist_status", status);

        //****************************

        if (!wrk_id.equals("")) {

            DefectListDataList defectListDataList = new DefectListDataList();
            defectListDataList.setWrk_id(Integer.valueOf(wrk_id));
            defectListDataList.setCncl_Comp_comment(submitComment);
            defectListDataList.setCompleted_by(emp_id);
            defectListDataList.setStatus(Integer.valueOf(status));

            Thread threadSetSubmit = new Thread(new Runnable() {
                @Override
                public void run() {
                    checkListDatabase.productDao().updateDefectListCancelComplete(wrk_id, submitComment,userDetailsList.get(0).getUser_id(), status);
                }
            });
            threadSetSubmit.start();

        }

        //*****************************
        JSONObject jsonObject = new JSONObject(params);
        try {

            String dataKey = "details";

            MultipartUtility multipart = new MultipartUtility(ApiUrlClass.cancel_complete_url, ApiUrlClass.charset, dataKey, jsonObject);
            List<File> file1 = new ArrayList<>();

            if (photoFile != null) {
                file1.add(0, new File(photoFile.getAbsolutePath()));
                multipart.addFilePart("userimg", file1);
            }

            List<String> response = multipart.finish();
            Log.e("SERVER REPLIED:", String.valueOf(response));

            for (String line : response) {
                System.out.println(line);
                Log.e("res", line);
                pdLoading.dismiss();
                finish();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
