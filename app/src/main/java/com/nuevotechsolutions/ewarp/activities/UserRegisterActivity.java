package com.nuevotechsolutions.ewarp.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.MailTo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.android.volley.BuildConfig;
import com.android.volley.VolleyError;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.GmailSender;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import static android.os.Build.VERSION_CODES.N;
import static android.os.Build.VERSION_CODES.O_MR1;

@SuppressWarnings("All")
public class UserRegisterActivity extends AppCompatActivity {

    private ImageView userimg;
    private ImageView imgcamera;
    Uri file;
    File file1, photofile;
    String imageFileName, path;
    private static final int REQUEST_CODE = 1;
    private Bitmap bitmap, rotatedBitmap;
    private ImageView backarrowregister;
    EditText firstNameEditText, middleNameEditText, lastNameEditText, employeeIdEditText,
            emailEditText, passwordEditText, confirmpasswordEditText, companyNameEditText,
            vesselEditText, rankEditText, phoneEditText, etOtp;
    Button registerSubmitButton;
    private String generatedPassword;
    private long otpTime;
    private TextView txtresend, txt2;
    String firstName, lastName, emailId, password, cnfrmPassword, rank, moNumber;
    ProgressBar progressbarRegister,progressbarOTPDialog;
    private Dialog dialog;
    private TextView tvCounter;
    public int counter;
    public LinearLayout llregister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);
        imgcamera = findViewById(R.id.imgcamera);
        userimg = findViewById(R.id.userimg);
        backarrowregister = findViewById(R.id.backarrowregister);
        firstNameEditText = findViewById(R.id.firstNameEditText);
        lastNameEditText = findViewById(R.id.lastNameEditText);
        emailEditText = findViewById(R.id.emailEditText);
        progressbarRegister = findViewById(R.id.progressbarRegister);
        passwordEditText = findViewById(R.id.passwordEditText);
        confirmpasswordEditText = findViewById(R.id.confirmpasswordEditText);
        companyNameEditText = findViewById(R.id.companyNameEditText);
//        vesselEditText = findViewById(R.id.vesselEditText);
        rankEditText = findViewById(R.id.rankEditText);
        phoneEditText = findViewById(R.id.phoneEditText);
        registerSubmitButton = findViewById(R.id.registerSubmitButton);
        llregister = findViewById(R.id.llregister);
        imgcamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture(v);
            }
        });
        backarrowregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        CountryCodePicker country_picker = findViewById(R.id.country_picker);
        String country_code = country_picker.getSelectedCountryCode();
        registerSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstName = firstNameEditText.getText().toString();
                lastName = lastNameEditText.getText().toString();
                emailId = emailEditText.getText().toString();
                password = passwordEditText.getText().toString();
                cnfrmPassword = confirmpasswordEditText.getText().toString();
                /*if (!password.equals(cnfrmPassword)) {
                    Toast.makeText(UserRegisterActivity.this, "Password Not matching", Toast.LENGTH_SHORT).show();
                }
                passwordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            if (passwordEditText.getText().toString().length() < 6)
                                passwordEditText.setError("Password must contain min 6 character. ");
                            else
                                passwordEditText.setError(null);
                        }
                    }
                });*/

//                rank = rankEditText.getText().toString();
                moNumber = phoneEditText.getText().toString();
                if (firstName.equals("") || lastName.equals("") || emailId.equals("") ||
                        password.equals("") || cnfrmPassword.equals("") || /*cmpName.equals("") ||
//                        vesselId.equals("") ||rank.equals("") || */ moNumber.equals("")) {
                    if (firstName.equals("")) {
                        firstNameEditText.setError("Please enter first name!");
                    } else if (lastName.equals("")) {
                        lastNameEditText.setError("Please enter last name!");
                    } else if (moNumber.equals("")) {
                        phoneEditText.setError("Please enter mobile number!");
                    } else if (emailId.equals("")) {
                        emailEditText.setError("Please enter email!");
                    } else if (password.equals("")) {
                        passwordEditText.setError("Please enter password!");
                    } else if (cnfrmPassword.equals("")) {
                        confirmpasswordEditText.setError("Please enter confirm password!");

                    }
//                    else if (rank.equals("")) {
//                        rankEditText.setError("Please enter rank!");
//                    }

                }else if (password.length()<6) {
                    passwordEditText.setError("Password must contain min 6 character.");
                } else if (!password.equals(cnfrmPassword)) {
                    confirmpasswordEditText.setError("Password Mismatch!");
                } else if (emailId != null && !isValidEmailId(emailEditText.getText().toString().trim())) {
                    Toast.makeText(getApplicationContext(), "InValid Email Address.", Toast.LENGTH_SHORT).show();
                } else {
                    emailCheck();
//                    OtpDialog();

                }
            }
        });

    }

    private boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public void reverseTimer(int Seconds, final TextView tv) {

        new CountDownTimer(Seconds * 1000 + 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);

                int hours = seconds / (60 * 60);
                int tempMint = (seconds - (hours * 60 * 60));
                int minutes = tempMint / 60;
                seconds = tempMint - (minutes * 60);

//                tv.setText(String.format("%02d", minutes)
//                        + ":" + String.format("%02d", seconds));
                txtresend.setText("Resend OTP in " + String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds) + "s" );
                txtresend.setClickable(false);

            }

            public void onFinish() {
                tv.setText("");
                txtresend.setVisibility(View.VISIBLE);
                txtresend.setText("Resend OTP");
                txtresend.setTextColor(Color.parseColor("#3c79af"));
                txtresend.setClickable(true);
            }
        }.start();
    }

    private void emailCheck() {
        Map<String, String> params = new HashMap<>();
        params.put("valtype", "email");
        params.put("email", emailId);
        progressbarRegister.setVisibility(View.VISIBLE);
        VolleyMethods.makePostJsonObjectRequest(params, UserRegisterActivity.this, ApiUrlClass.ValidationEmailCheck, new PostJsonObjectRequestCallback() {

            @Override
            public void onSuccessResponse(JSONObject response) {

                if (response != null) {
                    Log.e("responseotp", String.valueOf(response));

                    try {
                        String Status = response.getString("status");

                        if (Status == "true") {
                            Snackbar snackbar = Snackbar.make(llregister, "This email is already exist!", Snackbar.LENGTH_LONG);
                            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.white));
                            TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
                            textView.setTextColor(Color.RED);
                            snackbar.show();
                            progressbarRegister.setVisibility(View.GONE);
                        } else {
                            generatedPassword  = (response.getString("otp"));
                            Log.e("generatedotp",generatedPassword);
                            OtpDialog();
                            progressbarRegister.setVisibility(View.GONE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onVolleyError(VolleyError error) {

            }

            @Override
            public void onTokenExpire() {

            }
        });


    }

    private void register() {


        Map<String, String> params = new HashMap<>();
        params.put("fname", firstName);
//                        params.put("mname", midleName);
        params.put("lname", lastName);
        /*params.put("empid", empId);*/
        params.put("email", emailId);
        params.put("pass", password);
                        /*params.put("companynm", cmpName);
                        params.put("vslName", vesselId);*/
        params.put("user_rank", "");
        params.put("number", moNumber);
        progressbarRegister.setVisibility(View.VISIBLE);

        VolleyMethods.makePostJsonObjectRequest(params, UserRegisterActivity.this, ApiUrlClass.REGISTRATION, new PostJsonObjectRequestCallback() {
            @Override
            public void onSuccessResponse(JSONObject response) {
                if (response != null) {
                    try {
                        Integer code = response.getInt("response");
                        if (code.equals(200)) {
                            progressbarRegister.setVisibility(View.GONE);
                            Integer user_id = response.getInt("user_id");
                            Toast.makeText(UserRegisterActivity.this, "Registered Successfully!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UserRegisterActivity.this, SlidingDotActivity.class);
                            intent.putExtra("user_id", user_id);
                            startActivity(intent);
                            finish();
                        } else if (code.equals(1062)) {
                            progressbarRegister.setVisibility(View.GONE);
                            emailEditText.setError("Email already exit!");
                        } else if (code.equals(1063)) {
                            progressbarRegister.setVisibility(View.GONE);
                            phoneEditText.setError("Mobile number already exit!");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(UserRegisterActivity.this, "Response is null!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onVolleyError(VolleyError error) {
                Toast.makeText(UserRegisterActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTokenExpire() {

            }
        });

    }


    private void takePicture(View view) {
        if (ActivityCompat.checkSelfPermission(UserRegisterActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(UserRegisterActivity.this, new String[]{Manifest.permission.CAMERA}, 0);
            return;
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "IMG_" + timeStamp + ".jpg";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            photofile = getPhotoFileUri(imageFileName);
            file = FileProvider.getUriForFile(UserRegisterActivity.this, BuildConfig.APPLICATION_ID + ".provider", photofile);

        } else {
            file = Uri.fromFile(getOutputMediaFile());
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, REQUEST_CODE);
    }

    public File getPhotoFileUri(String fileName) {
        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "LoginImage");
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {

        }
        file1 = new File(mediaStorageDir.getPath() + File.separator + fileName);

        return file1;
    }

    private static File getOutputMediaFile() {

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "LoginImage");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File imgpath = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
        return imgpath;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {
                    ExifInterface ei = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        bitmap = BitmapFactory.decodeFile(photofile.getAbsolutePath());
                        ei = new ExifInterface(photofile.getAbsolutePath());
                    } else {
                        bitmap = BitmapFactory.decodeFile(file.getPath());
                        ei = new ExifInterface(file.getPath());
                    }
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_UNDEFINED);
                    switch (orientation) {

                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotatedBitmap = rotateImage(bitmap, 90);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotatedBitmap = rotateImage(bitmap, 180);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotatedBitmap = rotateImage(bitmap, 270);
                            break;

                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                            rotatedBitmap = bitmap;
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        FileOutputStream fos = new FileOutputStream(photofile.getAbsolutePath());
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                        fos.close();
                        path = photofile.getAbsolutePath();
                    } else {
                        FileOutputStream fos = new FileOutputStream(file.getPath());
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                        fos.close();
                        path = file.getPath();

                    }

                    userimg.setImageBitmap(rotatedBitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private int generateRandomInt() {
        SecureRandom random = new SecureRandom();
        Random r = new Random(System.currentTimeMillis());
        int rand = ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
        generatedPassword = String.valueOf(rand);
        Log.d("MyApp", "Generated Password : " + generatedPassword);

        return rand;
//        generatedPassword = String.format("%04d", random.nextInt(10000));
//        Log.d("MyApp", "Generated Password : " + generatedPassword);
//        return random.nextInt(10000);
    }

    private void SendMailOTP() {
        generateRandomInt();
        GmailSender m = new GmailSender("ethicalworking@gmail.com", "Logix@415");
        Log.e("test", "test1");
        String[] toArr = {emailId};
        m.setTo(toArr);
        m.setFrom("ethicalworking@gmail.com");
        m.setSubject("EWARP - New user OTP");
        m.setBody("Your One Time Password is " + generatedPassword);

        try {
            if (m.send()) {
                progressbarOTPDialog.setVisibility(View.GONE);
                Toast.makeText(UserRegisterActivity.this, "Email was sent successfully.", Toast.LENGTH_LONG).show();
                tvCounter.setVisibility(View.VISIBLE);
                reverseTimer(60, tvCounter);
                otpTime = System.currentTimeMillis();
            } else {
                progressbarOTPDialog.setVisibility(View.GONE);
                Toast.makeText(UserRegisterActivity.this, "Email was not sent.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.e("MailApp", "Could not send email", e);
        }

    }

    private void OtpDialog() {
        dialog = new Dialog(UserRegisterActivity.this);
        dialog.setContentView(R.layout.otp_dialog);
        dialog.setCancelable(false);
        etOtp = dialog.findViewById(R.id.etOtp);
        progressbarOTPDialog = dialog.findViewById(R.id.progressbarOTPDialog);
        txtresend = dialog.findViewById(R.id.txtresend);
        txt2 = dialog.findViewById(R.id.txt2);
        ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
        tvCounter = dialog.findViewById(R.id.tvCounter);
        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
//        SendMailOTP();
        if (emailId != null) {
            StringBuilder myEmail = new StringBuilder(emailId);
            if (myEmail.length() > 4) {
                myEmail.setCharAt(1, 'x');
                myEmail.setCharAt(2, 'x');
                myEmail.setCharAt(3, 'x');
                txt2.setText("A OTP has been sent to " + myEmail + ".\n(Kindly check your spam folder in case OTP is not found in inbox.)");
            }
        } else {
            Toast.makeText(UserRegisterActivity.this, "Plaese fill all the field", Toast.LENGTH_LONG).show();
        }
        Button btnVerify = dialog.findViewById(R.id.btnVerify);

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otpValue = etOtp.getText().toString();
                long newOtpTime = System.currentTimeMillis();
                if (otpValue.equals(generatedPassword)) {
                    register();
                    dialog.dismiss();
                } else if (otpValue.equalsIgnoreCase("")) {
                    Toast.makeText(UserRegisterActivity.this, "Please enter your OTP.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(UserRegisterActivity.this, "OTP doesn't match", Toast.LENGTH_SHORT).show();
                }

            }

        });
        txtresend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtresend.setTextColor(Color.parseColor("#D5D1D1"));
                txtresend.setClickable(false);
                SendMailOTP();
                Toast.makeText(UserRegisterActivity.this, "OTP resend successfully", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();

    }

}
