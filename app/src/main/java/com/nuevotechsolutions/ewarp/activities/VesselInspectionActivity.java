package com.nuevotechsolutions.ewarp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.room.Room;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.navigation.NavigationView;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.adapter.DefaultImgSliderAdapter;
import com.nuevotechsolutions.ewarp.adapter.SliderImageAdapter;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ImageModel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.OnBackPressed;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class VesselInspectionActivity extends AppCompatActivity {

    TextView textView_vessel_header;
    DrawerLayout drawerLayout;

    Toolbar toolbar;

    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    CardView shipInspectionButton,PSCInspectionButtion;
    public static TextView badge_notification_s1, badge_notification_s2;
    List<MasterWorkId> list=new ArrayList<>();
    private Handler mHandler;

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<ImageModel> imageModelArrayList;
    private int[] myImageList = new int[]{R.drawable.navyslide, R.drawable.flipimagee,
            R.drawable.slideimage,R.drawable.slideimagee};
    CheckListDatabase cDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vessel_inspection);

        mHandler = new Handler();
        textView_vessel_header=findViewById(R.id.textView_homePage_header_vessel);
        textView_vessel_header.setText("Vessel Inspection");
        shipInspectionButton=findViewById(R.id.shipInspButton);
        PSCInspectionButtion=findViewById(R.id.PSCInspButton);
        imageModelArrayList = new ArrayList<>();
        imageModelArrayList = populateList();
        init();
        badge_notification_s1 = findViewById(R.id.badge_notification_s1);
        badge_notification_s2 = findViewById(R.id.badge_notification_s2);

        setUpToolbar();

        cDb = Room.databaseBuilder(this.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        list = cDb.productDao().getMasterWorkIdData();

        List<UserDetails> userDetails=new ArrayList<>();
        userDetails=cDb.productDao().getUserDetail();
        if (userDetails.get(0).getFirst_name()!=null){
            String user=userDetails.get(0).getFirst_name();
//            nav_user.setText(user);
        }else{
//            nav_user.setText("Guest User");
        }
        this.mHandler.postDelayed(m_Runnable,2000);

        shipInspectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(VesselInspectionActivity.this,ShipInspectionActivity.class);
                startActivity(intent);
            }
        });

        PSCInspectionButtion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(VesselInspectionActivity.this,PSCInspectionActivity.class);
                startActivity(intent);
            }
        });

    }

    private void checkListNotifications() {

        List<MasterWorkId> workinglist=new ArrayList<>();
        List<MasterWorkId> completedList=new ArrayList<>();
        List<MasterWorkId> assignList=new ArrayList<>();
        int j=0;
        for (int i=list.size()-1;i>=0;i--)
        {
            if (list.get(i).getWrk_status().equals(0) && list.get(i).getSeen()==false){
                workinglist.add(j,list.get(i));
                j++;
            }
        }

        int k=0;
        for (int i=list.size()-1;i>=0;i--)
        {
            if ((list.get(i).getWrk_status().equals(1)||list.get(i).getWrk_status().equals(2))
                    && list.get(i).getSeen()==false) {
                completedList.add(k,list.get(i));
                k++;
            }
        }

        int r=0;
        for (int i=list.size()-1;i>=0;i--)
        {
            if ((list.get(i).getWrk_status().equals(3)||list.get(i).getWrk_status().equals(5))
                    && list.get(i).getSeen()==false) {
                assignList.add(r,list.get(i));
                r++;
            }
        }
        String completedCount=String.valueOf(completedList.size());
        String resumeCount=String.valueOf(workinglist.size());
        String assignCount=String.valueOf(assignList.size());

        if (completedList.size()>0){
            badge_notification_s1.setVisibility(View.VISIBLE);
            badge_notification_s1.setText(completedCount);
        }
        if (workinglist.size()>0){
            badge_notification_s2.setVisibility(View.VISIBLE);
            badge_notification_s2.setText(resumeCount);
        }

    }
    private final Runnable m_Runnable = new Runnable()
    {
        public void run()
        {
            if (list.size()>0){
                checkListNotifications();
            }
            VesselInspectionActivity.this.mHandler.postDelayed(m_Runnable, 2000);
        }
    };

    private void setUpToolbar() {
//        drawerLayout = findViewById(R.id.drawerLayoutVessel);
        toolbar = findViewById(R.id.toolbarVessel);
        ImageButton backarrowVesselsInspection = findViewById(R.id.backarrowVesselsInspection);
        backarrowVesselsInspection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnBackPressed().onBackPressedFunction(VesselInspectionActivity.this);
                finish();
            }
        });
        setSupportActionBar(toolbar);
//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, app_name, app_name);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
    }

    private ArrayList<ImageModel> populateList(){

        ArrayList<ImageModel> list = new ArrayList<>();

        for(int i = 0; i < 4; i++){
            ImageModel imageModel = new ImageModel();
            imageModel.setImage_drawable(myImageList[i]);
            list.add(imageModel);
        }

        return list;
    }

    private void init() {

        mPager = (ViewPager) findViewById(R.id.pagerVessel);
        mPager.setAdapter(new DefaultImgSliderAdapter(VesselInspectionActivity.this,imageModelArrayList));

        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicatorVessel);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES =imageModelArrayList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

}
