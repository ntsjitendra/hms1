package com.nuevotechsolutions.ewarp.activities;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.room.Room;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.DepartmentList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserPointAccessData;
import com.nuevotechsolutions.ewarp.model.NoticeDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklist;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselComponent;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselMasterWorkId;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.LogoutClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import static com.nuevotechsolutions.ewarp.utils.ApiUrlClass.TempLoginData;


public class FingerprintActivity extends AppCompatActivity {

    ImageView fingerprintImage;
    TextView paraLabel, backonLoginPage;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private ProgressBar progressbarFingerprint;
    CheckListDatabase checkListDatabase;
    List<CheckList> checkLists;
    List<Component> componentList;
    List<UserDetails> userDetailsList;
    List<AuxEngDetails> auxEngDetailsList;
    List<AuxEngLabel> auxEngLabelList;
    List<TeamSelectionList> teamSelectionLists;
    List<ChecklistRecordDataList> checklistRecordDataList;
    List<MasterWorkId> masterWorkIdList;
    List<UserPointAccessData> userPointAccessDataList;
    List<EnginDescriptionData> enginDescriptionData;
    List<VesselChecklist> vesselChecklists;
    List<VesselComponent> vesselComponentList;
    List<VesselMasterWorkId> vesselMasterWorkIdList;
    List<DepartmentList> departmentLists;
    List<NoticeDataList> noticeLists;

    FingerprintManager fingerprintManager;
    KeyguardManager keyguardManager;
    int counter1 = 5;

    int counter = 0;
    int count;
    int remember = 0;
    List<CheckList> list;

    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprint);

        fingerprintImage = findViewById(R.id.fingerprint);
        paraLabel = findViewById(R.id.paraLebel);
        backonLoginPage = findViewById(R.id.backonLoginPage);
        progressbarFingerprint = findViewById(R.id.progressbarFingerprint);

        checkLists = new ArrayList<>();
        componentList = new ArrayList<>();
        userDetailsList = new ArrayList<>();
        auxEngDetailsList = new ArrayList<>();
        auxEngLabelList = new ArrayList<>();
        teamSelectionLists = new ArrayList<>();
        checklistRecordDataList = new ArrayList<>();
        masterWorkIdList = new ArrayList<>();
        userPointAccessDataList = new ArrayList<>();
        enginDescriptionData = new ArrayList<>();
        departmentLists = new ArrayList<>();
        noticeLists = new ArrayList<>();

        executor = ContextCompat.getMainExecutor(this);
        biometricPrompt = new BiometricPrompt(FingerprintActivity.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                fingerprintImage.setImageResource(R.mipmap.done);
                paraLabel.setTextColor(ContextCompat.getColor(FingerprintActivity.this, R.color.colorGreen));
                paraLabel.setText("Authentication Success");
                successLogin();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Biometric login for EWARP")
                .setSubtitle("Log in using your biometric credential")
                .setNegativeButtonText("Click here to close the dialog.")
                .build();

        // Prompt appears when user clicks "Log in".
        // Consider integrating with the keystore to unlock cryptographic operations,
        // if needed by your app.
        fingerprintImage.setOnClickListener(view -> {
            biometricPrompt.authenticate(promptInfo);
        });


        BiometricManager biometricManager = BiometricManager.from(this);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                paraLabel.setText("Click here to open the biometric sensor dialog.");
                biometricPrompt.authenticate(promptInfo);
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                paraLabel.setText("Fingerprint Scanner not Detected.");
                completeLogin();
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                paraLabel.setText("Biometric features are currently unavailable.");
                completeLogin();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                paraLabel.setText("You should add atleast 1 Fingerprint to use this Feature.");
                break;
        }

                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

            if (!fingerprintManager.isHardwareDetected()) {
                paraLabel.setText("Fingerprint Scanner not Detected");
            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                    paraLabel.setText("Permission not granted");
                } else if (!keyguardManager.isKeyguardSecure()) {
                    paraLabel.setText("Add biometric to your phone setting.");
                } else if (!fingerprintManager.hasEnrolledFingerprints()) {
                    paraLabel.setText("You should add atleast 1 Fingerprint to use this Feature");
                } else {
                    paraLabel.setText("Place Finger on the sensor");
                    FingerprintHandler fingerprintHandler = new FingerprintHandler(this);
                    fingerprintHandler.startAuth(fingerprintManager, null);
                }

            }
        } else {
            completeLogin();
        }*/
        backonLoginPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LogoutClass().logoutMethod(FingerprintActivity.this);
            }
        });
    }
    public void successLogin() {
        SharedPreferences.Editor editor = this.getSharedPreferences("FingerprintKey", MODE_PRIVATE).edit();
        editor.putInt("fingerprintFlagKey", 1);
        editor.apply();
        Bundle b = getIntent().getExtras();
        String u = b.getString("username");
        String id = b.getString("uid");
//            storeAllData();
        Intent intent = new Intent(FingerprintActivity.this, ImagePickActivity.class);
        intent.putExtra("username", u);
        intent.putExtra("uid", id);
        startActivity(intent);
        FingerprintActivity.this.finish();

    }

    public void completeLogin() {
        Bundle b = getIntent().getExtras();
        String u = b.getString("username");
        String id = b.getString("uid");

        pref = getSharedPreferences("username", MODE_PRIVATE);
        pref = getSharedPreferences("uid", MODE_PRIVATE);
        pref.edit();
        editor.commit();

        Intent intent = new Intent(FingerprintActivity.this, ImagePickActivity.class);
        intent.putExtra("username", u);
        intent.putExtra("uid", id);
        startActivity(intent);
        FingerprintActivity.this.finish();


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

        private Context context;

        public FingerprintHandler(Context context) {
            this.context = context;
        }

        public void startAuth(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject) {
            CancellationSignal cancellationSignal = new CancellationSignal();
            fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
        }

        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {
            this.update("" + errString, false);
        }

        @Override
        public void onAuthenticationFailed() {
            counter1--;
            this.update("Authentication Failed." + "\nPlease try again. " + "\n\n" + " Attempt remaining:" + counter1, false);

        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
            this.update("Error:" + helpString, false);

        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            fingerprintImage.setImageResource(R.mipmap.done);
            paraLabel.setTextColor(ContextCompat.getColor(context, R.color.colorGreen));
            paraLabel.setText("Authentication Success");
            successLogin();

        }

        private void update(String s, boolean b) {

            paraLabel.setText(s);

            if (b == false) {

                paraLabel.setTextColor(ContextCompat.getColor(context, R.color.colorRed));
                fingerprintImage.setImageResource(R.mipmap.failedfingerprint);
            } else {
                paraLabel.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                fingerprintImage.setImageResource(R.mipmap.done);


            }

        }

        /*public void successLogin() {
            SharedPreferences.Editor editor = context.getSharedPreferences("FingerprintKey", MODE_PRIVATE).edit();
            editor.putInt("fingerprintFlagKey", 1);
            editor.apply();
            Bundle b = getIntent().getExtras();
            String u = b.getString("username");
            String id = b.getString("uid");
//            storeAllData();
            Intent intent = new Intent(FingerprintActivity.this, ImagePickActivity.class);
            intent.putExtra("username", u);
            intent.putExtra("uid", id);
            startActivity(intent);
            FingerprintActivity.this.finish();

        }*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
