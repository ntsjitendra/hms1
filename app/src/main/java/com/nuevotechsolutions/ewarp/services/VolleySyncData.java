package com.nuevotechsolutions.ewarp.services;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.toolbox.Volley;

public class VolleySyncData {

  private static VolleySyncData mInstance;
  private com.android.volley.RequestQueue mRequestQueue;
  private static Context mCtx;

  private VolleySyncData(Context context) {
    mCtx = context;
    mRequestQueue = getRequestQueue();

  }

  public static synchronized VolleySyncData getInstance(Context context) {
    if (mInstance == null) {
      mInstance = new VolleySyncData(context);
    }
    return mInstance;
  }

  public com.android.volley.RequestQueue getRequestQueue() {
    if (mRequestQueue == null) {
      // getApplicationContext() is key, it keeps you from leaking the
      // Activity or BroadcastReceiver if someone passes one in.
      mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
    }
    return mRequestQueue;
  }

  public <T> void addToRequestQueue(Request<T> req) {
    getRequestQueue().add(req);
  }
}
