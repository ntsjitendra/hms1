package com.nuevotechsolutions.ewarp.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.room.Room;

import com.nuevotechsolutions.ewarp.activities.LoginActivity;
import com.nuevotechsolutions.ewarp.controller.DatabaseHandler;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;
import com.nuevotechsolutions.ewarp.utils.SharedPrefancesClearData;

public class TimeChangedReceiver extends BroadcastReceiver {
    DatabaseHandler db;
    CheckListDatabase cDb;

    @Override
    public void onReceive(Context context, Intent intent) {
        db = new DatabaseHandler(context);
        cDb = Room.databaseBuilder(context, CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        final String action = intent.getAction();

        if (action.equals(Intent.ACTION_TIME_CHANGED) ||
                action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {
            Toast.makeText(context,"You changed time manually,You will be logout from Ewarp app",Toast.LENGTH_LONG).show();
            Intent logout = new Intent(context, LoginActivity.class);
            new SharedPrefancesClearData().ClearBaseData(context);
            new SharedPrefancesClearData().ClearFingerprintData(context);
            logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            logout.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            db.deleteAllRecordAuxillaryEng();
            cDb.clearAllTables();
            System.exit(0);
            context.startActivity(logout);

        }
    }
    }

