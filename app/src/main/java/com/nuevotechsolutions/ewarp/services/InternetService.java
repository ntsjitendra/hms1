package com.nuevotechsolutions.ewarp.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.nuevotechsolutions.ewarp.utils.SubmitedDataSync;

public class InternetService extends IntentService {

    private static final String TAG = "InternetService";
    Context context;

    public InternetService() {
        super(TAG);
        context = this;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        boolean isNetworkConnected = extras.getBoolean("isNetworkConnected");
        Log.d("internet ", isNetworkConnected +"");

        if(isNetworkConnected){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    new SubmitedDataSync().getOfflineCreatedChecklist(context);
                }
            }).start();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    new SubmitedDataSync().getOfflineAssignChecklist(context);
                }
            }).start();
//            new SubmitedDataSync().getOfflineData(context);
        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("CustomService", "Service Destroyed");
    }
}
