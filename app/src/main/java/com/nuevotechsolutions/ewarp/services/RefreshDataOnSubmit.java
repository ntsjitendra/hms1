package com.nuevotechsolutions.ewarp.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.nuevotechsolutions.ewarp.utils.ChecklistRecordGetData;

public class RefreshDataOnSubmit extends IntentService {

    private static final String TAG = "RefreshDataOnSubmit";
    Context context;
    NetworkDetector networkDetector=new NetworkDetector();

    public RefreshDataOnSubmit() {
        super(TAG);
        context = this;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (networkDetector.isNetworkReachable(context)){
            Bundle b = intent.getExtras();
            String wrk_id=b.getString("wrk_id");
            String uid=b.getString("uid");
            String checklist_type=b.getString("checklist_type");

            new ChecklistRecordGetData().checklistRecordGetData(context,wrk_id,uid,checklist_type);
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

    }

}
