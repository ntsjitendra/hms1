package com.nuevotechsolutions.ewarp.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

public class NetworkDetector {
    public boolean isNetworkReachable(Context context) {
        ConnectivityManager manager =
                (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean isConnectionFast(Context context) {
        if(isNetworkReachable(context)== true){
            ConnectivityManager manager =
                    (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = manager.getActiveNetworkInfo();

            int type = networkInfo.getType();
            int subType = networkInfo.getSubtype();

            NetworkCapabilities nc = manager.getNetworkCapabilities(manager.getActiveNetwork());
            int downSpeed = nc.getLinkDownstreamBandwidthKbps();
            int upSpeed = nc.getLinkUpstreamBandwidthKbps();
            Log.e("speed..", String.valueOf(upSpeed + ":" + downSpeed));

            if (type == ConnectivityManager.TYPE_WIFI) {
                Log.e("NetworkType", "wifi");
                return true;
            } else if (type == ConnectivityManager.TYPE_MOBILE) {
                switch (subType) {
                    case TelephonyManager.NETWORK_TYPE_1xRTT:
                        Log.e("NetworkType", "50-100kbps");
                        return false; // ~ 50-100 kbps
                    case TelephonyManager.NETWORK_TYPE_CDMA:
                        Log.e("NetworkType", "14-64kbps");
                        return false; // ~ 14-64 kbps
                    case TelephonyManager.NETWORK_TYPE_EDGE:
                        Log.e("NetworkType1", "50-100kbps");
                        return false; // ~ 50-100 kbps
                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
                        Log.e("NetworkType", "400-1000kbps");
                        return true; // ~ 400-1000 kbps
                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
                        Log.e("NetworkType", "600-1400kbps");
                        return true; // ~ 600-1400 kbps
                    case TelephonyManager.NETWORK_TYPE_GPRS:
                        Log.e("NetworkType", "100kbps");
                        return false; // ~ 100 kbps
                    case TelephonyManager.NETWORK_TYPE_HSDPA:
                        Log.e("NetworkType", "2-14Mbps");
                        return true; // ~ 2-14 Mbps
                    case TelephonyManager.NETWORK_TYPE_HSPA:
                        Log.e("NetworkType", "700-1700kbps");
                        return true; // ~ 700-1700 kbps
                    case TelephonyManager.NETWORK_TYPE_HSUPA:
                        Log.e("NetworkType", "1-23Mbps");
                        return true; // ~ 1-23 Mbps
                    case TelephonyManager.NETWORK_TYPE_UMTS:
                        Log.e("NetworkType", "400-7000kbps");
                        return true; // ~ 400-7000 kbps
                    /*
                     * Above API level 7, make sure to set android:targetSdkVersion
                     * to appropriate level to use these
                     */
                    case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                        Log.e("NetworkType", "1-2Mbps");
                        return true; // ~ 1-2 Mbps
                    case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                        Log.e("NetworkType", "5Mbps");
                        return true; // ~ 5 Mbps
                    case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                        Log.e("NetworkType", "10-20Mbps");
                        return true; // ~ 10-20 Mbps
                    case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                        Log.e("NetworkType", "25Kbps");
                        return false; // ~25 kbps
                    case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                        Log.e("NetworkType", "10+Mbps");
                        return true; // ~ 10+ Mbps
                    // Unknown
                    case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                    default:
                        return false;

                }
            } else {
                return false;
            }


        }
    return false;
    }
    }
