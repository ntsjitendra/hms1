package com.nuevotechsolutions.ewarp.services;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class NetworkChangeReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        ComponentName comp = new ComponentName(context.getPackageName(),
                InternetService.class.getName());
        intent.putExtra("isNetworkConnected",isOnline(context));
        context.startService(intent.setComponent(comp));
//        try
//        {
//            if (isOnline(context)) {
////                new SubmitedDataSync().getOfflineData(context);
//            } else {
////                dialog(false);
//            }
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
    }

    private boolean isOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            return (netInfo != null && netInfo.isConnected());
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }

    }

