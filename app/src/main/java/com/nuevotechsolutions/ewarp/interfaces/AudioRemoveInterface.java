package com.nuevotechsolutions.ewarp.interfaces;

public interface AudioRemoveInterface {
    void onRemoveAudioClickListener(int position);

}
