package com.nuevotechsolutions.ewarp.interfaces;

import android.content.Context;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.nuevotechsolutions.ewarp.adapter.ImageListDataAdapter;

import java.util.List;
import java.util.Map;

public interface ImageInterface {

    void onPhotoButtonClickListener(int post, ImageView[] imageViews);

    void onPhotoButtonClickListenerAdapter(int post, String points, ImageView[] imageViews, ImageListDataAdapter adapter,
                                           List<String> imageList, List<String> locationList,List<String> timeStampList,List<String> photoTextValueList);

    void onSwitchButtonClickListener(int post, String points, Context context, Switch aSwitch, RadioButton[] radioButtons,
                                     String alertDeparment, EditText editTexts,LinearLayout linearLayout);

    void onImageClickListener(int post, String points, ImageView imageView, Context context, ImageButton button);

    void onSubmitButtonClickListener(int post, String points, RadioButton[] radioButtons,
                                     ImageButton editText, Switch aSwitch, ImageButton button,
                                     TextView textView, TextView editTextSubmit, String s,
                                     LinearLayout linearLayout, ImageButton b,
                                     ImageButton photoButton, ImageListDataAdapter adapter,
                                     List<String> imageList, List<String> locationList,
                                     List<String> timeStampList,
                                     List<String> photoTextValueList, Spinner spinner, LinearLayout pointEditSubmitedLinearLayout, String isVisible);

    void onGuidanceButtonClickListener(int post, String points, Context context);
    void onRecordButtonClickListener(int post);
    void onTableViewClickListener(int post,String Cpoints);
    void onPlayButtonClickListener(int post, String Cpoints);

    void onCommentButtonClickListener(int post, String points, Context context, TextView editText, LinearLayout linearLayout);
    void onAttachPositionClickListener(int post, Map<Integer,List<String>> imageMap,
                                       Map<Integer,List<String>> locationMap,
                                       Map<Integer,List<String>> timestampMap,
                                       Map<Integer,List<String>> photoTextValueMap,
                                       List<String> listPhoto,
                                       List<String> locationList,
                                       List<String> timeStampList,
                                       List<String> photoTextValueList);
    void onPhotoTextValue(int post, String photoTextValue, String imageName, Context context, int k);

    void onEditButtonClickListener(int post, Context context, String points, Switch aSwitch, RadioButton[] radioButtons, String txt,List<Boolean> booleanList);

}
