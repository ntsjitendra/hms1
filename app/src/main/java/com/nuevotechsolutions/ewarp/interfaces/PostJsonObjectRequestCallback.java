package com.nuevotechsolutions.ewarp.interfaces;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface PostJsonObjectRequestCallback {
    void onSuccessResponse(JSONObject response);
    void onVolleyError(VolleyError error);
    void onTokenExpire();
}
