package com.nuevotechsolutions.ewarp.interfaces;

import android.content.Context;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.nuevotechsolutions.ewarp.adapter.ImageListDataAdapter;

import java.util.List;

public interface VesselImageInterface {


    void onPhotoButtonClickListener(int post, ImageView[] imageViews);

    void onPhotoButtonClickListenerAdapter(int post, String points, ImageView[] imageViews, ImageListDataAdapter adapter, List<String> imageList, List<String> locationList, List<String> timeStampList, String locationValue);

    void onSwitchButtonClickListener(int post, String points, Context context, Switch aSwitch, RadioButton[] radioButtons, String alertDeparment,
                                     EditText editTexts, LinearLayout linearLayout);

    void onImageClickListener(int post, String points, ImageView imageView, Context context, ImageButton button);

    void onSubmitButtonClickListener(int post, String points, RadioButton[] radioButtons,
                                     ImageButton editText, Switch aSwitch, ImageButton button,
                                     TextView textView, TextView editTextSubmit, String s,
                                     LinearLayout linearLayout, ImageButton b,
                                     ImageButton photoButton, ImageListDataAdapter adapter,
                                     List<String> imageList, List<String> locationList,
                                     List<String> timeStampList, Spinner spinner);

    void onGuidanceButtonClickListener(int post, String points, Context context);

    void onCommentButtonClickListener(int post, String points, Context context, TextView editText, LinearLayout linearLayout);

}
