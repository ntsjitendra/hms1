package com.nuevotechsolutions.ewarp.interfaces;

import org.json.JSONObject;

public interface MultiPartResponseInerface {
     void onResponseSucess(String response);
     void onResponseError(String response);
}
