package com.nuevotechsolutions.ewarp.interfaces;

import com.android.volley.VolleyError;

public interface PostStringRequestCallback {
    void onSuccessResponse(String response);
    void onVolleyError(VolleyError error);
    void onTokenExpire();
}
