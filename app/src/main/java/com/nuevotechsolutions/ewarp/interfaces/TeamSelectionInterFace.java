package com.nuevotechsolutions.ewarp.interfaces;

import android.widget.EditText;
import android.widget.TextView;

public interface TeamSelectionInterFace {

    void onTeamSelectionButtonClickListener(int i, TextView engineerCtgryTextView, TextView engineerNameTextView,TextView emplId);

    void onDescriptionButtonClickListener(int post, EditText editText, TextView textInputLayout);


}
