package com.nuevotechsolutions.ewarp.interfaces;

import android.view.View;

public interface StickyHeaderInterface {

    int getHeaderPositionForItem(int itemPosition);

    int getHeaderLayout(int headerPosition);

    void bindHeaderData(View header, int headerPosition);

    boolean isHeader(int itemPosition);


}
