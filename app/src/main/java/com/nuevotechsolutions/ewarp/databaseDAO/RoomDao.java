package com.nuevotechsolutions.ewarp.databaseDAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecord;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.DepartmentList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.DefectListModel.DefectListDataList;
import com.nuevotechsolutions.ewarp.model.NoticeDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklist;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselComponent;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselMasterWorkId;

import java.util.List;

@Dao
public interface RoomDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setAllCheckList(List<CheckList> checklist);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setVesselSubmitData(VesselChecklistRecordDataList vesselSubmitData);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setAllComponentList(List<Component> componentList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setAllUserDetailsList(List<UserDetails> userDetailsList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setAllAuxEngDetailsList(List<AuxEngDetails> allAuxEngDetailsList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setAllAuxEngLabelList(List<AuxEngLabel> allAuxEngLabelList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setAllTeamSelectionList(List<TeamSelectionList> teamSelectionLists);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setChecklistRecordDataList(List<ChecklistRecordDataList> checklistRecordDataLists);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setVesselChecklistRecordDataList(List<VesselChecklistRecordDataList> vesselChecklistRecordDataList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setChecklistRecordDataList1(ChecklistRecordDataList checklistRecordDataLists);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setMasterWorkIdList(List<MasterWorkId> masterWorkIdList);

//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void setUserPointAccessDataList(List<UserPointAccessData> userPointAccessDataList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setEnginDescriptionData(List<EnginDescriptionData> enginDescriptionDataList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setDepartmentData(List<DepartmentList> departmentLists);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setChecklistRecordList(List<ChecklistRecord> checklistRecords);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setAllVesselCheckList(List<VesselChecklist> vesselCheckList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setAllVesselComponentList(List<VesselComponent> vesselComponentList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setVesselMasterWorkIdList(List<VesselMasterWorkId> vesselMasterWorkIdList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setVesselMasterWorkId(VesselMasterWorkId vesselMasterWorkIdList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setDefectList(DefectListDataList defectList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setDefectListDataList(List<DefectListDataList> defectList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setNoticeDataList(List<NoticeDataList> noticeDataList);


    @Query("SELECT * FROM checklist_table where delete_id=0")
    List<CheckList> getAllCheckList();

    @Query("SELECT * FROM checklist_table where delete_id=0 or delete_id=2")
    List<CheckList> getAllCheckList1();

    @Query("SELECT * FROM vessel_checklist")
    List<VesselChecklist> getAllVesselCheckList();

    @Query("SELECT * FROM aux_eng_dtls")
    List<AuxEngDetails> getAllAuxEngDtls();

    @Query("SELECT * FROM aux_eng_label")
    List<AuxEngLabel> getAllAuxEngLebels();

    @Query("SELECT * FROM component_table")
    List<Component> getAllComponents();

    @Query("SELECT * FROM department_table")
    List<DepartmentList> getAllDepartmentList();

    @Query("SELECT * FROM userdetail_table")
    List<UserDetails> getUserDetail();

    @Query("SELECT * FROM component_table WHERE uid =:uniqueId")
    List<Component> getComponentByUid(String uniqueId);

    @Query("SELECT * FROM vessel_component WHERE uid =:uniqueId")
    List<VesselComponent> getVesselComponentByUid(Integer uniqueId);

    @Query("SELECT * FROM team_selection_list")
    List<TeamSelectionList> getTeamSelectionData();

    @Query("SELECT * FROM checklist_record_data_list")
    List<ChecklistRecordDataList> getChecklistRecordDataList();

    @Query("SELECT * FROM vessel_checklist_record_data_list")
    List<VesselChecklistRecordDataList> getVesselChecklistRecordDataList();

    @Query("SELECT * FROM checklist_record_data_list WHERE flag=0")
    List<ChecklistRecordDataList> getChecklistRecordDataListWithFlag();

    @Query("SELECT * FROM master_wrk_id WHERE data_Upload_flag=0")
    List<MasterWorkId> getMasterWorkIdDataListWithFlag();

    @Query("SELECT * FROM master_wrk_id WHERE wrk_id < 0")
    List<MasterWorkId> getOfflineCreatedMasterWorkId();

    @Query("SELECT * FROM master_wrk_id WHERE offline_status = 0")
    List<MasterWorkId> getOfflineAssignChecklist();

    @Query("SELECT DISTINCT mw_id.* FROM master_wrk_id mw_id group by mw_id.wrk_id")
    List<MasterWorkId> getMasterWorkIdData();

    @Query("SELECT DISTINCT mw_id.* FROM vessel_master_wrk_id mw_id group by mw_id.wrk_id")
    List<VesselMasterWorkId> getVesselMasterWorkIdData();

    @Query("SELECT DISTINCT mw_id.* FROM defect_list mw_id group by mw_id.wrk_id")
    List<DefectListDataList> getDefectListData();

    @Query("SELECT * FROM check_list_details")
    List<ChecklistRecord> getChecklistRecordData();

    @Query("SELECT * FROM engin_description_data")
    List<EnginDescriptionData> getEnginDescriptionData();

    @Query("SELECT * FROM notice_list_table")
    List<NoticeDataList> getAllNoticeList();

    @Query("UPDATE checklist_record_data_list SET flag = :flag WHERE wrk_id = :wrk_id AND checklist_points = :point")
    void updateRecord(Integer wrk_id,Integer flag,String point);

    @Query("UPDATE master_wrk_id SET data_Upload_flag = :flag WHERE wrk_id = :wrk_id")
    void updateRecord1(Integer wrk_id,Integer flag);

    @Query("UPDATE master_wrk_id SET wrk_id = :online_wrk_id WHERE temp_Wrk_id = :temp_wrk_id")
    void updateRecord2(Integer temp_wrk_id,Integer online_wrk_id);

    @Query("UPDATE checklist_record_data_list SET wrk_id = :online_wrk_id WHERE wrk_id = :temp_wrk_id")
    void updateRecord3(Integer temp_wrk_id,Integer online_wrk_id);

 @Query("UPDATE checklist_record_data_list SET alert = :alert, severity = :severity, department = :depert, alrt_cmnt = :arltCommnt WHERE wrk_id = :wrk_id and checklist_points = :points")
    void updateRecord4(Integer wrk_id, String severity,String depert,String arltCommnt, String alert, String points);

    @Query("UPDATE engin_description_data SET wrk_id = :online_wrk_id WHERE wrk_id = :temp_wrk_id")
    void updateRecord4(Integer temp_wrk_id,Integer online_wrk_id);

    @Query("UPDATE master_wrk_id SET offline_status = :offline_status WHERE wrk_id = :online_wrk_id")
    void updateRecord5(Integer online_wrk_id,Integer offline_status);

    @Query("DELETE FROM master_wrk_id WHERE wrk_status= :wrk_status")
    void deleteMasterWorkId(int wrk_status);

    @Query("DELETE FROM checklist_table")
    void deleteChecklist();

    @Query("DELETE FROM team_selection_list")
    void deleteTeamList();

    @Query("DELETE FROM master_wrk_id WHERE wrk_status= :wrk_status AND wrk_status = :wrk_status1")
    void deleteMasterWorkId(int wrk_status,int wrk_status1);

    @Query("UPDATE master_wrk_id SET seen= :seen WHERE wrk_id = :wrk_id")
    void updateMasterWorkId(int wrk_id,boolean seen);

    @Query("UPDATE vessel_master_wrk_id SET seen= :seen WHERE wrk_id = :wrk_id")
    void updateVesselMasterWorkId(int wrk_id,boolean seen);

    @Query("DELETE FROM checklist_record_data_list WHERE wrk_id= :wrk_id")
    void deleteChecklistDataList(Integer wrk_id);

    @Query("DELETE FROM vessel_checklist_record_data_list WHERE wrk_id= :wrk_id")
    void deleteVesselChecklistDataList(Integer wrk_id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void setSingleEnginData(EnginDescriptionData enginData);

    @Query("UPDATE master_wrk_id SET comment = :comment, completed_by = :employee_id, modified_dt = :modified_dt, wrk_status = :wrk_status, user_img = :user_img, data_Upload_flag = :flag WHERE wrk_id =:wrk_id")
    void updateMasterWorkId(String wrk_id, String comment, String employee_id, String modified_dt, String wrk_status, String user_img, Integer flag);

    @Query("UPDATE defect_list SET cncl_Comp_comment = :comment, completed_by = :user_id, status = :wrk_status WHERE wrk_id =:wrk_id")
    void updateDefectListCancelComplete(String wrk_id, String comment, String user_id,  String wrk_status);


    @Query("UPDATE vessel_master_wrk_id SET comment = :comment, completed_by = :employee_id, last_modify_dt = :modified_dt, wrk_status = :wrk_status WHERE wrk_id =:wrk_id")
    void updateVesselMasterWorkId(String wrk_id, String comment, String employee_id, String modified_dt, String wrk_status);

    @Transaction
    default void setComponentToTables(List<Component> componentList){
        setAllComponentList(componentList);
    }

    @Transaction
    default void setDataToTables(List<CheckList> checklist, List<Component> componentList,
                                 List<AuxEngDetails>
                                             allAuxEngDetailsList, List<AuxEngLabel>
                                             allAuxEngLabelList, List<TeamSelectionList>
                                         teamSelectionLists, List<ChecklistRecordDataList>
                                             checklistRecordDataLists, List<MasterWorkId>
                                             masterWorkIdList,
                                             List<EnginDescriptionData> enginDescriptionData,
                                             List<DepartmentList> departmentLists,
                                 List<VesselChecklist> vesselChecklists,
                                 List<VesselComponent> vesselComponentList,
                                 List<VesselMasterWorkId> vesselMasterWorkIdList,
                                 List<NoticeDataList> noticeDataListList) {
        setAllCheckList(checklist);
        setAllComponentList(componentList);
        setAllAuxEngDetailsList(allAuxEngDetailsList);
        setAllAuxEngLabelList(allAuxEngLabelList);
        setAllTeamSelectionList(teamSelectionLists);
        setChecklistRecordDataList(checklistRecordDataLists);
        setMasterWorkIdList(masterWorkIdList);
        setEnginDescriptionData(enginDescriptionData);
        setDepartmentData(departmentLists);
        setAllVesselCheckList(vesselChecklists);
        setAllVesselComponentList(vesselComponentList);
        setVesselMasterWorkIdList(vesselMasterWorkIdList);
        setNoticeDataList(noticeDataListList);

    }

    @Transaction
    default  void setUserDataToTable(List<UserDetails> userDetails){
        setAllUserDetailsList(userDetails);
    }
    @Transaction
    default  void setResumeDataToTable(List<ChecklistRecordDataList> checklistRecordDataLists/*,
                                       List<EnginDescriptionData> enginDescriptionData*/){
        setChecklistRecordDataList(checklistRecordDataLists);
       // setEnginDescriptionData(enginDescriptionData);
    }

    @Transaction
    default  void setVesselResumeDataToTable(List<VesselChecklistRecordDataList> vesselChecklistRecordDataLists){
        setVesselChecklistRecordDataList(vesselChecklistRecordDataLists);
    }

    @Transaction
    default  void setChecklistDataToTable(List<MasterWorkId> masterWorkIdList,
                                          List<EnginDescriptionData> enginDescriptionData){
        setMasterWorkIdList(masterWorkIdList);
        setEnginDescriptionData(enginDescriptionData);

    }

    @Transaction
    default  void setDefectlistDataToTable(List<DefectListDataList> defectListDataLists){
        setDefectListDataList(defectListDataLists);
    }

    @Transaction
    default  void setShipInspDataList(List<VesselMasterWorkId> vesselMasterWorkIds){
        setVesselMasterWorkIdList(vesselMasterWorkIds);
    }
    @Transaction
    default  void setDescriptioToTable(List<EnginDescriptionData> enginDescriptionData){
        setEnginDescriptionData(enginDescriptionData);
    }

}
