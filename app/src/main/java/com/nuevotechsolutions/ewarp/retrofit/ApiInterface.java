package com.nuevotechsolutions.ewarp.retrofit;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @Multipart
    @POST("rest/files/upload")
    Call<String> uploadFile(@Part("submit_dtls") RequestBody description,
                                  @Part MultipartBody.Part[] fileImage,
                                  @Part MultipartBody.Part[] fileAudip);

}
