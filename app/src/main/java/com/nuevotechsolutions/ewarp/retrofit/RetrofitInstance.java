package com.nuevotechsolutions.ewarp.retrofit;

import com.nuevotechsolutions.ewarp.utils.ApiUrlClass;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitInstance {

    private static final int TIME = 20;
    private static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;

    public static ApiInterface getRetrofitInstance() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(TIME, TIME_UNIT)
                .writeTimeout(TIME, TIME_UNIT)
                .readTimeout(TIME, TIME_UNIT)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUrlClass.base_url + "/")
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ApiInterface.class);

    }

}
