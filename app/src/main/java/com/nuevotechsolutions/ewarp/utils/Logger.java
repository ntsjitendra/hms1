package com.nuevotechsolutions.ewarp.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class Logger {
    private static final boolean isShow = true;

    public static void printLog(String tag, String msg) {
        if (isShow) {
            Log.e(tag, msg);
        }
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
}
