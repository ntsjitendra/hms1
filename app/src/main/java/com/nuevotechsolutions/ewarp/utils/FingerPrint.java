package com.nuevotechsolutions.ewarp.utils;

import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.provider.MediaStore;

public class FingerPrint {
    private static FingerprintManager fingerprintManagerCompat;

    public static boolean isHardWareAvailable(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManagerCompat= (FingerprintManager) context.getSystemService(Context.FINGERPRINT_SERVICE);
            if (fingerprintManagerCompat.isHardwareDetected()){
                return true;
            }
            else {
                return false;
            }
        }
        return false;

    }


    public static Intent openFrontCamera(){
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        return intent;
    }
}
