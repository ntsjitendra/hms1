package com.nuevotechsolutions.ewarp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.nuevotechsolutions.ewarp.interfaces.MultiPartResponseInerface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SyncTableData {

    public void syncTableData(Context context) {

        SharedPreferences preferences = context.getSharedPreferences("qiestion_db", Context.MODE_PRIVATE);
        String jsonObject = preferences.getString("questions", "[]");

        Logger.printLog("Sasved Question Arr", "::  " + jsonObject);

        Map<String, String> params = new HashMap<>();
        try {
            JSONArray jsonArray = new JSONArray(jsonObject);

            if (jsonArray.length() == 0) {
                return;
            }

//            params.put("clmval", jsonArray.getJSONObject(0).getString("clmval"));
//            params.put("wrk_id", jsonArray.getJSONObject(0).getString("wrk_id"));
//            params.put("points", jsonArray.getJSONObject(0).getString("points"));
//            params.put("dbnm", jsonArray.getJSONObject(0).getString("dbnm"));
//            params.put("user_id", jsonArray.getJSONObject(0).getString("user_id"));
//            params.put("user_name", jsonArray.getJSONObject(0).getString("user_name"));
//            params.put("comment", jsonArray.getJSONObject(0).getString("comment"));
//            params.put("files", jsonArray.getJSONObject(0).getString("files"));
//
//            VolleyMethods.makePostJsonObjectRequest(params, context, ApiUrlClass.SubmitTabledata, new PostJsonObjectRequestCallback() {
//                @Override
//                public void onSuccessResponse(JSONObject response) {
//                    if (response != null) {
//                        try {
//                            String status = response.getString("status");
//                            Log.e("status", status);
//                            if (status.equals("1")) {
//                                jsonArray.remove(0);
//
//                                SharedPreferences.Editor editor = preferences.edit();
//                                editor.putString("questions", jsonArray.toString());
//                                editor.apply();
//                                syncTableData(context);
////                            dialog.dismiss();
////                            Toast.makeText(IsolationActivity.this, "Form Submited successfully.", Toast.LENGTH_SHORT).show();
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//
//                @Override
//                public void onVolleyError(VolleyError error) {
//
//                }
//
//                @Override
//                public void onTokenExpire() {
//
//                }
//            });
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


            JSONObject jsonObject1 = new JSONObject();
            try {
                jsonObject1.put("clmval", jsonArray.getJSONObject(0).getString("clmval"));
                jsonObject1.put("wrk_id", jsonArray.getJSONObject(0).getString("wrk_id"));
                jsonObject1.put("points", jsonArray.getJSONObject(0).getString("points"));
                jsonObject1.put("dbnm", jsonArray.getJSONObject(0).getString("dbnm"));
                jsonObject1.put("user_id", jsonArray.getJSONObject(0).getString("user_id"));
                jsonObject1.put("user_name", jsonArray.getJSONObject(0).getString("user_name"));
                jsonObject1.put("comment", jsonArray.getJSONObject(0).getString("comment"));
//                jsonObject1.put("files", jsonArray.getJSONObject(0).getString("files"));
                File imgFile1 = new File(jsonArray.getJSONObject(0).getString("files"));

                Logger.printLog("json", String.valueOf(jsonObject1));
                String dataKey = "details";
                MultipartUtility multipart = null;
                try {
                    multipart = new MultipartUtility(ApiUrlClass.SubmitTabledata, ApiUrlClass.charset, dataKey, jsonObject1, new MultiPartResponseInerface() {
                        @Override
                        public void onResponseSucess(String response) {
                            Log.e("SERVER response:", String.valueOf(response));
                            try {
                                JSONArray jsonArray1 = new JSONArray(response);
                                if (jsonArray1.getJSONObject(0).getInt("status") == 1) {
                                    jsonArray.remove(0);
                                    SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("questions", jsonArray.toString());
                                editor.apply();
                                syncTableData(context);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onResponseError(String response) {
                            Log.e("SERVER response:", String.valueOf(response));

                        }
                    });
                    List<File> tablefile1 = new ArrayList<>();
                    tablefile1.add(imgFile1);

                    multipart.addFilePart("files", tablefile1);

                    Log.e("img", String.valueOf(tablefile1));
                    String response = String.valueOf(multipart.finish());

                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

