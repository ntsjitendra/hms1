package com.nuevotechsolutions.ewarp.utils;

import android.util.Log;

import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DescriptionDataCalculation {

    public Set<String> descriptionDataFilter(List<EnginDescriptionData> DescriptionList, Integer uid, Integer wrk_id){
        Set<String> hs=new HashSet<>();
        for (int i=0;i<DescriptionList.size();i++){
            if (DescriptionList.get(i).getWrk_id().equals(wrk_id)){
                hs.add(DescriptionList.get(i).getDecription()+": "+DescriptionList.get(i).getDec_answer()+"\n");
            }
        }
        return hs;
    }

}
