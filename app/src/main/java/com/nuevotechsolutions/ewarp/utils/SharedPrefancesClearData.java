package com.nuevotechsolutions.ewarp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPrefancesClearData {

    public void ClearDescriptionData(Context context){
        SharedPreferences pref = context.getSharedPreferences("EngDescription", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    public void ClearDescriptionDataKey(Context context){
        SharedPreferences pref = context.getSharedPreferences("descriptionKey", MODE_PRIVATE);
        SharedPreferences.Editor editor1 = pref.edit();
        editor1.clear();
        editor1.commit();
    }

    public void ClearBaseData(Context context){
        SharedPreferences pref = context.getSharedPreferences("baseData", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }
    public void ClearFingerprintData(Context context){
        SharedPreferences pref = context.getSharedPreferences("FingerprintKey", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }

}
