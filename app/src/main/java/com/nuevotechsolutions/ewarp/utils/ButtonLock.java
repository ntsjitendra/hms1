package com.nuevotechsolutions.ewarp.utils;

import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;

public class ButtonLock {

    public void buttonLock(RadioButton[] buttons, ImageButton imageButton, ImageButton commentButton,
                           Switch aSwitch, ImageButton submitButton, ImageButton photoButton,
                           LinearLayout pointEditSubmitedLinearLayout, String isVisibleEditBtn){
        for (int i=0;i<buttons.length;i++){
            buttons[i].setEnabled(false);
        }
        imageButton.setEnabled(false);
        commentButton.setEnabled(false);
        aSwitch.setEnabled(false);
        submitButton.setEnabled(false);
        photoButton.setEnabled(false);
        if (isVisibleEditBtn.equals("true")) {
            pointEditSubmitedLinearLayout.setVisibility(View.VISIBLE);
        }
    }

    public void buttonLock(Spinner grade,ImageButton camera,ImageButton comment,ImageButton save){

        grade.setEnabled(false);
        camera.setEnabled(false);
        comment.setEnabled(false);
        save.setEnabled(false);

    }

    public void buttonLock(RadioButton[] radioButtons,ImageButton camera,ImageButton comment,ImageButton save){

        for (int i=0;i<radioButtons.length;i++){
            radioButtons[i].setEnabled(false);
        }
        camera.setEnabled(false);
        comment.setEnabled(false);
        save.setEnabled(false);

    }

}
