package com.nuevotechsolutions.ewarp.utils;

import android.os.StrictMode;
import android.util.Log;

import com.nuevotechsolutions.ewarp.interfaces.MultiPartResponseInerface;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This utility class provides an abstraction layer for sending multipart HTTP
 * POST requests to a web server.
 *
 * @author www.codejava.net
 */
public class MultipartUtility {
    // private final String boundary;
    private static final String LINE_FEED = "\r\n";
    private HttpURLConnection httpConn;
    private String charset;
    private OutputStream outputStream;
    private PrintWriter writer;
    String boundary = UUID.randomUUID().toString();
    String twoHyphens = "--";
    String crlf = "\r\n";
    MultiPartResponseInerface multiPartResponseInerface;

    public MultipartUtility(String requestURL, String charset, String datakey, JSONObject jsonPost, MultiPartResponseInerface multiPartResponseInerface) throws IOException {
        this.charset = charset;
        this.multiPartResponseInerface = multiPartResponseInerface;
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            // creates a unique boundary based on time stamp

            URL url = new URL(requestURL);
            httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setUseCaches(false);
            httpConn.setDoOutput(true); // indicates POST method
            httpConn.setDoInput(true);
            httpConn.setRequestMethod("POST");
            httpConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            httpConn.setRequestProperty("Accept", "*/*");

            outputStream = httpConn.getOutputStream();


            OutputStream outputStream = httpConn.getOutputStream();
            DataOutputStream dos = new DataOutputStream(outputStream);

            // FIRST PART; A JSON object
            dos.writeBytes(twoHyphens + boundary);
            dos.writeBytes(crlf);
            dos.writeBytes("Content-Type: application/json");
            dos.writeBytes(crlf);
            dos.writeBytes("Content-Disposition: form-data; name=\"" + datakey + "\"");
            dos.writeBytes(crlf);
            dos.writeBytes(crlf);
            dos.writeBytes(String.valueOf(jsonPost));
            dos.writeBytes(crlf);

            writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);
        }
    }

    /**
     * This constructor initializes a new HTTP POST request with content type
     * is set to multipart/form-data
     *
     * @param requestURL
     * @param charset
     * @throws IOException
     */
    public MultipartUtility(String requestURL, String charset, String datakey, JSONObject jsonPost, String datakey1, JSONObject jsonPost1)
            throws IOException {
        this.charset = charset;

        // creates a unique boundary based on time stamp

        URL url = new URL(requestURL);
        httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setUseCaches(false);
        httpConn.setDoOutput(true); // indicates POST method
        httpConn.setDoInput(true);
        httpConn.setRequestMethod("POST");
        httpConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
        httpConn.setRequestProperty("Accept", "*/*");

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here
            outputStream = httpConn.getOutputStream();
            OutputStream outputStream = httpConn.getOutputStream();
            DataOutputStream dos = new DataOutputStream(outputStream);

            // FIRST PART; A JSON object
            dos.writeBytes(twoHyphens + boundary);
            dos.writeBytes(crlf);
            dos.writeBytes("Content-Type: application/json");
            dos.writeBytes(crlf);
            dos.writeBytes("Content-Disposition: form-data; name=\"" + datakey + "\"");
            dos.writeBytes(crlf);
            dos.writeBytes(crlf);
            dos.writeBytes(String.valueOf(jsonPost));
            dos.writeBytes(crlf);


            // FIRST PART; B JSON object
            dos.writeBytes(twoHyphens + boundary);
            dos.writeBytes(crlf);
            dos.writeBytes("Content-Type: application/json");
            dos.writeBytes(crlf);
            dos.writeBytes("Content-Disposition: form-data; name=\"" + datakey1 + "\"");
            dos.writeBytes(crlf);
            dos.writeBytes(crlf);
            dos.writeBytes(String.valueOf(jsonPost1));
            dos.writeBytes(crlf);

            writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);
        }


    }

    public MultipartUtility(String requestURL, String charset, String datakey, JSONObject jsonPost) throws IOException {
        this.charset = charset;
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            // creates a unique boundary based on time stamp

            URL url = new URL(requestURL);
            httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setUseCaches(false);
            httpConn.setDoOutput(true); // indicates POST method
            httpConn.setDoInput(true);
            httpConn.setRequestMethod("POST");
            httpConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            httpConn.setRequestProperty("Accept", "*/*");

            outputStream = httpConn.getOutputStream();


            OutputStream outputStream = httpConn.getOutputStream();
            DataOutputStream dos = new DataOutputStream(outputStream);

            // FIRST PART; A JSON object
            dos.writeBytes(twoHyphens + boundary);
            dos.writeBytes(crlf);
            dos.writeBytes("Content-Type: application/json");
            dos.writeBytes(crlf);
            dos.writeBytes("Content-Disposition: form-data; name=\"" + datakey + "\"");
            dos.writeBytes(crlf);
            dos.writeBytes(crlf);
            dos.writeBytes(String.valueOf(jsonPost));
            dos.writeBytes(crlf);

            writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);
        }
    }

    /**
     * Adds a form field to the request
     *
     * @param name  field name
     * @param value field value
     */
    public void addFormField(String name, String value) {
        writer.append("--" + boundary).append(LINE_FEED);
        writer.append("Content-Type: application/json; charset=" + charset).append(
                LINE_FEED);
        writer.append(LINE_FEED);
        writer.append(value).append(LINE_FEED);
        writer.flush();
    }

    /**
     * Adds a upload file section to the request
     *
     * @param fieldName  name attribute in <input type="file" name="..." />
     * @param uploadFile a File to be uploaded
     * @throws IOException
     */
    public void addFilePart(String fieldName, List<File> uploadFile)
            throws IOException {

        for (int i = 0; i < uploadFile.size(); i++) {
            String fileName = uploadFile.get(i).getName();
            writer.append("--" + boundary).append(LINE_FEED);
            writer.append("Content-Disposition: form-data; name=\"" + fieldName
                    + "\"; filename=\"" + fileName + "\"")
                    .append(LINE_FEED);
            writer.append(
                    "Content-Type: "
                            + httpConn.guessContentTypeFromName(fileName))
                    .append(LINE_FEED);
            writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
            writer.append(LINE_FEED);
            writer.flush();
            Log.e("uploadFile::", String.valueOf(uploadFile.get(i)));
            FileInputStream inputStream = new FileInputStream(uploadFile.get(i));
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            writer.append(crlf);
            writer.append(twoHyphens + boundary + crlf);
            outputStream.flush();
            inputStream.close();

            writer.flush();
        }


    }

    /**
     * Adds a header field to the request.
     *
     * @param name  - name of the header field
     * @param value - value of the header field
     */
    public void addHeaderField(String name, String value) {
        writer.append(name + ": " + value).append(LINE_FEED);
        writer.flush();
    }

    /**
     * Completes the request and receives response from the server.
     *
     * @return a list of Strings as response in case the server returned
     * status OK, otherwise an exception is thrown.
     * @throws IOException
     */
    public List<String> finish() throws IOException {
        List<String> response = new ArrayList<String>();

        writer.append(LINE_FEED).flush();
        writer.append("--" + boundary + "--").append(LINE_FEED);
        writer.close();

        // checks server's status code first
        int status = httpConn.getResponseCode();
        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                response.add(line);
            }
            reader.close();
            httpConn.disconnect();
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }

        if (multiPartResponseInerface != null)

            multiPartResponseInerface.onResponseSucess(String.valueOf(response));

        return response;
    }

}