package com.nuevotechsolutions.ewarp.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.room.Room;

import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nuevotechsolutions.ewarp.R;
import com.nuevotechsolutions.ewarp.activities.ImagePickActivity;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.DepartmentList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserPointAccessData;
import com.nuevotechsolutions.ewarp.model.NoticeDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklist;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselComponent;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselMasterWorkId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nuevotechsolutions.ewarp.utils.ApiUrlClass.TempLoginData;


public class SyncData {
    CheckListDatabase checkListDatabase;
    List<CheckList> checkLists;
    List<Component> componentList;
    List<UserDetails>  userDetailsList;
    List<AuxEngDetails>  auxEngDetailsList;
    List<AuxEngLabel> auxEngLabelList;
    List<TeamSelectionList> teamSelectionLists;
    List<ChecklistRecordDataList> checklistRecordDataList;
    List<MasterWorkId> masterWorkIdList;
    List<UserPointAccessData> userPointAccessDataList;
    List<EnginDescriptionData> enginDescriptionData;
    List<VesselChecklist> vesselChecklists;
    List<VesselComponent> vesselComponentList;
    List<VesselMasterWorkId> vesselMasterWorkIdList;
    List<DepartmentList> departmentLists;
    List<NoticeDataList> noticeLists;
    ProgressDialog pdLoading;

    int counter = 0;
    int count;
    int remember = 0;
    List<CheckList> list;
    Context context;


    public boolean storeAllData(Context context){
        this.context=context;

        pdLoading = new ProgressDialog(context);
        pdLoading.setTitle(context.getString(R.string.please_wait));
        pdLoading.show();
        checkLists=new ArrayList<>();
        componentList=new ArrayList<>();
        userDetailsList=new ArrayList<>();
        auxEngDetailsList=new ArrayList<>();
        auxEngLabelList=new ArrayList<>();
        teamSelectionLists =new ArrayList<>();
        checklistRecordDataList=new ArrayList<>();
        masterWorkIdList=new ArrayList<>();
        userPointAccessDataList=new ArrayList<>();
        enginDescriptionData=new ArrayList<>();
        departmentLists=new ArrayList<>();
        noticeLists=new ArrayList<>();

        vesselChecklists=new ArrayList<>();
        vesselComponentList=new ArrayList<>();
        vesselMasterWorkIdList=new ArrayList<>();
//        pdLoading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        pdLoading.show();
        List<UserDetails> userDetails=new ArrayList<>();
        checkListDatabase = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails=checkListDatabase.productDao().getUserDetail();
        Map<String, String> params = new HashMap<>();
        params.put("dbnm", userDetails.get(0).getDbnm());
        params.put("user_id", userDetails.get(0).getUser_id());
        params.put("employee_id", userDetails.get(0).getEmployee_id());
        params.put("accessToken", userDetails.get(0).getAccessToken());

        VolleyMethods.makePostJsonObjectRequest(params, context, new ApiUrlClass().user_loginData, new PostJsonObjectRequestCallback() {
            @Override
            public void onSuccessResponse(JSONObject response) {
                Log.d("Responsedata1", String.valueOf(response));
                if (response != null) {
//                  Log.d("Responsedata", String.valueOf(response));
                    try {
                        Integer res = response.getInt("response");
                        if (res.equals(200)){
                            JSONObject jsonObject = response.getJSONObject("data");
                            JSONObject noticeJsonObject = response.getJSONObject("notice_data");
                            JSONObject vesselJsonObject = response.getJSONObject("vessel_inspection_data");
                            ObjectMapper objectMapper = new ObjectMapper();

                            if (jsonObject != null) {
                                JSONArray list = jsonObject.getJSONArray("list");
//                                JSONArray component = jsonObject.getJSONArray("component");
                                JSONArray auxEngLabellData = jsonObject.getJSONArray("aux_eng-label");
                                JSONArray teamSelectionData = jsonObject.getJSONArray("teamselectionlist");
                                JSONArray checklistRecordData = jsonObject.getJSONArray("lastdata");
                                JSONArray massterWorkIdData = jsonObject.getJSONArray("masterdata");
                                JSONArray enginDescriptionDataArray = jsonObject.getJSONArray("engin_data");
                                JSONArray departmentData = jsonObject.getJSONArray("dept");

                                if (list != null && list.length() > 0) {
                                    for (int i = 0; i < list.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) list.get(i);
                                        CheckList checkList = objectMapper.readValue(jsonObject1.toString(), CheckList.class);
                                        checkLists.add(checkList);
                                    }
                                }

                                /*if (component != null && component.length() > 0) {
                                    for (int i = 0; i < component.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) component.get(i);
                                        Component component1 = objectMapper.readValue(jsonObject1.toString(), Component.class);
                                        componentList.add(component1);
                                    }

                                }
*/

//                            if (auxEngDetailData != null && auxEngDetailData.length() > 0) {
//                                for (int i = 0; i < auxEngDetailData.length(); i++) {
//                                    JSONObject jsonObject1 = (JSONObject) auxEngDetailData.get(i);
//                                    AuxEngDetails auxEngDetails = objectMapper.readValue(jsonObject1.toString(), AuxEngDetails.class);
//                                    auxEngDetailsList.add(auxEngDetails);
//                                }
//
//                            }

                                if (auxEngLabellData != null && auxEngLabellData.length() > 0) {
                                    for (int i = 0; i < auxEngLabellData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) auxEngLabellData.get(i);
                                        AuxEngLabel auxEngLabel = objectMapper.readValue(jsonObject1.toString(), AuxEngLabel.class);
                                        auxEngLabelList.add(auxEngLabel);
                                    }

                                }

                                if (teamSelectionData != null && teamSelectionData.length() > 0) {
                                    for (int i = 0; i < teamSelectionData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) teamSelectionData.get(i);
                                        TeamSelectionList teamSelectionList1 = objectMapper.readValue
                                                (jsonObject1.toString(), TeamSelectionList.class);
                                        teamSelectionLists.add(teamSelectionList1);
                                    }
                                }

                                if (checklistRecordData != null && checklistRecordData.length() > 0) {
                                    for (int i = 0; i < checklistRecordData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) checklistRecordData.get(i);
                                        ChecklistRecordDataList checklistRecordDataList1 = objectMapper.readValue(jsonObject1.toString(), ChecklistRecordDataList.class);
                                        checklistRecordDataList.add(checklistRecordDataList1);
                                    }
                                }

                                if (massterWorkIdData != null && massterWorkIdData.length() > 0) {
                                    for (int i = 0; i < massterWorkIdData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) massterWorkIdData.get(i);
                                        MasterWorkId masterWorkId = objectMapper.readValue
                                                (jsonObject1.toString(), MasterWorkId.class);
                                        masterWorkIdList.add(masterWorkId);
                                    }
                                }

                                if (enginDescriptionDataArray != null && enginDescriptionDataArray.length() > 0) {
                                    for (int i = 0; i < enginDescriptionDataArray.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) enginDescriptionDataArray.get(i);
                                        EnginDescriptionData enginDescriptionData1 = objectMapper.readValue(jsonObject1.toString(), EnginDescriptionData.class);
                                        enginDescriptionData.add(enginDescriptionData1);

                                    }

                                }

                                if (departmentData != null && departmentData.length() > 0) {
                                    for (int i = 0; i < departmentData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) departmentData.get(i);
                                        DepartmentList departmentList = objectMapper.readValue(jsonObject1.toString(), DepartmentList.class);
                                        departmentLists.add(departmentList);

                                    }

                                }
                            }

                            if (noticeJsonObject!=null){
                                JSONArray noticeList = noticeJsonObject.getJSONArray("notice");

                                if (noticeList != null && noticeList.length() > 0) {
                                    for (int i = 0; i < noticeList.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) noticeList.get(i);
                                        NoticeDataList noticeDataList = objectMapper.readValue(jsonObject1.toString(), NoticeDataList.class);
                                        noticeLists.add(noticeDataList);
                                    }
                                }
                            }
                            if (vesselJsonObject!=null && vesselJsonObject.length()>0){
                                JSONArray vesselList = vesselJsonObject.getJSONArray("list");
                                JSONArray vesselComponent = vesselJsonObject.getJSONArray("checklist_points");
                                JSONArray vesselMasterWorkIdData = vesselJsonObject.getJSONArray("masterdata");

                                if (vesselList != null && vesselList.length() > 0) {
                                    for (int i = 0; i < vesselList.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) vesselList.get(i);
                                        VesselChecklist vesselChecklist = objectMapper.readValue(jsonObject1.toString(), VesselChecklist.class);
                                        vesselChecklists.add(vesselChecklist);
                                    }
                                }

                                if (vesselComponent != null && vesselComponent.length() > 0) {
                                    for (int i = 0; i < vesselComponent.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) vesselComponent.get(i);
                                        VesselComponent vesselComponent1 = objectMapper.readValue(jsonObject1.toString(), VesselComponent.class);
                                        vesselComponentList.add(vesselComponent1);
                                    }
                                }

                                if (vesselMasterWorkIdData != null && vesselMasterWorkIdData.length() > 0) {
                                    for (int i = 0; i < vesselMasterWorkIdData.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) vesselMasterWorkIdData.get(i);
                                        VesselMasterWorkId vesselMasterWorkId = objectMapper.readValue(jsonObject1.toString(), VesselMasterWorkId.class);
                                        vesselMasterWorkIdList.add(vesselMasterWorkId);
                                    }
                                }
                            }

                            deleteAndAddData();
                            Log.e("data", "...");
                            pdLoading.dismiss();

                        }else if (res.equals(10003)){
                            new LogoutClass().logoutMethod(context);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (JsonParseException e) {
                        e.printStackTrace();
                    } catch (JsonMappingException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onVolleyError(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    //This indicates that the reuest has either time out or there is no connection

                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (pdLoading != null && pdLoading.isShowing()) {
                        pdLoading.dismiss();
                        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }


            @Override
            public void onTokenExpire() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (pdLoading != null && pdLoading.isShowing()) {
                        pdLoading.dismiss();
                    }
                }
            }
        });


        return true;
    }
    private void deleteAndAddData() {
        Thread threadDeleteAllTables=new Thread(new Runnable() {
            @Override
            public void run() {
                //checkListDatabase.productDao().deleteChecklist();
                checkListDatabase.productDao().deleteTeamList();
                checkListDatabase.productDao().setDataToTables(checkLists, componentList,
                        auxEngDetailsList,auxEngLabelList, teamSelectionLists,
                        checklistRecordDataList, masterWorkIdList,enginDescriptionData,departmentLists,
                        vesselChecklists,vesselComponentList,vesselMasterWorkIdList,noticeLists);

                getComponentData();

            }
        });
        threadDeleteAllTables.start();
    }

    private void getComponentData() {
        checkListDatabase = Room.databaseBuilder(context, CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        list = checkListDatabase.productDao().getAllCheckList1();
        userDetailsList = checkListDatabase.productDao().getUserDetail();

        if (list != null && list.size() > 0) {
            int size = list.size();
//            Log.e("count", String.valueOf(count));
//            if (count > 0 && count < 3) {
//                count = 1;
//                Log.e("count1", String.valueOf(count));
//            } else if (count > 2) {
//                if (count % 3 == 0) {
//                    count = count / 3;
//                    Log.e("count2", String.valueOf(count));
//                } else {
//                    count = count / 3;
//                    count = count + 1;
//                    Log.e("count3", String.valueOf(count));
//                }
//            }
            if (size <= 3) {
                count = 1;
            } else {
                count = size / 3;
                if (size % 3 > 0) {
                    count = count + 1;
                }
            }

            callApi();
        }
    }

    private void callApi() {
        if (counter < count) {
            JSONArray arr = new JSONArray();
            for (int i = 0; i < 3; i++) {
                if (remember < list.size()) {
                    arr.put(Integer.parseInt(list.get(remember).getUid()));
                    remember++;
                }
            }
            Map<String, String> params = new HashMap<>();
            params.put("dbnm", userDetailsList.get(0).getDbnm());
            params.put("user_id", userDetailsList.get(0).getUser_id());
            params.put("accessToken", userDetailsList.get(0).getAccessToken());
            params.put("uiddata", arr.toString());
            Log.e("uidData..",arr.toString());
            VolleyMethods.makePostJsonObjectRequest(params, context, TempLoginData, new PostJsonObjectRequestCallback() {
                @Override
                public void onSuccessResponse(JSONObject response) {
                    Log.e("response:jk", response.toString());
                    if (response != null) {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            JSONArray component = response.getJSONArray("component");

                            if (component != null && component.length() > 0) {
                                for (int i = 0; i < component.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) component.get(i);
                                    Component component1 = objectMapper.readValue(jsonObject1.toString(), Component.class);
                                    componentList.add(component1);
                                }
                            }

//                            new Thread(new Runnable() {
//                                @Override
//                                public void run() {
                                    checkListDatabase.productDao().setComponentToTables(componentList);
                                    counter++;
                                    callApi();
//                                }
//                            }).start();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        } catch (JsonMappingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onVolleyError(VolleyError error) {
                    Log.e("err", error.toString());
                    callApi();
                }

                @Override
                public void onTokenExpire() {

                }
            });
        }
    }

}
