package com.nuevotechsolutions.ewarp.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import androidx.room.Room;

import com.google.gson.Gson;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SubmitedDataSync {

    CheckListDatabase db;
    List<UserDetails> userDetails;
    String accessToken,dbnm,user_id;

    public  void getOfflineData(Context context) {
        userDetails = new ArrayList<>();
        db = Room.databaseBuilder(context,
                CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails =db.productDao().getUserDetail();
       if (userDetails!=null){
           accessToken = userDetails.get(0).getAccessToken();
           dbnm = userDetails.get(0).getDbnm();
           user_id = userDetails.get(0).getUser_id();
       }
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                 List<ChecklistRecordDataList> checklistdatarecord = new ArrayList<>();
                List<MasterWorkId> masterWorkIds = new ArrayList<>();
                checklistdatarecord = db.productDao().getChecklistRecordDataListWithFlag();
                masterWorkIds = db.productDao().getMasterWorkIdDataListWithFlag();

                if (checklistdatarecord != null) {
                    for (int i = 0; i < checklistdatarecord.size(); i++) {
                        Gson g = new Gson();
                        String json = g.toJson(checklistdatarecord.get(i));

                        Log.e("ggf", json);
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            jsonObject.put("accessToken",accessToken);
                            jsonObject.put("dbnm",dbnm);
                            String imagedata = jsonObject.getString("image_data");
                            imagedata = imagedata.replaceAll("\\\\", "");
                            Log.e("image", imagedata);
                            JSONObject jsonphoto = new JSONObject(imagedata);

                            String dataKey = "submit_dtls";

                            MultipartUtility multipart = new MultipartUtility(ApiUrlClass.submitdata, ApiUrlClass.charset, dataKey, jsonObject);
                            List<File> file1 = new ArrayList<>();
                            List<File> audioFile = new ArrayList<>();


                            if (jsonphoto != null) {
                                for (int j = 0; j < jsonphoto.length(); j++) {
                                    file1.add(j, new File(jsonphoto.getString(String.valueOf(j))));
                                    Log.e("datalist1", String.valueOf(jsonphoto.getString(String.valueOf(j))));
                                }
                                multipart.addFilePart("files", file1);
                            }
                            File root = new File(context.getExternalFilesDir(Environment.DIRECTORY_MUSIC), "/EWARPRecoder");
                            String audio_d = checklistdatarecord.get(i).getAudio_data();
                            Log.e("audio_d:",audio_d);
                            JSONArray jsonArray=new JSONArray(audio_d);
                            if (jsonArray!=null){
                                for (int k=0;k<jsonArray.length();k++){
                                    JSONObject jObj=jsonArray.getJSONObject(k);
                                    Log.e("javaT::",root.getAbsolutePath()+"/"+jObj.getString("audionm"));
                                    audioFile.add(k, new File(root.getAbsolutePath()+"/"+jObj.getString("audionm")));
                                }
                                multipart.addFilePart("audioFiles", audioFile);
                            }


                            List<String> response = multipart.finish();
                            Log.e("SERVER REPLIED:", String.valueOf(response));
                            for (String line : response) {

                                String point="";
                                Integer wrk_id=0,flag=0;
                                JSONObject res=new JSONObject(line);
                                flag=res.getInt("flag");
                                if (flag==1 || flag==1062){
                                    wrk_id=res.getInt("wrk_id");
                                    point=res.getString("checklist_points");
                                    if (wrk_id!=null && flag!=null && point!=null){
                                        updateResponse(wrk_id,flag,point);
                                    }
                                }else{
                                    Log.e("res", line);
                                }

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        });


        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                 List<MasterWorkId> masterWorkIds = new ArrayList<>();
                masterWorkIds = db.productDao().getMasterWorkIdDataListWithFlag();

                if (masterWorkIds != null) {
                    for (int i = 0; i < masterWorkIds.size(); i++) {
                        Integer wrk_id=masterWorkIds.get(i).getWrk_id();
                        Gson g = new Gson();
                        String json = g.toJson(masterWorkIds.get(i));

                        Log.e("ggf1", json);
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            jsonObject.put("accessToken",accessToken);
                            jsonObject.put("dbnm",dbnm);
                            jsonObject.put("user_id",user_id);
                            String imagedata= jsonObject.optString("user_img");

                            if (imagedata!=null) {
                                imagedata = imagedata.replaceAll("\\\\", "");
                                Log.e("user_img", imagedata);
                            }
                            String dataKey = "details";

                            MultipartUtility multipart = new MultipartUtility(ApiUrlClass.cancel_complete_url, ApiUrlClass.charset, dataKey, jsonObject);
                            List<File> file1 = new ArrayList<>();

                            if (imagedata != null && !imagedata.equals("")) {
                                for (int j = 0; j < 1; j++) {
                                    file1.add(j, new File(imagedata));
                                }
                                multipart.addFilePart("userimg", file1);
                            }

                            List<String> response = multipart.finish();
                            Log.e("SERVER REPLIED:", String.valueOf(response));
                            for (String line : response) {

                                JSONObject res=new JSONObject(line);
                                Integer code = res.getInt("response");
                                if (code==200){
                                        updateResponse1(wrk_id,1);
                                }else{
                                    Log.e("res", line);
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        });

        thread.start();
        thread1.start();

    }

    private void updateResponse(Integer wrk_id,Integer flag,String point) {

        Thread threadSetSubmit = new Thread(new Runnable() {
            @Override
            public void run() {
                db.productDao().updateRecord(wrk_id,flag,point);
            }
        });

        threadSetSubmit.start();
    }

    private void updateResponse1(Integer wrk_id,Integer flag) {

        Thread threadSetSubmit = new Thread(new Runnable() {
            @Override
            public void run() {
                db.productDao().updateRecord1(wrk_id,flag);
            }
        });

        threadSetSubmit.start();
    }

    public void getOfflineCreatedChecklist(Context context){
        userDetails = new ArrayList<>();
        db = Room.databaseBuilder(context,
                CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails =db.productDao().getUserDetail();
        if (userDetails!=null){
            accessToken = userDetails.get(0).getAccessToken();
            dbnm = userDetails.get(0).getDbnm();
            user_id = userDetails.get(0).getUser_id();
        }
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                List<MasterWorkId> masterWorkIdList = new ArrayList<>();
                masterWorkIdList = db.productDao().getOfflineCreatedMasterWorkId();
                if (masterWorkIdList != null) {
                    for (int i = 0; i < masterWorkIdList.size(); i++) {
                        Integer wrk_id=masterWorkIdList.get(i).getWrk_id();
                        JSONObject checklistdata1 = new JSONObject();
                        JSONObject teammember1 = new JSONObject();

                        try {
                            checklistdata1.put("description", masterWorkIdList.get(i).getDescription());
                            checklistdata1.put("uid", masterWorkIdList.get(i).getUid());
                            checklistdata1.put("rank_id", userDetails.get(0).getRank_id());
                            checklistdata1.put("grtd_by", user_id);
                            checklistdata1.put("cmp_name", userDetails.get(0).getComp_name());
                            checklistdata1.put("crtntime", masterWorkIdList.get(i).getModified_dt());
                            checklistdata1.put("user_id", user_id);
                            checklistdata1.put("wrk_status", 0);
                            checklistdata1.put("dbnm", dbnm);
                            checklistdata1.put("temp_wrk_id", wrk_id);
                            checklistdata1.put("accessToken", accessToken);

                            JSONArray arr = new JSONArray(masterWorkIdList.get(i).getTeam_list());
                            teammember1.put("teamdtls",arr);
                            Log.e("teamlist....","jk+"+masterWorkIdList.get(i).getTeam_list());
                            Log.e("team....","jk+"+checklistdata1);

                            String dataKey = "checklistdata";
                            String dataKey1 = "teammember";

                        MultipartUtility multipart = new MultipartUtility(ApiUrlClass.create, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember1);
                        List<File> file1 = new ArrayList<>();

                            List<String> response = multipart.finish();
                            Log.e("SERVER REPLIED:", String.valueOf(response));
                            for (String line : response) {

                                JSONObject res=new JSONObject(line);
                                Integer code = res.getInt("response");
                                if (code==200){
                                    Integer online_wrk_id = Integer.valueOf(res.getString("wrk_id"));
                                    Log.d("online_wrk_id:", String.valueOf(online_wrk_id));
                                    updateResponse2(wrk_id,online_wrk_id);
                                }else{
                                    Log.e("res", line);
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    getOfflineData(context);
                }
            }
        });
        thread2.start();

    }

    private void updateResponse2(Integer temp_wrk_id, Integer online_wrk_id) {

        Thread threadSetSubmit = new Thread(new Runnable() {
            @Override
            public void run() {
                //here write update database code
                db.productDao().updateRecord2(temp_wrk_id,online_wrk_id);
                db.productDao().updateRecord3(temp_wrk_id,online_wrk_id);
                db.productDao().updateRecord4(temp_wrk_id,online_wrk_id);
            }
        });

        threadSetSubmit.start();
    }

    public void getOfflineAssignChecklist(Context context){
        userDetails = new ArrayList<>();
        db = Room.databaseBuilder(context,
                CheckListDatabase.class, ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails =db.productDao().getUserDetail();
        if (userDetails!=null){
            accessToken = userDetails.get(0).getAccessToken();
            dbnm = userDetails.get(0).getDbnm();
            user_id = userDetails.get(0).getUser_id();
        }
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                List<MasterWorkId> masterWorkIdList = new ArrayList<>();
                masterWorkIdList = db.productDao().getOfflineAssignChecklist();
                if (masterWorkIdList != null) {
                    for (int i = 0; i < masterWorkIdList.size(); i++) {
                        Integer wrk_id=masterWorkIdList.get(i).getWrk_id();
                        JSONObject checklistdata1 = new JSONObject();
                        JSONObject teammember1 = new JSONObject();

                        try {
                            checklistdata1.put("description", masterWorkIdList.get(i).getDescription());
                            checklistdata1.put("uid", masterWorkIdList.get(i).getUid());
                            checklistdata1.put("wrk_id", masterWorkIdList.get(i).getWrk_id());
                            checklistdata1.put("rank_id", userDetails.get(0).getRank_id());
                            checklistdata1.put("grtd_by", user_id);
                            checklistdata1.put("cmp_name", userDetails.get(0).getComp_name());
                            checklistdata1.put("crtntime", masterWorkIdList.get(i).getModified_dt());
                            checklistdata1.put("user_id", user_id);
                            checklistdata1.put("wrk_status", 0);
                            checklistdata1.put("dbnm", dbnm);
                            checklistdata1.put("accessToken", accessToken);

                            JSONArray arr = new JSONArray(masterWorkIdList.get(i).getTeam_list());
                            teammember1.put("teamdtls",arr);
                            Log.e("teamlist....","jk+"+masterWorkIdList.get(i).getTeam_list());
                            Log.e("description....","jk+"+masterWorkIdList.get(i).getDescription());
                            Log.e("description....1","jk+"+checklistdata1.toString());

                            String dataKey = "checklistdata";
                            String dataKey1 = "teammember";

                            MultipartUtility multipart = new MultipartUtility(ApiUrlClass.assignApi, ApiUrlClass.charset, dataKey, checklistdata1, dataKey1, teammember1);
                            List<File> file1 = new ArrayList<>();

                            List<String> response = multipart.finish();
                            Log.e("SERVER REPLIED:", String.valueOf(response));
                            for (String line : response) {

                                JSONObject res=new JSONObject(line);
                                Integer code = res.getInt("response");
                                if (code==200){
                                    Integer online_wrk_id = Integer.valueOf(res.getString("wrk_id"));
                                    Log.d("online_wrk_id:", String.valueOf(online_wrk_id));
                                    updateResponse3(online_wrk_id,1);
                                }else{
                                    Log.e("res", line);
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        });
        thread2.start();

    }

    private void updateResponse3(Integer online_wrk_id, Integer offline_status) {

        Thread threadSetSubmit = new Thread(new Runnable() {
            @Override
            public void run() {
                //here write update database code
                db.productDao().updateRecord5(online_wrk_id,offline_status);
            }
        });

        threadSetSubmit.start();
    }

}
