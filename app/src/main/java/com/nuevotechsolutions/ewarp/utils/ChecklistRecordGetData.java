package com.nuevotechsolutions.ewarp.utils;

import android.content.Context;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.room.Room;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nuevotechsolutions.ewarp.adapter.ImageListDataAdapter;
import com.nuevotechsolutions.ewarp.adapter.RecycleCheckpointAdapter;
import com.nuevotechsolutions.ewarp.controller.VolleyMethods;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;
import com.nuevotechsolutions.ewarp.interfaces.ImageInterface;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChecklistRecordGetData {
    CheckListDatabase checkListDatabase;
    List<ChecklistRecordDataList> checklistRecordDataList = new ArrayList<>();
    List<UserDetails> userDetails = new ArrayList<>();
    List<Component> componentList = new ArrayList<>();
    List<Boolean>viewEnabled;
    private Map<Integer, List<String>> imageMap = new HashMap<>();
    private Map<Integer, List<String>> locationMap = new HashMap<>();
    private Map<Integer, List<String>> timeStampMap = new HashMap<>();
    private Map<Integer, List<String>> photoTextValueMap = new HashMap<>();
    Map<Integer, ArrayList<String>> audioFileMap = new HashMap<>();

    private RecycleCheckpointAdapter recycleCheckpointAdapter;


    public void checklistRecordGetData(Context context, String wrk_id, String uid, String checklist_type) {
        checkListDatabase = Room.databaseBuilder(context.getApplicationContext(), CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();
        userDetails = checkListDatabase.productDao().getUserDetail();
        componentList = checkListDatabase.productDao().getComponentByUid(uid);
        getDataFromDatabaseByUid();
        Map<String, String> params = new HashMap<>();
        params.put("wrk_id", wrk_id);
        params.put("user_id", userDetails.get(0).getUser_id());
        params.put("dbnm", userDetails.get(0).getDbnm());
        params.put("comp_name", userDetails.get(0).getComp_name());
        params.put("accessToken", userDetails.get(0).getAccessToken());

        VolleyMethods.makePostJsonObjectRequest(params, context, new ApiUrlClass().resumeApi, new PostJsonObjectRequestCallback() {
            @Override
            public void onSuccessResponse(JSONObject response) {
                if (response != null) {
                    try {
                        ObjectMapper objectMapper = new ObjectMapper();
                        JSONArray checklistRecordData = response.getJSONArray("lastdata");

                        Log.e("lastdata..j", String.valueOf(checklistRecordData));
                        if (checklistRecordData != null && checklistRecordData.length() > 0) {
                            for (int i = 0; i < checklistRecordData.length(); i++) {
                                JSONObject jsonObject1 = (JSONObject) checklistRecordData.get(i);
                                ChecklistRecordDataList checklistRecordDataList1 = objectMapper.readValue(jsonObject1.toString(), ChecklistRecordDataList.class);
                                checklistRecordDataList.add(checklistRecordDataList1);
                            }
                        }

                        resumeAndAddData1();
                        recycleCheckpointAdapter = new RecycleCheckpointAdapter(context,wrk_id,
                                componentList,
                                viewEnabled,
                                checklistRecordDataList,
                                imageMap,
                                locationMap,
                                timeStampMap,
                                photoTextValueMap,
                                checklist_type,
                                audioFileMap,
                                new ImageInterface() {
                                    @Override
                                    public void onPhotoButtonClickListener(int post, ImageView[] imageViews) {

                                    }

                                    @Override
                                    public void onPhotoButtonClickListenerAdapter(int post, String points, ImageView[] imageViews, ImageListDataAdapter adapter, List<String> imageList, List<String> locationList, List<String> timeStampList, List<String> photoTextValueList) {

                                    }

                                    @Override
                                    public void onSwitchButtonClickListener(int post, String points, Context context, Switch aSwitch, RadioButton[] radioButtons, String alertDeparment, EditText editTexts, LinearLayout linearLayout) {

                                    }

                                    @Override
                                    public void onImageClickListener(int post, String points, ImageView imageView, Context context, ImageButton button) {

                                    }

                                    @Override
                                    public void onSubmitButtonClickListener(int post, String points, RadioButton[] radioButtons, ImageButton editText,
                                                                            Switch aSwitch, ImageButton button, TextView textView, TextView editTextSubmit,
                                                                            String s, LinearLayout linearLayout, ImageButton b, ImageButton photoButton,
                                                                            ImageListDataAdapter adapter, List<String> imageList, List<String> locationList,
                                                                            List<String> timeStampList, List<String> photoTextValueList, Spinner spinner,
                                                                            LinearLayout pointEditSubmitedLinearLayout, String isVisibleEditBtn) {

                                    }

                                    @Override
                                    public void onGuidanceButtonClickListener(int post, String points, Context context) {

                                    }

                                    @Override
                                    public void onRecordButtonClickListener(int post) {

                                    }

                                    @Override
                                    public void onTableViewClickListener(int post, String Cpoints) {

                                    }


                                    @Override
                                    public void onPlayButtonClickListener(int post, String Cpoints) {

                                    }


                                    @Override
                                    public void onCommentButtonClickListener(int post, String points, Context context, TextView editText, LinearLayout linearLayout) {

                                    }

                                    @Override
                                    public void onAttachPositionClickListener(int post, Map<Integer, List<String>> imageMapLocal,
                                                                              Map<Integer, List<String>> locationMapLocal,
                                                                              Map<Integer, List<String>> timestampMapLocal,
                                                                              Map<Integer, List<String>> photoTextValueMapLocal,
                                                                              List<String> listPhotoLocal,
                                                                              List<String> locationListLocal,
                                                                              List<String> timeStampListLocal,
                                                                              List<String> photoTextValueListLocal) {

                                    }

                                    @Override
                                    public void onPhotoTextValue(int post, String photoTextValue, String imageName, Context context, int k) {

                                    }

                                    @Override
                                    public void onEditButtonClickListener(int post, Context context, String points, Switch aSwitch, RadioButton[] radioButtons, String txt,List<Boolean> booleanList) {

                                    }

                                });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (JsonParseException e) {
                        e.printStackTrace();
                    } catch (JsonMappingException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onVolleyError(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTokenExpire() {
                Toast.makeText(context, "Session Expired!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void resumeAndAddData1() {
        Thread threadDeleteAllTables = new Thread(new Runnable() {
            @Override
            public void run() {
                checkListDatabase.productDao().setResumeDataToTable(checklistRecordDataList/*, enginDescriptionData*/);
            }
        });
        threadDeleteAllTables.start();
    }

    private void getDataFromDatabaseByUid() {
        Thread threadComponentByUid = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < componentList.size(); i++) {
                    imageMap.put(i, new ArrayList<>());
                    locationMap.put(i, new ArrayList<>());
                    timeStampMap.put(i, new ArrayList<>());
                }
            }
        });
        threadComponentByUid.start();


    }

}
