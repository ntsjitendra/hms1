package com.nuevotechsolutions.ewarp.utils;

public class ApiUrlClass {


//    public final static String base_url = "https://www.ethicalworking.com/ewarpapi";
// public final static String base_url = "http://ec2-13-232-200-41.ap-south-1.compute.amazonaws.com:8080/NEWAPIFORHOSTING";
public final static String base_url = "https://www.ethicalworking.com/ewarp"; //Main ip

//      public final static String base_url="http://15.207.216.17:8080/ewarp";

    public final static String login_api = base_url + "/androidapi/user_login2";
    public final static String user_loginData = base_url + "/androidapi/user_loginData";
    public final static String TempLoginData = base_url + "/androidapi/component";

    public final static String create = base_url + "/rest/start_checklist/create";
    public final static String changepass_url = base_url + "/androidapi/change";
    public final static String cancel_complete_url = base_url + "/rest/ccck/submit_checklist";
    public final static String submitdata = base_url + "/rest/files/upload";
    public final static String updateData = base_url + "/rest/update/ckdata";
    public final static String resumeApi = base_url + "/androidapi/CompleteCancelData";
    public final static String checklistApi = base_url + "/androidapi/GetChecklist";
    public final static String assignApi = base_url + "/rest/SetAssignDescription/assign";
    public final static String loginImgUpload = base_url + "/rest/userimg/userImageUpload";
    public final static String CREATE_VESSLE_LIST = base_url + "/vessel/vessel_create";
    public final static String SHIP_INSPECTION_DETAIL_PAGE = base_url + "/vessel/vessel_by_id";
    public final static String SHIP_INSPECTION_COMPLETE_CANCEL = base_url + "/rest/completeCancel/ckecklistd";
    public final static String SHIP_INSPECTION_LIST = base_url + "/vessel/vesselInspdatalistall";
    public final static String SHIP_INSPECTION_DETAILS = base_url + "/vessel/vesselInspDatabyWrkid";
    public final static String SUBMIT_VESSEL_DATA = base_url + "/rest/vsl/vslupload";
    public final static String MANUALS_DATA = base_url + "/";
    public final static String DIFECT_LIST = base_url + "/defect/defectlistal";
    public final static String REGISTRATION = base_url + "/androidapi/registration";
    public final static String ForgetApi = base_url + "/androidapi/forget_Password";
    public final static String CreateOrJoinAPI = base_url + "/androidapi/joinuser";
    public final static String View_Checklist_List_API = base_url + "/androidapi/viewchecklistList";
    public final static String View_Checklist_API = base_url + "/androidapi/viewchecklist";
    public final static String Download_Checklist_API = base_url + "/androidapi/downloadck";

    //    public final static String TempLoginData="http://13.232.239.103:8080/NEWAPIFORHOSTING/androidapi/component";
    public final static String ValidationEmailCheck = base_url + "/androidapi/validate";
    public final static String SubmitTabledata = base_url + "/rest/table/submit";

    public final static String charset = "UTF-8";
    public static final String roomDatabaseName = "hms";

}
