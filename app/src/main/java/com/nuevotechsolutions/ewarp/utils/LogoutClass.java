package com.nuevotechsolutions.ewarp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.room.Room;
import com.nuevotechsolutions.ewarp.activities.LoginActivity;
import com.nuevotechsolutions.ewarp.controller.DatabaseHandler;
import com.nuevotechsolutions.ewarp.database.CheckListDatabase;

public class LogoutClass {
    DatabaseHandler db;
    CheckListDatabase cDb;
    public void logoutMethod(Context context){
        db = new DatabaseHandler(context);
        cDb = Room.databaseBuilder(context, CheckListDatabase.class,
                ApiUrlClass.roomDatabaseName).allowMainThreadQueries().build();

        Intent logout = new Intent(context, LoginActivity.class);
        new SharedPrefancesClearData().ClearBaseData(context);
        new SharedPrefancesClearData().ClearFingerprintData(context);
        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        logout.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        db.deleteAllRecordAuxillaryEng();
        cDb.clearAllTables();
        context.startActivity(logout);
        ((Activity)context).finish();
    }
}
