package com.nuevotechsolutions.ewarp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilClassFuntions {
    String date1;

    public String dateConversion(String date) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
            date1 = formatter1.format(formatter.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date1;
    }

    public String currentDateTime() {

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = formatter.format(date);
        return strDate;
    }

}
