package com.nuevotechsolutions.ewarp.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Vibrator;

import com.nuevotechsolutions.ewarp.R;

import static android.content.Context.VIBRATOR_SERVICE;

public class OnBackPressed {

    public void onBackPressedFunction(Context context){
        AudioManager am = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

        Vibrator vibrator = (Vibrator)context.getSystemService(VIBRATOR_SERVICE);
        switch (am.getRingerMode()) {
            case AudioManager.RINGER_MODE_SILENT:
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                vibrator.vibrate(80);

                break;
            case AudioManager.RINGER_MODE_NORMAL:
                MediaPlayer mp =MediaPlayer.create(context, R.raw.btnclick);
                mp.start();
                break;
        }
    }
}
