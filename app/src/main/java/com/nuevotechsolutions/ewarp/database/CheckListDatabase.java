package com.nuevotechsolutions.ewarp.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.nuevotechsolutions.ewarp.databaseDAO.RoomDao;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngDetails;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.AuxEngLabel;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.CheckList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecord;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.Component;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.DepartmentList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.EnginDescriptionData;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.MasterWorkId;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.TeamSelectionList;
import com.nuevotechsolutions.ewarp.model.ChecklistModel.UserDetails;
import com.nuevotechsolutions.ewarp.model.DefectListModel.DefectListDataList;
import com.nuevotechsolutions.ewarp.model.NoticeDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklist;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselChecklistRecordDataList;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselComponent;
import com.nuevotechsolutions.ewarp.model.VesselModel.VesselMasterWorkId;

import static com.nuevotechsolutions.ewarp.utils.ApiUrlClass.roomDatabaseName;



@Database(entities = {CheckList.class, Component.class, UserDetails.class,
        AuxEngDetails.class, AuxEngLabel.class, TeamSelectionList.class,
        ChecklistRecordDataList.class, MasterWorkId.class, ChecklistRecord.class,
         EnginDescriptionData.class, DepartmentList.class, VesselChecklist.class, VesselComponent.class,
        VesselMasterWorkId.class, VesselChecklistRecordDataList.class, DefectListDataList.class, NoticeDataList.class}, version = 1,exportSchema = false)
public abstract class CheckListDatabase extends RoomDatabase {

    public abstract RoomDao productDao();
    private static CheckListDatabase INSTANCE;

    static CheckListDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CheckList.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            CheckListDatabase.class,
                            roomDatabaseName).build();
                }
            }
        }
        return INSTANCE;
    }
}
