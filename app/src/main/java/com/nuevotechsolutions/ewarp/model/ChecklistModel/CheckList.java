package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "checklist_table", indices = @Index(value = {"uid"}, unique = true))
public class CheckList implements Serializable {


    @PrimaryKey(autoGenerate = false)
    @NonNull
//    @ColumnInfo(name = "id")
//    int id;
    @ColumnInfo(name = "uid")
    String uid;
    @ColumnInfo(name = "list")
    String list;
    @ColumnInfo(name = "checklist_type")
    String checklist_type;
    @ColumnInfo(name = "option_btn_type")
    String option_btn_type;
    @ColumnInfo(name = "short_nm")
    String short_nm;
    @ColumnInfo(name = "status")
    Integer status;
    @ColumnInfo(name = "singleuser")
    String singleuser;
    @ColumnInfo(name = "delete_id")
    Integer delete_id;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

   /* public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }*/

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getChecklist_type() {
        return checklist_type;
    }

    public void setChecklist_type(String checklist_type) {
        this.checklist_type = checklist_type;
    }

    public String getOption_btn_type() {
        return option_btn_type;
    }

    public void setOption_btn_type(String option_btn_type) {
        this.option_btn_type = option_btn_type;
    }

    public String getShort_nm() {
        return short_nm;
    }

    public void setShort_nm(String short_nm) {
        this.short_nm = short_nm;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSingleuser() {
        return singleuser;
    }

    public void setSingleuser(String singleuser) {
        this.singleuser = singleuser;
    }

    public Integer getDelete_id() {
        return delete_id;
    }

    public void setDelete_id(Integer delete_id) {
        this.delete_id = delete_id;
    }
}
