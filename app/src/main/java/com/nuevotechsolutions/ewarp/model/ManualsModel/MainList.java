package com.nuevotechsolutions.ewarp.model.ManualsModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "main_list")
public class MainList implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    int id;
    @ColumnInfo(name = "main_list_key")
    Integer main_list_key;
    @ColumnInfo(name = "flag")
    String flag;
    @ColumnInfo(name = "main_list_name")
    String main_list_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getMain_list_key() {
        return main_list_key;
    }

    public void setMain_list_key(Integer main_list_key) {
        this.main_list_key = main_list_key;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMain_list_name() {
        return main_list_name;
    }

    public void setMain_list_name(String main_list_name) {
        this.main_list_name = main_list_name;
    }
}
