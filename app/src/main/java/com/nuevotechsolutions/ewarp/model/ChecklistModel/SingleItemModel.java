package com.nuevotechsolutions.ewarp.model.ChecklistModel;

public class SingleItemModel {

    String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
