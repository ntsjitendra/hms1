package com.nuevotechsolutions.ewarp.model.VesselModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;



@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "vessel_checklist", indices = @Index(value = {"uid"}, unique = true))
public class VesselChecklist implements Serializable {

    @PrimaryKey(autoGenerate = false)
    @NonNull
    /*@ColumnInfo(name = "id")
    int id;*/
    @ColumnInfo(name = "uid")
    String uid;
    @ColumnInfo(name = "list")
    String list;

    /*public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }*/

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

   }
