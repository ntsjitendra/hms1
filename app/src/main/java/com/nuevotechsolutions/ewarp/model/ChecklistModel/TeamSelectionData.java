package com.nuevotechsolutions.ewarp.model.ChecklistModel;

public class TeamSelectionData {

    private String engineers_desig;

    private String engineers_name;

    public String getEngineers_desig ()
    {
        return engineers_desig;
    }

    public void setEngineers_desig (String engineers_desig)
    {
        this.engineers_desig = engineers_desig;
    }

    public String getEngineers_name ()
    {
        return engineers_name;
    }

    public void setEngineers_name (String engineers_name)
    {
        this.engineers_name = engineers_name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [engineers_desig = "+engineers_desig+", engineers_name = "+engineers_name+"]";
    }
}
