package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "check_list_details", indices = @Index(value = {"id"}, unique = true))
public class ChecklistRecord implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "checkListId")
    private int checkListId;

    @ColumnInfo(name = "wrk_id")
    int wrk_id;
    @ColumnInfo(name = "grtd_by")
    String grtd_by;
    @ColumnInfo(name = "cmp_name")
    String cmp_name;
    @ColumnInfo(name = "ctgry_name")
    String ctgry_name;
    @ColumnInfo(name = "grtn_fr_wrk")
    String grtn_fr_wrk;
    @ColumnInfo(name = "grtn_fr_wrk_sub_ctg")
    String grtn_fr_wrk_sub_ctg;
    @ColumnInfo(name = "wrk_id_crtn_dt")
    String wrk_id_crtn_dt;
    @ColumnInfo(name = "modified_dt")
    String modified_dt;
    @ColumnInfo(name = "wrk_status")
    String wrk_status;
    @ColumnInfo(name = "device_id")
    String device_id;
    @ColumnInfo(name = "comment")
    String comment;
    @ColumnInfo(name = "room_no")
    String room_no;
    @ColumnInfo(name = "type")
    String type;
    @ColumnInfo(name = "user_name")
    String user_name;

    @ColumnInfo(name = "btn_nm")
    int btn_nm;
    @ColumnInfo(name = "rb")
    String rb;
    @ColumnInfo(name = "cmnt")
    String cmnt;
    @ColumnInfo(name = "ph")
    String ph;
    @ColumnInfo(name = "alrt_rb")
    String alrt_rb;
    @ColumnInfo(name = "crnt_dt_tm")
    String crnt_dt_tm;
    @ColumnInfo(name = "crnt_lctn")
    String crnt_lctn;
    @ColumnInfo(name = "sbmt_dtls")
    String sbmt_dtls;
    @ColumnInfo(name = "cordinates")
    String cordinates;
    @ColumnInfo(name = "emp_id")
    String emp_id;

    @ColumnInfo(name = "id")
    int id;
    @ColumnInfo(name = "uid")
    String uid;
    @ColumnInfo(name = "checklist_name")
    String checklist_name;
    @ColumnInfo(name = "points")
    String points;
    @ColumnInfo(name = "photo")
    String photo;
    @ColumnInfo(name = "alert")
    String alert;
    @ColumnInfo(name = "title_of_section")
    String title_of_section;
    @ColumnInfo(name = "designation")
    String designation;
    @ColumnInfo(name = "rb_points_description")
    String rb_points_description;

    public ChecklistRecord()
    {

    }



    public int getBtn_nm() {
        return btn_nm;
    }

    public void setBtn_nm(int btn_nm) {
        this.btn_nm = btn_nm;
    }

    public String getRb() {
        return rb;
    }

    public void setRb(String rb) {
        this.rb = rb;
    }

    public String getCmnt() {
        return cmnt;
    }

    public void setCmnt(String cmnt) {
        this.cmnt = cmnt;
    }

    public String getPh() {
        return ph;
    }

    public void setPh(String ph) {
        this.ph = ph;
    }

    public String getAlrt_rb() {
        return alrt_rb;
    }

    public void setAlrt_rb(String alrt_rb) {
        this.alrt_rb = alrt_rb;
    }

    public String getCrnt_dt_tm() {
        return crnt_dt_tm;
    }

    public void setCrnt_dt_tm(String crnt_dt_tm) {
        this.crnt_dt_tm = crnt_dt_tm;
    }

    public String getCrnt_lctn() {
        return crnt_lctn;
    }

    public void setCrnt_lctn(String crnt_lctn) {
        this.crnt_lctn = crnt_lctn;
    }

    public String getSbmt_dtls() {
        return sbmt_dtls;
    }

    public void setSbmt_dtls(String sbmt_dtls) {
        this.sbmt_dtls = sbmt_dtls;
    }

    public String getCordinates() {
        return cordinates;
    }

    public void setCordinates(String cordinates) {
        this.cordinates = cordinates;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public int getWrk_id() {
        return wrk_id;
    }

    public void setWrk_id(int wrk_id) {
        this.wrk_id = wrk_id;
    }

    public String getGrtd_by() {
        return grtd_by;
    }

    public void setGrtd_by(String grtd_by) {
        this.grtd_by = grtd_by;
    }

    public String getCmp_name() {
        return cmp_name;
    }

    public void setCmp_name(String cmp_name) {
        this.cmp_name = cmp_name;
    }

    public String getCtgry_name() {
        return ctgry_name;
    }

    public void setCtgry_name(String ctgry_name) {
        this.ctgry_name = ctgry_name;
    }

    public String getGrtn_fr_wrk() {
        return grtn_fr_wrk;
    }

    public void setGrtn_fr_wrk(String grtn_fr_wrk) {
        this.grtn_fr_wrk = grtn_fr_wrk;
    }

    public String getGrtn_fr_wrk_sub_ctg() {
        return grtn_fr_wrk_sub_ctg;
    }

    public void setGrtn_fr_wrk_sub_ctg(String grtn_fr_wrk_sub_ctg) {
        this.grtn_fr_wrk_sub_ctg = grtn_fr_wrk_sub_ctg;
    }

    public String getWrk_id_crtn_dt() {
        return wrk_id_crtn_dt;
    }

    public void setWrk_id_crtn_dt(String wrk_id_crtn_dt) {
        this.wrk_id_crtn_dt = wrk_id_crtn_dt;
    }

    public String getModified_dt() {
        return modified_dt;
    }

    public void setModified_dt(String modified_dt) {
        this.modified_dt = modified_dt;
    }

    public String getWrk_status() {
        return wrk_status;
    }

    public void setWrk_status(String wrk_status) {
        this.wrk_status = wrk_status;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRoom_no() {
        return room_no;
    }

    public void setRoom_no(String room_no) {
        this.room_no = room_no;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getChecklist_name() {
        return checklist_name;
    }

    public void setChecklist_name(String checklist_name) {
        this.checklist_name = checklist_name;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getTitle_of_section() {
        return title_of_section;
    }

    public void setTitle_of_section(String title_of_section) {
        this.title_of_section = title_of_section;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getRb_points_description() {
        return rb_points_description;
    }

    public void setRb_points_description(String rb_points_description) {
        this.rb_points_description = rb_points_description;
    }

    public int getCheckListId() {
        return checkListId;
    }

    public void setCheckListId(int checkListId) {
        this.checkListId = checkListId;
    }
}
