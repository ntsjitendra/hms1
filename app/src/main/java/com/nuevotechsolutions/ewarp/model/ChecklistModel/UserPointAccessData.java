package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "user_access_point", indices = @Index(value = {"id"}, unique = true))
public class UserPointAccessData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;

    @ColumnInfo(name = "wrk_id")
    private Integer wrk_id;

    @ColumnInfo(name ="uid" )
    private Integer uid;

    @ColumnInfo(name ="rank_id" )
    private Integer rank_id;

    @ColumnInfo(name ="employee_id" )
    private String employee_id;

    @ColumnInfo(name ="points" )
    private String points;

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public Integer getWrk_id() {
        return wrk_id;
    }

    public void setWrk_id(Integer wrk_id) {
        this.wrk_id = wrk_id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getRank_id() {
        return rank_id;
    }

    public void setRank_id(Integer rank_id) {
        this.rank_id = rank_id;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
