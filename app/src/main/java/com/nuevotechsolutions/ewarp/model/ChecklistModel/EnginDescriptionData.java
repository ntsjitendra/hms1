package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "engin_description_data", indices = @Index(value = {"id"}, unique = true))
public class EnginDescriptionData implements Serializable {

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name = "id")
    private Integer id;
    @ColumnInfo(name = "uid")
    Integer uid;
    @ColumnInfo(name = "entry_time")
    String entry_time;
    @ColumnInfo(name = "wrk_id")
    Integer wrk_id;
    @ColumnInfo(name = "decription")
    String decription;
    @ColumnInfo(name = "dec_answer")
    String dec_answer;
    @ColumnInfo(name = "comp_name")
    String comp_name;
    @ColumnInfo(name = "emp_id")
    String emp_id;
    @ColumnInfo(name = "user_id")
    String user_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getEntry_time() {
        return entry_time;
    }

    public void setEntry_time(String entry_time) {
        this.entry_time = entry_time;
    }

    public Integer getWrk_id() {
        return wrk_id;
    }

    public void setWrk_id(Integer wrk_id) {
        this.wrk_id = wrk_id;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public String getDec_answer() {
        return dec_answer;
    }

    public void setDec_answer(String dec_answer) {
        this.dec_answer = dec_answer;
    }

    public String getComp_name() {
        return comp_name;
    }

    public void setComp_name(String comp_name) {
        this.comp_name = comp_name;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
