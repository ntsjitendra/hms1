package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "userdetail_table")
public class UserDetails implements Serializable {


    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;


    @ColumnInfo(name ="date_added" )
    private String date_added;

    @ColumnInfo(name ="password" )
    private String password;

    @ColumnInfo(name ="user_id" )
    private String user_id;

    @ColumnInfo(name ="serial" )
    private String serial;

    @ColumnInfo(name ="comp_name" )
    private String comp_name;

    @ColumnInfo(name ="employee_id" )
    private String employee_id;

    @ColumnInfo(name ="last_name" )
    private String last_name;

    @ColumnInfo(name ="access_token" )
    private String accessToken;

    @ColumnInfo(name ="rank" )
    private String rank;

    @ColumnInfo(name ="first_name" )
    private String first_name;

    @ColumnInfo(name ="email" )
    private String email;

    @ColumnInfo(name ="status" )
    private String status;

    @ColumnInfo(name ="rank_id" )
    private String rank_id;

    @ColumnInfo(name ="vessel_name" )
    private String vessel_name;

    @ColumnInfo(name ="dbnm" )
    private String dbnm;

    @ColumnInfo(name ="user_image" )
    private String user_image;

    public String getRank_id() {
        return rank_id;
    }

    public void setRank_id(String rank_id) {
        this.rank_id = rank_id;
    }

    public String getVessel_name() {
        return vessel_name;
    }

    public void setVessel_name(String vessel_name) {
        this.vessel_name = vessel_name;
    }

    public String getDate_added ()
    {
        return date_added;
    }

    public void setDate_added (String date_added)
    {
        this.date_added = date_added;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getSerial ()
    {
        return serial;
    }

    public void setSerial (String serial)
    {
        this.serial = serial;
    }

    public String getComp_name() {
        return comp_name;
    }

    public void setComp_name(String comp_name) {
        this.comp_name = comp_name;
    }

    public String getEmployee_id ()
    {
        return employee_id;
    }

    public void setEmployee_id (String employee_id)
    {
        this.employee_id = employee_id;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRank ()
    {
        return rank;
    }

    public void setRank (String rank)
    {
        this.rank = rank;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [date_added = "+date_added+", password = "+password+", user_id = "+user_id+", serial = "+serial+", employee_id = "+employee_id+", last_name = "+last_name+", rank = "+rank+", first_name = "+first_name+", email = "+email+", status = "+status+"]";
    }

    public String getDbnm() {
        return dbnm;
    }

    public void setDbnm(String dbnm) {
        this.dbnm = dbnm;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }
}
