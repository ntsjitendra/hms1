package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;



@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "aux_eng_label", indices = @Index(value = {"id"}, unique = true))
public class AuxEngLabel implements Serializable {

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "uid")
    String uid;

    @ColumnInfo(name ="eng_no_label" )
    private String eng_label;

    @ColumnInfo(name ="eng_maint_type_label" )
    private String eng_maint_type_label;

    @ColumnInfo(name ="unit_no_label" )
    private String unit_no_label;

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEng_label() {
        return eng_label;
    }

    public void setEng_label(String eng_label) {
        this.eng_label = eng_label;
    }

    public String getEng_maint_type_label() {
        return eng_maint_type_label;
    }

    public void setEng_maint_type_label(String eng_maint_type_label) {
        this.eng_maint_type_label = eng_maint_type_label;
    }

    public String getUnit_no_label() {
        return unit_no_label;
    }

    public void setUnit_no_label(String unit_no_label) {
        this.unit_no_label = unit_no_label;
    }
}
