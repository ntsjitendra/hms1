package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "master_wrk_id", indices = @Index(value = {"wrk_id"}, unique = true))
public class MasterWorkId implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private  Integer id;

    @ColumnInfo(name = "grtd_by")
    private String grtd_by;

    @ColumnInfo(name = "wrk_id")
    private Integer wrk_id;

    @ColumnInfo(name = "grtdby_name")
    private String grtdby_name;

    @ColumnInfo(name = "device_id")
    private String device_id;

    @ColumnInfo(name = "engine_type_nmbr")
    private Integer engine_type_nmbr;

    @ColumnInfo(name = "grtdby_lastname")
    private String grtdby_lastname;

    @ColumnInfo(name = "cmp_name")
    private String cmp_name;

    @ColumnInfo(name = "list")
    private String list;

    @ColumnInfo(name = "wrk_id_crtn_dt")
    private String wrk_id_crtn_dt;

    @ColumnInfo(name = "uid")
    private Integer uid;

    @ColumnInfo(name = "modified_dt")
    private String modified_dt;

    @ColumnInfo(name = "unit_no")
    private String unit_no;

    @ColumnInfo(name = "target_complition_date")
    private String target_complition_date;

    @ColumnInfo(name = "wrk_status")
    private Integer wrk_status;

    @ColumnInfo(name = "comment")
    private String comment;

    @ColumnInfo(name = "completed_by")
    private String completed_by;

    @ColumnInfo(name = "assign_by")
    private String assign_by;

    @ColumnInfo(name = "assign_type")
    private String assign_type;

    @ColumnInfo(name = "checklist_type")
    private String checklist_type;

    @ColumnInfo(name = "seen")
    private boolean seen;

    @ColumnInfo(name = "team_list")
    private String team_list;

    @ColumnInfo(name = "short_nm")
    private String short_nm;

    @ColumnInfo(name = "user_img")
    private String user_img;

    @ColumnInfo(name = "data_Upload_flag")
    private Integer data_Upload_flag;

    @ColumnInfo(name = "temp_Wrk_id")
    private Integer temp_Wrk_id;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "offline_status")
    private Integer offline_status;

    @ColumnInfo(name = "delete_id")
    private Integer delete_id;

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public String getGrtd_by() {
        return grtd_by;
    }

    public void setGrtd_by(String grtd_by) {
        this.grtd_by = grtd_by;
    }

    public Integer getWrk_id() {
        return wrk_id;
    }

    public void setWrk_id(Integer wrk_id) {
        this.wrk_id = wrk_id;
    }

    public String getGrtdby_name() {
        return grtdby_name;
    }

    public void setGrtdby_name(String grtdby_name) {
        this.grtdby_name = grtdby_name;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public Integer getEngine_type_nmbr() {
        return engine_type_nmbr;
    }

    public void setEngine_type_nmbr(Integer engine_type_nmbr) {
        this.engine_type_nmbr = engine_type_nmbr;
    }

    public String getGrtdby_lastname() {
        return grtdby_lastname;
    }

    public void setGrtdby_lastname(String grtdby_lastname) {
        this.grtdby_lastname = grtdby_lastname;
    }

    public String getCmp_name() {
        return cmp_name;
    }

    public void setCmp_name(String cmp_name) {
        this.cmp_name = cmp_name;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getWrk_id_crtn_dt() {
        return wrk_id_crtn_dt;
    }

    public void setWrk_id_crtn_dt(String wrk_id_crtn_dt) {
        this.wrk_id_crtn_dt = wrk_id_crtn_dt;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getModified_dt() {
        return modified_dt;
    }

    public void setModified_dt(String modified_dt) {
        this.modified_dt = modified_dt;
    }

    public String getUnit_no() {
        return unit_no;
    }

    public void setUnit_no(String unit_no) {
        this.unit_no = unit_no;
    }

    public String getTarget_complition_date() {
        return target_complition_date;
    }

    public void setTarget_complition_date(String target_complition_date) {
        this.target_complition_date = target_complition_date;
    }

    public Integer getWrk_status() {
        return wrk_status;
    }

    public void setWrk_status(Integer wrk_status) {
        this.wrk_status = wrk_status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCompleted_by() {
        return completed_by;
    }

    public void setCompleted_by(String completed_by) {
        this.completed_by = completed_by;
    }

    public String getAssign_by() {
        return assign_by;
    }

    public void setAssign_by(String assign_by) {
        this.assign_by = assign_by;
    }

    public String getAssign_type() {
        return assign_type;
    }

    public void setAssign_type(String assign_type) {
        this.assign_type = assign_type;
    }

    public String getChecklist_type() {
        return checklist_type;
    }

    public void setChecklist_type(String checklist_type) {
        this.checklist_type = checklist_type;
    }

    public boolean getSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getTeam_list() {
        return team_list;
    }

    public void setTeam_list(String team_list) {
        this.team_list = team_list;
    }

    public String getShort_nm() {
        return short_nm;
    }

    public void setShort_nm(String short_nm) {
        this.short_nm = short_nm;
    }


    public String getUser_img() {
        return user_img;
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
    }

    public Integer getData_Upload_flag() {
        return data_Upload_flag;
    }

    public void setData_Upload_flag(Integer data_Upload_flag) {
        this.data_Upload_flag = data_Upload_flag;
    }

    public Integer getTemp_Wrk_id() {
        return temp_Wrk_id;
    }

    public void setTemp_Wrk_id(Integer temp_Wrk_id) {
        this.temp_Wrk_id = temp_Wrk_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getOffline_status() {
        return offline_status;
    }

    public void setOffline_status(Integer offline_status) {
        this.offline_status = offline_status;
    }

    public Integer getDelete_id() {
        return delete_id;
    }

    public void setDelete_id(Integer delete_id) {
        this.delete_id = delete_id;
    }
}
