package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "department_table", indices = @Index(value = {"id"}, unique = true))
public class DepartmentList {

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name = "id")
    int id;
    @ColumnInfo(name = "department_nm")
    String department_nm;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDepartment_nm() {
        return department_nm;
    }

    public void setDepartment_nm(String department_nm) {
        this.department_nm = department_nm;
    }
}
