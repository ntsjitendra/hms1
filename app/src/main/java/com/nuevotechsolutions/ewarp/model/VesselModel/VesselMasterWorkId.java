package com.nuevotechsolutions.ewarp.model.VesselModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "vessel_master_wrk_id", indices = @Index(value = {"wrk_id"}, unique = true))
public class VesselMasterWorkId implements Serializable {

    @PrimaryKey(autoGenerate = false)
    @NonNull
   /* @ColumnInfo(name = "id")
    private  Integer id;*/

   @ColumnInfo(name = "wrk_id")
   private Integer wrk_id;

    @ColumnInfo(name = "grtd_by")
    private String grtd_by;

    @ColumnInfo(name = "grtdby_name")
    private String grtdby_name;

    @ColumnInfo(name = "device_id")
    private String device_id;

    @ColumnInfo(name = "engine_type_nmbr")
    private Integer engine_type_nmbr;

    @ColumnInfo(name = "grtdby_lastname")
    private String grtdby_lastname;

    @ColumnInfo(name = "cmp_name")
    private String cmp_name;

    @ColumnInfo(name = "list")
    private String list;

    @ColumnInfo(name = "wrk_id_crtn_dt")
    private String wrk_id_crtn_dt;

    @ColumnInfo(name = "uid")
    private Integer uid;

    @ColumnInfo(name = "last_modify_dt")
    private String last_modify_dt;

    @ColumnInfo(name = "unit_no")
    private String unit_no;

    @ColumnInfo(name = "target_complition_date")
    private String target_complition_date;

    @ColumnInfo(name = "wrk_status")
    private Integer wrk_status;

    @ColumnInfo(name = "comment")
    private String comment;

    @ColumnInfo(name = "completed_by")
    private String completed_by;

    @ColumnInfo(name = "assign_by")
    private String assign_by;

    @ColumnInfo(name = "assign_type")
    private String assign_type;

    @ColumnInfo(name = "checklist_type")
    private String checklist_type;

    @ColumnInfo(name = "seen")
    private boolean seen;

    @ColumnInfo(name = "newstatus")
    private String newstatus;

    @ColumnInfo(name = "comp_date")
    private String comp_date;

    @ColumnInfo(name = "employee_id")
    private String employee_id;

    @ColumnInfo(name = "vsl_id")
    private Integer vsl_id;

    @ColumnInfo(name = "created_by")
    private String created_by;

    @ColumnInfo(name = "crnt_date")
    private String crnt_date;

    @ColumnInfo(name = "status")
    private Integer status;



    /*@NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }*/

    public String getGrtd_by() {
        return grtd_by;
    }

    public void setGrtd_by(String grtd_by) {
        this.grtd_by = grtd_by;
    }

    public Integer getWrk_id() {
        return wrk_id;
    }

    public void setWrk_id(Integer wrk_id) {
        this.wrk_id = wrk_id;
    }

    public String getGrtdby_name() {
        return grtdby_name;
    }

    public void setGrtdby_name(String grtdby_name) {
        this.grtdby_name = grtdby_name;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public Integer getEngine_type_nmbr() {
        return engine_type_nmbr;
    }

    public void setEngine_type_nmbr(Integer engine_type_nmbr) {
        this.engine_type_nmbr = engine_type_nmbr;
    }

    public String getGrtdby_lastname() {
        return grtdby_lastname;
    }

    public void setGrtdby_lastname(String grtdby_lastname) {
        this.grtdby_lastname = grtdby_lastname;
    }

    public String getCmp_name() {
        return cmp_name;
    }

    public void setCmp_name(String cmp_name) {
        this.cmp_name = cmp_name;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getWrk_id_crtn_dt() {
        return wrk_id_crtn_dt;
    }

    public void setWrk_id_crtn_dt(String wrk_id_crtn_dt) {
        this.wrk_id_crtn_dt = wrk_id_crtn_dt;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getLast_modify_dt() {
        return last_modify_dt;
    }

    public void setLast_modify_dt(String last_modify_dt) {
        this.last_modify_dt = last_modify_dt;
    }

    public String getUnit_no() {
        return unit_no;
    }

    public void setUnit_no(String unit_no) {
        this.unit_no = unit_no;
    }

    public String getTarget_complition_date() {
        return target_complition_date;
    }

    public void setTarget_complition_date(String target_complition_date) {
        this.target_complition_date = target_complition_date;
    }

    public Integer getWrk_status() {
        return wrk_status;
    }

    public void setWrk_status(Integer wrk_status) {
        this.wrk_status = wrk_status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCompleted_by() {
        return completed_by;
    }

    public void setCompleted_by(String completed_by) {
        this.completed_by = completed_by;
    }

    public String getAssign_by() {
        return assign_by;
    }

    public void setAssign_by(String assign_by) {
        this.assign_by = assign_by;
    }

    public String getAssign_type() {
        return assign_type;
    }

    public void setAssign_type(String assign_type) {
        this.assign_type = assign_type;
    }

    public String getChecklist_type() {
        return checklist_type;
    }

    public void setChecklist_type(String checklist_type) {
        this.checklist_type = checklist_type;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getNewstatus() {
        return newstatus;
    }

    public void setNewstatus(String newstatus) {
        this.newstatus = newstatus;
    }

    public String getComp_date() {
        return comp_date;
    }

    public void setComp_date(String comp_date) {
        this.comp_date = comp_date;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public Integer getVsl_id() {
        return vsl_id;
    }

    public void setVsl_id(Integer vsl_id) {
        this.vsl_id = vsl_id;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCrnt_date() {
        return crnt_date;
    }

    public void setCrnt_date(String crnt_date) {
        this.crnt_date = crnt_date;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

   }
