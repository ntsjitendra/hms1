package com.nuevotechsolutions.ewarp.model;


import java.util.List;

public class VoiceRecordData {

    String Uri;
    String fileName;
//    String filePath;
    boolean isPlaying = false;
    boolean isDownloaded = false;
    public VoiceRecordData(String uri, String filePath, String fileName, boolean isPlaying, boolean isDownloaded) {
        Uri = uri;
        this.fileName = fileName;
//        this.filePath = filePath;
        this.isPlaying = isPlaying;
        this.isDownloaded = isDownloaded;
    }

//    public String getFilePath() {
//        return filePath;
//    }

    public String getUri() {
        return Uri;
    }

    public String getFileName() {
        return fileName;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        this.isPlaying = playing;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public void setDownloaded(boolean downloaded) {
        isDownloaded = downloaded;
    }
}