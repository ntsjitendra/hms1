package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;



@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "aux_eng_dtls", indices = @Index(value = {"id"}, unique = true))
public class AuxEngDetails implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "uid")
    String uid;

    @ColumnInfo(name ="auxillary_eng_number" )
    private int auxillary_eng_number;

    @ColumnInfo(name ="auxillary_eng_type" )
    private String auxillary_eng_type;

    @ColumnInfo(name ="unit_no" )
    private String unit_no;

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getAuxillary_eng_number() {
        return auxillary_eng_number;
    }

    public void setAuxillary_eng_number(int auxillary_eng_number) {
        this.auxillary_eng_number = auxillary_eng_number;
    }

    public String getAuxillary_eng_type() {
        return auxillary_eng_type;
    }

    public void setAuxillary_eng_type(String auxillary_eng_type) {
        this.auxillary_eng_type = auxillary_eng_type;
    }

    public String getUnit_no() {
        return unit_no;
    }

    public void setUnit_no(String unit_no) {
        this.unit_no = unit_no;
    }
}
