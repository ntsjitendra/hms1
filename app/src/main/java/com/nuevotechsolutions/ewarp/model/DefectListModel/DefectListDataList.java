package com.nuevotechsolutions.ewarp.model.DefectListModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "defect_list")
public class DefectListDataList {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    int id;
    @ColumnInfo(name = "wrk_id")
    Integer wrk_id;
    @ColumnInfo(name = "description")
    String description;
    @ColumnInfo(name = "deficiency")
    String deficiency;
    @ColumnInfo(name = "action_byship")
    String action_byship;
    @ColumnInfo(name = "action_byoffice")
    String action_byoffice;
    @ColumnInfo(name = "remarks")
    String remarks;
    @ColumnInfo(name = "closed_date")
    String closed_date;
    @ColumnInfo(name = "rev_target_date")
    String rev_target_date;
    @ColumnInfo(name = "target_date")
    String target_date;
    @ColumnInfo(name = "reason_for_delay")
    String reason_for_delay;
    @ColumnInfo(name = "before_img")
    String before_img;
    @ColumnInfo(name = "after_img")
    String after_img;
    @ColumnInfo(name = "status")
    Integer status;
    @ColumnInfo(name = "added_date")
    String added_date;
    @ColumnInfo(name = "created_by")
    String created_by;
    @ColumnInfo(name = "Cncl_Comp_comment")
    String Cncl_Comp_comment;
    @ColumnInfo(name = "completed_by")
    String completed_by;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getWrk_id() {
        return wrk_id;
    }

    public void setWrk_id(Integer wrk_id) {
        this.wrk_id = wrk_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeficiency() {
        return deficiency;
    }

    public void setDeficiency(String deficiency) {
        this.deficiency = deficiency;
    }

    public String getAction_byship() {
        return action_byship;
    }

    public void setAction_byship(String action_byship) {
        this.action_byship = action_byship;
    }

    public String getAction_byoffice() {
        return action_byoffice;
    }

    public void setAction_byoffice(String action_byoffice) {
        this.action_byoffice = action_byoffice;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getClosed_date() {
        return closed_date;
    }

    public void setClosed_date(String closed_date) {
        this.closed_date = closed_date;
    }

    public String getRev_target_date() {
        return rev_target_date;
    }

    public void setRev_target_date(String rev_target_date) {
        this.rev_target_date = rev_target_date;
    }

    public String getTarget_date() {
        return target_date;
    }

    public void setTarget_date(String target_date) {
        this.target_date = target_date;
    }

    public String getReason_for_delay() {
        return reason_for_delay;
    }

    public void setReason_for_delay(String reason_for_delay) {
        this.reason_for_delay = reason_for_delay;
    }

    public String getBefore_img() {
        return before_img;
    }

    public void setBefore_img(String before_img) {
        this.before_img = before_img;
    }

    public String getAfter_img() {
        return after_img;
    }

    public void setAfter_img(String after_img) {
        this.after_img = after_img;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAdded_date() {
        return added_date;
    }

    public void setAdded_date(String added_date) {
        this.added_date = added_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCncl_Comp_comment() {
        return Cncl_Comp_comment;
    }

    public void setCncl_Comp_comment(String cncl_Comp_comment) {
        Cncl_Comp_comment = cncl_Comp_comment;
    }

    public String getCompleted_by() {
        return completed_by;
    }

    public void setCompleted_by(String completed_by) {
        this.completed_by = completed_by;
    }
}
