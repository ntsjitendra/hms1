package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;



@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "component_table", indices = @Index(value = {"id"}, unique = true))
public class Component implements Serializable {


    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name = "id")
    int id;

    @ColumnInfo(name = "uid")
    String uid;
    @ColumnInfo(name = "rb_points_description")
    String rb_points_description;
    @ColumnInfo(name = "cuserpoint")
    String cuserpoint;
    @ColumnInfo(name = "checklist_name")
    String checklist_name;
    @ColumnInfo(name = "alert")
    String alert;
    @ColumnInfo(name = "photo")
    String photo;
    @ColumnInfo(name = "title_of_section")
    String title_of_section;
    @ColumnInfo(name = "comment")
    String comment;
    @ColumnInfo(name = "designation")
    String designation;
    @ColumnInfo(name = "points")
    String points;
    @ColumnInfo(name = "guidance")
    String guidance;
    @ColumnInfo(name = "guidance_image")
    String guidance_image;
    @ColumnInfo(name = "guidance_text")
    String guidance_text;
    @ColumnInfo(name = "rank")
    String rank;
    @ColumnInfo(name = "active_time")
    String active_time;
    @ColumnInfo(name = "crnt_dt_tm")
    String crnt_dt_tm;
    @ColumnInfo(name = "point")
    String point;
    @ColumnInfo(name = "status")
    String status;
    @ColumnInfo(name ="image_data")
    public String  image_data;
    @ColumnInfo(name ="suggested_ans")
    public String  suggested_ans;
    @ColumnInfo(name ="selfy_on_submit")
    public String  selfy_on_submit;
    @ColumnInfo(name ="photo_location")
    public String  photo_location;
    @ColumnInfo(name ="option_btn_type")
    public String  option_btn_type;
    @ColumnInfo(name ="edit")
    public String  edit;
    @ColumnInfo(name = "voice_record")
    public String voice_record;

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    @ColumnInfo(name = "additional")
    public String additional;
    @ColumnInfo(name = "yesnona")
    public String yesnona;

    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }

    @ColumnInfo(name = "column_name")
    public String column_name;





    public String getYesnona() {
        return yesnona;
    }

    public void setYesnona(String yesnona) {
        this.yesnona = yesnona;
    }





    public String getImage_data() {
        return image_data;
    }

    public void setImage_data(String image_data) {
        this.image_data = image_data;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getActive_time() {
        return active_time;
    }

    public void setActive_time(String active_time) {
        this.active_time = active_time;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRb_points_description() {
        return rb_points_description;
    }

    public void setRb_points_description(String rb_points_description) {
        this.rb_points_description = rb_points_description;
    }

    public String getChecklist_name() {
        return checklist_name;
    }

    public void setChecklist_name(String checklist_name) {
        this.checklist_name = checklist_name;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTitle_of_section() {
        return title_of_section;
    }

    public void setTitle_of_section(String title_of_section) {
        this.title_of_section = title_of_section;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getGuidance() {
        return guidance;
    }

    public void setGuidance(String guidance) {
        this.guidance = guidance;
    }

    public String getGuidance_image() {
        return guidance_image;
    }

    public void setGuidance_image(String guidance_image) {
        this.guidance_image = guidance_image;
    }

    public String getGuidance_text() {
        return guidance_text;
    }

    public String getCrnt_dt_tm() {
        return crnt_dt_tm;
    }

    public void setCrnt_dt_tm(String crnt_dt_tm) {
        this.crnt_dt_tm = crnt_dt_tm;
    }

    public void setGuidance_text(String guidance_text) {
        this.guidance_text = guidance_text;
    }

    public String getCuserpoint() {
        return cuserpoint;
    }

    public void setCuserpoint(String cuserpoint) {
        this.cuserpoint = cuserpoint;
    }

    public String getSuggested_ans() {
        return suggested_ans;
    }

    public void setSuggested_ans(String suggested_ans) {
        this.suggested_ans = suggested_ans;
    }

    public String getSelfy_on_submit() {
        return selfy_on_submit;
    }

    public void setSelfy_on_submit(String selfy_on_submit) {
        this.selfy_on_submit = selfy_on_submit;
    }

    public String getPhoto_location() {
        return photo_location;
    }

    public void setPhoto_location(String photo_location) {
        this.photo_location = photo_location;
    }

    public String getOption_btn_type() {
        return option_btn_type;
    }

    public void setOption_btn_type(String option_btn_type) {
        this.option_btn_type = option_btn_type;
    }

    public String getEdit() {
        return edit;
    }

    public void setEdit(String edit) {
        this.edit = edit;
    }

    public String getVoice_record() {
        return voice_record;
    }

    public void setVoice_record(String voice_record) {
        this.voice_record = voice_record;
    }
}
