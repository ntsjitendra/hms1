package com.nuevotechsolutions.ewarp.model.VesselModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "vessel_checklist_record_data_list")
public class VesselChecklistRecordDataList implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "sbmt_btn_image")
    private String sbmt_btn_image;

    @ColumnInfo(name = "rank_id")
    private Integer rank_id;

    @ColumnInfo(name = "wrk_id")
    private Integer wrk_id;

    @ColumnInfo(name = "gnrtn_date")
    private String gnrtn_date;

    @ColumnInfo(name = "target_date")
    private String target_date;

    @ColumnInfo(name = "ans")
    private String ans;

    @ColumnInfo(name = "checklist_points")
    private String checklist_points;

    @ColumnInfo(name = "uid")
    private Integer uid;

    @ColumnInfo(name = "q")
    private String q;

    @ColumnInfo(name = "crnt_lctn")
    private String crnt_lctn;

    @ColumnInfo(name = "alert")
    private String alert;

    @ColumnInfo(name = "employee_id")
    private String employee_id;

    @ColumnInfo(name = "image_data")
    private String image_data;

    @ColumnInfo(name = "sbmt_dtals")
    private String sbmt_dtals;

    @ColumnInfo(name = "comment")
    private String comment;

    @ColumnInfo(name = "flag")
    private Integer flag;

    @ColumnInfo(name = "imageJson")
    private String imageJson;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSbmt_btn_image() {
        return sbmt_btn_image;
    }

    public void setSbmt_btn_image(String sbmt_btn_image) {
        this.sbmt_btn_image = sbmt_btn_image;
    }

    public Integer getRank_id() {
        return rank_id;
    }

    public void setRank_id(Integer rank_id) {
        this.rank_id = rank_id;
    }

    public Integer getWrk_id() {
        return wrk_id;
    }

    public void setWrk_id(Integer wrk_id) {
        this.wrk_id = wrk_id;
    }

    public String getGnrtn_date() {
        return gnrtn_date;
    }

    public void setGnrtn_date(String gnrtn_date) {
        this.gnrtn_date = gnrtn_date;
    }

    public String getTarget_date() {
        return target_date;
    }

    public void setTarget_date(String target_date) {
        this.target_date = target_date;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public String getChecklist_points() {
        return checklist_points;
    }

    public void setChecklist_points(String checklist_points) {
        this.checklist_points = checklist_points;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String getCrnt_lctn() {
        return crnt_lctn;
    }

    public void setCrnt_lctn(String crnt_lctn) {
        this.crnt_lctn = crnt_lctn;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getImage_data() {
        return image_data;
    }

    public void setImage_data(String image_data) {
        this.image_data = image_data;
    }

    public String getSbmt_dtals() {
        return sbmt_dtals;
    }

    public void setSbmt_dtals(String sbmt_dtals) {
        this.sbmt_dtals = sbmt_dtals;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

   public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getImageJson() {
        return imageJson;
    }

    public void setImageJson(String imageJson) {
        this.imageJson = imageJson;
    }
}
