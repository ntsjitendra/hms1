package com.nuevotechsolutions.ewarp.model.VesselModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "vessel_component")
public class VesselComponent implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    int id;
    @ColumnInfo(name = "uid")
    Integer uid;
    @ColumnInfo(name = "rb_points_description")
    String rb_points_description;
    @ColumnInfo(name = "title_of_section")
    String title_of_section;
    @ColumnInfo(name = "points")
    String points;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getRb_points_description() {
        return rb_points_description;
    }

    public void setRb_points_description(String rb_points_description) {
        this.rb_points_description = rb_points_description;
    }

    public String getTitle_of_section() {
        return title_of_section;
    }

    public void setTitle_of_section(String title_of_section) {
        this.title_of_section = title_of_section;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    /*@ColumnInfo(name = "uid")
    String uid;
    @ColumnInfo(name = "rb_points_description")
    String rb_points_description;
    @ColumnInfo(name = "cuserpoint")
    String cuserpoint;
    @ColumnInfo(name = "checklist_name")
    String checklist_name;
    @ColumnInfo(name = "alert")
    String alert;
    @ColumnInfo(name = "photo")
    String photo;
    @ColumnInfo(name = "title_of_section")
    String title_of_section;
    @ColumnInfo(name = "comment")
    String comment;
    @ColumnInfo(name = "designation")
    String designation;
    @ColumnInfo(name = "points")
    String points;
    @ColumnInfo(name = "guidance")
    String guidance;
    @ColumnInfo(name = "guidance_image")
    String guidance_image;
    @ColumnInfo(name = "guidance_text")
    String guidance_text;
    @ColumnInfo(name = "rank")
    String rank;
    @ColumnInfo(name = "active_time")
    String active_time;
    @ColumnInfo(name = "crnt_dt_tm")
    String crnt_dt_tm;
    @ColumnInfo(name = "point")
    String point;
    @ColumnInfo(name = "status")
    String status;
    @ColumnInfo(name ="image_data" )
    String  image_data;

//    @ColumnInfo(name = "onln_offln_flag")
//    boolean onln_offln_flag;
    @ColumnInfo(name = "additional")
    String additional;
    @ColumnInfo(name = "yesnona")
    String yesnona;
    @ColumnInfo(name = "suggested_ans")
    String suggested_ans;
    @ColumnInfo(name = "remove_data")
    Integer remove_data;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRb_points_description() {
        return rb_points_description;
    }

    public void setRb_points_description(String rb_points_description) {
        this.rb_points_description = rb_points_description;
    }

    public String getCuserpoint() {
        return cuserpoint;
    }

    public void setCuserpoint(String cuserpoint) {
        this.cuserpoint = cuserpoint;
    }

    public String getChecklist_name() {
        return checklist_name;
    }

    public void setChecklist_name(String checklist_name) {
        this.checklist_name = checklist_name;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTitle_of_section() {
        return title_of_section;
    }

    public void setTitle_of_section(String title_of_section) {
        this.title_of_section = title_of_section;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getGuidance() {
        return guidance;
    }

    public void setGuidance(String guidance) {
        this.guidance = guidance;
    }

    public String getGuidance_image() {
        return guidance_image;
    }

    public void setGuidance_image(String guidance_image) {
        this.guidance_image = guidance_image;
    }

    public String getGuidance_text() {
        return guidance_text;
    }

    public void setGuidance_text(String guidance_text) {
        this.guidance_text = guidance_text;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getActive_time() {
        return active_time;
    }

    public void setActive_time(String active_time) {
        this.active_time = active_time;
    }

    public String getCrnt_dt_tm() {
        return crnt_dt_tm;
    }

    public void setCrnt_dt_tm(String crnt_dt_tm) {
        this.crnt_dt_tm = crnt_dt_tm;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage_data() {
        return image_data;
    }

    public void setImage_data(String image_data) {
        this.image_data = image_data;
    }

//    public boolean isOnln_offln_flag() {
//        return onln_offln_flag;
//    }
//
//    public void setOnln_offln_flag(boolean onln_offln_flag) {
//        this.onln_offln_flag = onln_offln_flag;
//    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public String getYesnona() {
        return yesnona;
    }

    public void setYesnona(String yesnona) {
        this.yesnona = yesnona;
    }

    public String getSuggested_ans() {
        return suggested_ans;
    }

    public void setSuggested_ans(String suggested_ans) {
        this.suggested_ans = suggested_ans;
    }

    public Integer getRemove_data() {
        return remove_data;
    }

    public void setRemove_data(Integer remove_data) {
        this.remove_data = remove_data;
    }
*/}
