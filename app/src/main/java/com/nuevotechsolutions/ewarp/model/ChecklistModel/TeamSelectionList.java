package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "team_selection_list", indices = @Index(value = {"id"}, unique = true))
public class TeamSelectionList implements Serializable  {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;

    @ColumnInfo(name = "wrk_id")
    private Integer wrk_id;

    @ColumnInfo(name ="uid" )
    private Integer uid;

    @ColumnInfo(name ="rank_id" )
    private Integer rank_id;

    @ColumnInfo(name ="employee_id" )
    private String employee_id;

    @ColumnInfo(name ="last_name" )
    private String last_name;

    @ColumnInfo(name ="rank" )
    private String rank;

    @ColumnInfo(name ="first_name" )
    private String first_name;

    @ColumnInfo(name ="status" )
    private String status;

    @ColumnInfo(name ="points" )
    private String points;

    @ColumnInfo(name ="user_id" )
    private String user_id;

    public Integer getId() {
        return id;
    }

    @NonNull
    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public Integer getWrk_id() {
        return wrk_id;
    }

    public void setWrk_id( Integer wrk_id) {
        this.wrk_id = wrk_id;
    }


    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getRank_id() {
        return rank_id;
    }

    public void setRank_id(Integer rank_id) {
        this.rank_id = rank_id;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    private boolean isChecked;
    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }


}
