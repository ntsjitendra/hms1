package com.nuevotechsolutions.ewarp.model.ChecklistModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "checklist_record_data_list", indices = @Index(value = {"id"}, unique = true))
public class ChecklistRecordDataList implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "sbmt_btn_image")
    private String sbmt_btn_image;

    @ColumnInfo(name = "rank_id")
    private Integer rank_id;

    @ColumnInfo(name = "wrk_id")
    private Integer wrk_id;

    @ColumnInfo(name = "gnrtn_date")
    private String gnrtn_date;

    @ColumnInfo(name = "target_date")
    private String target_date;

    @ColumnInfo(name = "ans")
    private String ans;


    @ColumnInfo(name = "department")
    private String department;

    @ColumnInfo(name = "alertstatus")
    private Integer alertstatus;

    @ColumnInfo(name = "checklist_points")
    private String checklist_points;

    @ColumnInfo(name = "alrt_cmnt")
    private String alrt_cmnt;

    @ColumnInfo(name = "uid")
    private Integer uid;

    @ColumnInfo(name = "q")
    private String q;

    @ColumnInfo(name = "crnt_dt_tm")
    private String crnt_dt_tm;

    @ColumnInfo(name = "crnt_lctn")
    private String crnt_lctn;

    @ColumnInfo(name = "alert")
    private String alert;

    @ColumnInfo(name = "employee_id")
    private String employee_id;

    @ColumnInfo(name = "user_id")
    private String user_id;

    @ColumnInfo(name = "image_data")
    private String image_data;

    @ColumnInfo(name = "sbmt_dtals")
    private String sbmt_dtals;

    @ColumnInfo(name = "mngrcmnt")
    private String mngrcmnt;

    @ColumnInfo(name = "comment")
    private String comment;

    @ColumnInfo(name = "severity")
    private String severity;

    @ColumnInfo(name = "flag")
    private Integer flag;

    @ColumnInfo(name = "imageJson")
    private String imageJson;

    @ColumnInfo(name = "done_by")
    private String done_by;

    @ColumnInfo(name = "audio_data")
    private String audio_data;

    @ColumnInfo(name = "clm_value")
    private String clm_value;

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getSbmt_btn_image() {
        return sbmt_btn_image;
    }

    public void setSbmt_btn_image(String sbmt_btn_image) {
        this.sbmt_btn_image = sbmt_btn_image;
    }

    public Integer getRank_id() {
        return rank_id;
    }

    public void setRank_id(Integer rank_id) {
        this.rank_id = rank_id;
    }

    public Integer getWrk_id() {
        return wrk_id;
    }

    public void setWrk_id(Integer wrk_id) {
        this.wrk_id = wrk_id;
    }

    public String getGnrtn_date() {
        return gnrtn_date;
    }

    public void setGnrtn_date(String gnrtn_date) {
        this.gnrtn_date = gnrtn_date;
    }

    public String getTarget_date() {
        return target_date;
    }

    public void setTarget_date(String target_date) {
        this.target_date = target_date;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Integer getAlertstatus() {
        return alertstatus;
    }

    public void setAlertstatus(Integer alertstatus) {
        this.alertstatus = alertstatus;
    }

    public String getChecklist_points() {
        return checklist_points;
    }

    public void setChecklist_points(String checklist_points) {
        this.checklist_points = checklist_points;
    }

    public String getAlrt_cmnt() {
        return alrt_cmnt;
    }

    public void setAlrt_cmnt(String alrt_cmnt) {
        this.alrt_cmnt = alrt_cmnt;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String getCrnt_dt_tm() {
        return crnt_dt_tm;
    }

    public void setCrnt_dt_tm(String crnt_dt_tm) {
        this.crnt_dt_tm = crnt_dt_tm;
    }

    public String getCrnt_lctn() {
        return crnt_lctn;
    }

    public void setCrnt_lctn(String crnt_lctn) {
        this.crnt_lctn = crnt_lctn;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }


    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImage_data() {
        return image_data;
    }

    public void setImage_data(String image_data) {
        this.image_data = image_data;
    }

    public String getSbmt_dtals() {
        return sbmt_dtals;
    }

    public void setSbmt_dtals(String sbmt_dtals) {
        this.sbmt_dtals = sbmt_dtals;
    }

    public String getMngrcmnt() {
        return mngrcmnt;
    }

    public void setMngrcmnt(String mngrcmnt) {
        this.mngrcmnt = mngrcmnt;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getImageJson() {
        return imageJson;
    }

    public void setImageJson(String imageJson) {
        this.imageJson = imageJson;
    }

    public String getDone_by() {
        return done_by;
    }

    public void setDone_by(String done_by) {
        this.done_by = done_by;
    }

    public String getAudio_data() {
        return audio_data;
    }

    public void setAudio_data(String audio_data) {
        this.audio_data = audio_data;
    }

    public String getClm_value() {
        return clm_value;
    }

    public void setClm_value(String clm_value) {
        this.clm_value = clm_value;
    }
}