package com.nuevotechsolutions.ewarp.model.ManualsModel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "sub_list")
public class SubList implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    int id;
    @ColumnInfo(name = "main_list_key")
    Integer main_list_key;
    @ColumnInfo(name = "sub_list_name")
    String sub_list_name;
    @ColumnInfo(name = "url")
    String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getMain_list_key() {
        return main_list_key;
    }

    public void setMain_list_key(Integer main_list_key) {
        this.main_list_key = main_list_key;
    }

    public String getSub_list_name() {
        return sub_list_name;
    }

    public void setSub_list_name(String sub_list_name) {
        this.sub_list_name = sub_list_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
