package com.nuevotechsolutions.ewarp.controller;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageData   {

    String imageName;
    String path="/storage/emulated/0/Android/data/com.nuevotechsolutions.ewarp/files/Pictures/EWARP/";
    ImageView imageView, imageView1;

    public ImageData(ImageView imageView,String imageName, ImageView imageView1) {
       this.imageView=imageView;
       this.imageName=imageName;
        this.imageView1=imageView1;
    }

      public void saveFile ()
      {
          try {
              BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
              Bitmap bitmap = drawable.getBitmap();
              FileOutputStream fos = new FileOutputStream(path + imageName);
              bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
              fos.close();
              imageView1.setImageBitmap(bitmap);
          } catch (FileNotFoundException e) {
              e.printStackTrace();
          } catch (IOException e) {
              e.printStackTrace();
          }

      }

}
