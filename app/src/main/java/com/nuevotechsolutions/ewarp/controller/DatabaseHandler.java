package com.nuevotechsolutions.ewarp.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.nuevotechsolutions.ewarp.model.ChecklistModel.ChecklistRecord;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION=1;
    public static final String DATABASE_NAME="hotel_management_system";
    public static final String TABLE_NAME1="mstr_wrk_id";
    public static final String TABLE_NAME2="entrance_door";
    public static final String TABLE_NAME3="drapes";
    public static final String TABLE_NAME4="beds";
    public static final String TABLE_NAME5="furnitures";
    public static final String TABLE_NAME6="lighting";
    public static final String TABLE_NAME7="bathroom";
    public static final String TABLE_NAME8="aux_eng_of_isolation_for_offline";
    //**********************TABLE1*********************************************
    public static final String ID="id";
    public static final String UID="uid";
    public static final String CHECKLIST_NAME="checklist_name";
    public static final String POINTS="points";
    public static final String PHOTO="photo";
    public static final String ALERT="alert";
    public static final String TITLE_OF_SECTION="title_of_section";
    public static final String DESIGNATION="designation";
    public static final String QUES="rb_points_description";
    public static final String WRK_ID="wrk_id";
    public static final String GRTD_BY="grtd_by";
    public static final String CMP_NAME="cmp_name";
    public static final String CTGRY_NAME="ctgry_name";
    public static final String GRTN_FR_WRK="grtn_fr_wrk";
    public static final String GRTN_FR_WRK_SUB_CTG="grtn_fr_wrk_sub_ctg";
    public static final String WRK_ID_CRTN_DT="wrk_id_crtn_dt";
    public static final String MODIFIED_DT="modified_dt";
    public static final String WRK_STATUS="wrk_status";
    public static final String DEVICE_ID="device_id";
    public static final String COMMENT="comment";
    public static final String ROOM_NO="room_no";
    public static final String TYPE="type";
    public static final String USER_NAME="user_name";
    public static final String STATUS="status";

    public static final String RB="rb";
    public static final String CMNT="cmnt";
    public static final String PH="ph";
    public static final String ALRT_RB="alrt_rb";
    public static final String CRNT_DT_TM="crnt_dt_tm";
    public static final String CRNT_LCTN="crnt_lctn";
    public static final String SBMT_DTLS="sbmt_dtls";
    public static final String CORDINATES="cordinates";
    public static final String EMP_ID="emp_id";
    public static final String NM0="0";
    public static final String NM1="1";
    public static final String NM2="2";
    public static final String NM3="3";
    public static final String NM4="4";
    public static final String NM5="5";
    public static final String NM6="6";
    public static final String NM7="7";

    int wrk_id;

    public static final String COLUMN_STATUS = "1";

    //This Constructor are responsible to create database
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query1="CREATE TABLE " + TABLE_NAME1+ "(" + WRK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +GRTD_BY + " TEXT," +CMP_NAME + " TEXT," +CTGRY_NAME + " TEXT," +GRTN_FR_WRK + " TEXT," +GRTN_FR_WRK_SUB_CTG + " TEXT," +WRK_ID_CRTN_DT + " TEXT," +MODIFIED_DT + " TEXT," +WRK_STATUS + " TEXT," +DEVICE_ID + " TEXT," +COMMENT + " TEXT," +ROOM_NO + " TEXT," +TYPE + " TEXT," +USER_NAME + " TEXT," +STATUS + " TEXT" + ");" ;
        String query2="CREATE TABLE " + TABLE_NAME2+ "(" + WRK_ID + " INTEGER ," +GRTD_BY+ " TEXT," +RB +NM0+ " TEXT," +CMNT +NM0+ " TEXT," +PH +NM0+ " TEXT," +ALRT_RB +NM0+ " TEXT," +CRNT_DT_TM +NM0+ " TEXT," +CRNT_LCTN +NM0+ " TEXT," +SBMT_DTLS +NM0+ " TEXT," +CORDINATES +NM0+ " TEXT," +EMP_ID +NM0+ " TEXT," +RB +NM1+ " TEXT," +CMNT +NM1+ " TEXT," +PH +NM1+ " TEXT," +ALRT_RB +NM1+ " TEXT," +CRNT_DT_TM +NM1+ " TEXT," +CRNT_LCTN +NM1+ " TEXT," +SBMT_DTLS +NM1+ " TEXT," +CORDINATES +NM1+ " TEXT," +EMP_ID +NM1+ " TEXT," +RB +NM2+ " TEXT," +CMNT +NM2+ " TEXT," +PH +NM2+ " TEXT," +ALRT_RB +NM2+ " TEXT," +CRNT_DT_TM +NM2+ " TEXT," +CRNT_LCTN +NM2+ " TEXT," +SBMT_DTLS +NM2+ " TEXT," +CORDINATES +NM2+ " TEXT," +EMP_ID +NM2+ " TEXT," +RB +NM3+ " TEXT," +CMNT +NM3+ " TEXT," +PH +NM3+ " TEXT," +ALRT_RB +NM3+ " TEXT," +CRNT_DT_TM +NM3+ " TEXT," +CRNT_LCTN +NM3+ " TEXT," +SBMT_DTLS +NM3+ " TEXT," +CORDINATES +NM3+ " TEXT," +EMP_ID +NM3+ " TEXT," +RB +NM4+ " TEXT," +CMNT +NM4+ " TEXT," +PH +NM4+ " TEXT," +ALRT_RB +NM4+ " TEXT," +CRNT_DT_TM +NM4+ " TEXT," +CRNT_LCTN +NM4+ " TEXT," +SBMT_DTLS +NM4+ " TEXT," +CORDINATES +NM4+ " TEXT," +EMP_ID +NM4+ " TEXT," +STATUS+ " TEXT" + ");" ;
        String query3="CREATE TABLE " + TABLE_NAME3+ "(" + WRK_ID + " INTEGER ," +GRTD_BY+ " TEXT," +RB +NM0+ " TEXT," +CMNT +NM0+ " TEXT," +PH +NM0+ " TEXT," +ALRT_RB +NM0+ " TEXT," +CRNT_DT_TM +NM0+ " TEXT," +CRNT_LCTN +NM0+ " TEXT," +SBMT_DTLS +NM0+ " TEXT," +CORDINATES +NM0+ " TEXT," +EMP_ID +NM0+ " TEXT," +RB +NM1+ " TEXT," +CMNT +NM1+ " TEXT," +PH +NM1+ " TEXT," +ALRT_RB +NM1+ " TEXT," +CRNT_DT_TM +NM1+ " TEXT," +CRNT_LCTN +NM1+ " TEXT," +SBMT_DTLS +NM1+ " TEXT," +CORDINATES +NM1+ " TEXT," +EMP_ID +NM1+ " TEXT," +RB +NM2+ " TEXT," +CMNT +NM2+ " TEXT," +PH +NM2+ " TEXT," +ALRT_RB +NM2+ " TEXT," +CRNT_DT_TM +NM2+ " TEXT," +CRNT_LCTN +NM2+ " TEXT," +SBMT_DTLS +NM2+ " TEXT," +CORDINATES +NM2+ " TEXT," +EMP_ID +NM2+ " TEXT," +STATUS+ " TEXT" + ");" ;
        String query4="CREATE TABLE " + TABLE_NAME4+ "(" + WRK_ID + " INTEGER ," +GRTD_BY+ " TEXT," +RB +NM0+ " TEXT," +CMNT +NM0+ " TEXT," +PH +NM0+ " TEXT," +ALRT_RB +NM0+ " TEXT," +CRNT_DT_TM +NM0+ " TEXT," +CRNT_LCTN +NM0+ " TEXT," +SBMT_DTLS +NM0+ " TEXT," +CORDINATES +NM0+ " TEXT," +EMP_ID +NM0+ " TEXT," +RB +NM1+ " TEXT," +CMNT +NM1+ " TEXT," +PH +NM1+ " TEXT," +ALRT_RB +NM1+ " TEXT," +CRNT_DT_TM +NM1+ " TEXT," +CRNT_LCTN +NM1+ " TEXT," +SBMT_DTLS +NM1+ " TEXT," +CORDINATES +NM1+ " TEXT," +EMP_ID +NM1+ " TEXT," +RB +NM2+ " TEXT," +CMNT +NM2+ " TEXT," +PH +NM2+ " TEXT," +ALRT_RB +NM2+ " TEXT," +CRNT_DT_TM +NM2+ " TEXT," +CRNT_LCTN +NM2+ " TEXT," +SBMT_DTLS +NM2+ " TEXT," +CORDINATES +NM2+ " TEXT," +EMP_ID +NM2+ " TEXT," +RB +NM3+ " TEXT," +CMNT +NM3+ " TEXT," +PH +NM3+ " TEXT," +ALRT_RB +NM3+ " TEXT," +CRNT_DT_TM +NM3+ " TEXT," +CRNT_LCTN +NM3+ " TEXT," +SBMT_DTLS +NM3+ " TEXT," +CORDINATES +NM3+ " TEXT," +EMP_ID +NM3+ " TEXT," +STATUS+ " TEXT" + ");" ;
        String query5="CREATE TABLE " + TABLE_NAME5+ "(" + WRK_ID + " INTEGER ," +GRTD_BY+ " TEXT," +RB +NM0+ " TEXT," +CMNT +NM0+ " TEXT," +PH +NM0+ " TEXT," +ALRT_RB +NM0+ " TEXT," +CRNT_DT_TM +NM0+ " TEXT," +CRNT_LCTN +NM0+ " TEXT," +SBMT_DTLS +NM0+ " TEXT," +CORDINATES +NM0+ " TEXT," +EMP_ID +NM0+ " TEXT," +RB +NM1+ " TEXT," +CMNT +NM1+ " TEXT," +PH +NM1+ " TEXT," +ALRT_RB +NM1+ " TEXT," +CRNT_DT_TM +NM1+ " TEXT," +CRNT_LCTN +NM1+ " TEXT," +SBMT_DTLS +NM1+ " TEXT," +CORDINATES +NM1+ " TEXT," +EMP_ID +NM1+ " TEXT," +RB +NM2+ " TEXT," +CMNT +NM2+ " TEXT," +PH +NM2+ " TEXT," +ALRT_RB +NM2+ " TEXT," +CRNT_DT_TM +NM2+ " TEXT," +CRNT_LCTN +NM2+ " TEXT," +SBMT_DTLS +NM2+ " TEXT," +CORDINATES +NM2+ " TEXT," +EMP_ID +NM2+ " TEXT," +RB +NM3+ " TEXT," +CMNT +NM3+ " TEXT," +PH +NM3+ " TEXT," +ALRT_RB +NM3+ " TEXT," +CRNT_DT_TM +NM3+ " TEXT," +CRNT_LCTN +NM3+ " TEXT," +SBMT_DTLS +NM3+ " TEXT," +CORDINATES +NM3+ " TEXT," +EMP_ID +NM3+ " TEXT," +RB +NM4+ " TEXT," +CMNT +NM4+ " TEXT," +PH +NM4+ " TEXT," +ALRT_RB +NM4+ " TEXT," +CRNT_DT_TM +NM4+ " TEXT," +CRNT_LCTN +NM4+ " TEXT," +SBMT_DTLS +NM4+ " TEXT," +CORDINATES +NM4+ " TEXT," +EMP_ID +NM4+ " TEXT," +STATUS+ " TEXT" + ");" ;
        String query6="CREATE TABLE " + TABLE_NAME6+ "(" + WRK_ID + " INTEGER ," +GRTD_BY+ " TEXT," +RB +NM0+ " TEXT," +CMNT +NM0+ " TEXT," +PH +NM0+ " TEXT," +ALRT_RB +NM0+ " TEXT," +CRNT_DT_TM +NM0+ " TEXT," +CRNT_LCTN +NM0+ " TEXT," +SBMT_DTLS +NM0+ " TEXT," +CORDINATES +NM0+ " TEXT," +EMP_ID +NM0+ " TEXT," +RB +NM1+ " TEXT," +CMNT +NM1+ " TEXT," +PH +NM1+ " TEXT," +ALRT_RB +NM1+ " TEXT," +CRNT_DT_TM +NM1+ " TEXT," +CRNT_LCTN +NM1+ " TEXT," +SBMT_DTLS +NM1+ " TEXT," +CORDINATES +NM1+ " TEXT," +EMP_ID +NM1+ " TEXT," +RB +NM2+ " TEXT," +CMNT +NM2+ " TEXT," +PH +NM2+ " TEXT," +ALRT_RB +NM2+ " TEXT," +CRNT_DT_TM +NM2+ " TEXT," +CRNT_LCTN +NM2+ " TEXT," +SBMT_DTLS +NM2+ " TEXT," +CORDINATES +NM2+ " TEXT," +EMP_ID +NM2+ " TEXT," +STATUS+ " TEXT" + ");" ;
        String query7="CREATE TABLE " + TABLE_NAME7+ "(" + WRK_ID + " INTEGER ," +GRTD_BY+ " TEXT," +RB +NM0+ " TEXT," +CMNT +NM0+ " TEXT," +PH +NM0+ " TEXT," +ALRT_RB +NM0+ " TEXT," +CRNT_DT_TM +NM0+ " TEXT," +CRNT_LCTN +NM0+ " TEXT," +SBMT_DTLS +NM0+ " TEXT," +CORDINATES +NM0+ " TEXT," +EMP_ID +NM0+ " TEXT," +RB +NM1+ " TEXT," +CMNT +NM1+ " TEXT," +PH +NM1+ " TEXT," +ALRT_RB +NM1+ " TEXT," +CRNT_DT_TM +NM1+ " TEXT," +CRNT_LCTN +NM1+ " TEXT," +SBMT_DTLS +NM1+ " TEXT," +CORDINATES +NM1+ " TEXT," +EMP_ID +NM1+ " TEXT," +RB +NM2+ " TEXT," +CMNT +NM2+ " TEXT," +PH +NM2+ " TEXT," +ALRT_RB +NM2+ " TEXT," +CRNT_DT_TM +NM2+ " TEXT," +CRNT_LCTN +NM2+ " TEXT," +SBMT_DTLS +NM2+ " TEXT," +CORDINATES +NM2+ " TEXT," +EMP_ID +NM2+ " TEXT," +RB +NM3+ " TEXT," +CMNT +NM3+ " TEXT," +PH +NM3+ " TEXT," +ALRT_RB +NM3+ " TEXT," +CRNT_DT_TM +NM3+ " TEXT," +CRNT_LCTN +NM3+ " TEXT," +SBMT_DTLS +NM3+ " TEXT," +CORDINATES +NM3+ " TEXT," +EMP_ID +NM3+ " TEXT," +RB +NM4+ " TEXT," +CMNT +NM4+ " TEXT," +PH +NM4+ " TEXT," +ALRT_RB +NM4+ " TEXT," +CRNT_DT_TM +NM4+ " TEXT," +CRNT_LCTN +NM4+ " TEXT," +SBMT_DTLS +NM4+ " TEXT," +CORDINATES +NM4+ " TEXT," +EMP_ID +NM4+ " TEXT," +RB +NM5+ " TEXT," +CMNT +NM5+ " TEXT," +PH +NM5+ " TEXT," +ALRT_RB +NM5+ " TEXT," +CRNT_DT_TM +NM5+ " TEXT," +CRNT_LCTN +NM5+ " TEXT," +SBMT_DTLS +NM5+ " TEXT," +CORDINATES +NM5+ " TEXT," +EMP_ID +NM5+ " TEXT," +RB +NM6+ " TEXT," +CMNT +NM6+ " TEXT," +PH +NM6+ " TEXT," +ALRT_RB +NM6+ " TEXT," +CRNT_DT_TM +NM6+ " TEXT," +CRNT_LCTN +NM6+ " TEXT," +SBMT_DTLS +NM6+ " TEXT," +CORDINATES +NM6+ " TEXT," +EMP_ID +NM6+ " TEXT," +RB +NM7+ " TEXT," +CMNT +NM7+ " TEXT," +PH +NM7+ " TEXT," +ALRT_RB +NM7+ " TEXT," +CRNT_DT_TM +NM7+ " TEXT," +CRNT_LCTN +NM7+ " TEXT," +SBMT_DTLS +NM7+ " TEXT," +CORDINATES +NM7+ " TEXT," +EMP_ID +NM7+ " TEXT," +STATUS+ " TEXT" + ");" ;
        String query8="CREATE TABLE " + TABLE_NAME8+ "(" + ID + " INTEGER ," +UID+ " TEXT," +CHECKLIST_NAME+ " TEXT," +POINTS+ " TEXT," +PHOTO+ " TEXT," +COMMENT+ " TEXT," +ALERT+ " TEXT," +TITLE_OF_SECTION+ " TEXT," +DESIGNATION+ " TEXT," +QUES+ " TEXT" + ");" ;


        db.execSQL(query1);
        db.execSQL(query2);
        db.execSQL(query3);
        db.execSQL(query4);
        db.execSQL(query5);
        db.execSQL(query6);
        db.execSQL(query7);
        db.execSQL(query8);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME1);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME2);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME3);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME4);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME5);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME6);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME7);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME8);
        onCreate(db);

    }

    public void auxillaryEngIsolation(ChecklistRecord data)
    {
        SQLiteDatabase database=getWritableDatabase();
        ContentValues values8=new ContentValues();

        values8.put(ID,data.getId());
        values8.put(UID,data.getUid());
        values8.put(CHECKLIST_NAME,data.getChecklist_name());
        values8.put(POINTS,data.getPoints());
        values8.put(PHOTO,data.getPhoto());
        values8.put(COMMENT,data.getComment());
        values8.put(ALERT,data.getAlert());
        values8.put(TITLE_OF_SECTION,data.getTitle_of_section());
        values8.put(DESIGNATION,data.getDesignation());
        values8.put(QUES,data.getRb_points_description());

        database.insert(TABLE_NAME8,null,values8);
        database.close();
    }

    public boolean addChecklistRecord(ChecklistRecord refrence)
    {

        SQLiteDatabase database=getWritableDatabase();
        ContentValues values=new ContentValues();
        ContentValues values2=new ContentValues();
        ContentValues values3=new ContentValues();
        ContentValues values4=new ContentValues();
        ContentValues values5=new ContentValues();
        ContentValues values6=new ContentValues();
        ContentValues values7=new ContentValues();

        //values.put(WRK_ID,refrence.getWrk_id());
        values.put(GRTD_BY,refrence.getGrtd_by());
        values.put(CMP_NAME,refrence.getCmp_name());
        values.put(CTGRY_NAME,refrence.getCtgry_name());
        values.put(GRTN_FR_WRK,refrence.getGrtn_fr_wrk());
        values.put(GRTN_FR_WRK_SUB_CTG,refrence.getGrtn_fr_wrk_sub_ctg());
        values.put(WRK_ID_CRTN_DT,refrence.getWrk_id_crtn_dt());
        values.put(MODIFIED_DT,refrence.getModified_dt());
        values.put(WRK_STATUS,refrence.getWrk_status());
        values.put(DEVICE_ID,refrence.getDevice_id());
        values.put(COMMENT,refrence.getComment());
        values.put(ROOM_NO,refrence.getRoom_no());
        values.put(TYPE,refrence.getType());
        values.put(USER_NAME,refrence.getUser_name());

        database.insert(TABLE_NAME1,null,values);
        Cursor cursor = database.rawQuery("SELECT  * FROM " + TABLE_NAME1, null);
        if (cursor.moveToLast()) {
            refrence.setWrk_id(cursor.getInt(cursor.getColumnIndex("wrk_id")));
        }
        cursor.close();
        values2.put(WRK_ID,refrence.getWrk_id());
        values2.put(GRTD_BY,refrence.getEmp_id());
        values3.put(WRK_ID,refrence.getWrk_id());
        values3.put(GRTD_BY,refrence.getEmp_id());
        values4.put(WRK_ID,refrence.getWrk_id());
        values4.put(GRTD_BY,refrence.getEmp_id());
        values5.put(WRK_ID,refrence.getWrk_id());
        values5.put(GRTD_BY,refrence.getEmp_id());
        values6.put(WRK_ID,refrence.getWrk_id());
        values6.put(GRTD_BY,refrence.getEmp_id());
        values7.put(WRK_ID,refrence.getWrk_id());
        values7.put(GRTD_BY,refrence.getEmp_id());

        database.insert(TABLE_NAME2,null,values2);
        database.insert(TABLE_NAME3,null,values3);
        database.insert(TABLE_NAME4,null,values4);
        database.insert(TABLE_NAME5,null,values5);
        database.insert(TABLE_NAME6,null,values6);
        database.insert(TABLE_NAME7,null,values7);
        database.close();

        return true;
    }

    public boolean updateChecklistRecord(ChecklistRecord record)
    {
        SQLiteDatabase database=getWritableDatabase();
        ContentValues values=new ContentValues();

        int BTN_NM=record.getBtn_nm();

        if (BTN_NM>=1 && BTN_NM<=5)
        {
            if (BTN_NM==1)
            {

                values.put(RB+NM0,record.getRb());
                values.put(CMNT+NM0,record.getCmnt());
                values.put(PH+NM0,record.getPh());
                values.put(ALRT_RB+NM0,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM0,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM0,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM0,record.getSbmt_dtls());
                values.put(CORDINATES+NM0,record.getCordinates());
                values.put(EMP_ID+NM0,record.getEmp_id());
                database.update(TABLE_NAME2,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }else if (BTN_NM==2)
            {

                values.put(RB+NM1,record.getRb());
                values.put(CMNT+NM1,record.getCmnt());
                values.put(PH+NM1,record.getPh());
                values.put(ALRT_RB+NM1,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM1,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM1,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM1,record.getSbmt_dtls());
                values.put(CORDINATES+NM1,record.getCordinates());
                values.put(EMP_ID+NM1,record.getEmp_id());
                database.update(TABLE_NAME2,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==3)
            {

                values.put(RB+NM2,record.getRb());
                values.put(CMNT+NM2,record.getCmnt());
                values.put(PH+NM2,record.getPh());
                values.put(ALRT_RB+NM2,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM2,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM2,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM2,record.getSbmt_dtls());
                values.put(CORDINATES+NM2,record.getCordinates());
                values.put(EMP_ID+NM2,record.getEmp_id());Log.e("dataHandler","right3");
                database.update(TABLE_NAME2,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==4)
            {

                values.put(RB+NM3,record.getRb());
                values.put(CMNT+NM3,record.getCmnt());
                values.put(PH+NM3,record.getPh());
                values.put(ALRT_RB+NM3,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM3,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM3,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM3,record.getSbmt_dtls());
                values.put(CORDINATES+NM3,record.getCordinates());
                values.put(EMP_ID+NM3,record.getEmp_id());
                database.update(TABLE_NAME2,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==5)
            {

                values.put(RB+NM4,record.getRb());
                values.put(CMNT+NM4,record.getCmnt());
                values.put(PH+NM4,record.getPh());
                values.put(ALRT_RB+NM4,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM4,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM4,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM4,record.getSbmt_dtls());
                values.put(CORDINATES+NM4,record.getCordinates());
                values.put(EMP_ID+NM4,record.getEmp_id());
                database.update(TABLE_NAME2,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }

        }else if (BTN_NM>5 && BTN_NM<=8)
        {

            if (BTN_NM==6)
            {

                values.put(RB+NM0,record.getRb());
                values.put(CMNT+NM0,record.getCmnt());
                values.put(PH+NM0,record.getPh());
                values.put(ALRT_RB+NM0,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM0,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM0,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM0,record.getSbmt_dtls());
                values.put(CORDINATES+NM0,record.getCordinates());
                values.put(EMP_ID+NM0,record.getEmp_id());
                database.update(TABLE_NAME3,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }else if (BTN_NM==7)
            {

                values.put(RB+NM1,record.getRb());
                values.put(CMNT+NM1,record.getCmnt());
                values.put(PH+NM1,record.getPh());
                values.put(ALRT_RB+NM1,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM1,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM1,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM1,record.getSbmt_dtls());
                values.put(CORDINATES+NM1,record.getCordinates());
                values.put(EMP_ID+NM1,record.getEmp_id());
                database.update(TABLE_NAME3,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==8)
            {

                values.put(RB+NM2,record.getRb());
                values.put(CMNT+NM2,record.getCmnt());
                values.put(PH+NM2,record.getPh());
                values.put(ALRT_RB+NM2,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM2,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM2,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM2,record.getSbmt_dtls());
                values.put(CORDINATES+NM2,record.getCordinates());
                values.put(EMP_ID+NM2,record.getEmp_id());
                database.update(TABLE_NAME3,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }

        }else if (BTN_NM>8 && BTN_NM<=12)
        {

            if (BTN_NM==9)
            {

                values.put(RB+NM0,record.getRb());
                values.put(CMNT+NM0,record.getCmnt());
                values.put(PH+NM0,record.getPh());
                values.put(ALRT_RB+NM0,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM0,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM0,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM0,record.getSbmt_dtls());
                values.put(CORDINATES+NM0,record.getCordinates());
                values.put(EMP_ID+NM0,record.getEmp_id());
                database.update(TABLE_NAME4,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }else if (BTN_NM==10)
            {

                values.put(RB+NM1,record.getRb());
                values.put(CMNT+NM1,record.getCmnt());
                values.put(PH+NM1,record.getPh());
                values.put(ALRT_RB+NM1,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM1,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM1,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM1,record.getSbmt_dtls());
                values.put(CORDINATES+NM1,record.getCordinates());
                values.put(EMP_ID+NM1,record.getEmp_id());
                database.update(TABLE_NAME4,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==11)
            {

                values.put(RB+NM2,record.getRb());
                values.put(CMNT+NM2,record.getCmnt());
                values.put(PH+NM2,record.getPh());
                values.put(ALRT_RB+NM2,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM2,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM2,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM2,record.getSbmt_dtls());
                values.put(CORDINATES+NM2,record.getCordinates());
                values.put(EMP_ID+NM2,record.getEmp_id());
                database.update(TABLE_NAME4,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==12)
            {

                values.put(RB+NM3,record.getRb());
                values.put(CMNT+NM3,record.getCmnt());
                values.put(PH+NM3,record.getPh());
                values.put(ALRT_RB+NM3,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM3,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM3,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM3,record.getSbmt_dtls());
                values.put(CORDINATES+NM3,record.getCordinates());
                values.put(EMP_ID+NM3,record.getEmp_id());
                database.update(TABLE_NAME4,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }

        }else if (BTN_NM>12 && BTN_NM<=17)
        {

            if (BTN_NM==13)
            {

                values.put(RB+NM0,record.getRb());
                values.put(CMNT+NM0,record.getCmnt());
                values.put(PH+NM0,record.getPh());
                values.put(ALRT_RB+NM0,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM0,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM0,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM0,record.getSbmt_dtls());
                values.put(CORDINATES+NM0,record.getCordinates());
                values.put(EMP_ID+NM0,record.getEmp_id());
                database.update(TABLE_NAME5,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }else if (BTN_NM==14)
            {

                values.put(RB+NM1,record.getRb());
                values.put(CMNT+NM1,record.getCmnt());
                values.put(PH+NM1,record.getPh());
                values.put(ALRT_RB+NM1,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM1,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM1,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM1,record.getSbmt_dtls());
                values.put(CORDINATES+NM1,record.getCordinates());
                values.put(EMP_ID+NM1,record.getEmp_id());
                database.update(TABLE_NAME5,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==15)
            {

                values.put(RB+NM2,record.getRb());
                values.put(CMNT+NM2,record.getCmnt());
                values.put(PH+NM2,record.getPh());
                values.put(ALRT_RB+NM2,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM2,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM2,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM2,record.getSbmt_dtls());
                values.put(CORDINATES+NM2,record.getCordinates());
                values.put(EMP_ID+NM2,record.getEmp_id());
                database.update(TABLE_NAME5,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==16)
            {

                values.put(RB+NM3,record.getRb());
                values.put(CMNT+NM3,record.getCmnt());
                values.put(PH+NM3,record.getPh());
                values.put(ALRT_RB+NM3,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM3,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM3,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM3,record.getSbmt_dtls());
                values.put(CORDINATES+NM3,record.getCordinates());
                values.put(EMP_ID+NM3,record.getEmp_id());
                database.update(TABLE_NAME5,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==17)
            {

                values.put(RB+NM4,record.getRb());
                values.put(CMNT+NM4,record.getCmnt());
                values.put(PH+NM4,record.getPh());
                values.put(ALRT_RB+NM4,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM4,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM4,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM4,record.getSbmt_dtls());
                values.put(CORDINATES+NM4,record.getCordinates());
                values.put(EMP_ID+NM4,record.getEmp_id());
                database.update(TABLE_NAME5,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }

        }else if (BTN_NM>17 && BTN_NM<=20)
        {

            if (BTN_NM==18)
            {

                values.put(RB+NM0,record.getRb());
                values.put(CMNT+NM0,record.getCmnt());
                values.put(PH+NM0,record.getPh());
                values.put(ALRT_RB+NM0,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM0,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM0,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM0,record.getSbmt_dtls());
                values.put(CORDINATES+NM0,record.getCordinates());
                values.put(EMP_ID+NM0,record.getEmp_id());
                database.update(TABLE_NAME6,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }else if (BTN_NM==19)
            {

                values.put(RB+NM1,record.getRb());
                values.put(CMNT+NM1,record.getCmnt());
                values.put(PH+NM1,record.getPh());
                values.put(ALRT_RB+NM1,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM1,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM1,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM1,record.getSbmt_dtls());
                values.put(CORDINATES+NM1,record.getCordinates());
                values.put(EMP_ID+NM1,record.getEmp_id());
                database.update(TABLE_NAME6,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==20)
            {

                values.put(RB+NM2,record.getRb());
                values.put(CMNT+NM2,record.getCmnt());
                values.put(PH+NM2,record.getPh());
                values.put(ALRT_RB+NM2,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM2,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM2,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM2,record.getSbmt_dtls());
                values.put(CORDINATES+NM2,record.getCordinates());
                values.put(EMP_ID+NM2,record.getEmp_id());
                database.update(TABLE_NAME6,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }

        }else if (BTN_NM>20 && BTN_NM<=30)
        {

            if (BTN_NM==21)
            {

                values.put(RB+NM0,record.getRb());
                values.put(CMNT+NM0,record.getCmnt());
                values.put(PH+NM0,record.getPh());
                values.put(ALRT_RB+NM0,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM0,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM0,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM0,record.getSbmt_dtls());
                values.put(CORDINATES+NM0,record.getCordinates());
                values.put(EMP_ID+NM0,record.getEmp_id());
                database.update(TABLE_NAME7,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }else if (BTN_NM==22)
            {

                values.put(RB+NM1,record.getRb());
                values.put(CMNT+NM1,record.getCmnt());
                values.put(PH+NM1,record.getPh());
                values.put(ALRT_RB+NM1,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM1,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM1,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM1,record.getSbmt_dtls());
                values.put(CORDINATES+NM1,record.getCordinates());
                values.put(EMP_ID+NM1,record.getEmp_id());
                database.update(TABLE_NAME7,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==23)
            {

                values.put(RB+NM2,record.getRb());
                values.put(CMNT+NM2,record.getCmnt());
                values.put(PH+NM2,record.getPh());
                values.put(ALRT_RB+NM2,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM2,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM2,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM2,record.getSbmt_dtls());
                values.put(CORDINATES+NM2,record.getCordinates());
                values.put(EMP_ID+NM2,record.getEmp_id());
                database.update(TABLE_NAME7,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            if (BTN_NM==24)
            {

                values.put(RB+NM3,record.getRb());
                values.put(CMNT+NM3,record.getCmnt());
                values.put(PH+NM3,record.getPh());
                values.put(ALRT_RB+NM3,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM3,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM3,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM3,record.getSbmt_dtls());
                values.put(CORDINATES+NM3,record.getCordinates());
                values.put(EMP_ID+NM3,record.getEmp_id());
                database.update(TABLE_NAME7,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }else if (BTN_NM==25)
            {

                values.put(RB+NM4,record.getRb());
                values.put(CMNT+NM4,record.getCmnt());
                values.put(PH+NM4,record.getPh());
                values.put(ALRT_RB+NM4,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM4,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM4,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM4,record.getSbmt_dtls());
                values.put(CORDINATES+NM4,record.getCordinates());
                values.put(EMP_ID+NM4,record.getEmp_id());
                database.update(TABLE_NAME7,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            else if (BTN_NM==26)
            {

                values.put(RB+NM5,record.getRb());
                values.put(CMNT+NM5,record.getCmnt());
                values.put(PH+NM5,record.getPh());
                values.put(ALRT_RB+NM5,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM5,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM5,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM5,record.getSbmt_dtls());
                values.put(CORDINATES+NM5,record.getCordinates());
                values.put(EMP_ID+NM5,record.getEmp_id());
                database.update(TABLE_NAME7,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            if (BTN_NM==27)
            {

                values.put(RB+NM6,record.getRb());
                values.put(CMNT+NM6,record.getCmnt());
                values.put(PH+NM6,record.getPh());
                values.put(ALRT_RB+NM6,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM6,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM6,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM6,record.getSbmt_dtls());
                values.put(CORDINATES+NM6,record.getCordinates());
                values.put(EMP_ID+NM6,record.getEmp_id());
                database.update(TABLE_NAME7,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            if (BTN_NM==28)
            {

                values.put(RB+NM7,record.getRb());
                values.put(CMNT+NM7,record.getCmnt());
                values.put(PH+NM7,record.getPh());
                values.put(ALRT_RB+NM7,record.getAlrt_rb());
                values.put(CRNT_DT_TM+NM7,record.getCrnt_dt_tm());
                values.put(CRNT_LCTN+NM7,record.getCrnt_lctn());
                values.put(SBMT_DTLS+NM7,record.getSbmt_dtls());
                values.put(CORDINATES+NM7,record.getCordinates());
                values.put(EMP_ID+NM7,record.getEmp_id());
                database.update(TABLE_NAME7,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            if (BTN_NM==29)
            {
                values.put(WRK_STATUS,record.getWrk_status());
                values.put(COMMENT,record.getComment());
                database.update(TABLE_NAME1,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }
            if (BTN_NM==30)
            {
                values.put(WRK_STATUS,record.getWrk_status());
                values.put(COMMENT,record.getComment());
                database.update(TABLE_NAME1,values,WRK_ID+"=?" ,new String[]{String.valueOf(record.getWrk_id())});
                database.close();
            }

        }

        return  true;
    }

    /*
     * this method is for getting all the unsynced name
     * so that we can sync it with database
     * */
    public Cursor getUnsyncedChecklistRecord() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_NAME2 + " WHERE " + COLUMN_STATUS + " = 0;";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    public List<ChecklistRecord> getAllRecord()
    {
        ArrayList<ChecklistRecord> records=new ArrayList<>();
        SQLiteDatabase database=getReadableDatabase();

        String query="SELECT * FROM " + TABLE_NAME2;

        Cursor cursor=database.rawQuery(query,null);

        if (cursor.moveToFirst())
        {
            do{
                ChecklistRecord ref=new ChecklistRecord();
                ref.setRb(cursor.getString(0));
                ref.setCmnt(cursor.getString(1));
                ref.setPh(cursor.getString(2));
                ref.setAlrt_rb(cursor.getString(3));

                // add all record to arraylist

                records.add(ref);
            }
            while(cursor.moveToNext());
        }
        //Return all records...
        return records;
    }

    public void deleteAllRecordAuxillaryEng()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME8,null,null);
    }

    public ArrayList<ChecklistRecord> getAllRecordAuxillaryEng() {

        ArrayList<ChecklistRecord> records=new ArrayList<>();
        SQLiteDatabase database=getReadableDatabase();

        String query="SELECT * FROM " + TABLE_NAME8;

        Cursor cursor=database.rawQuery(query,null);

        if (cursor.moveToFirst())
        {
            do{
                ChecklistRecord ref=new ChecklistRecord();
                ref.setChecklist_name(cursor.getString(2));
                ref.setPoints(cursor.getString(3));
                ref.setPhoto(cursor.getString(4));
                ref.setComment(cursor.getString(5));
                ref.setAlert(cursor.getString(6));
                ref.setTitle_of_section(cursor.getString(7));
                ref.setRb_points_description(cursor.getString(9));

                // add all record to arraylist

                records.add(ref);
            }
            while(cursor.moveToNext());
        }
        return records;
    }

}