package com.nuevotechsolutions.ewarp.controller;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class ImageEventHandler implements View.OnTouchListener {

    Canvas canvas;
    Paint paint;
    Matrix matrix;
    float downx = 0;
    float downy = 0;
    float upx = 0;
    float upy = 0;
    ImageView imageView57;
    ImageView imageView1;

    public ImageEventHandler(ImageView imageView57, ImageView imageView1) {
        this.imageView57 = imageView57;
        this.imageView1 = imageView1;
        BitmapDrawable drawable = (BitmapDrawable) imageView57.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        Bitmap alt = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        canvas = new Canvas(alt);
        paint = new Paint();
        paint.setColor(Color.YELLOW);
        paint.setStrokeWidth(15);
        matrix = new Matrix();
        canvas.drawBitmap(bitmap, matrix, paint);
        imageView57.setImageBitmap(alt);
        // imageView1.setImageBitmap(alt);

    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                downx = event.getX();
                downy = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                upx = event.getX();
                upy = event.getY();
                canvas.drawLine(downx, downy, upx, upy, paint);
                imageView57.invalidate();
                downx = upx;
                downy = upy;
                break;
            case MotionEvent.ACTION_UP:
                upx = event.getX();
                upy = event.getY();
                canvas.drawLine(downx, downy, upx, upy, paint);
                imageView57.invalidate();
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
            default:
                break;
        }
        return true;
    }
}
