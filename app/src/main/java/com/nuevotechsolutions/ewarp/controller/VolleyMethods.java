package com.nuevotechsolutions.ewarp.controller;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nuevotechsolutions.ewarp.interfaces.GetJsonObjectRequestCallback;
import com.nuevotechsolutions.ewarp.interfaces.PostJsonObjectRequestCallback;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyMethods {


    public static void makePostJsonObjectRequest(Map<String,String> params, Context context,String api, PostJsonObjectRequestCallback postJsonObjectRequestCallback){

        RequestQueue requestQueue=Volley.newRequestQueue(context);

        JSONObject jsonObject=new JSONObject(params);
        Log.e("obj", String.valueOf(jsonObject));

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, api,
                jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (response!=null){

                   postJsonObjectRequestCallback.onSuccessResponse(response);
                  /*  ObjectMapper objectMapper=new ObjectMapper();
                    try {
                        objectMapper.readValue(response.toString(), TypeFactory.defaultInstance().constructCollectionType(List.class, ChecklistRecord.class));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                postJsonObjectRequestCallback.onVolleyError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> header=new HashMap<>();
                header.put("Content-Type", "application/json");
                return header;
            }

           /* @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parameters=new HashMap<String,String>();
                parameters.put("username","jeet");
                parameters.put("password","123");
                return parameters;
            }*/

              @Override
            public String getBodyContentType() {
                return "application/json";
            }


        };
       int socketTimeOut = 20000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeOut,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
         jsonObjectRequest.setRetryPolicy(policy);
        requestQueue.add(jsonObjectRequest);
    }


    public static void makeGetJsonObjectRequest(Map<String,String> params, Context context,String api, GetJsonObjectRequestCallback getJsonObjectRequestCallback){

        RequestQueue requestQueue=Volley.newRequestQueue(context);

        JSONObject jsonObject=new JSONObject(params);
        Log.e("obj", String.valueOf(jsonObject));

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, api,
                jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (response!=null){
                    getJsonObjectRequestCallback.onSuccessResponse(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getJsonObjectRequestCallback.onVolleyError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> header=new HashMap<>();
                header.put("Content-Type", "application/json");
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }


        };
        int socketTimeOut = 20000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeOut,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        requestQueue.add(jsonObjectRequest);
    }




}
