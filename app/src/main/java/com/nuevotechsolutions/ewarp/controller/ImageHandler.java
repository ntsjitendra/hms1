package com.nuevotechsolutions.ewarp.controller;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

public class ImageHandler {

    public Bitmap imgView(ImageView imageView, Bitmap bitmap) {
        if (null == imageView.getDrawable()) {
            return null;
        } else {
//            BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
//            bitmap = drawable.getBitmap();
            bitmap= ((BitmapDrawable)imageView.getDrawable().getCurrent()).getBitmap();

        }
        return bitmap;
    }
}
